VAP : Versatile Aggregate Profiler
==================================

This readme is relative to the vap_interface module.

The official VAP website is **[here](http://lab-jacques.recherche.usherbrooke.ca/vap/)**

Copyright (C) 2013,2014 Christian Poitras, Pierre-Étienne Jacques  
Université de Sherbrooke. All rights reserved.

Download
---------
The vap_interface binaries can be downloaded [here](http://bitbucket.org/labjacquespe/vap_interface/downloads).

* [Windows](https://bitbucket.org/labjacquespe/vap_interface/downloads/vap-2.0.0-alpha1.exe)

* [macOS](https://bitbucket.org/labjacquespe/vap_interface/downloads/vap-2.0.0-alpha1.dmg)

* [Linux package for Ubuntu and other Debian distributions](https://bitbucket.org/labjacquespe/vap_interface/downloads/vap-2.0.0-alpha1.deb)

* [Linux self contained app](https://bitbucket.org/labjacquespe/vap_interface/downloads/vap-2.0.0-alpha1.tar.xz)

Requirements
------------
[Java 7 Update 9 or later](http://www.java.com/)

Run
---
Double-click on the jar file, the vap_interface should open. Alternatively, in a terminal or command prompt type : 
```
java -jar vap_interface-1.0.0.jar
```

Build Requirements
------------
* [**JDK** 7 Update 9 or later](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven](http://maven.apache.org/download.cgi)
* [Git](http://git-scm.com/)

Build
-----
1. Clone the repository.

	```
	git clone https://your_username@bitbucket.org/labjacquespe/vap_interface.git
	```

2. Run maven install.

	```
	mvn install
	```

3. Binary files are in the target subfolder.

Documentation
-------------
See the [VAP Documentation](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/documentation/).

Release Notes
-------------
See the [VAP Releases Notes](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/home/)
to find out about the latest changes.

Bug Reports and Feedback
------------------------
You can report a bug [here](http://bitbucket.org/labjacquespe/vap_interface/issues?status=new&status=open).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

Licensing
---------
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt).

Some files in VAP source code are released under other licenses.
For further details see the COPYING file in this directory.

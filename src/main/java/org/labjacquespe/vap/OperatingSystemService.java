/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import static org.labjacquespe.vap.OperatingSystem.MAC;
import static org.labjacquespe.vap.OperatingSystem.OTHER;
import static org.labjacquespe.vap.OperatingSystem.UNIX;
import static org.labjacquespe.vap.OperatingSystem.WINDOWS;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.stereotype.Component;

/**
 * Service for operating system.
 */
@Component
public class OperatingSystemService {
  /**
   * Returns current operating system.
   *
   * @return current operating system
   */
  public OperatingSystem currentOs() {
    if (SystemUtils.IS_OS_WINDOWS) {
      return WINDOWS;
    } else if (SystemUtils.IS_OS_MAC) {
      return MAC;
    } else if (SystemUtils.IS_OS_UNIX) {
      return UNIX;
    }
    return OTHER;
  }

  /**
   * Returns data file extension.
   * <p>
   * This is mostly a patch for Mac OS X since <code>"*"</code> does not work on Mac OS X.
   * </p>
   *
   * @param os
   *          operating system
   * @return data file extension
   */
  public String[] dataFileExtensions(OperatingSystem os) {
    if (os == MAC) {
      return new String[] { "*", "*.txt", "*.bed", "*.tab", "*.wig", "*.bigWig", "*.bw",
          "*.bedGraph", "*.bg", "*.wiggle", "*.genePred", "*.gp", "*.gtf", "*.coord", "*.coordX",
          "*.coord1", "*.coord2", "*.coord3", "*.coord4", "*.coord5", "*.coord6" };
    }
    return new String[] { "*" };
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TextInputControl;

/**
 * Move a text input's caret to the end when focus is lost.
 */
public class MoveCaretToEndOnLostFocus {
  /**
   * Adds listeners to control to move caret at end of input when focus is lost.
   *
   * @param control
   *          control
   */
  public static void moveCaretToEndOnLostFocus(TextInputControl control) {
    new CaretMover(control);
  }

  private static final class CaretMover {
    private final BooleanProperty overrideNextCaretChange = new SimpleBooleanProperty();
    private final TextInputControl control;

    CaretMover(TextInputControl control) {
      this.control = control;
      control.focusedProperty().addListener((obv, ov, nv) -> {
        if (!nv) {
          moveCaretToEnd();
          overrideNextCaretChange.set(true);
        } else {
          overrideNextCaretChange.set(false);
        }
      });
      control.caretPositionProperty().addListener((obv, ov, nv) -> {
        if (overrideNextCaretChange.get()) {
          moveCaretToEnd();
        }
      });
    }

    private void moveCaretToEnd() {
      String text = control.getText();
      if (text != null) {
        control.positionCaret(text.length());
      }
    }
  }
}

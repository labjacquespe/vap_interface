/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Multiple files selection presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MultipleFilesSelectionPresenter extends BasePresenter {
  public ListProperty<File> filesProperty = new SimpleListProperty<>();
  @FXML
  private ResizablePane filesContainer;
  @FXML
  private TextField file;
  private FileChooser fileChooser = new FileChooser();
  private FileListView files = new FileListView();

  @FXML
  private void initialize() {
    filesProperty.set(FXCollections.observableArrayList(new ArrayList<File>()));
    files.itemsProperty().bindBidirectional(filesProperty);
    files.setPrefHeight(120.0);
    files.setEditable(true);
    filesContainer.setContent(files);
    files.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    fileChooser.setTitle(message("fileChooser.title"));
    fileChooser.getExtensionFilters()
        .add(new ExtensionFilter(message("fileChooser.description"), "*"));
  }

  public ListProperty<File> filesProperty() {
    return filesProperty;
  }

  public final ObjectProperty<File> initialDirectoryProperty() {
    return fileChooser.initialDirectoryProperty();
  }

  /**
   * Returns files.
   *
   * @return files
   */
  public List<File> getFiles() {
    List<File> files = new ArrayList<>();
    if (filesProperty.get() != null) {
      for (File file : filesProperty.get()) {
        files.add(file);
      }
    }
    return files;
  }

  /**
   * Sets files.
   *
   * @param files
   *          files
   */
  public void setFiles(Collection<File> files) {
    if (files != null) {
      filesProperty.set(FXCollections.observableArrayList(files));
    } else {
      filesProperty.set(FXCollections.observableArrayList(new ArrayList<File>()));
    }
  }

  /**
   * Show multiple file selection dialog to add files.
   *
   * @param event
   *          event
   */
  public void addFiles(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(fileChooser);
    List<File> selections = fileChooser.showOpenMultipleDialog(files.getScene().getWindow());
    if (selections != null) {
      if (!selections.isEmpty()) {
        fileChooser.setInitialDirectory(selections.get(0).getParentFile());
      }
      for (File selection : selections) {
        if (!files.getItems().contains(selection)) {
          files.getItems().add(selection);
        }
      }
    }
  }

  @FXML
  private void addFile(ActionEvent event) {
    if (file.getText() != null && !file.getText().isEmpty()) {
      files.getItems().add(new File(file.getText()));
    }
  }

  public void removeFiles(ActionEvent event) {
    files.removeSelectedFiles();
  }

  /**
   * Moves selected files up in the list.
   *
   * @param event
   *          event
   */
  public void upFiles(ActionEvent event) {
    List<Integer> selections = new ArrayList<>(files.getSelectionModel().getSelectedIndices());
    Collections.sort(selections);
    int numberOfElementsToSkip = 0;
    while (numberOfElementsToSkip < selections.size()
        && numberOfElementsToSkip == selections.get(numberOfElementsToSkip)) {
      numberOfElementsToSkip++;
    }

    if (numberOfElementsToSkip < selections.size()) {
      for (int i = numberOfElementsToSkip; i < selections.size(); i++) {
        int sourceIndex = selections.get(i);

        File element = files.getItems().get(sourceIndex);
        files.getItems().remove(sourceIndex);
        files.getItems().add(sourceIndex - 1, element);

        selections.set(i, selections.get(i) - 1);
      }
    }
    files.getSelectionModel().clearSelection();
    for (Integer selection : selections) {
      files.getSelectionModel().select(selection.intValue());
    }
  }

  /**
   * Moves selected files down in the list.
   *
   * @param event
   *          event
   */
  public void downFiles(ActionEvent event) {
    List<Integer> selections = new ArrayList<>(files.getSelectionModel().getSelectedIndices());
    Collections.sort(selections);
    Collections.reverse(selections);
    int numberOfElementsToSkip = 0;
    while (numberOfElementsToSkip < selections.size() && files.getItems().size()
        - numberOfElementsToSkip - 1 == selections.get(numberOfElementsToSkip)) {
      numberOfElementsToSkip++;
    }

    if (numberOfElementsToSkip < selections.size()) {
      for (int i = numberOfElementsToSkip; i < selections.size(); i++) {
        int sourceIndex = selections.get(i);

        File element = files.getItems().get(sourceIndex);
        files.getItems().remove(sourceIndex);
        files.getItems().add(sourceIndex + 1, element);

        selections.set(i, selections.get(i) + 1);
      }
    }
    files.getSelectionModel().clearSelection();
    for (Integer selection : selections) {
      files.getSelectionModel().select(selection.intValue());
    }
  }

  public void remvoveStyleClassFromCells(String classStyle) {
    files.remvoveStyleClassFromCells(classStyle);
  }

  public void addStyleClass(File file, String styleClass) {
    files.addStyleClass(file, styleClass);
  }

  public void setFileChooser(FileChooser fileChooser) {
    this.fileChooser = fileChooser;
  }
}

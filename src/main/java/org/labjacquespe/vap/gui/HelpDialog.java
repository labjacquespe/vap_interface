/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import java.util.ResourceBundle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * Help window.
 */
public class HelpDialog {
  private Stage stage;

  /**
   * Shows help dialog.
   *
   * @param owner
   *          dialog's owner
   */
  public HelpDialog(Window owner) {
    stage = new Stage();
    stage.initOwner(owner);
    stage.initModality(Modality.NONE);

    HelpDialogView view = new HelpDialogView();
    ResourceBundle resources = view.getResourceBundle();
    Parent root = view.getView();
    JavafxUtils.setMaxSizeForScreen(stage);

    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setTitle(resources.getString("title"));
  }

  public void show() {
    stage.show();
  }

  public void hide() {
    stage.hide();
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import static org.labjacquespe.vap.FindbugsJustifications.PRIVATE_METHOD_CALLED_BY_JAVAFX;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.stream.Collectors;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.labjacquespe.vap.chart.gui.CreateGraphPresenter;
import org.labjacquespe.vap.chart.gui.CreateGraphView;
import org.labjacquespe.vap.chart.gui.HelpChartParameterDialog;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.gui.HelpParameterDialog;
import org.labjacquespe.vap.core.gui.ParameterFilePresenter;
import org.labjacquespe.vap.core.gui.ParameterFileView;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.force.ForceDialog;
import org.labjacquespe.vap.javafx.force.ForceDialog.ForceOption;
import org.labjacquespe.vap.javafx.message.MessageDialog;
import org.labjacquespe.vap.javafx.message.MessageDialog.MessageDialogType;
import org.labjacquespe.vap.validation.JavafxValidationErrorAccumulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Main window presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MainPanePresenter extends BasePresenter {
  private final ObjectProperty<File> parametersProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private MenuItem openParameters;
  @FXML
  private MenuItem useMammalianDefaults;
  @FXML
  private MenuItem useYeastDefaults;
  @FXML
  private MenuItem saveParameters;
  @FXML
  private MenuItem saveParametersAs;
  @FXML
  private Menu testMenu;
  @FXML
  private MenuItem useTestData;
  @FXML
  private TabPane tabPane;
  @FXML
  private Tab parametersTab;
  @FXML
  private Tab createGraphTab;
  @Autowired
  private ParameterFileService textParameterService;
  private ParameterFilePresenter parameterFilePresenter;
  private CreateGraphPresenter createGraphPresenter;
  private TestHelpDialog testHelpDialog;
  private HelpDialog helpDialog;
  private HelpParameterDialog helpParameterDialog;
  private HelpChartParameterDialog helpChartParameterDialog;
  private final FileChooser parametersChooser = new FileChooser();

  @FXML
  private void initialize() {
    ParameterFileView parameterFileView = new ParameterFileView();
    parameterFilePresenter = (ParameterFilePresenter) parameterFileView.getPresenter();
    parametersTab.setContent(parameterFileView.getView());
    CreateGraphView createGraphView = new CreateGraphView();
    createGraphPresenter = (CreateGraphPresenter) createGraphView.getPresenter();
    createGraphTab.setContent(createGraphView.getView());

    tabPane.getSelectionModel().selectedItemProperty().addListener(tabListener());
    parametersChooser.setTitle(message("parametersChooser.title"));
    parametersChooser.getExtensionFilters()
        .add(new ExtensionFilter(message("parametersChooser.description"), "*.txt", "*.apt"));

    // Bindings.
    parameterFilePresenter.initialDirectoryProperty()
        .bindBidirectional(parametersChooser.initialDirectoryProperty());
    createGraphPresenter.initialDirectoryProperty()
        .bindBidirectional(parametersChooser.initialDirectoryProperty());

    // Disable test data option if test data is not available.
    testMenu.setVisible(getClass().getResource("/test") != null);
  }

  private ChangeListener<Tab> tabListener() {
    return (obv, ov, nv) -> {
      boolean disableParametersMenus = nv != parametersTab;
      openParameters.setDisable(disableParametersMenus);
      useMammalianDefaults.setDisable(disableParametersMenus);
      useYeastDefaults.setDisable(disableParametersMenus);
      saveParameters.setDisable(disableParametersMenus);
      saveParametersAs.setDisable(disableParametersMenus);
      useTestData.setDisable(disableParametersMenus);
    };
  }

  public AnalysisParameters getAnalysisParameters() {
    return parameterFilePresenter.getAnalysisParameters();
  }

  @FXML
  @SuppressFBWarnings(
      value = "UPM_UNCALLED_PRIVATE_METHOD",
      justification = PRIVATE_METHOD_CALLED_BY_JAVAFX)
  private void openParameters(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(parametersChooser);
    File file = parametersChooser.showOpenDialog(view.getScene().getWindow());
    if (file != null) {
      parametersChooser.setInitialDirectory(file.getParentFile());
      if (openParameters(file)) {
        parametersProperty.set(file);
      }
    }
  }

  private boolean openParameters(File file) {
    try {
      JavafxValidationErrorAccumulator errorHandler = new JavafxValidationErrorAccumulator();
      textParameterService.validate(file, Locale.getDefault(), errorHandler);
      boolean open = false;
      if (!errorHandler.errors().isEmpty()) {
        String forceMessage = message("openParameters.force", file.getName());
        ForceDialog.ForceOption option = new ForceDialog(view.getScene().getWindow(),
            message("validationError.title"), forceMessage,
            errorHandler.errors().stream().map(error -> error.error).collect(Collectors.toList()))
                .getOption();
        if (option == ForceOption.FORCE) {
          open = true;
        }
      } else {
        open = true;
      }
      if (open) {
        AnalysisParameters analysisParameters = textParameterService.parse(file);
        parameterFilePresenter.setAnalysisParameters(analysisParameters);
        return true;
      } else {
        return false;
      }
    } catch (IOException e) {
      String error = message("load.IOException.message", file.getPath());
      new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
          message("load.IOException.title"), error);
      return false;
    }
  }

  @FXML
  private void useMammalianDefaults(ActionEvent event) {
    parameterFilePresenter.setMammalianDefaults();
  }

  @FXML
  private void useYeastDefaults(ActionEvent event) {
    parameterFilePresenter.setYeastDefaults();
  }

  @FXML
  private void saveParameters(ActionEvent event) {
    if (parametersProperty.get() != null) {
      File parameters = parametersProperty.get();
      try {
        textParameterService.export(parameters, getAnalysisParameters(), true);
      } catch (IOException e) {
        String error = message("save.IOException.message", parameters.getPath());
        new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
            message("save.IOException.title"), error);
      }
    } else {
      saveParametersAs(event);
    }
  }

  @FXML
  private void saveParametersAs(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(parametersChooser);
    File file = parametersChooser.showSaveDialog(view.getScene().getWindow());
    if (file != null) {
      parametersChooser.setInitialDirectory(file.getParentFile());
      try {
        if (!file.getName().endsWith(".txt") && !file.getName().endsWith(".apt")) {
          file = new File(file.getPath() + ".txt");
        }
        parametersProperty.set(file);
        textParameterService.export(file, getAnalysisParameters(), true);
      } catch (IOException e) {
        String error = message("save.IOException.message", file.getPath());
        new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
            message("save.IOException.title"), error);
      }
    }
  }

  @FXML
  private void exit(ActionEvent event) {
    System.exit(0);
  }

  @FXML
  private void useTestData(ActionEvent event) {
    parameterFilePresenter.useTestData();
  }

  @FXML
  private void testHelp(ActionEvent event) {
    if (testHelpDialog == null) {
      testHelpDialog = new TestHelpDialog(view.getScene().getWindow());
    }
    testHelpDialog.show();
  }

  @FXML
  private void description(ActionEvent event) {
    if (helpDialog == null) {
      helpDialog = new HelpDialog(view.getScene().getWindow());
    }
    helpDialog.show();
  }

  @FXML
  private void parameterDescription(ActionEvent event) {
    if (tabPane.getSelectionModel().getSelectedItem() == parametersTab) {
      if (helpParameterDialog == null) {
        helpParameterDialog = new HelpParameterDialog(view.getScene().getWindow());
      }
      helpParameterDialog.show();
    } else {
      if (helpChartParameterDialog == null) {
        helpChartParameterDialog = new HelpChartParameterDialog(view.getScene().getWindow());
      }
      helpChartParameterDialog.show();
    }
  }

  @FXML
  private void about(ActionEvent event) {
    new AboutDialog(view.getScene().getWindow());
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class SelectAllListener {
  /**
   * Returns listener that select all text on mouse double click.
   * 
   * @return listener that select all text on mouse double click
   */
  public static EventHandler<MouseEvent> selectAllListener() {
    return event -> {
      if (event.getEventType() == MouseEvent.MOUSE_CLICKED && event.getClickCount() == 2) {
        if (event.getSource() instanceof TextField) {
          ((TextField) event.getSource()).selectAll();
        } else {
          throw new IllegalStateException(SelectAllListener.class.getSimpleName()
              + " does not support type " + event.getSource().getClass());
        }
      }
    };
  }
}

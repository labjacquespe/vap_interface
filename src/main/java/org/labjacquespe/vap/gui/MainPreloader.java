/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Preloader;
import javafx.stage.Stage;
import org.labjacquespe.vap.ApplicationStarted;
import org.labjacquespe.vap.Main;
import org.labjacquespe.vap.javafx.message.MessageDialog;
import org.labjacquespe.vap.javafx.message.MessageDialog.MessageDialogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaFX application preloader.
 */
public class MainPreloader extends Preloader {
  private static final Logger logger = LoggerFactory.getLogger(Main.class);
  private Stage stage;
  private SplashScreen splash;

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.stage = primaryStage;
    splash = new SplashScreen(primaryStage);
    splash.show();
  }

  @Override
  public void handleApplicationNotification(PreloaderNotification info) {
    if (info instanceof ApplicationStarted) {
      splash.hide();
    }
  }

  @Override
  public boolean handleErrorNotification(ErrorNotification info) {
    logger.error("Could not start application", info.getCause());
    com.airhacks.afterburner.injection.Injector.resetInstanceSupplier();
    ResourceBundle resources = ResourceBundle.getBundle(getClass().getName(), Locale.getDefault());
    new MessageDialog(stage, MessageDialogType.ERROR, resources.getString("error.title"),
        info.getCause().getMessage()).showAndWait();
    return true;
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import java.util.ResourceBundle;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.labjacquespe.vap.AbstractSpringBootJavafxApplication;
import org.labjacquespe.vap.ApplicationStarted;
import org.labjacquespe.vap.javafx.SpringAfterburnerInstanceSupplier;

/**
 * JavaFX application.
 */
public class MainApplication extends AbstractSpringBootJavafxApplication {
  @Override
  public void init() throws Exception {
    super.init();
    com.airhacks.afterburner.injection.Injector
        .setInstanceSupplier(new SpringAfterburnerInstanceSupplier(applicationContext));
  }

  @Override
  public void start(final Stage stage) throws Exception {
    final MainPaneView view = new MainPaneView();
    Pane root = (Pane) view.getView();
    root.setPrefHeight(800);
    root.setPrefWidth(1500);
    JavafxUtils.setMaxSizeForScreen(stage);
    ResourceBundle resources = view.getResourceBundle();
    stage.setTitle(resources.getString("title"));
    Scene scene = new Scene(view.getView());
    scene.getStylesheets().add("application.css");
    stage.setScene(scene);
    notifyPreloader(new ApplicationStarted());
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

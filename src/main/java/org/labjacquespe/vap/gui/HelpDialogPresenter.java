/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.web.WebView;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Help dialog presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HelpDialogPresenter {
  private static final String DOCUMENT =
      HelpDialogPresenter.class.getResource("/description.html").toString();

  @FXML
  private WebView webView;
  @FXML
  private Button back;
  @FXML
  private Button forward;

  @FXML
  private void initialize() {
    webView.getEngine().load(DOCUMENT);
    webView.getEngine().getHistory().currentIndexProperty().addListener((ob, ov, nv) -> {
      int index = nv.intValue();
      back.setDisable(index == 0);
      forward.setDisable(index == webView.getEngine().getHistory().getEntries().size() - 1);
    });
    back.setDisable(true);
    forward.setDisable(true);
  }

  @FXML
  private void back() {
    Platform.runLater(() -> {
      webView.getEngine().executeScript("history.back()");
    });
  }

  @FXML
  private void forward() {
    Platform.runLater(() -> {
      webView.getEngine().executeScript("history.forward()");
    });
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.Node;
import javafx.scene.control.Cell;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFilesOverHandler;
import org.labjacquespe.vap.util.FileUtils;

/**
 * List view for files.
 */
public class FileListView extends ListView<File> {
  private MapProperty<File, ObservableSet<String>> fileClassesProperty = new SimpleMapProperty<>(
      FXCollections.observableMap(new HashMap<File, ObservableSet<String>>()));
  private List<String> fileCellDefaultClasses;

  /**
   * Creates a {@link ListView} of files.
   */
  public FileListView() {
    fileCellDefaultClasses = Collections.unmodifiableList(new FileCell().getStyleClass());
    setCellFactory(dropableFileCellFactory());
    setOnKeyPressed(event -> {
      if (event.getCode() == KeyCode.DELETE) {
        removeSelectedFiles();
      }
    });

    // Enable drag and drop.
    setOnDragDetected(event -> {
      ClipboardContent content = new ClipboardContent();
      boolean allAbsolute = true;
      for (File file : getSelectionModel().getSelectedItems()) {
        allAbsolute &= file.isAbsolute();
      }
      if (allAbsolute) {
        content.putFiles(getSelectionModel().getSelectedItems());
      }
      StringBuilder builder = new StringBuilder();
      for (File file : getSelectionModel().getSelectedItems()) {
        builder.append("\n");
        builder.append(file.getPath());
      }
      if (builder.length() > 0) {
        builder.deleteCharAt(0);
      }
      content.putString(builder.toString());
      Dragboard db = startDragAndDrop(TransferMode.ANY);
      db.setContent(content);
      event.consume();
    });
    setOnDragDone(event -> {
      if (event.getTransferMode() == TransferMode.MOVE) {
        for (String path : event.getDragboard().getString().split("\\n")) {
          getItems().remove(new File(path));
        }
      }
      event.consume();
    });
    setOnDragOver(new DragFilesOverHandler(this, this));
    setOnDragExited(new DragExitedHandler(this));
    setOnDragDropped(event -> {
      boolean acceptFiles = true;
      if (event.getDragboard().hasFiles()) {
        for (File file : event.getDragboard().getFiles()) {
          file = FileUtils.resolveWindowsShorcut(file);
          acceptFiles &= file.isFile();
        }
      }
      if (event.getDragboard().hasFiles() && acceptFiles) {
        for (File file : event.getDragboard().getFiles()) {
          file = FileUtils.resolveWindowsShorcut(file);
          getItems().add(file);
        }
        event.setDropCompleted(true);
        event.consume();
      } else if (event.getDragboard().hasString()) {
        for (String path : event.getDragboard().getString().split("\\n")) {
          getItems().add(new File(path));
        }
        event.setDropCompleted(true);
        event.consume();
      }
    });
  }

  /**
   * Removes files that are selected.
   */
  public void removeSelectedFiles() {
    List<Integer> selections = new ArrayList<>(getSelectionModel().getSelectedIndices());
    Collections.sort(selections);
    Collections.reverse(selections);
    for (Integer selection : selections) {
      if (selection >= 0) {
        getItems().remove(selection.intValue());
      }
    }
  }

  /**
   * Removes style class from all cells.
   *
   * @param classStyle
   *          style class to remove
   */
  public void remvoveStyleClassFromCells(String classStyle) {
    for (Set<String> classes : fileClassesProperty.values()) {
      classes.remove(classStyle);
    }
    updateCellStyleClasses();
  }

  /**
   * Add style class to cell containing this file.
   *
   * @param file
   *          file
   * @param styleClass
   *          style class to remove
   */
  public void addStyleClass(File file, String styleClass) {
    if (!fileClassesProperty.containsKey(file)) {
      fileClassesProperty.put(file, FXCollections.observableSet(styleClass));
    } else {
      fileClassesProperty.get(file).add(styleClass);
    }
    updateCellStyleClasses();
  }

  private void updateCellStyleClasses() {
    Set<Node> cells = lookupAll(".list-cell");
    for (Node node : cells) {
      if (node instanceof FileCell) {
        FileCell cell = (FileCell) node;
        cell.getStyleClass().retainAll(fileCellDefaultClasses);
        if (cell.getItem() != null && fileClassesProperty.containsKey(cell.getItem())) {
          File file = cell.getItem();
          if (fileClassesProperty.containsKey(cell.getItem())) {
            cell.getStyleClass().addAll(fileClassesProperty.get(file));
          }
        }
      }
    }
  }

  private Callback<ListView<File>, ListCell<File>> dropableFileCellFactory() {
    return list -> {
      final ListCell<File> cell = new FileCell();
      cell.itemProperty().addListener(fileCellListener(cell));
      cell.setEditable(true);
      // Enable drop.
      cell.setOnDragOver(new DragFilesOverHandler(cell, cell) {
        @Override
        protected boolean accept(DragEvent event) {
          return acceptDrop(cell, event);
        }

        @Override
        protected void setAcceptTransferModes(DragEvent event) {
          event.acceptTransferModes(TransferMode.MOVE);
        }
      });
      cell.setOnDragExited(new DragExitedHandler(cell));
      cell.setOnDragDropped(event -> {
        if (acceptDrop(cell, event)) {
          int dropIndex = cell.getIndex();
          if (dropIndex > getItems().size()) {
            dropIndex = getItems().size();
          }

          // Remove files to move.
          for (String path : event.getDragboard().getString().split("\\n")) {
            File file = new File(path);
            int index = getItems().indexOf(file);
            if (index >= 0 && dropIndex > index) {
              dropIndex--;
            }
            getItems().remove(file);
          }

          // Move file at new location.
          getSelectionModel().clearSelection();
          for (String path : event.getDragboard().getString().split("\\n")) {
            File file = new File(path);
            getItems().add(dropIndex, file);
            getSelectionModel().selectIndices(dropIndex);
            dropIndex++;
          }
          // Hack so that MOVE will not remove files from source.
          event.setDropCompleted(false);
          event.consume();
        }
      });
      return cell;
    };
  }

  private boolean acceptDrop(ListCell<File> cell, DragEvent event) {
    return event.getGestureSource() == FileListView.this && event.getDragboard().hasString()
        && (cell.getItem() == null
            || !dragStringContains(event.getDragboard().getString(), cell.getItem().getPath()));
  }

  private boolean dragStringContains(String dragString, String find) {
    boolean found = false;
    for (String element : dragString.split("\\n")) {
      found |= element.equals(find);
    }
    return found;
  }

  private ChangeListener<File> fileCellListener(Cell<File> cell) {
    return (obv, ov, nv) -> {
      cell.getStyleClass().retainAll(fileCellDefaultClasses);
      if (nv != null && fileClassesProperty.containsKey(nv)) {
        cell.getStyleClass().addAll(fileClassesProperty.get(nv));
      }
    };
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import javafx.collections.ListChangeListener;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;

/**
 * Label that supports selection.
 */
public class SelectableLabel extends Label {
  private Label label = new Label();
  private TextField textField = new TextField();

  /**
   * Creates a label that contain text that can be selected.
   */
  public SelectableLabel() {
    getStyleClass().add("selectable-label");
    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    StackPane stack = new StackPane();
    stack.getChildren().addAll(label, textField);
    setGraphic(stack);
    label.setVisible(false);
    label.setWrapText(false);
    textField.setEditable(false);
    textField.setPrefWidth(0);

    getStyleClass().addListener(styleClassListener());
    textField.getStyleClass().add("label");
    label.textProperty().bind(textProperty());
    textProperty().bindBidirectional(textField.textProperty());
    tooltipProperty().bindBidirectional(textField.tooltipProperty());
  }

  public SelectableLabel(String text) {
    this();
    this.setText(text);
  }

  private ListChangeListener<String> styleClassListener() {
    return change -> {
      while (change.next()) {
        if (change.wasAdded()) {
          label.getStyleClass().addAll(change.getAddedSubList());
          textField.getStyleClass().addAll(change.getAddedSubList());
        } else if (change.wasRemoved()) {
          label.getStyleClass().removeAll(change.getRemoved());
          textField.getStyleClass().removeAll(change.getRemoved());
        }
      }
    };
  }
}

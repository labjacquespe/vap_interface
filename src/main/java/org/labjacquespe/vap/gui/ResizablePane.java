/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.gui;

import javafx.beans.DefaultProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 * {@link Pane} that the user can resize by selecting it's edges.
 */
@DefaultProperty("content")
public class ResizablePane extends StackPane {
  private final ObjectProperty<Node> contentProperty = new SimpleObjectProperty<>();
  private final BooleanProperty resizeVerticallyProperty = new SimpleBooleanProperty();
  private final BooleanProperty resizeHorizontallyProperty = new SimpleBooleanProperty();
  private Pane container = new StackPane();
  private Pane resize = new StackPane();

  /**
   * Creates a resizable pane.
   */
  public ResizablePane() {
    getStyleClass().add("resizable");
    resize.getStyleClass().add("resize");
    AnchorPane anchor = new AnchorPane();
    getChildren().add(anchor);
    anchor.getChildren().addAll(container, resize);
    AnchorPane.setTopAnchor(container, 0.0);
    AnchorPane.setRightAnchor(container, 5.0);
    AnchorPane.setBottomAnchor(container, 5.0);
    AnchorPane.setLeftAnchor(container, 0.0);
    AnchorPane.setBottomAnchor(resize, 0.0);
    AnchorPane.setRightAnchor(resize, 0.0);
    resize.setPrefWidth(5);
    resize.setPrefHeight(5);
    contentProperty.addListener(contentListener());
    resize.setOnMouseEntered(event -> {
      if (resizeVerticallyProperty.get() && resizeHorizontallyProperty.get()) {
        setCursor(Cursor.SE_RESIZE);
      } else if (resizeVerticallyProperty.get()) {
        setCursor(Cursor.S_RESIZE);
      } else if (resizeHorizontallyProperty.get()) {
        setCursor(Cursor.E_RESIZE);
      } else {
        setCursor(null);
      }
    });
    resize.setOnMouseExited(event -> setCursor(null));
    resize.setOnMouseDragged(resizeHandler());

    // Default values.
    resizeVerticallyProperty.set(true);
    resizeHorizontallyProperty.set(true);
  }

  public ObjectProperty<Node> contentProperty() {
    return contentProperty;
  }

  public BooleanProperty resizeVerticallyProperty() {
    return resizeVerticallyProperty;
  }

  public BooleanProperty resizeHorizontallyProperty() {
    return resizeHorizontallyProperty;
  }

  public Node getContent() {
    return contentProperty.get();
  }

  public void setContent(Node content) {
    contentProperty.set(content);
  }

  public boolean isResizeVertically() {
    return resizeVerticallyProperty.get();
  }

  public void setResizeVertically(boolean resizeVertically) {
    resizeVerticallyProperty.set(resizeVertically);
  }

  public boolean isResizeHorizontally() {
    return resizeHorizontallyProperty.get();
  }

  public void setResizeHorizontally(boolean resizeHorizontally) {
    resizeHorizontallyProperty.set(resizeHorizontally);
  }

  private ChangeListener<Node> contentListener() {
    return (obv, ov, nv) -> {
      container.getChildren().clear();
      if (nv != null) {
        container.getChildren().add(nv);
      }
    };
  }

  private EventHandler<MouseEvent> resizeHandler() {
    return event -> {
      if (resizeVerticallyProperty.get()) {
        setPrefHeight(resize.getBoundsInParent().getMinY() + event.getY() + 5);
      }
      if (resizeHorizontallyProperty.get()) {
        setPrefWidth(resize.getBoundsInParent().getMinX() + event.getX() + 5);
      }
      if (resizeVerticallyProperty.get() || resizeHorizontallyProperty.get()) {
        requestLayout();
      }
      event.consume();
    };
  }
}

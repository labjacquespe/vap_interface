/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Accumulator for JavaFX validation error.
 */
public class JavafxValidationErrorAccumulator
    implements BiConsumer<String, String>, Consumer<String> {
  private List<JavafxValidationError> errors = new ArrayList<>();

  @Override
  public void accept(String error) {
    errors.add(new JavafxValidationError(error, null));
  }

  @Override
  public void accept(String error, String tooltip) {
    errors.add(new JavafxValidationError(error, tooltip));
  }

  public List<JavafxValidationError> errors() {
    return errors;
  }
}

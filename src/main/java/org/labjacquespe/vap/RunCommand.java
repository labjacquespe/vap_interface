/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * Command line parameters.
 */
@Parameters(
    separators = " =",
    commandNames = RunCommand.RUN_COMMAND,
    commandDescription = "Run vap core and generate graphs")
public class RunCommand {
  public static final String RUN_COMMAND = "run";

  @Parameter(names = { "-p" }, required = true)
  private String parameterFile;
  @Parameter(names = { "-gag", "--generate_aggregate_graphs" })
  private String generateAggregateGraphs;
  @Parameter(names = { "-gh", "--generate_heatmaps" })
  private String generateHeatmap;
  @Parameter(names = { "-ddv", "--display_dispersion_values" })
  private String dipersion;
  @Parameter(names = { "-yas", "--y_axis_scale" })
  private String yaxisScale;

  public String getParameterFile() {
    return parameterFile;
  }

  public void setParameterFile(String parameterFile) {
    this.parameterFile = parameterFile;
  }

  public String getGenerateAggregateGraphs() {
    return generateAggregateGraphs;
  }

  public void setGenerateAggregateGraphs(String generateAggregateGraphs) {
    this.generateAggregateGraphs = generateAggregateGraphs;
  }

  public String getGenerateHeatmap() {
    return generateHeatmap;
  }

  public void setGenerateHeatmap(String generateHeatmap) {
    this.generateHeatmap = generateHeatmap;
  }

  public String getDipersion() {
    return dipersion;
  }

  public void setDipersion(String dipersion) {
    this.dipersion = dipersion;
  }

  public String getYaxisScale() {
    return yaxisScale;
  }

  public void setYaxisScale(String yaxisScale) {
    this.yaxisScale = yaxisScale;
  }
}

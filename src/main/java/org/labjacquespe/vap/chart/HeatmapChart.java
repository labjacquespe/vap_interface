/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.beans.property.ListProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * Chart showing heatmap.
 */
public class HeatmapChart<X, Y> extends XYChart<X, Y> {
  protected class ColorLegend extends HBox {
    private ListProperty<Label> textColorValuesProperty =
        new SimpleListProperty<>(FXCollections.observableList(new ArrayList<Label>()));
    private MapProperty<Label, Pane> textColorValuesPaneProperty =
        new SimpleMapProperty<>(FXCollections.observableMap(new HashMap<Label, Pane>()));
    private Rectangle colorGradient = new Rectangle();
    private Pane colorGradientContainer = new HBox();
    private NumberFormat textColorValueFormat = new DecimalFormat("#.##");
    private GridPane grid = new GridPane();

    private ColorLegend() {
      getStyleClass().addAll("chart-legend", "chart-heatmap-color-legend");
      getChildren().add(grid);
      colorGradientContainer.getChildren().add(colorGradient);
      colorGradient.getStyleClass().add("gradient");
      colorGradient.setHeight(GRADIENT_LEGEND_HEIGHT);
      grid.add(colorGradientContainer, 0, 1, Integer.MAX_VALUE, 1);
      colorsProperty.addListener((ov, oldValue, newValue) -> updateGradient());
      updateGradient();
      grid.setMinWidth(LEGEND_MIN_WIDTH);
      prefWidthProperty().addListener((ov, oldValue, newValue) -> grid
          .setPrefWidth(newValue.doubleValue() - LEGEND_GRID_PADDING));
    }

    @Override
    protected void layoutChildren() {
      super.layoutChildren();
      double width = grid.getWidth();
      double columnWidth = width / colorsProperty.size();
      double colorGradientWidth = columnWidth * (colorsProperty.size() - 1);
      colorGradient.setWidth(colorGradientWidth);
      double padding = columnWidth / 2;
      colorGradientContainer.setPadding(new Insets(0, padding, 0, padding));
      for (Pane pane : textColorValuesPaneProperty.values()) {
        pane.setPrefWidth(columnWidth);
        pane.setMinWidth(columnWidth);
        pane.setMaxWidth(columnWidth);
      }
    }

    private void updateGradient() {
      double colorStopStep = 1.0 / (colorsProperty.size() - 1);
      Stop[] stops = new Stop[colorsProperty.size()];
      for (int i = 0; i < colorsProperty.size(); i++) {
        stops[i] = new Stop(i * colorStopStep, colorsProperty.get(i));
      }
      LinearGradient gradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
      colorGradient.setFill(gradient);
      while (textColorValuesProperty.size() < colorsProperty.size()) {
        Label text = new Label();
        HBox textContainer = new HBox();
        textContainer.getStyleClass().addAll("color-value-container");
        textContainer.getChildren().add(text);
        textContainer.setAlignment(Pos.CENTER);
        textColorValuesPaneProperty.put(text, textContainer);
        grid.add(textContainer, textColorValuesProperty.size(), 0);
        textColorValuesProperty.add(text);
      }
      while (textColorValuesProperty.size() > colorsProperty.size()) {
        Label text = textColorValuesProperty.remove(textColorValuesProperty.size() - 1);
        Pane textContainer = textColorValuesPaneProperty.remove(text);
        grid.getChildren().remove(textContainer);
      }
    }

    private void updateTextColorValues(Map<Color, Double> colorValues) {
      int index = 0;
      for (Color color : colorsProperty) {
        Double value = colorValues.get(color);
        if (value != null) {
          Label text = textColorValuesProperty.get(index++);
          text.setText(textColorValueFormat.format(value));
        }
      }
    }
  }

  private static final double GRADIENT_LEGEND_HEIGHT = 20;
  private static final double LEGEND_MIN_WIDTH = 400;
  private static final double LEGEND_GRID_PADDING = 20;
  protected static final double LEGEND_PREF_WIDTH_PERCENTAGE = 0.8;

  private final ListProperty<Color> colorsProperty = new SimpleListProperty<>();
  private final Set<X> xvalues = new HashSet<>();
  private final Set<Y> yvalues = new HashSet<>();
  private final ObjectProperty<Double> minHeatProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<Double> maxHeatProperty = new SimpleObjectProperty<>();
  protected ColorLegend colorLegend;

  public HeatmapChart(Axis<X> xaxis, Axis<Y> yaxis) {
    this(xaxis, yaxis, FXCollections.<Series<X, Y>>observableArrayList());
  }

  /**
   * Creates heatmap chart with axises and data.
   *
   * @param xaxis
   *          X axis
   * @param yaxis
   *          Y axis
   * @param data
   *          heatmap data
   */
  public HeatmapChart(Axis<X> xaxis, Axis<Y> yaxis, ObservableList<Series<X, Y>> data) {
    super(xaxis, yaxis);
    this.getStyleClass().add("chart-heatmap");
    colorsProperty.set(getDefaultColors());
    this.setData(data);
    colorLegend = new ColorLegend();
    this.setLegend(colorLegend);
    colorLegend.visibleProperty().bindBidirectional(legendVisibleProperty());
    widthProperty().addListener((ov, oldValue, newValue) -> {
      double colorLegendWidth = newValue.doubleValue() * 0.8;
      colorLegend.setPrefWidth(colorLegendWidth);
      colorLegend.setMinWidth(colorLegendWidth);
      colorLegend.setMaxWidth(colorLegendWidth);
    });
  }

  private ObservableList<Color> getDefaultColors() {
    ObservableList<Color> colors = FXCollections.observableArrayList();
    colors.add(Color.web("#000099"));
    colors.add(Color.web("#0000FF"));
    colors.add(Color.web("#0099FF"));
    colors.add(Color.web("#00FFFF"));
    colors.add(Color.web("#00FF99"));
    colors.add(Color.web("#00FF00"));
    colors.add(Color.web("#99FF00"));
    colors.add(Color.web("#FFFF00"));
    colors.add(Color.web("#FF9900"));
    colors.add(Color.web("#FF0000"));
    colors.add(Color.web("#990000"));
    return colors;
  }

  public ListProperty<Color> colorsProperty() {
    return colorsProperty;
  }

  public ObservableList<Color> getColors() {
    return colorsProperty.get();
  }

  public void setColors(ObservableList<Color> colors) {
    colorsProperty.set(colors);
  }

  public ObjectProperty<Double> minHeatProperty() {
    return minHeatProperty;
  }

  public Double getMinHeat() {
    return minHeatProperty.get();
  }

  public void setMinHeat(Double minHeat) {
    minHeatProperty.set(minHeat);
  }

  public ObjectProperty<Double> maxHeatProperty() {
    return maxHeatProperty;
  }

  public Double getMaxHeat() {
    return maxHeatProperty.get();
  }

  public void setMaxHeat(Double maxHeat) {
    maxHeatProperty.set(maxHeat);
  }

  private void findXAndYValues() {
    xvalues.clear();
    yvalues.clear();
    if (!getData().isEmpty()) {
      Series<X, Y> series = getData().get(0);
      Iterator<Data<X, Y>> iter = getDisplayedDataIterator(series);
      while (iter.hasNext()) {
        Data<X, Y> item = iter.next();
        xvalues.add(item.getXValue());
        yvalues.add(item.getYValue());
      }
    }
    for (Series<X, Y> series : getData()) {
      Iterator<Data<X, Y>> iter = getDisplayedDataIterator(series);
      if (iter.hasNext()) {
        Data<X, Y> item = iter.next();
        xvalues.add(item.getXValue());
        yvalues.add(item.getYValue());
      }
    }
  }

  @Override
  protected void seriesAdded(Series<X, Y> series, int seriesIndex) {
    // handle any data already in series
    for (int j = 0; j < series.getData().size(); j++) {
      dataItemAdded(series, j, series.getData().get(j));
    }
  }

  @Override
  protected void seriesRemoved(final Series<X, Y> series) {
    // remove all symbol nodes
    if (shouldAnimate()) {
      ParallelTransition pt = new ParallelTransition();
      pt.setOnFinished(event -> removeSeriesFromDisplay(series));
      for (final Data<X, Y> d : series.getData()) {
        final Node symbol = d.getNode();
        // fade out old symbol
        FadeTransition ft = new FadeTransition(Duration.millis(500), symbol);
        ft.setToValue(0);
        ft.setOnFinished(event -> getPlotChildren().remove(symbol));
        pt.getChildren().add(ft);
      }
      pt.play();
    } else {
      for (final Data<X, Y> d : series.getData()) {
        final Node symbol = d.getNode();
        getPlotChildren().remove(symbol);
      }
      removeSeriesFromDisplay(series);
    }
  }

  @Override
  protected void dataItemAdded(Series<X, Y> series, int itemIndex, Data<X, Y> item) {
    Rectangle symbol = (Rectangle) item.getNode();
    // check if symbol has already been created
    if (symbol == null) {
      symbol = new Rectangle();
      item.setNode(symbol);
    }
    // add and fade in new symbol if animated
    if (shouldAnimate()) {
      symbol.setOpacity(0);
    }
    getPlotChildren().add(symbol);
    if (shouldAnimate()) {
      FadeTransition ft = new FadeTransition(Duration.millis(500), symbol);
      ft.setToValue(1);
      ft.play();
    }
  }

  @Override
  protected void dataItemRemoved(final Data<X, Y> item, final Series<X, Y> series) {
    final Node symbol = item.getNode();
    if (shouldAnimate()) {
      // fade out old symbol
      FadeTransition ft = new FadeTransition(Duration.millis(500), symbol);
      ft.setToValue(0);
      ft.setOnFinished(event -> {
        getPlotChildren().remove(symbol);
        removeDataItemFromDisplay(series, item);
      });
      ft.play();
    } else {
      getPlotChildren().remove(symbol);
      removeDataItemFromDisplay(series, item);
    }
  }

  @Override
  protected void dataItemChanged(Data<X, Y> item) {
  }

  @Override
  protected void updateLegend() {
  }

  @Override
  protected void layoutPlotChildren() {
    findXAndYValues();
    double[] heatRange = getHeatRange();
    double minHeat = heatRange[0];
    double maxHeat = heatRange[1];
    updateColorValues(minHeat, maxHeat);
    // Update rectangle position/size to display heat.
    Map<X, Double> xpositions = new HashMap<>();
    Map<Y, Double> ypositions = new HashMap<>();
    for (X value : xvalues) {
      xpositions.put(value, getXAxis().getDisplayPosition(value));
    }
    for (Y value : yvalues) {
      ypositions.put(value, getYAxis().getDisplayPosition(value));
    }
    Dimension2D symbolDimension = getSymbolDimension(xpositions, ypositions);
    double width = symbolDimension.getWidth();
    double height = symbolDimension.getHeight();
    for (Series<X, Y> series : getData()) {
      Iterator<Data<X, Y>> iter = getDisplayedDataIterator(series);
      while (iter.hasNext()) {
        Data<X, Y> item = iter.next();
        Rectangle symbol = (Rectangle) item.getNode();
        X xcurrent = getCurrentDisplayedXValue(item);
        Y ycurrent = getCurrentDisplayedYValue(item);
        double xvalue;
        if (xpositions.containsKey(xcurrent)) {
          xvalue = xpositions.get(xcurrent);
        } else {
          xvalue = getXAxis().getDisplayPosition(xcurrent);
        }
        double yvalue;
        if (ypositions.containsKey(ycurrent)) {
          yvalue = ypositions.get(ycurrent);
        } else {
          yvalue = getYAxis().getDisplayPosition(ycurrent);
        }
        Color color = Color.TRANSPARENT;
        symbol.setWidth(width);
        symbol.setHeight(height);
        symbol.resizeRelocate(xvalue - width / 2, yvalue - height / 2, width, height);
        Double heat = (Double) item.getExtraValue();
        if (heat != null && !heat.isNaN()) {
          color = getSymbolColor(heat, minHeat, maxHeat);
        }
        symbol.setFill(color);
        symbol.setStroke(color);
      }
    }
    colorLegend.setPrefWidth(getWidth() * LEGEND_PREF_WIDTH_PERCENTAGE);
  }

  protected void updateColorValues(double minHeat, double maxHeat) {
    updateColorValues(colorLegend, minHeat, maxHeat);
  }

  protected void updateColorValues(ColorLegend legend, double minHeat, double maxHeat) {
    Map<Color, Double> colorValues = new HashMap<>();
    for (int i = 0; i < colorsProperty.size(); i++) {
      Double value = (i * (maxHeat - minHeat) / (colorsProperty.size() - 1)) + minHeat;
      colorValues.put(colorsProperty.get(i), value);
    }
    legend.updateTextColorValues(colorValues);
  }

  protected double[] getHeatRange() {
    double minHeat = Double.MAX_VALUE;
    double maxHeat = Double.MIN_VALUE;
    if (minHeatProperty.get() == null || maxHeatProperty.get() == null) {
      for (Series<X, Y> series : getData()) {
        for (Data<X, Y> item : series.getData()) {
          Double value = (Double) item.getExtraValue();
          if (value != null && !value.isInfinite() && !value.isNaN()) {
            minHeat = Math.min(minHeat, value);
            maxHeat = Math.max(maxHeat, value);
          }
        }
      }
    }
    if (minHeatProperty.get() != null) {
      minHeat = minHeatProperty.get();
    }
    if (maxHeatProperty.get() != null) {
      maxHeat = maxHeatProperty.get();
    }
    return new double[] { minHeat, maxHeat };
  }

  private Dimension2D getSymbolDimension(Map<X, Double> xpositions, Map<Y, Double> ypositions) {
    double firstX = Double.MAX_VALUE;
    double secondX = Double.MAX_VALUE;
    double firstY = Double.MAX_VALUE;
    double secondY = Double.MAX_VALUE;
    for (Double position : xpositions.values()) {
      if (position < firstX) {
        secondX = firstX;
        firstX = position;
      } else if (position > firstX && position < secondX) {
        secondX = position;
      }
    }
    for (Double position : ypositions.values()) {
      if (position < firstY) {
        secondY = firstY;
        firstY = position;
      } else if (position > firstY && position < secondY) {
        secondY = position;
      }
    }
    double width = Math.abs(firstX - secondX);
    double height = Math.abs(firstY - secondY);
    return new Dimension2D(width, height);
  }

  protected Color getSymbolColor(Double heat, double min, double max) {
    int colorIndex = (int) Math.round(((heat - min) / ((max - min) / (colorsProperty.size() - 1))));
    colorIndex = Math.max(colorIndex, 0);
    colorIndex = Math.min(colorIndex, colorsProperty.size() - 1);
    return colorsProperty.get(colorIndex);
  }

  /**
   * Returns color legend.
   *
   * @return color legend
   */
  public Region getColorLegend() {
    ColorLegend legend = new ColorLegend();
    legend.updateGradient();
    double[] heatRange = getHeatRange();
    double minHeat = heatRange[0];
    double maxHeat = heatRange[1];
    updateColorValues(legend, minHeat, maxHeat);
    return legend;
  }
}

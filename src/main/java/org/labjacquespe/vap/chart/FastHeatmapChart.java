/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.DoubleSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.scene.chart.Axis;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * {@link HeatmapChart} that is faster and consumes less memory.
 * <p>
 * Limitations:
 * </p>
 * <ul>
 * <li>Does not support animation.</li>
 * <li>May slightly differ from {@link HeatmapChart} implementation.</li>
 * <li>May appear blurry.</li>
 * </ul>
 */
public class FastHeatmapChart<X, Y> extends HeatmapChart<X, Y> {
  private static final int ITEM_HEAT_WIDTH = 10;
  private static final int ITEM_HEAT_HEIGHT = 5;

  public FastHeatmapChart(Axis<X> xaxis, Axis<Y> yaxis) {
    this(xaxis, yaxis, FXCollections.<Series<X, Y>>observableArrayList());
  }

  public FastHeatmapChart(Axis<X> xaxis, Axis<Y> yaxis, ObservableList<Series<X, Y>> data) {
    super(xaxis, yaxis, data);
  }

  @Override
  protected void seriesAdded(Series<X, Y> series, int seriesIndex) {
  }

  @Override
  protected void seriesRemoved(Series<X, Y> series) {
  }

  @Override
  protected void dataItemAdded(Series<X, Y> series, int itemIndex, Data<X, Y> item) {
  }

  @Override
  protected void dataItemRemoved(Data<X, Y> item, Series<X, Y> series) {
  }

  @Override
  protected void dataItemChanged(Data<X, Y> item) {
  }

  @Override
  protected void layoutPlotChildren() {
    Map<X, Double> xpositions = items().map(item -> item.getXValue()).distinct().collect(
        Collectors.toMap(xvalue -> xvalue, xvalue -> getXAxis().getDisplayPosition(xvalue)));
    Map<Y, Double> ypositions = items().map(item -> item.getYValue()).distinct().collect(
        Collectors.toMap(yvalue -> yvalue, yvalue -> getYAxis().getDisplayPosition(yvalue)));
    DoubleSummaryStatistics plotWidthStats =
        xpositions.values().stream().mapToDouble(value -> value).distinct().summaryStatistics();
    DoubleSummaryStatistics plotHeightStats =
        ypositions.values().stream().mapToDouble(value -> value).distinct().summaryStatistics();
    DoubleSummaryStatistics itemWidthStats = xpositions.values().stream()
        .mapToDouble(value -> value).distinct().sorted().limit(2).summaryStatistics();
    DoubleSummaryStatistics itemHeightStats = ypositions.values().stream()
        .mapToDouble(value -> value).distinct().sorted().limit(2).summaryStatistics();
    Dimension2D itemDimension = new Dimension2D(itemWidthStats.getMax() - itemWidthStats.getMin(),
        itemHeightStats.getMax() - itemHeightStats.getMin());
    Dimension2D plotDimension = new Dimension2D(
        plotWidthStats.getMax() - plotWidthStats.getMin() + itemDimension.getWidth(),
        plotHeightStats.getMax() - plotHeightStats.getMin() + itemDimension.getHeight());
    Map<X, Integer> xindexes = xpositions.entrySet().stream()
        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> (int) Math
            .round((entry.getValue() - itemWidthStats.getMin()) / itemDimension.getWidth())));
    Map<Y, Integer> yindexes = ypositions.entrySet().stream()
        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> (int) Math
            .round((entry.getValue() - itemHeightStats.getMin()) / itemDimension.getHeight())));
    double[] heatRange = getHeatRange();
    double minHeat = heatRange[0];
    double maxHeat = heatRange[1];
    getPlotChildren().clear();
    WritableImage heatmap = new WritableImage(
        (xindexes.values().stream().mapToInt(v -> v).max().orElse(1) + 1) * ITEM_HEAT_WIDTH + 1,
        (yindexes.values().stream().mapToInt(v -> v).max().orElse(1) + 1) * ITEM_HEAT_HEIGHT + 1);
    PixelWriter writer = heatmap.getPixelWriter();
    items().forEach(item -> {
      int xvalue = xindexes.get(item.getXValue()) * ITEM_HEAT_WIDTH;
      int yvalue = yindexes.get(item.getYValue()) * ITEM_HEAT_HEIGHT;
      Color color = Color.TRANSPARENT;
      Double heat = (Double) item.getExtraValue();
      if (heat != null && !heat.isNaN()) {
        color = getSymbolColor(heat, minHeat, maxHeat);
      }
      for (int x = xvalue; x < xvalue + ITEM_HEAT_WIDTH; x++) {
        for (int y = yvalue; y < yvalue + ITEM_HEAT_HEIGHT; y++) {
          writer.setColor(x, y, color);
        }
      }
    });
    ImageView heatmapView = new ImageView(heatmap);
    heatmapView.setFitWidth(plotDimension.getWidth());
    heatmapView.setFitHeight(plotDimension.getHeight());
    heatmapView.resizeRelocate(itemWidthStats.getMin() - itemDimension.getWidth() / 2,
        itemHeightStats.getMin() - itemDimension.getHeight() / 2, plotDimension.getWidth(),
        plotDimension.getHeight());
    getPlotChildren().add(heatmapView);
    updateColorValues(minHeat, maxHeat);
    colorLegend.setPrefWidth(getWidth() * LEGEND_PREF_WIDTH_PERCENTAGE);
  }

  private Stream<Data<X, Y>> items() {
    return getData().stream().flatMap(serie -> serie.getData().stream());
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.animation.FadeTransition;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 * {@link LineChart} with standard deviations in data.
 */
public class StandardDeviationLineChart<X> extends LineChart<X, Number> {
  private class StandardDeviationBar extends Line {
    public void update(double highOffset, double lowOffset) {
      setStartY(highOffset);
      setEndY(lowOffset);
    }
  }

  /**
   * Creates a standard deviation line chart with axises.
   *
   * @param xaxis
   *          X axis
   * @param yaxis
   *          Y axis
   */
  public StandardDeviationLineChart(Axis<X> xaxis, Axis<Number> yaxis) {
    super(xaxis, yaxis);
    this.getStyleClass().add("chart-stdev");
    this.setCreateSymbols(false);
  }

  public StandardDeviationLineChart(Axis<X> xaxis, Axis<Number> yaxis,
      ObservableList<Series<X, Number>> data) {
    this(xaxis, yaxis);
    this.setData(data);
  }

  @Override
  protected void dataItemAdded(Series<X, Number> series, int itemIndex, Data<X, Number> item) {
    super.dataItemAdded(series, itemIndex, item);
    Node bar = createStandardDeviationBar(getData().indexOf(series), item, itemIndex);
    if (shouldAnimate()) {
      bar.setOpacity(0);
      getPlotChildren().add(bar);
      // fade in new bar
      FadeTransition ft = new FadeTransition(Duration.millis(500), bar);
      ft.setToValue(1);
      ft.play();
    } else {
      getPlotChildren().add(bar);
    }
  }

  @Override
  protected void dataItemRemoved(Data<X, Number> item, Series<X, Number> series) {
    super.dataItemRemoved(item, series);
    final Node bar = item.getNode();
    if (shouldAnimate()) {
      // fade out old bar
      FadeTransition ft = new FadeTransition(Duration.millis(500), bar);
      ft.setToValue(0);
      ft.setOnFinished(event -> getPlotChildren().remove(bar));
      ft.play();
    } else {
      getPlotChildren().remove(bar);
    }
  }

  @Override
  protected void seriesAdded(Series<X, Number> series, int seriesIndex) {
    super.seriesAdded(series, seriesIndex);
    // handle any data already in series
    for (int j = 0; j < series.getData().size(); j++) {
      Data<X, Number> item = series.getData().get(j);
      Node bar = createStandardDeviationBar(seriesIndex, item, j);
      if (shouldAnimate()) {
        bar.setOpacity(0);
        getPlotChildren().add(bar);
        // fade in new bar
        FadeTransition ft = new FadeTransition(Duration.millis(500), bar);
        ft.setToValue(1);
        ft.play();
      } else {
        getPlotChildren().add(bar);
      }
    }
  }

  @Override
  protected void seriesRemoved(Series<X, Number> series) {
    super.seriesRemoved(series);
    // remove all bar nodes
    for (Data<X, Number> d : series.getData()) {
      final Node bar = d.getNode();
      if (shouldAnimate()) {
        // fade out old bar
        FadeTransition ft = new FadeTransition(Duration.millis(500), bar);
        ft.setToValue(0);
        ft.setOnFinished(event -> getPlotChildren().remove(bar));
        ft.play();
      } else {
        getPlotChildren().remove(bar);
      }
    }
  }

  /**
   * Create a new StandardDeviationBar node to represent a single data item.
   *
   * @param seriesIndex
   *          index of the series the data item is in
   * @param item
   *          data item to create node for
   * @param itemIndex
   *          index of the data item in the series
   * @return new StandardDeviationBarnode to represent the give data item
   */
  private Node createStandardDeviationBar(int seriesIndex, final Data<X, Number> item,
      int itemIndex) {
    Node bar = item.getNode();
    // check if bar has already been created
    if (bar == null) {
      bar = new StandardDeviationBar();
      item.setNode(bar);
    }
    if (bar != null) {
      bar.getStyleClass().setAll("chart-line-symbol", "series" + seriesIndex, "data" + itemIndex,
          "default-color" + (seriesIndex % 8));
    }
    return bar;
  }

  @Override
  protected void layoutPlotChildren() {
    super.layoutPlotChildren();
    // update bar positions
    for (int seriesIndex = 0; seriesIndex < getData().size(); seriesIndex++) {
      Series<X, Number> series = getData().get(seriesIndex);
      Iterator<Data<X, Number>> iter = getDisplayedDataIterator(series);
      while (iter.hasNext()) {
        Data<X, Number> data = iter.next();
        double xvalue = getXAxis().getDisplayPosition(getCurrentDisplayedXValue(data));
        double yvalue = getYAxis().getDisplayPosition(getCurrentDisplayedYValue(data));
        Node itemNode = data.getNode();
        Double standardDeviation = getStandardDeviation(data);
        if (itemNode instanceof StandardDeviationLineChart.StandardDeviationBar
            && standardDeviation != null && !Double.isNaN(standardDeviation)) {
          @SuppressWarnings("unchecked")
          StandardDeviationBar bar = (StandardDeviationBar) itemNode;
          double high = getYAxis().getDisplayPosition(
              getYAxis().toRealValue(data.getYValue().doubleValue() + standardDeviation));
          double low = getYAxis().getDisplayPosition(
              getYAxis().toRealValue(data.getYValue().doubleValue() - standardDeviation));
          // update bar
          bar.update(high - yvalue, low - yvalue);

          // position the bar
          bar.setLayoutX(xvalue);
          bar.setLayoutY(yvalue);
        }
      }
    }
  }

  /**
   * This is called when the range has been invalidated and we need to update it. If the axis are
   * auto ranging then we compile a list of all data that the given axis has to plot and call
   * invalidateRange() on the axis passing it that data.
   */
  @Override
  protected void updateAxisRange() {
    // For standard deviation chart we need to override this method as we
    // need to let the axis know that they need to be able to cover the
    // whole area occupied by the high to low range not just its center data
    // value
    final Axis<X> xa = getXAxis();
    final Axis<Number> ya = getYAxis();
    List<X> xdata = null;
    List<Number> ydata = null;
    if (xa.isAutoRanging()) {
      xdata = new ArrayList<>();
    }
    if (ya.isAutoRanging()) {
      ydata = new ArrayList<>();
    }
    if (xdata != null || ydata != null) {
      for (Series<X, Number> series : getData()) {
        for (Data<X, Number> data : series.getData()) {
          if (xdata != null) {
            xdata.add(data.getXValue());
          }
          if (ydata != null && !Double.isNaN(data.getYValue().doubleValue())) {
            Double standardDeviation = getStandardDeviation(data);
            if (standardDeviation != null && !Double.isNaN(standardDeviation)) {
              ydata.add(data.getYValue().doubleValue() + standardDeviation);
              ydata.add(data.getYValue().doubleValue() - standardDeviation);
            } else {
              ydata.add(data.getYValue());
            }
          }
        }
      }
      if (xdata != null) {
        xa.invalidateRange(xdata);
      }
      if (ydata != null) {
        ya.invalidateRange(ydata);
      }
    }
  }

  protected Double getStandardDeviation(Data<X, Number> data) {
    if (data.getExtraValue() instanceof Double) {
      return (Double) data.getExtraValue();
    } else {
      return null;
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

/**
 * Analysis data.
 */
public class Data {
  public enum Type {
    NORMAL, SEPARATOR
  }

  private Type type;
  private Double originalX;
  private Double xvalue;
  private Double length;
  private Double average;
  private Double standardDeviation;
  private Double prop;

  public Double getX() {
    return xvalue;
  }

  public Double getAverage() {
    return average;
  }

  public Double getStandardDeviation() {
    return standardDeviation;
  }

  public Double getProp() {
    return prop;
  }

  public void setX(Double xvalue) {
    this.xvalue = xvalue;
  }

  public void setAverage(Double average) {
    this.average = average;
  }

  public void setStandardDeviation(Double standardDeviation) {
    this.standardDeviation = standardDeviation;
  }

  public void setProp(Double prop) {
    this.prop = prop;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Double getLength() {
    return length;
  }

  public void setLength(Double length) {
    this.length = length;
  }

  public Double getOriginalX() {
    return originalX;
  }

  public void setOriginalX(Double originalX) {
    this.originalX = originalX;
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.ArrayList;
import java.util.List;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.YAxisScale;

/**
 * Parameters used to create gene charts.
 */
public class GeneChartParameters {
  public static final boolean DEFAULT_DISPERSION = false;
  public static final ReferenceType DEFAULT_REFERENCE_TYPE = ReferenceType.ANNOTATIONS;
  public static final RepresentationType DEFAULT_REPRESENTATION_TYPE = RepresentationType.ABSOLUTE;
  public static final int DEFAULT_REFERENCE_POINTS = 4;
  public static final int EXON_REFERENCE_POINTS = 6;
  public static final Boundary DEFAULT_BOUNDARY = Boundary.FIVE_PRIME;
  public static final long DEFAULT_WINDOW_SIZE = 50;
  public static final boolean DEFAULT_GENERATE_AGGREGATE_GRAPHS = true;
  public static final boolean DEFAULT_GENERATE_HEATMAP = false;
  public boolean dispersion = DEFAULT_DISPERSION;
  public YAxisScale yaxisScale = new YAxisScale();
  public ReferenceType referenceType = DEFAULT_REFERENCE_TYPE;
  public RepresentationType representationType = DEFAULT_REPRESENTATION_TYPE;
  public int referencePoints = DEFAULT_REFERENCE_POINTS;
  public Boundary boundary = DEFAULT_BOUNDARY;
  public long windowSize = DEFAULT_WINDOW_SIZE;
  public List<Block> blocks = new ArrayList<>();
  public boolean generateAggregateGraphs = DEFAULT_GENERATE_AGGREGATE_GRAPHS;
  public boolean generateHeatmap = DEFAULT_GENERATE_HEATMAP;
  public HeatmapParameters heatmapParameters = new HeatmapParameters();

  public GeneChartParameters() {
  }

  /**
   * Creates {@link GeneChartParameters} by copying the parameters present in
   * {@link AnalysisParameters}.
   *
   * @param parameters
   *          {@link AnalysisParameters}
   */
  @Deprecated
  public GeneChartParameters(AnalysisParameters parameters) {
    dispersion = parameters.geneChartParameters.dispersion;
    yaxisScale = parameters.geneChartParameters.yaxisScale;
    referenceType = parameters.geneChartParameters.referenceType;
    representationType = parameters.geneChartParameters.representationType;
    referencePoints = parameters.geneChartParameters.referencePoints;
    boundary = parameters.geneChartParameters.boundary;
    windowSize = parameters.geneChartParameters.windowSize;
    blocks = parameters.geneChartParameters.blocks;
    generateAggregateGraphs = parameters.geneChartParameters.generateAggregateGraphs;
    generateHeatmap = parameters.geneChartParameters.generateHeatmap;
    heatmapParameters = parameters.geneChartParameters.heatmapParameters;
  }
}

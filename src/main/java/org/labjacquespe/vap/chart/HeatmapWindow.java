/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

/**
 * Heatmap window.
 */
public class HeatmapWindow {
  /**
   * Name of heatmap window.
   */
  private String name;
  /**
   * True if this window is a separator.
   */
  private boolean separator;
  private Double originalX;
  private Double xvalue;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isSeparator() {
    return separator;
  }

  public void setSeparator(boolean separator) {
    this.separator = separator;
  }

  public Double getOriginalX() {
    return originalX;
  }

  public void setOriginalX(Double originalX) {
    this.originalX = originalX;
  }

  public Double getX() {
    return xvalue;
  }

  public void setX(Double xvalue) {
    this.xvalue = xvalue;
  }
}

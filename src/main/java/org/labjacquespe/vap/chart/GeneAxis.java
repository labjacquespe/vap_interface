/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.ValueAxis;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.labjacquespe.vap.message.MessageResources;

/**
 * Gene axis.
 */
public class GeneAxis extends ValueAxis<Number> {
  public static class Separator {
    private DoubleProperty startProperty = new SimpleDoubleProperty();
    private DoubleProperty endProperty = new SimpleDoubleProperty();

    public Separator() {
    }

    public Separator(double start, double end) {
      this.startProperty.set(start);
      this.endProperty.set(end);
    }

    public DoubleProperty startProperty() {
      return startProperty;
    }

    public DoubleProperty endProperty() {
      return endProperty;
    }

    public double getLength() {
      return endProperty.get() - startProperty.get();
    }

    public double getStart() {
      return startProperty.get();
    }

    public void setStart(double start) {
      this.startProperty.set(start);
    }

    public double getEnd() {
      return endProperty.get();
    }

    public void setEnd(double end) {
      this.endProperty.set(end);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      long temp;
      temp = Double.doubleToLongBits(endProperty.get());
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(startProperty.get());
      result = prime * result + (int) (temp ^ (temp >>> 32));
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (!(obj instanceof Separator)) {
        return false;
      }
      Separator other = (Separator) obj;
      if (Double.doubleToLongBits(endProperty.get()) != Double
          .doubleToLongBits(other.endProperty.get())) {
        return false;
      }
      if (Double.doubleToLongBits(startProperty.get()) != Double
          .doubleToLongBits(other.startProperty.get())) {
        return false;
      }
      return true;
    }
  }

  public static class Region {
    private DoubleProperty startProperty = new SimpleDoubleProperty();
    private DoubleProperty endProperty = new SimpleDoubleProperty();
    private BooleanProperty lineProperty = new SimpleBooleanProperty();
    private ObjectProperty<BlockType> typeProperty = new SimpleObjectProperty<>();
    private final boolean reference;

    public Region() {
      this.reference = false;
    }

    public Region(boolean reference) {
      this.reference = reference;
    }

    /**
     * Creates a region from start to end using block type.
     *
     * @param start
     *          start
     * @param end
     *          end
     * @param type
     *          block type
     */
    public Region(double start, double end, BlockType type) {
      this.startProperty.set(start);
      this.endProperty.set(end);
      this.typeProperty.set(type);
      this.reference = false;
    }

    /**
     * Creates a region from start to end using block type and showing reference if true.
     *
     * @param start
     *          start
     * @param end
     *          end
     * @param type
     *          block type
     * @param reference
     *          show reference
     */
    public Region(double start, double end, BlockType type, boolean reference) {
      this.startProperty.set(start);
      this.endProperty.set(end);
      this.typeProperty.set(type);
      this.reference = reference;
    }

    public DoubleProperty startProperty() {
      return startProperty;
    }

    public DoubleProperty endProperty() {
      return endProperty;
    }

    public ObjectProperty<BlockType> typeProperty() {
      return typeProperty;
    }

    public double getLength() {
      return endProperty.get() - startProperty.get();
    }

    public double getStart() {
      return startProperty.get();
    }

    public void setStart(double start) {
      this.startProperty.set(start);
    }

    public double getEnd() {
      return endProperty.get();
    }

    public void setEnd(double end) {
      this.endProperty.set(end);
    }

    public boolean isLine() {
      return lineProperty.get();
    }

    public void setLine(boolean line) {
      lineProperty.set(line);
    }

    public BlockType getType() {
      return typeProperty.get();
    }

    public void setType(BlockType type) {
      typeProperty.set(type);
    }

    public boolean isReference() {
      return reference;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      long temp;
      temp = Double.doubleToLongBits(endProperty.get());
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(startProperty.get());
      result = prime * result + (int) (temp ^ (temp >>> 32));
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (!(obj instanceof Region)) {
        return false;
      }
      Region other = (Region) obj;
      if (Double.doubleToLongBits(endProperty.get()) != Double
          .doubleToLongBits(other.endProperty.get())) {
        return false;
      }
      if (Double.doubleToLongBits(startProperty.get()) != Double
          .doubleToLongBits(other.startProperty.get())) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder("Region");
      builder.append("_");
      builder.append(getStart());
      builder.append("_");
      builder.append(getEnd());
      builder.append("_");
      builder.append(isLine());
      builder.append("_");
      builder.append(getType());
      builder.append("_");
      builder.append(isReference());
      return builder.toString();
    }
  }

  protected static class TranscriptionStartArrow extends Path {
    protected TranscriptionStartArrow() {
      getStyleClass().add("transcription-start-arrow");
      getElements().add(new MoveTo(0.0, 0.0));
      getElements().add(new LineTo(0.0, -17.0));
      getElements().add(new LineTo(20.0, -17.0));
      getElements().add(new LineTo(17.0, -14.0));
      getElements().add(new LineTo(25.0, -17.0));
      getElements().add(new LineTo(17.0, -20.0));
      getElements().add(new LineTo(20.0, -17.0));
      getElements().add(new LineTo(0.0, -17.0));
      getElements().add(new ClosePath());
    }
  }

  protected class RegionBox extends StackPane {
    private final Text text = new Text();
    private final Rectangle rectangle = new Rectangle();
    private final BooleanProperty lineProperty = new SimpleBooleanProperty();
    private final DoubleProperty startProperty = new SimpleDoubleProperty();
    private final DoubleProperty endProperty = new SimpleDoubleProperty();

    protected RegionBox() {
      getStyleClass().add("region-box");
      rectangle.getStyleClass().add("rectangle");
      text.getStyleClass().add("text");
      getChildren().add(rectangle);
      getChildren().add(text);
      lineProperty.addListener((ov, oldValue, newValue) -> {
        if (newValue) {
          rectangle.setHeight(REGION_LINE_HEIGHT);
          rectangle.getStyleClass().add("line");
        } else {
          rectangle.setHeight(REGION_BOX_HEIGHT);
          rectangle.getStyleClass().remove("line");
        }
      });
      rectangle.setHeight(REGION_BOX_HEIGHT);
    }

    public StringProperty textProperty() {
      return text.textProperty();
    }

    public BooleanProperty lineProperty() {
      return lineProperty;
    }

    public DoubleProperty startProperty() {
      return startProperty;
    }

    public DoubleProperty endProperty() {
      return endProperty;
    }

    public String getText() {
      return textProperty().get();
    }

    public void setText(String text) {
      textProperty().set(text);
    }

    public Boolean isLine() {
      return lineProperty.get();
    }

    public void setLine(Boolean line) {
      lineProperty.set(line);
    }

    public Double getStart() {
      return startProperty.get();
    }

    public void setStart(Double start) {
      startProperty.set(start);
    }

    public Double getEnd() {
      return endProperty.get();
    }

    public void setEnd(Double end) {
      endProperty.set(end);
    }

    public boolean isTextVisible() {
      return text.isVisible();
    }

    @Override
    protected void layoutChildren() {
      super.layoutChildren();
      setTranslateX(getDisplayPosition(getStart()));
      double width = computeLength(getEnd() - getStart());
      rectangle.setWidth(width);
      text.setWrappingWidth(width);
      switch (this.getAlignment().getHpos()) {
        case LEFT:
          rectangle.setX(-getPadding().getLeft());
          break;
        case RIGHT:
          rectangle.setX(getPadding().getRight());
          break;
        case CENTER:
          break;
        default:
          break;
      }
      switch (this.getAlignment().getVpos()) {
        case TOP:
          rectangle.setY(-getPadding().getTop());
          break;
        case BOTTOM:
        case BASELINE:
          rectangle.setY(getPadding().getBottom());
          break;
        case CENTER:
          break;
        default:
          break;
      }
    }

    public double getRectangleWidth() {
      return rectangle.getWidth();
    }
  }

  protected class ScaleLegend extends VBox {
    private final Text text = new Text();
    private final Line line = new Line();
    private final StringProperty textProperty = new SimpleStringProperty();
    private final LongProperty lengthProperty = new SimpleLongProperty();

    protected ScaleLegend() {
      getStyleClass().add("scale-legend");
      setFillWidth(false);
      text.getStyleClass().add("text");
      line.getStyleClass().add("line");
      getChildren().add(text);
      getChildren().add(line);
      lengthProperty.addListener((ov, oldValue, newValue) -> {
        if (newValue != null) {
          text.setText(resources.message("scaleLegend.text", newValue));
        }
      });
      text.textProperty().bindBidirectional(textProperty);
    }

    public StringProperty textProperty() {
      return textProperty;
    }

    public String getText() {
      return textProperty.get();
    }

    public void setText(String text) {
      textProperty.set(text);
    }

    public LongProperty lengthProperty() {
      return lengthProperty;
    }

    public Long getLength() {
      return lengthProperty.get();
    }

    public void setLength(Long length) {
      lengthProperty.set(length);
    }

    @Override
    protected void layoutChildren() {
      super.layoutChildren();
      GeneAxis axis = GeneAxis.this;
      double length = computeLength(axis, getLength());
      text.setWrappingWidth(length);
      line.setEndX(length - line.getStrokeWidth());
      setTranslateX(axis.getDisplayPosition(axis.getUpperBound()) - length - getPadding().getLeft()
          - getPadding().getRight() - SCALE_LEGEND_RIGHT_MARGIN);
      if (axis.getUpperBound() - axis.getLowerBound() < getLength()) {
        setVisible(false);
      }
    }

    private double computeLength(ValueAxis<Number> xaxis, double valueLength) {
      return xaxis.getDisplayPosition(xaxis.getLowerBound() + valueLength)
          - xaxis.getDisplayPosition(xaxis.getLowerBound());
    }
  }

  private static final double REGION_LINE_HEIGHT = 2;
  private static final double REGION_BOX_HEIGHT = 20;
  private static final long DEFAULT_SCALE_LENGTH = 500;
  private static final double SCALE_LEGEND_RIGHT_MARGIN = 10;

  private final MessageResources resources;
  private SetProperty<Separator> separatorsProperty = new SimpleSetProperty<>();
  private ListProperty<Region> regionsProperty = new SimpleListProperty<>();
  private TranscriptionStartArrow transcriptionStartArrow;
  private ScaleLegend scaleLegend;

  /**
   * Creates a gene axis.
   */
  public GeneAxis() {
    resources = new MessageResources(getClass(), Locale.getDefault());

    this.getStyleClass().add("gene-axis");
    transcriptionStartArrow = new TranscriptionStartArrow();
    getChildren().add(transcriptionStartArrow);
    scaleLegend = new ScaleLegend();
    getChildren().add(scaleLegend);
    scaleLegend.setLength(DEFAULT_SCALE_LENGTH);
  }

  /**
   * Creates a gene axis.
   *
   * @param lowerBound
   *          axis' lower bound
   * @param upperBound
   *          axis' upper bound
   * @param separators
   *          where to show separators
   * @param regions
   *          axis' regions
   */
  public GeneAxis(double lowerBound, double upperBound, Set<Separator> separators,
      List<Region> regions) {
    this(lowerBound, upperBound, separators);

    this.regionsProperty.set(FXCollections.observableList(regions));
    computeRegionLineProperty();
  }

  /**
   * Creates a gene axis.
   *
   * @param lowerBound
   *          axis' lower bound
   * @param upperBound
   *          axis' upper bound
   * @param separators
   *          where to show separators
   */
  public GeneAxis(double lowerBound, double upperBound, Set<Separator> separators) {
    this();

    setAutoRanging(false);
    setLowerBound(lowerBound);
    setUpperBound(upperBound);
    this.separatorsProperty.set(FXCollections.observableSet(separators));
  }

  protected Region referenceRegion() {
    List<Region> regions = regionsProperty.get();
    if (regions != null && !regions.isEmpty()) {
      // Find based on reference property.
      for (Region region : regions) {
        if (region.isReference()) {
          return region;
        }
      }
      // Find based on location.
      Region reference = regions.get(0);
      for (Region region : regions) {
        if (region.getStart() == 0) {
          // Perfect match!
          reference = region;
          break;
        } else if (Math.abs(reference.getStart()) > Math.abs(region.getStart())) {
          reference = region;
        }
      }
      return reference;
    } else {
      return null;
    }
  }

  protected void computeRegionLineProperty() {
    List<Region> regions = regionsProperty.get();
    if (regions != null && !regions.isEmpty()) {
      Region reference = referenceRegion();
      int referenceIndex = regions.indexOf(reference);
      for (int i = 0; i < regions.size(); i++) {
        Region region = regions.get(i);
        region.setLine(Math.abs(referenceIndex - i) % 2 == 1);
      }
    }
  }

  public SetProperty<Separator> separatorsProperty() {
    return separatorsProperty;
  }

  public ListProperty<Region> regionsProperty() {
    return regionsProperty;
  }

  public LongProperty scaleLegendLengthProperty() {
    return scaleLegend.lengthProperty();
  }

  @Override
  protected List<Number> calculateMinorTickMarks() {
    return new ArrayList<>();
  }

  @Override
  protected void setRange(Object range, boolean animate) {
    double[] rangeProperties = (double[]) range;
    double lowerBound = rangeProperties[0];
    double upperBound = rangeProperties[1];
    double scale = rangeProperties[2];
    final double oldLowerBound = getLowerBound();
    setLowerBound(lowerBound);
    setUpperBound(upperBound);
    setScale(scale);
    if (animate) {
      Timeline timeline = new Timeline();
      timeline.getKeyFrames().addAll(
          new KeyFrame(Duration.ZERO, new KeyValue(currentLowerBound, oldLowerBound)),
          new KeyFrame(Duration.millis(700), new KeyValue(currentLowerBound, lowerBound)));
      timeline.play();
    } else {
      currentLowerBound.set(lowerBound);
    }
  }

  @Override
  protected Object getRange() {
    return new double[] { getLowerBound(), getUpperBound(), getScale() };
  }

  @Override
  protected List<Number> calculateTickValues(double length, Object range) {
    List<Number> ticks = new ArrayList<>();
    if (0 > getLowerBound() && 0 < getUpperBound()) {
      ticks.add(0);
    }
    for (Separator separator : separatorsProperty) {
      ticks.add(separator.getStart());
      ticks.add(separator.getEnd());
    }
    List<Region> regions = regionsProperty.get();
    if (regions != null && !regions.isEmpty()) {
      for (Region region : regions) {
        ticks.add(region.getStart());
        ticks.add(region.getEnd());
      }
    }
    return ticks;
  }

  @Override
  protected String getTickMarkLabel(Number value) {
    return "";
  }

  @Override
  protected void layoutChildren() {
    super.layoutChildren();
    transcriptionStartArrow.setTranslateX(getDisplayPosition(0));
    // Remove old region boxes.
    removeRegionBoxes(getChildren());
    List<RegionBox> regionBoxes = createRegionBoxes();
    if (!regionBoxes.isEmpty()) {
      RegionBox region = regionBoxes.get(0);
      switch (region.getAlignment().getVpos()) {
        case TOP:
          transcriptionStartArrow.setTranslateY(REGION_BOX_HEIGHT / 2);
          break;
        case BOTTOM:
        case BASELINE:
          transcriptionStartArrow.setTranslateY(-REGION_BOX_HEIGHT / 2);
          break;
        case CENTER:
          break;
        default:
          break;
      }
    }
    getChildren().addAll(regionBoxes);
  }

  private void removeRegionBoxes(ObservableList<Node> nodes) {
    Iterator<Node> nodeIter = nodes.iterator();
    while (nodeIter.hasNext()) {
      Node node = nodeIter.next();
      if (node instanceof RegionBox) {
        nodeIter.remove();
      }
    }
  }

  protected List<RegionBox> createRegionBoxes() {
    List<RegionBox> regionBoxes = new ArrayList<>();
    List<Region> regions = regionsProperty.get();
    int exonIndex = 0;
    if (regions != null && !regions.isEmpty()) {
      // Find reference region.
      for (int i = 0; i < regions.size(); i++) {
        Region region = regions.get(i);
        RegionBox box = new RegionBox();
        box.visibleProperty().bind(visibleProperty());
        box.setLine(region.isLine());
        box.setStart(region.getStart());
        box.setEnd(region.getEnd());
        if (region.getType() != null) {
          if (region.getType() == BlockType.EXON) {
            box.setText(resources.message("region." + region.getType() + "." + exonIndex++));
          } else {
            box.setText(resources.message("region." + region.getType()));
          }
        }
        regionBoxes.add(box);
      }
    }
    return regionBoxes;
  }

  private double computeLength(double valueLength) {
    return getDisplayPosition(getLowerBound() + valueLength) - getDisplayPosition(getLowerBound());
  }

  protected ScaleLegend getScaleLegend() {
    return scaleLegend;
  }

  public Set<Separator> getSeparators() {
    return separatorsProperty.get();
  }

  public void setSeparators(Set<Separator> separators) {
    separatorsProperty.set(FXCollections.observableSet(separators));
  }

  public List<Region> getRegions() {
    return regionsProperty.get();
  }

  public void setRegions(List<Region> regions) {
    regionsProperty.set(FXCollections.observableList(regions));
    computeRegionLineProperty();
  }

  public Long getScaleLegendLength() {
    return scaleLegend.lengthProperty().get();
  }

  public void setScaleLegendLength(Long length) {
    scaleLegend.lengthProperty().set(length);
  }
}

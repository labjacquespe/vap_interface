/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javafx.scene.paint.Color;

/**
 * Heatmap colors.
 */
public enum HeatmapColors {
  RAINBOW, WHITE_RED, WHITE_GREEN, WHITE_BLUE, WHITE_PURPLE, BLACK_YELLOW, RED_YELLOW,
  BLUE_WHITE_RED, BLUE_BLACK_RED, BLUE_BLACK_YELLOW, GREEN_BLACK_RED, YELLOW_GREEN, RED_PURPLE_BLUE;

  /**
   * Returns colors associated with this enum.
   *
   * @return colors
   */
  public List<Color> getColors() {
    Properties properties = new Properties();
    try (InputStream input = getClass().getResourceAsStream(
        "/" + HeatmapColors.class.getName().replaceAll("\\.", "/") + ".properties")) {
      properties.load(input);
    } catch (IOException e) {
      throw new IllegalStateException("Could not load color values", e);
    }
    List<Color> colors = new ArrayList<>();
    String colorProperty = properties.getProperty(name());
    if (!colorProperty.isEmpty()) {
      String[] values = properties.getProperty(name()).split(",");
      for (String value : values) {
        colors.add(Color.valueOf(value));
      }
    }
    return colors;
  }
}

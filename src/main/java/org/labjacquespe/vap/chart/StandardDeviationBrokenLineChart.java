/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.Iterator;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 * {@link LineChart} that breaks line on invalid values.
 */
public class StandardDeviationBrokenLineChart<X> extends StandardDeviationLineChart<X> {
  public static interface Extra {
    public Double getStandardDeviation();

    public boolean isBroken();
  }

  public StandardDeviationBrokenLineChart(Axis<X> xaxis, Axis<Number> yaxis) {
    super(xaxis, yaxis);
  }

  public StandardDeviationBrokenLineChart(Axis<X> xaxis, Axis<Number> yaxis,
      ObservableList<javafx.scene.chart.XYChart.Series<X, Number>> data) {
    this(xaxis, yaxis);
    this.setData(data);
  }

  @Override
  protected void layoutPlotChildren() {
    super.layoutPlotChildren();
    for (int seriesIndex = 0; seriesIndex < getData().size(); seriesIndex++) {
      Series<X, Number> series = getData().get(seriesIndex);
      Path serieLine = (Path) series.getNode();
      serieLine.getElements().clear();
      Iterator<Data<X, Number>> iter = getDisplayedDataIterator(series);
      boolean first = true;
      while (iter.hasNext()) {
        Data<X, Number> data = iter.next();
        double xvalue = getXAxis().getDisplayPosition(getCurrentDisplayedXValue(data));
        double yvalue = getYAxis().getDisplayPosition(getCurrentDisplayedYValue(data));
        boolean broken = isBroken(data);
        Node itemNode = data.getNode();
        if (itemNode != null) {
          itemNode.setVisible(true);
        }
        if (broken || first) {
          serieLine.getElements().add(new MoveTo(xvalue, yvalue));
          if (itemNode != null) {
            itemNode.setVisible(false);
          }
        } else {
          serieLine.getElements().add(new LineTo(xvalue, yvalue));
        }
        first = false;
      }
    }
  }

  @Override
  protected Double getStandardDeviation(Data<X, Number> data) {
    if (data.getExtraValue() instanceof Extra) {
      Extra extra = (Extra) data.getExtraValue();
      return extra.getStandardDeviation();
    }
    return super.getStandardDeviation(data);
  }

  protected boolean isBroken(Data<X, Number> data) {
    if (data.getExtraValue() instanceof Extra) {
      Extra extra = (Extra) data.getExtraValue();
      return extra.isBroken();
    } else if (data.getExtraValue() instanceof Boolean) {
      return (Boolean) data.getExtraValue();
    } else {
      return false;
    }
  }
}

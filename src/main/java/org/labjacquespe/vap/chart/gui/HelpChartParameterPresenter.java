/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Help parameter presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HelpChartParameterPresenter {
  private static final String DOCUMENT =
      HelpChartParameterPresenter.class.getResource("/createGraphsDescription.html").toString();

  private StringProperty hashtagProperty = new SimpleStringProperty();
  @FXML
  private Pane view;
  @FXML
  private WebView webView;
  private ChangeListener<Object> hashtagSetter = (ov, oldValue, newValue) -> {
    State state = getState();
    if (state == State.SUCCEEDED) {
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
          String hashtag = hashtagProperty.get();
          if (hashtag != null && !hashtag.equals("")) {
            webView.getEngine().executeScript("window.location.hash = '" + hashtag + "'");
          } else {
            webView.getEngine().executeScript("window.location.hash = ''");
          }
        }
      });
    }
  };

  @FXML
  private void initialize() {
    webView.getEngine().getLoadWorker().stateProperty().addListener(hashtagSetter);

    hashtagProperty.addListener(hashtagSetter);

    webView.getEngine().load(DOCUMENT);
  }

  public StringProperty hashtagProperty() {
    return hashtagProperty;
  }

  public String getHashtag() {
    return hashtagProperty.get();
  }

  public void setHashtag(String hashtag) {
    hashtagProperty.set(hashtag);
  }

  private State getState() {
    return webView.getEngine().getLoadWorker().getState();
  }
}

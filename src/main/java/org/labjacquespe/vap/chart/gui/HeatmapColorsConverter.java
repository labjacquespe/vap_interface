/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javafx.util.StringConverter;
import org.labjacquespe.vap.chart.HeatmapColors;
import org.labjacquespe.vap.message.MessageResources;

/**
 * {@link StringConverter} for {@link HeatmapColors}.
 */
public class HeatmapColorsConverter extends StringConverter<HeatmapColors> {
  private final Map<HeatmapColors, String> colorsToString;
  private final Map<String, HeatmapColors> stringToColors;

  /**
   * Creates {@link HeatmapColorsConverter}.
   */
  public HeatmapColorsConverter() {
    MessageResources resources = new MessageResources(getClass(), Locale.getDefault());
    colorsToString = new HashMap<>();
    stringToColors = new HashMap<>();
    for (HeatmapColors colors : HeatmapColors.values()) {
      String string = resources.message(colors.name());
      colorsToString.put(colors, string);
      stringToColors.put(string, colors);
    }
  }

  @Override
  public HeatmapColors fromString(String input) {
    return stringToColors.get(input);
  }

  @Override
  public String toString(HeatmapColors colors) {
    return colors != null ? colorsToString.get(colors) : "";
  }
}

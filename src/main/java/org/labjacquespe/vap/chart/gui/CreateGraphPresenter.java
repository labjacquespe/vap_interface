/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import static org.labjacquespe.vap.chart.ChartParameters.CREATE_GRAPH_FROM_PARAM_FILE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.labjacquespe.vap.chart.CreateGraphFiles;
import org.labjacquespe.vap.chart.CreateGraphFilesType;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.gui.output.GenerateAggregateGraphsPresenter;
import org.labjacquespe.vap.core.gui.output.GenerateAggregateGraphsView;
import org.labjacquespe.vap.core.gui.output.OutputHeatmapPresenter;
import org.labjacquespe.vap.core.gui.output.OutputHeatmapView;
import org.labjacquespe.vap.core.gui.output.YAxisScalePresenter;
import org.labjacquespe.vap.core.gui.output.YAxisScaleView;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.gui.FileConverter;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.gui.NullOnExceptionConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileOverHandler;
import org.labjacquespe.vap.javafx.message.MessageDialog;
import org.labjacquespe.vap.javafx.message.MessageDialog.MessageDialogType;
import org.labjacquespe.vap.javafx.progress.ProgressDialog;
import org.labjacquespe.vap.validation.JavafxValidationErrorAccumulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Create graph presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CreateGraphPresenter extends BasePresenter {
  private static final Logger logger = LoggerFactory.getLogger(CreateGraphPresenter.class);
  private final ObjectProperty<CreateGraphFilesType> createGraphTypeProperty =
      new SimpleObjectProperty<>();
  private final ObjectProperty<File> parametersProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup createGraphTypeGroup;
  @FXML
  private Toggle fromParameters;
  @FXML
  private Toggle fromListFiles;
  @FXML
  private Pane elements;
  @FXML
  private Pane parametersPane;
  @FXML
  private TextField parameters;
  @Autowired
  private ParameterFileService textParameterService;
  @Autowired
  private CreateGraphTaskFactory createGraphTaskFactory;
  private GenerateAggregateGraphsPresenter generateAggregateGraphsPresenter;
  private GenerateAggregateGraphsView generateAggregateGraphsView;
  private OutputHeatmapPresenter outputHeatmapPresenter;
  private OutputHeatmapView outputHeatmapView;
  private YAxisScalePresenter yaxisScalePresenter;
  private YAxisScaleView yaxisScaleView;
  private final FileChooser chooser = new FileChooser();
  private ChangeListener<CreateGraphFilesType> createGraphFilesTypeListener =
      (ov, oldValue, newValue) -> {
        switch (newValue) {
          case PARAMETER_FILE:
            fromParameters.setSelected(true);
            break;
          case LIST_FILES:
            fromListFiles.setSelected(true);
            break;
          default:
            break;
        }
        updateChildren();
      };
  private ChangeListener<Toggle> createGraphFilesTypeToggleListener = (ov, oldValue, newValue) -> {
    if (newValue != null) {
      createGraphTypeProperty.set((CreateGraphFilesType) newValue.getUserData());
    } else {
      createGraphTypeGroup.selectToggle(oldValue);
    }
  };
  private ChangeListener<File> parameterListener = (ov, oldValue, newValue) -> {
    if (newValue != null && newValue.exists()) {
      try {
        AnalysisParameters analysisParameters = textParameterService.parse(newValue);
        generateAggregateGraphsPresenter.setGenerateAggregateGraphs(
            analysisParameters.geneChartParameters.generateAggregateGraphs);
        generateAggregateGraphsPresenter
            .setDispersion(analysisParameters.geneChartParameters.dispersion);
        outputHeatmapPresenter
            .setOutputHeatmap(analysisParameters.geneChartParameters.generateHeatmap);
        yaxisScalePresenter.setYAxisScale(analysisParameters.geneChartParameters.yaxisScale);
      } catch (IOException e) {
        // Ignore errors.
      }
    }
  };

  @FXML
  private void initialize() {
    generateAggregateGraphsView = new GenerateAggregateGraphsView();
    generateAggregateGraphsPresenter =
        (GenerateAggregateGraphsPresenter) generateAggregateGraphsView.getPresenter();
    outputHeatmapView = new OutputHeatmapView();
    outputHeatmapPresenter = (OutputHeatmapPresenter) outputHeatmapView.getPresenter();
    yaxisScaleView = new YAxisScaleView();
    yaxisScalePresenter = (YAxisScalePresenter) yaxisScaleView.getPresenter();
    yaxisScalePresenter.setTab(GuiTabs.CREATE_GRAPHS);

    createGraphTypeProperty.addListener(createGraphFilesTypeListener);
    createGraphTypeGroup.selectedToggleProperty().addListener(createGraphFilesTypeToggleListener);
    generateAggregateGraphsPresenter.setTab(GuiTabs.CREATE_GRAPHS);
    generateAggregateGraphsPresenter.createGraphFilesTypeProperty().bind(createGraphTypeProperty);
    outputHeatmapPresenter.setTab(GuiTabs.CREATE_GRAPHS);
    outputHeatmapPresenter.createGraphFilesTypeProperty().bind(createGraphTypeProperty);
    fromParameters.setUserData(CreateGraphFilesType.PARAMETER_FILE);
    fromListFiles.setUserData(CreateGraphFilesType.LIST_FILES);
    parameters.textProperty().bindBidirectional(parametersProperty,
        new NullOnExceptionConverter<>(new FileConverter()));
    parametersProperty.addListener(parameterListener);
    moveCaretToEndOnLostFocus(parameters);

    // Enable drag and drop.
    parameters.setOnDragDetected(new DragFileDetectedHandler(parameters));
    parameters.setOnDragDone(new DragFileDoneHandler(parameters));
    parametersPane.setOnDragOver(new DragFileOverHandler(parametersPane, parameters));
    parametersPane.setOnDragExited(new DragExitedHandler(parametersPane));
    parametersPane.setOnDragDropped(new DragFileDroppedHandler(parameters));

    // Default values.
    createGraphTypeProperty.set(CreateGraphFilesType.LIST_FILES);
  }

  public ObjectProperty<File> parametersProperty() {
    return parametersProperty;
  }

  public ObjectProperty<File> initialDirectoryProperty() {
    return chooser.initialDirectoryProperty();
  }

  public BooleanProperty outputHeatmapProperty() {
    return outputHeatmapPresenter.outputHeatmapProperty();
  }

  public BooleanProperty dispersionProperty() {
    return generateAggregateGraphsPresenter.dispersionProperty();
  }

  public ObjectProperty<Double> fromProperty() {
    return yaxisScalePresenter.fromProperty();
  }

  public ObjectProperty<Double> toProperty() {
    return yaxisScalePresenter.toProperty();
  }

  /**
   * Returns gene chart parameters.
   *
   * @return gene chart parameters
   * @throws IOException
   *           could not parse parameter file
   */
  public GeneChartParameters getGeneChartParameters() throws IOException {
    AnalysisParameters analysisParameters = null;
    if (createGraphTypeProperty.get() == CreateGraphFilesType.PARAMETER_FILE) {
      analysisParameters = textParameterService.parse(parametersProperty.get());
    } else {
      analysisParameters = new AnalysisParameters();
    }
    GeneChartParameters parameters = analysisParameters.geneChartParameters;
    parameters.dispersion = generateAggregateGraphsPresenter.isDispersion();
    parameters.yaxisScale = yaxisScalePresenter.getYAxisScale();
    parameters.generateAggregateGraphs =
        generateAggregateGraphsPresenter.isGenerateAggregateGraphs();
    parameters.generateHeatmap = outputHeatmapPresenter.isOutputHeatmap();
    parameters.heatmapParameters = outputHeatmapPresenter.getHeatmapParameters();
    return parameters;
  }

  public File getParameters() {
    return parametersProperty.get();
  }

  /**
   * Sets parameter file.
   *
   * @param file
   *          parameter file
   */
  public void setParameters(File file) {
    if (file != null) {
      parametersProperty.set(file);
      parameters.positionCaret(parameters.getText().length());
    } else {
      parametersProperty.set(null);
    }
  }

  private void updateChildren() {
    List<Node> children = elements.getChildren();
    children.clear();
    switch (createGraphTypeProperty.get()) {
      case PARAMETER_FILE:
        children.add(parametersPane);
        children.add(generateAggregateGraphsView.getView());
        children.add(outputHeatmapView.getView());
        children.add(yaxisScaleView.getView());
        break;
      case LIST_FILES:
        children.add(generateAggregateGraphsView.getView());
        children.add(outputHeatmapView.getView());
        children.add(yaxisScaleView.getView());
        break;
      default:
        break;
    }
  }

  @FXML
  private void run() {
    JavafxValidationErrorAccumulator errorHandler = new JavafxValidationErrorAccumulator();
    try {
      validate(errorHandler);
    } catch (IOException e) {
      String error = message("createGraphs.load.IOException.message", getParameters().toString());
      new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
          message("createGraphs.load.IOException.title"), error);
    }
    if (!errorHandler.errors().isEmpty()) {
      // TODO Add tooltip.
      new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
          message("validationError.title"),
          errorHandler.errors().stream().map(error -> error.error).collect(Collectors.toList()));
    } else {
      final Window window = view.getScene().getWindow();
      try {
        CreateGraphFiles files = new CreateGraphFiles();
        files.type(createGraphTypeProperty.get());
        files.parameterFile(getParameters());
        files.chartMapFile(generateAggregateGraphsPresenter.getMapGraph());
        files.listHeatmapFile(outputHeatmapPresenter.getListHeatmap());
        GeneChartParameters parameters = getGeneChartParameters();
        final CreateGraphTask task = createGraphTaskFactory.create(files, parameters);
        final ProgressDialog progressDialog = new ProgressDialog(view.getScene().getWindow(), task);
        task.stateProperty().addListener((ov, oldValue, newValue) -> {
          if (newValue == State.FAILED || newValue == State.SUCCEEDED
              || newValue == State.CANCELLED) {
            progressDialog.hide();
          }
          if (newValue == State.FAILED) {
            // Show error message.
            Throwable error = task.getException();
            logger.error("Could not create graphs", error);
            new MessageDialog(window, MessageDialogType.ERROR, message("createGraphs.failed.title"),
                message("createGraphs.failed.message"), error.getMessage());
          } else if (newValue == State.SUCCEEDED) {
            // Show confirm message.
            new MessageDialog(window, MessageDialogType.INFORMATION,
                message("createGraphs.succeeded.title"), message("createGraphs.succeeded.message"));
          }
        });
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
      } catch (IOException e) {
        String error = message("createGraphs.load.IOException.message", getParameters().toString());
        new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
            message("createGraphs.load.IOException.title"), error);
      }
    }
  }

  @FXML
  private void parametersBrowse(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(chooser);
    File file = chooser.showOpenDialog(view.getScene().getWindow());
    if (file != null) {
      chooser.setInitialDirectory(file.getParentFile());
      setParameters(file);
    }
  }

  @FXML
  private void parametersClear(ActionEvent event) {
    setParameters(null);
  }

  @FXML
  private void parametersHelp(ActionEvent event) {
    HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
    helpPopup.setHashtag(CREATE_GRAPH_FROM_PARAM_FILE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates gene chart parameters.
   *
   * @param errorHandler
   *          handles validation errors
   * @throws IOException
   *           could not parse parameters file
   */
  public void validate(BiConsumer<String, String> errorHandler) throws IOException {
    parametersPane.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      parametersPane.getStyleClass().add("error");
    }));
    if (!generateAggregateGraphsPresenter.isGenerateAggregateGraphs()
        && !outputHeatmapPresenter.isOutputHeatmap()) {
      errorHandler.accept(message("error.outputRequired"), null);
    }
    if (createGraphTypeProperty.get() == CreateGraphFilesType.PARAMETER_FILE) {
      File parameters = parametersProperty.get();
      if (parameters == null) {
        errorHandler.accept(message("error.parametersRequired"), null);
      } else if (!parameters.isFile()) {
        errorHandler.accept(message("error.parametersNotExists", parameters.getName()),
            parameters.toString());
      } else {
        final BiConsumer<String, String> finalErrorHandler = errorHandler;
        textParameterService.validate(parameters, Locale.getDefault(),
            error -> finalErrorHandler.accept(error, null));
      }
    }
    generateAggregateGraphsPresenter.validate(errorHandler);
    outputHeatmapPresenter.validate(errorHandler);
    yaxisScalePresenter.validate(errorHandler);
  }
}

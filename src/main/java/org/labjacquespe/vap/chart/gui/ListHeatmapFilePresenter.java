/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import static org.labjacquespe.vap.chart.ChartParameters.CREATE_GRAPH_FROM_LIST_IND_FILE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;
import static org.labjacquespe.vap.gui.SelectAllListener.selectAllListener;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileOverHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * List heatmap file presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ListHeatmapFilePresenter extends BasePresenter {
  private final BooleanProperty outputHeatmapProperty = new SimpleBooleanProperty();
  @FXML
  private Pane view;
  @FXML
  private TextField listHeatmap;
  @Autowired
  private AnalysisChartService analysisChartService;
  private final FileChooser chooser = new FileChooser();

  @FXML
  private void initialize() {
    moveCaretToEndOnLostFocus(listHeatmap);
    listHeatmap.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());

    // Enable drag and drop.
    listHeatmap.setOnDragDetected(new DragFileDetectedHandler(listHeatmap));
    listHeatmap.setOnDragDone(new DragFileDoneHandler(listHeatmap));
    view.setOnDragOver(new DragFileOverHandler(view, listHeatmap));
    view.setOnDragExited(new DragExitedHandler(view));
    view.setOnDragDropped(new DragFileDroppedHandler(listHeatmap));

    view.disableProperty().bind(outputHeatmapProperty.not());
  }

  public BooleanProperty outputHeatmapProperty() {
    return outputHeatmapProperty;
  }

  /**
   * Returns list heatmap file.
   *
   * @return list heatmap file
   */
  public File getListHeatmap() {
    if (listHeatmap.getText() != null && !listHeatmap.getText().isEmpty()) {
      return new File(listHeatmap.getText());
    } else {
      return null;
    }
  }

  /**
   * Sets list heatmap file.
   *
   * @param file
   *          list heatmap file
   */
  public void setListHeatmap(File file) {
    if (file != null) {
      this.listHeatmap.setText(file.toString());
      this.listHeatmap.positionCaret(this.listHeatmap.getText().length());
    } else {
      this.listHeatmap.setText(null);
    }
  }

  @FXML
  private void browse(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(chooser);
    File file = chooser.showOpenDialog(view.getScene().getWindow());
    if (file != null) {
      chooser.setInitialDirectory(file.getParentFile());
      setListHeatmap(file);
    }
  }

  @FXML
  private void clear(ActionEvent event) {
    setListHeatmap(null);
  }

  @FXML
  private void help(ActionEvent event) {
    HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
    helpPopup.setHashtag(CREATE_GRAPH_FROM_LIST_IND_FILE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates list heatmap file.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (outputHeatmapProperty.get()) {
      if (listHeatmap.getText() == null || listHeatmap.getText().isEmpty()) {
        errorHandler.accept(message("error.listHeatmapRequired"), null);
      } else {
        File listHeatmap = getListHeatmap();
        if (!listHeatmap.isFile()) {
          errorHandler.accept(message("error.listHeatmapNotExists", listHeatmap.getName()),
              listHeatmap.getPath());
        } else {
          try {
            final BiConsumer<String, String> finalErrorHandler = errorHandler;
            analysisChartService.validateHeatmapList(listHeatmap, Locale.getDefault(),
                error -> finalErrorHandler.accept(error, null));
          } catch (IOException e) {
            errorHandler.accept(message("error.listHeatmap.IOException", listHeatmap.getName()),
                listHeatmap.getPath());
          }
        }
      }
    }
  }
}

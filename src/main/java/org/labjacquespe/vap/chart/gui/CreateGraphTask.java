/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import java.io.File;
import javafx.concurrent.Task;
import org.labjacquespe.vap.chart.CreateGraphFiles;
import org.labjacquespe.vap.chart.CreateGraphFilesType;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.progressbar.JavafxProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Create graphs based on analysis results.
 */
public class CreateGraphTask extends Task<Void> {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(CreateGraphTask.class);

  private final AnalysisChartService analysisChartService;
  private final ParameterFileService parameterFileService;
  private final CreateGraphFiles files;
  private final GeneChartParameters parameters;

  protected CreateGraphTask(AnalysisChartService analysisChartService,
      ParameterFileService parameterFileService, CreateGraphFiles files,
      GeneChartParameters parameters) {
    this.analysisChartService = analysisChartService;
    this.parameterFileService = parameterFileService;
    this.files = files;
    this.parameters = parameters;
  }

  private File validFile() {
    switch (files.getType()) {
      case PARAMETER_FILE:
        return files.getParameterFile();
      case LIST_FILES:
        if (files.getChartMapFile() != null) {
          return files.getChartMapFile();
        } else {
          return files.getListHeatmapFile();
        }
      default:
        throw new AssertionError(CreateGraphFilesType.class.getSimpleName() + " " + files.getType()
            + " not covered in switch");
    }
  }

  @Override
  protected Void call() throws Exception {
    File outputFolder = validFile().getParentFile();
    File chartMapFile = files.getChartMapFile();
    File listHeatmapFile = files.getListHeatmapFile();
    if (files.getType() == CreateGraphFilesType.PARAMETER_FILE) {
      AnalysisParameters analysisParameters = parameterFileService.parse(files.getParameterFile());
      String prefix = analysisParameters.ouputFilePrefix;
      outputFolder = analysisParameters.outputFolder;
      chartMapFile = analysisChartService.chartMap(outputFolder, prefix);
      listHeatmapFile = analysisChartService.heatmapList(outputFolder, prefix);
    }
    addLogAppender(outputFolder);
    JavafxProgressBar progressBar = new JavafxProgressBar();
    progressBar.title().addListener((ov, oldValue, newValue) -> updateTitle(newValue));
    progressBar.message().addListener((ov, oldValue, newValue) -> updateMessage(newValue));
    progressBar.progress().addListener((ov, oldValue,
        newValue) -> updateProgress(newValue.doubleValue(), Math.max(1.0, newValue.doubleValue())));
    try {
      analysisChartService.createCharts(chartMapFile, listHeatmapFile, parameters, progressBar);
      return null;
    } finally {
      removeLogAppender();
    }
  }

  private void addLogAppender(File outputFolder) {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
    FileAppender<ILoggingEvent> sourceAppender =
        (FileAppender<ILoggingEvent>) rootLogger.getAppender("FILE");
    PatternLayoutEncoder sourceEncoder = (PatternLayoutEncoder) sourceAppender.getEncoder();
    PatternLayoutEncoder encoder = new PatternLayoutEncoder();
    encoder.setPattern(sourceEncoder.getPattern());
    encoder.setContext(context);
    encoder.start();
    FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
    fileAppender.setName(appenderName());
    fileAppender.setFile(outputFolder.getPath() + "/VAP_interface_logfile.log");
    fileAppender.setEncoder(encoder);
    fileAppender.setContext(context);
    fileAppender.start();
    rootLogger.addAppender(fileAppender);
  }

  private void removeLogAppender() {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
    rootLogger.detachAppender(appenderName());
  }

  private String appenderName() {
    return getClass().getName() + "-" + Thread.currentThread().getName();
  }
}

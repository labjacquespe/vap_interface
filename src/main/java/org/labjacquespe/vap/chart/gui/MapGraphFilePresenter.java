/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import static org.labjacquespe.vap.chart.ChartParameters.CREATE_GRAPH_FROM_LIST_AGG_FILE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;
import static org.labjacquespe.vap.gui.SelectAllListener.selectAllListener;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileOverHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Map graph file presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MapGraphFilePresenter extends BasePresenter {
  private final BooleanProperty generateAggregateGraphsProperty = new SimpleBooleanProperty();
  @FXML
  private Pane view;
  @FXML
  private TextField mapGraph;
  @Autowired
  private AnalysisChartService analysisChartService;
  private final FileChooser chooser = new FileChooser();

  @FXML
  private void initialize() {
    moveCaretToEndOnLostFocus(mapGraph);
    mapGraph.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());

    // Enable drag and drop.
    mapGraph.setOnDragDetected(new DragFileDetectedHandler(mapGraph));
    mapGraph.setOnDragDone(new DragFileDoneHandler(mapGraph));
    view.setOnDragOver(new DragFileOverHandler(view, mapGraph));
    view.setOnDragExited(new DragExitedHandler(view));
    view.setOnDragDropped(new DragFileDroppedHandler(mapGraph));

    view.disableProperty().bind(generateAggregateGraphsProperty.not());
  }

  public BooleanProperty generateAggregateGraphsProperty() {
    return generateAggregateGraphsProperty;
  }

  /**
   * Returns map graph file.
   *
   * @return map graph file
   */
  public File getMapGraph() {
    if (mapGraph.getText() != null && !mapGraph.getText().isEmpty()) {
      return new File(mapGraph.getText());
    } else {
      return null;
    }
  }

  /**
   * Sets map graph file.
   *
   * @param file
   *          map graph file
   */
  public void setMapGraph(File file) {
    if (file != null) {
      this.mapGraph.setText(file.toString());
      this.mapGraph.positionCaret(this.mapGraph.getText().length());
    } else {
      this.mapGraph.setText(null);
    }
  }

  @FXML
  private void browse(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(chooser);
    File file = chooser.showOpenDialog(view.getScene().getWindow());
    if (file != null) {
      chooser.setInitialDirectory(file.getParentFile());
      setMapGraph(file);
    }
  }

  @FXML
  private void clear(ActionEvent event) {
    setMapGraph(null);
  }

  @FXML
  private void help(ActionEvent event) {
    HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
    helpPopup.setHashtag(CREATE_GRAPH_FROM_LIST_AGG_FILE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates map graph.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (generateAggregateGraphsProperty.get()) {
      if (mapGraph.getText() == null || mapGraph.getText().isEmpty()) {
        errorHandler.accept(message("error.mapGraphRequired"), null);
      } else {
        File mapGraph = getMapGraph();
        if (!mapGraph.isFile()) {
          errorHandler.accept(message("error.mapGraphNotExists", mapGraph.getName()),
              mapGraph.getPath());
        } else {
          try {
            final BiConsumer<String, String> finalErrorHandler = errorHandler;
            analysisChartService.validateHeatmapList(mapGraph, Locale.getDefault(),
                error -> finalErrorHandler.accept(error, null));
          } catch (IOException e) {
            errorHandler.accept(message("error.mapGraph.IOException", mapGraph.getName()),
                mapGraph.getPath());
          }
        }
      }
    }
  }
}

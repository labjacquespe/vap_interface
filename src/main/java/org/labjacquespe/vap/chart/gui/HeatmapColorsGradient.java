/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import org.labjacquespe.vap.chart.HeatmapColors;

/**
 * Gradients for {@link HeatmapColors}.
 */
public class HeatmapColorsGradient extends BorderPane {
  private final ObjectProperty<HeatmapColors> colorsProperty = new SimpleObjectProperty<>();
  private Rectangle colorGradient = new Rectangle();

  /**
   * Creates a {@link HeatmapColorsGradient}.
   */
  public HeatmapColorsGradient() {
    getChildren().add(colorGradient);
    colorsProperty.addListener((ov, oldValue, newValue) -> updateGradient());
  }

  public HeatmapColorsGradient(HeatmapColors colors) {
    this();
    colorsProperty.set(colors);
  }

  private void updateGradient() {
    List<Color> colors =
        colorsProperty.get() != null ? colorsProperty.get().getColors() : new ArrayList<>();
    double colorStopStep = 1.0 / (colors.size() - 1);
    Stop[] stops = new Stop[colors.size()];
    for (int i = 0; i < colors.size(); i++) {
      stops[i] = new Stop(i * colorStopStep, colors.get(i));
    }
    LinearGradient gradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
    colorGradient.setFill(gradient);
  }

  @Override
  protected void layoutChildren() {
    super.layoutChildren();
    colorGradient.setHeight(getHeight());
    colorGradient.setWidth(getWidth());
  }
}

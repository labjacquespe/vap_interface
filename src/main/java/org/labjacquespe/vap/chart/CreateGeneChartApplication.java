/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Locale;
import javafx.application.Platform;
import javafx.concurrent.Worker.State;
import javafx.stage.Stage;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.labjacquespe.vap.AbstractSpringBootJavafxApplication;
import org.labjacquespe.vap.CommandLineParameterService;
import org.labjacquespe.vap.CommandLineParameterService.ParsedGeneChartArguments;
import org.labjacquespe.vap.chart.gui.CreateGraphTask;
import org.labjacquespe.vap.chart.gui.CreateGraphTaskFactory;
import org.labjacquespe.vap.javafx.SpringAfterburnerInstanceSupplier;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.validation.ConsumerMarker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application that creates gene charts.
 */
public class CreateGeneChartApplication extends AbstractSpringBootJavafxApplication {
  private static final Logger logger = LoggerFactory.getLogger(CreateGeneChartApplication.class);
  @Inject
  private CommandLineParameterService commandLineParameterService;
  @Inject
  private CreateGraphTaskFactory createGraphTaskFactory;
  private final MessageResources resources;
  private boolean printedHeader;

  public CreateGeneChartApplication() {
    resources = new MessageResources(getClass(), Locale.getDefault());
  }

  @Override
  public void init() throws Exception {
    super.init();
    com.airhacks.afterburner.injection.Injector
        .setInstanceSupplier(new SpringAfterburnerInstanceSupplier(applicationContext));
  }

  @Override
  public void start(final Stage stage) throws Exception {
    try {
      ConsumerMarker<String> errorHandler = errorHandler(resources);
      commandLineParameterService.validateGeneChartArguments(
          getParameters().getRaw().toArray(new String[] {}), Locale.getDefault(), errorHandler);
      if (errorHandler.isConsumed()) {
        showHelp();
        Platform.exit();
      } else {
        ParsedGeneChartArguments parsedArguments = commandLineParameterService
            .parseGeneChartArguments(getParameters().getRaw().toArray(new String[] {}));
        final CreateGraphTask task = createGraphTaskFactory.create(parsedArguments.getFiles(),
            parsedArguments.getParameters());
        task.messageProperty().addListener((ov, oldValue, newValue) -> System.out
            .println(MessageFormat.format(resources.message("message"), newValue)));
        task.stateProperty().addListener((ov, oldValue, newValue) -> {
          if (newValue == State.CANCELLED) {
            System.err.println(resources.message("cancelled"));
            Platform.exit();
          } else if (newValue == State.FAILED) {
            // Show error message.
            Throwable error = task.getException();
            logger.error("Could not create graphs", error);
            System.err.println(resources.message("fail"));
            error.printStackTrace();
            Platform.exit();
          } else if (newValue == State.SUCCEEDED) {
            // Show confirm message.
            System.out.println(resources.message("success"));
            Platform.exit();
          }
        });
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        Platform.setImplicitExit(false);
        thread.start();
      }
    } catch (IOException e) {
      printErrorTitle(resources);
      e.printStackTrace();
    }
  }

  private void showHelp() {
    try {
      try (Reader reader = new BufferedReader(
          new InputStreamReader(getClass().getResourceAsStream("/commandLineHelp.txt"), UTF_8))) {
        IOUtils.copy(reader, System.out, Charset.defaultCharset());
      }
    } catch (IOException e) {
      System.err.print("Error: could not open help file");
    }
  }

  private ConsumerMarker<String> errorHandler(MessageResources bundle) {
    return new ConsumerMarker<>(error -> {
      if (!printedHeader) {
        printedHeader = true;
        printErrorTitle(bundle);
      }

      System.err.println(error);
    });
  }

  private void printErrorTitle(MessageResources bundle) {
    System.err.println(resources.message("error.title"));
    System.err.println();
  }
}

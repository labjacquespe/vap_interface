/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.io.File;

/**
 * Files that can be used to create graphs.
 */
public class CreateGraphFiles {
  private CreateGraphFilesType type;
  private File parameterFile;
  private File chartMapFile;
  private File listHeatmapFile;

  public CreateGraphFilesType getType() {
    return type;
  }

  public CreateGraphFiles type(CreateGraphFilesType type) {
    this.type = type;
    return this;
  }

  public File getParameterFile() {
    return parameterFile;
  }

  public CreateGraphFiles parameterFile(File parameterFile) {
    this.parameterFile = parameterFile;
    return this;
  }

  public File getChartMapFile() {
    return chartMapFile;
  }

  public CreateGraphFiles chartMapFile(File chartMapFile) {
    this.chartMapFile = chartMapFile;
    return this;
  }

  public File getListHeatmapFile() {
    return listHeatmapFile;
  }

  public CreateGraphFiles listHeatmapFile(File listHeatmapFile) {
    this.listHeatmapFile = listHeatmapFile;
    return this;
  }
}

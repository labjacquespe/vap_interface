/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

/**
 * Heatmap parameters.
 */
public class HeatmapParameters {
  public static final HeatmapColors DEFAULT_COLORS = HeatmapColors.RAINBOW;
  public static final int DEFAULT_WIDTH = 130;
  public static final int MINIMAL_WIDTH = 40;
  public static final int DEFAULT_HEIGHT = 70;
  public static final int MINIMAL_HEIGHT = 40;
  public static final boolean DEFAULT_AXIS_VISIBLE = true;
  public HeatmapColors heatmapColors = DEFAULT_COLORS;
  public int imageHeight = DEFAULT_HEIGHT;
  public int imageWidth = DEFAULT_WIDTH;
  public boolean axisVisible = DEFAULT_AXIS_VISIBLE;
}

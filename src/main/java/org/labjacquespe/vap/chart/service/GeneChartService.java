/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.BrokenLineChart;
import org.labjacquespe.vap.chart.CategoryValueAxis;
import org.labjacquespe.vap.chart.Data;
import org.labjacquespe.vap.chart.DataGroup;
import org.labjacquespe.vap.chart.FastHeatmapChart;
import org.labjacquespe.vap.chart.GeneAxis;
import org.labjacquespe.vap.chart.GeneChart;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.HeatmapChart;
import org.labjacquespe.vap.chart.HeatmapColors;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapWindow;
import org.labjacquespe.vap.chart.StandardDeviationBrokenLineChart;
import org.labjacquespe.vap.chart.StandardDeviationBrokenLineChart.Extra;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.message.MessageResources;
import org.springframework.stereotype.Component;

/**
 * JavaFX implementation of {@link GeneChartService}.
 */
@Component
public class GeneChartService {
  private class GeneChartDefault extends StackPane implements GeneChart<Number, Number> {
    private class RepresentationTypeListener implements ChangeListener<RepresentationType> {
      @Override
      public void changed(ObservableValue<? extends RepresentationType> observableValue,
          RepresentationType oldValue, RepresentationType newValue) {
        if (newValue != null) {
          representationTypeLegend
              .setText(resources.message("representationTypeLegend." + newValue.name()));
        } else {
          representationTypeLegend.setText("");
        }
      }
    }

    private final ObjectProperty<RepresentationType> representationTypeProperty =
        new SimpleObjectProperty<>();
    private XYChart<Number, Number> chart;
    private RepresentationTypeLegend representationTypeLegend;

    private GeneChartDefault(XYChart<Number, Number> chart) {
      this.chart = chart;
      representationTypeProperty.addListener(new RepresentationTypeListener());
      representationTypeLegend = new RepresentationTypeLegend(chart);
      getChildren().add(chart);
      GridPane container = new GridPane();
      container.add(representationTypeLegend, 0, 0);
      getChildren().add(container);
    }

    @Override
    public Pane getNode() {
      return this;
    }

    @Override
    public XYChart<Number, Number> getChart() {
      return chart;
    }

    @Override
    public Node getRepresentationTypeLegend() {
      return representationTypeLegend;
    }

    private void setRepresentationType(RepresentationType representationType) {
      representationTypeProperty.set(representationType);
    }
  }

  private static class HeatmapChartDefault extends StackPane implements GeneChart<Number, Integer> {
    private final XYChart<Number, Integer> chart;

    private HeatmapChartDefault(XYChart<Number, Integer> chart) {
      this.chart = chart;
      getStyleClass().addAll("chart-heatmap-container");
      getChildren().add(chart);
    }

    @Override
    public Pane getNode() {
      return this;
    }

    @Override
    public XYChart<Number, Integer> getChart() {
      return chart;
    }

    @Override
    public Node getRepresentationTypeLegend() {
      return null;
    }
  }

  protected static class RepresentationTypeLegend extends VBox {
    private final StringProperty textProperty = new SimpleStringProperty();
    private final XYChart<Number, Number> chart;
    private final Text text = new Text();

    private RepresentationTypeLegend(XYChart<Number, Number> chart) {
      this.chart = chart;

      getStyleClass().addAll("vbox", "representation-type");
      textProperty.bindBidirectional(text.textProperty());
      text.getStyleClass().add("text");
      getChildren().add(text);
    }

    public String getText() {
      return textProperty.get();
    }

    public void setText(String text) {
      textProperty.set(text);
    }

    public Text getTextNode() {
      return text;
    }

    @Override
    protected void layoutChildren() {
      super.layoutChildren();
      Region background = (Region) chart.lookup(".chart-plot-background");
      Bounds backgroundBounds = background.localToScene(background.getBoundsInLocal());
      Bounds thisBounds = localToScene(getBoundsInLocal());
      double translateX =
          backgroundBounds.getMinX() - thisBounds.getMinX() + getPadding().getLeft();
      double translateY = backgroundBounds.getMinY() - thisBounds.getMinY() + getPadding().getTop();
      text.setTranslateX(translateX);
      text.setTranslateY(translateY);
    }
  }

  private static class Range {
    private double min;
    private double max;

    private Range(double min, double max) {
      this.min = min;
      this.max = max;
    }

    public double getMin() {
      return min;
    }

    public double getMax() {
      return max;
    }
  }

  private static class StandardDeviationBrokenLineChartExtraDefault implements Extra {
    private final Double standardDeviation;
    private final boolean broken;

    private StandardDeviationBrokenLineChartExtraDefault(Double standardDeviation, boolean broken) {
      this.standardDeviation = standardDeviation;
      this.broken = broken;
    }

    @Override
    public Double getStandardDeviation() {
      return standardDeviation;
    }

    @Override
    public boolean isBroken() {
      return broken;
    }
  }

  private static final double SPLIT_MIN_Y = 0.0;
  private static final double SPLIT_MAX_Y = 1.0;
  private static final double RELATIVE_SCALE_LENGTH = 1.0 / 10.0;
  private static final HeatmapColors DEFAULT_HEATMAP_COLORS = HeatmapColors.RAINBOW;
  private final MessageResources resources;

  protected GeneChartService() {
    resources = new MessageResources(GeneChartService.class, Locale.getDefault());
  }

  /**
   * Creates gene chart based on data found in file.
   *
   * @param groups
   *          data for samples in chart
   * @param parameters
   *          gene chart parameters
   * @return gene chart
   */
  public GeneChart<Number, Number> geneChart(List<DataGroup> groups,
      GeneChartParameters parameters) {
    RepresentationType representationType = parameters.representationType;
    if (representationType == null) {
      representationType = guessRepresentationType(groups);
    }

    // Convert to JavaFX data.
    ObservableList<XYChart.Series<Number, Number>> chartData = chartData(groups,
        parameters.dispersion, data -> new XYChart.Data<>(data.getX(), data.getAverage()));

    Range xaxisRange = xaxisRange(groups);
    Range yaxisRange = yaxisRange(chartData, parameters.dispersion);
    // Adjust Y scale.
    YAxisScale scale = parameters.yaxisScale;
    if (scale != null && scale.from != null) {
      yaxisRange.min = scale.from;
    }
    if (scale != null && scale.to != null) {
      yaxisRange.max = scale.to;
    }
    GeneAxis xaxis = new GeneAxis(xaxisRange.getMin(), xaxisRange.getMax(), separators(groups));
    // Prepare regions.
    if (parameters.referenceType == null || parameters.representationType == null
        || parameters.blocks == null || parameters.blocks.isEmpty()
        || (parameters.referencePoints == 1 && parameters.boundary == null)) {
      // Cannot compute regions.
    } else {
      xaxis.setRegions(geneRegions(groups, parameters));
    }
    NumberAxis yaxis =
        new NumberAxis(resources.message("geneChart.yaxis.label"), yaxisRange.getMin(),
            yaxisRange.getMax(), computeTickUnit(yaxisRange.getMin(), yaxisRange.getMax()));
    XYChart<Number, Number> chart;
    if (parameters.dispersion) {
      chart = new StandardDeviationBrokenLineChart<>(xaxis, yaxis,
          FXCollections.observableList(chartData));
    } else {
      chart = new BrokenLineChart<>(xaxis, yaxis, FXCollections.observableList(chartData));
    }
    chart.getStyleClass().add("gene-chart");
    GeneChartDefault geneChart = new GeneChartDefault(chart);
    geneChart.setRepresentationType(representationType);
    double scaleLegendLength = scaleLegendLength(xaxisRange.getMin(), xaxisRange.getMax());
    xaxis.setScaleLegendLength((long) Math.floor(scaleLegendLength));
    if (representationType != null) {
      xaxis.getStyleClass().add(representationType.name());
    }
    return geneChart;
  }

  /**
   * Creates proportion chart based on data found in file.
   *
   * @param groups
   *          data for samples in chart
   * @param parameters
   *          gene chart parameters
   * @return proportion chart
   */
  public GeneChart<Number, Number> proportionChart(List<DataGroup> groups,
      GeneChartParameters parameters) {
    RepresentationType representationType = parameters.representationType;
    if (representationType == null) {
      representationType = guessRepresentationType(groups);
    }

    // Convert to JavaFX data.
    ObservableList<XYChart.Series<Number, Number>> chartData =
        chartData(groups, false, data -> new XYChart.Data<>(data.getX(), data.getProp()));

    Range xaxisRange = xaxisRange(groups);
    GeneAxis xaxis = new GeneAxis(xaxisRange.getMin(), xaxisRange.getMax(), separators(groups));
    // Prepare regions.
    if (parameters.referenceType == null || parameters.representationType == null
        || parameters.blocks == null || parameters.blocks.isEmpty()
        || (parameters.referencePoints == 1 && parameters.boundary == null)) {
      // Cannot compute regions.
    } else {
      xaxis.setRegions(geneRegions(groups, parameters));
    }
    NumberAxis yaxis = new NumberAxis(resources.message("proportionChart.yaxis.label"), SPLIT_MIN_Y,
        SPLIT_MAX_Y, computeTickUnit(SPLIT_MIN_Y, SPLIT_MAX_Y));
    XYChart<Number, Number> chart =
        new BrokenLineChart<>(xaxis, yaxis, FXCollections.observableList(chartData));
    chart.getStyleClass().add("proportion-chart");
    GeneChartDefault geneChart = new GeneChartDefault(chart);
    geneChart.setRepresentationType(representationType);
    double scaleLegendLength = scaleLegendLength(xaxisRange.getMin(), xaxisRange.getMax());
    xaxis.setScaleLegendLength((long) Math.floor(scaleLegendLength));
    if (representationType != null) {
      xaxis.getStyleClass().add(representationType.name());
    }
    return geneChart;
  }

  /**
   * Creates heatmap chart.
   *
   * @param metadata
   *          heatmap metadata
   * @param features
   *          heatmap data
   * @param parameters
   *          gene chart parameters
   * @return heatmap chart
   */
  public GeneChart<Number, Integer> heatmapChart(HeatmapMetadata metadata,
      List<HeatmapFeature> features, GeneChartParameters parameters) {
    RepresentationType representationType = parameters.representationType;
    if (representationType == null) {
      representationType = guessRepresentationType(metadata);
    }
    ObservableList<XYChart.Series<Number, Integer>> chartData = chartData(metadata, features);
    Collections.reverse(chartData);

    Range xaxisRange = xaxisRange(metadata);
    Range heatRange = heatRange(metadata);
    // Adjust Y scale.
    YAxisScale scale = parameters.yaxisScale;
    if (scale != null && scale.from != null) {
      heatRange.min = scale.from;
    }
    if (scale != null && scale.to != null) {
      heatRange.max = scale.to;
    }
    GeneAxis xaxis = new GeneAxis(xaxisRange.getMin(), xaxisRange.getMax(), separators(metadata));
    // Prepare regions.
    if (parameters.referenceType == null || parameters.representationType == null
        || parameters.blocks == null || parameters.blocks.isEmpty()
        || (parameters.referencePoints == 1 && parameters.boundary == null)) {
      // Cannot compute regions.
    } else {
      xaxis.setRegions(geneRegions(metadata, parameters));
    }
    CategoryValueAxis yaxis =
        new CategoryValueAxis(0, features.size(), Math.max(1, features.size() / 1000));
    yaxis.setMinorTickVisible(false);
    yaxis.setTickLabels(FXCollections.observableMap(IntStream.range(0, features.size()).boxed()
        .collect(Collectors.toMap(i -> i, i -> features.get(i).name))));
    HeatmapChart<Number, Integer> chart = new FastHeatmapChart<>(xaxis, yaxis, chartData);
    if (parameters.heatmapParameters == null) {
      chart.setColors(FXCollections.observableArrayList(DEFAULT_HEATMAP_COLORS.getColors()));
    } else {
      chart.setColors(FXCollections
          .observableArrayList(parameters.heatmapParameters.heatmapColors.getColors()));
    }
    chart.setMinHeat(heatRange.min);
    chart.setMaxHeat(heatRange.max);
    chart.getStyleClass().add("gene-chart");
    chart.setHorizontalGridLinesVisible(false);
    chart.setAlternativeRowFillVisible(false);
    chart.getYAxis().setTickMarkVisible(false);
    HeatmapChartDefault heatmapChart = new HeatmapChartDefault(chart);
    double scaleLegendLength = scaleLegendLength(xaxisRange.getMin(), xaxisRange.getMax());
    xaxis.setScaleLegendLength((long) Math.floor(scaleLegendLength));
    if (representationType != null) {
      xaxis.getStyleClass().add(representationType.name());
    }
    return heatmapChart;
  }

  private RepresentationType guessRepresentationType(List<DataGroup> groups) {
    RepresentationType representationType = null;
    for (DataGroup group : groups) {
      if (group.getDatas().size() > 1) {
        Data data1 = group.getDatas().get(0);
        Data data2 = group.getDatas().get(1);
        double diff = Math.abs(data1.getX() - data2.getX());
        if (diff < 1.0001 && diff > 0.9999) {
          representationType = RepresentationType.RELATIVE;
        } else {
          representationType = RepresentationType.ABSOLUTE;
        }
        break;
      }
    }
    return representationType;
  }

  private RepresentationType guessRepresentationType(HeatmapMetadata metadata) {
    RepresentationType representationType = null;
    if (metadata.windows.size() > 1) {
      HeatmapWindow window1 = metadata.windows.get(0);
      HeatmapWindow window2 = metadata.windows.get(1);
      double diff = Math.abs(window1.getX() - window2.getX());
      if (diff < 1.0001 && diff > 0.9999) {
        representationType = RepresentationType.RELATIVE;
      } else {
        representationType = RepresentationType.ABSOLUTE;
      }
    }
    return representationType;
  }

  private Extra broken(Double std) {
    return new StandardDeviationBrokenLineChartExtraDefault(std, true);
  }

  private ObservableList<XYChart.Series<Number, Number>> chartData(List<DataGroup> groups,
      boolean dispersion, Function<Data, XYChart.Data<Number, Number>> dataCreator) {
    ObservableList<XYChart.Series<Number, Number>> chartData = FXCollections.observableArrayList();
    for (DataGroup group : groups) {
      XYChart.Series<Number, Number> serie = new XYChart.Series<>();
      serie.setName(group.getName());
      List<XYChart.Data<Number, Number>> serieDatas = new ArrayList<>();
      boolean separator = false;
      for (Data data : group.getDatas()) {
        switch (data.getType()) {
          case SEPARATOR:
            separator = true;
            break;
          case NORMAL:
            if (data.getAverage().isNaN()) {
              separator = true;
            } else {
              XYChart.Data<Number, Number> serieData = dataCreator.apply(data);
              if (dispersion) {
                serieData.setExtraValue(data.getStandardDeviation());
                if (separator) {
                  serieData.setExtraValue(broken(data.getStandardDeviation()));
                }
              } else if (separator) {
                serieData.setExtraValue(true);
              }
              serieDatas.add(serieData);
              separator = false;
            }
            break;
          default:
            break;
        }
      }
      serie.setData(FXCollections.observableList(serieDatas));
      chartData.add(serie);
    }
    return chartData;
  }

  private ObservableList<XYChart.Series<Number, Integer>> chartData(HeatmapMetadata metadata,
      List<HeatmapFeature> data) {
    ObservableList<XYChart.Series<Number, Integer>> chartData = FXCollections.observableArrayList();
    int index = 0;
    for (HeatmapFeature feature : data) {
      String name = feature.name;
      XYChart.Series<Number, Integer> serie = new XYChart.Series<>();
      serie.setName(name);
      ObservableList<XYChart.Data<Number, Integer>> serieData = FXCollections.observableArrayList();
      for (int i = 0; i < metadata.windows.size(); i++) {
        HeatmapWindow window = metadata.windows.get(i);
        if (!window.isSeparator()) {
          Double value = feature.heats.get(i);
          if (value != null && !value.isNaN()) {
            serieData.add(new XYChart.Data<Number, Integer>(window.getX(), index, value));
          }
        }
      }
      serie.setData(serieData);
      chartData.add(serie);
      index++;
    }
    return chartData;
  }

  private List<GeneAxis.Region> geneRegions(List<DataGroup> groups,
      GeneChartParameters parameters) {
    Map<Double, Double> originalXMap = new HashMap<>();
    for (DataGroup group : groups) {
      for (Data data : group.getDatas()) {
        if (data.getType() == Data.Type.NORMAL) {
          originalXMap.put(data.getOriginalX(), data.getX());
        }
      }
    }
    return geneRegions(originalXMap, parameters);
  }

  private List<GeneAxis.Region> geneRegions(HeatmapMetadata metadata,
      GeneChartParameters parameters) {
    Map<Double, Double> originalXMap = new HashMap<>();
    for (HeatmapWindow window : metadata.windows) {
      if (!window.isSeparator()) {
        originalXMap.put(window.getOriginalX(), window.getX());
      }
    }
    return geneRegions(originalXMap, parameters);
  }

  private List<GeneAxis.Region> geneRegions(Map<Double, Double> originalXMap,
      GeneChartParameters parameters) {
    List<Block> regions = parameters.blocks;
    int regionsCount = Math.min(parameters.referencePoints + 1, regions.size());
    int geneStartRegionIndex = regionsCount / 2;
    if (parameters.referenceType == ReferenceType.EXONS) {
      geneStartRegionIndex = 1;
    }
    long startPosition = 0;
    for (int i = geneStartRegionIndex - 1; i >= 0; i--) {
      Block region = regions.get(i);
      long length = regionLength(region, parameters);
      startPosition -= length;
    }
    List<GeneAxis.Region> axisRegions = new ArrayList<>();
    for (int i = 0; i < regionsCount; i++) {
      Block region = regions.get(i);
      long length = regionLength(region, parameters);
      boolean reference = isReference(parameters, i);
      GeneAxis.Region axisRegion = new GeneAxis.Region(reference);
      axisRegion.setStart(xposition((double) startPosition, originalXMap));
      axisRegion.setEnd(xposition((double) startPosition + length, originalXMap));
      BlockType type = region.type;
      if (reference && parameters.referenceType != ReferenceType.EXONS) {
        type = BlockType.REFERENCE_FEATURE;
      }
      axisRegion.setType(type);
      startPosition += length;
      axisRegions.add(axisRegion);
    }
    return axisRegions;
  }

  private boolean isReference(GeneChartParameters parameters, int index) {
    List<Block> regions = parameters.blocks;
    if (regions.get(index).type == BlockType.REFERENCE_FEATURE) {
      return true;
    }
    boolean referenceFeaturePresent = false;
    for (Block region : regions) {
      referenceFeaturePresent |= region.type == BlockType.REFERENCE_FEATURE;
    }
    if (referenceFeaturePresent) {
      return false;
    } else if (parameters.referencePoints == 1) {
      Boundary boundary = parameters.boundary;
      switch (boundary) {
        case FIVE_PRIME:
          return index == 1;
        case THREE_PRIME:
          return index == 0;
        default:
          throw new AssertionError("Boundary " + boundary + " not covered in switch");
      }
    } else {
      return index == 1;
    }
  }

  private long regionLength(Block region, GeneChartParameters parameters) {
    switch (parameters.representationType) {
      case ABSOLUTE:
        return parameters.windowSize * region.windows;
      case RELATIVE:
        return region.windows;
      default:
        throw new AssertionError(
            "Representation type " + parameters.representationType + " not covered in switch");
    }
  }

  private double xposition(Double originalPosition, Map<Double, Double> originalXMap) {
    if (originalXMap.containsKey(originalPosition)) {
      // Perfect match!
      return originalXMap.get(originalPosition);
    } else if (!originalXMap.isEmpty()) {
      double bestKey = originalXMap.keySet().iterator().next();
      for (Double key : originalXMap.keySet()) {
        if (Math.abs(originalPosition - bestKey) > Math.abs(originalPosition - key)) {
          bestKey = key;
        }
      }
      return originalXMap.get(bestKey);
    } else {
      return Double.NaN;
    }
  }

  private Set<GeneAxis.Separator> separators(List<DataGroup> groups) {
    Set<GeneAxis.Separator> separators = new HashSet<>();
    for (DataGroup group : groups) {
      for (Data data : group.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          separators.add(new GeneAxis.Separator(data.getX(), data.getX() + data.getLength()));
        }
      }
    }
    return separators;
  }

  private Set<GeneAxis.Separator> separators(HeatmapMetadata metadata) {
    Set<GeneAxis.Separator> separators = new HashSet<>();
    GeneAxis.Separator separtor = null;
    boolean lastSeparator = false;
    for (HeatmapWindow window : metadata.windows) {
      if (window.isSeparator()) {
        if (lastSeparator) {
          separtor.setEnd(window.getX() + metadata.windowLength);
        } else {
          separtor = new GeneAxis.Separator(window.getX(), window.getX() + metadata.windowLength);
          separators.add(separtor);
        }
      }
      lastSeparator = window.isSeparator();
    }
    return separators;
  }

  private <Y> Range xaxisRange(List<DataGroup> groups) {
    double min = Double.POSITIVE_INFINITY;
    double max = Double.NEGATIVE_INFINITY;
    for (DataGroup group : groups) {
      for (Data data : group.getDatas()) {
        Double value = data.getX();
        if (value != null && !value.isNaN()) {
          min = Math.min(value, min);
          if (data.getLength() != null) {
            max = Math.max(value + data.getLength(), max);
          }
        }
      }
    }
    return new Range(min, max);
  }

  private <Y> Range xaxisRange(HeatmapMetadata metadata) {
    double min = Double.POSITIVE_INFINITY;
    double max = Double.NEGATIVE_INFINITY;
    for (HeatmapWindow window : metadata.windows) {
      Double value = window.getX();
      if (value != null && !value.isNaN()) {
        min = Math.min(value, min);
        max = Math.max(value, max);
        max = Math.max(value + metadata.windowLength, max);
      }
    }
    return new Range(min, max);
  }

  private <X> Range yaxisRange(Collection<XYChart.Series<X, Number>> data, boolean dispersion) {
    double min = Double.POSITIVE_INFINITY;
    double max = Double.NEGATIVE_INFINITY;
    for (XYChart.Series<X, Number> serie : data) {
      for (XYChart.Data<X, Number> singleData : serie.getData()) {
        Double value = (Double) singleData.getYValue();
        if (value != null && !value.isNaN()) {
          if (dispersion) {
            Double deviation = null;
            if (singleData.getExtraValue() instanceof Double) {
              deviation = (Double) singleData.getExtraValue();
            } else if (singleData.getExtraValue() instanceof Extra) {
              deviation = ((Extra) singleData.getExtraValue()).getStandardDeviation();
            }
            if (deviation != null && !deviation.isNaN()) {
              min = Math.min(min, value - deviation);
              max = Math.max(max, value + deviation);
            }
          } else {
            min = Math.min(value, min);
            max = Math.max(value, max);
          }
        }
      }
    }
    return new Range(min, max);
  }

  private <X> Range heatRange(HeatmapMetadata metadata) {
    return new Range(metadata.minHeat, metadata.maxHeat);
  }

  private double computeTickUnit(double min, double max) {
    return Math.abs(max - min) / 5;
  }

  private double scaleLegendLength(double min, double max) {
    double diff = Math.abs(max - min);
    int power10 = (int) Math.floor(Math.log10(diff));
    int firstDigit = (int) Math.round(diff / Math.pow(10, power10));
    if (firstDigit < 3) {
      firstDigit = 1;
    } else if (firstDigit < 8) {
      firstDigit = 5;
    } else {
      firstDigit = 10;
    }
    double diffOneDigit = firstDigit * Math.pow(10, power10);
    return diffOneDigit * RELATIVE_SCALE_LENGTH;
  }

  /**
   * Returns region type.
   *
   * @param regionIndex
   *          region's index
   * @param referenceType
   *          reference type
   * @param referencePoints
   *          number of points of reference
   * @return region type
   */
  public BlockType regionType(int regionIndex, ReferenceType referenceType,
      Integer referencePoints) {
    // Sanity check.
    if (referenceType == null || referencePoints == null || referencePoints <= 0 || regionIndex < 0
        || regionIndex > referencePoints) {
      return null;
    }

    BlockType blockType = null;
    switch (referenceType) {
      case ANNOTATIONS:
        if ((referencePoints - 1) / 2 % 2 == 0 ^ regionIndex % 2 == 0) {
          blockType = BlockType.ANNOTATION;
        } else {
          blockType = BlockType.INTER;
        }
        // Reference feature.
        if (regionIndex == (referencePoints / 2) + referencePoints % 2) {
          blockType = BlockType.REFERENCE_FEATURE;
        }
        break;
      case EXONS:
        if ((referencePoints - 1) / 2 % 2 == 0 ^ regionIndex % 2 == 0) {
          blockType = BlockType.EXON;
        } else {
          blockType = BlockType.INTRON;
        }
        break;
      case COORDINATES:
        if ((referencePoints - 1) / 2 % 2 == 0 ^ regionIndex % 2 == 0) {
          blockType = BlockType.REGION;
        } else {
          blockType = BlockType.INTER;
        }
        // Reference feature.
        if (regionIndex == (referencePoints / 2) + referencePoints % 2) {
          blockType = BlockType.REFERENCE_FEATURE;
        }
        break;
      default:
        break;
    }
    if (regionIndex <= 0) {
      blockType = BlockType.UPSTREAM;
    } else if (regionIndex >= referencePoints) {
      blockType = BlockType.DOWNSTREAM;
    }
    return blockType;
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.labjacquespe.vap.chart.Data;
import org.labjacquespe.vap.chart.DataGroup;
import org.springframework.stereotype.Component;

/**
 * Parses data inside a file.
 */
@Component
public class DataParser {
  public static enum Column {
    POSITION, AVERAGE, STANDARD_ERROR, SPLIT
  }

  private static class Line {
    private final List<String> columns;
    private final int columnsCount;
    private final int groupMultiplier = Column.values().length;

    private Line(String rawLine) {
      rawLine = rawLine.replaceAll("\\s*$", "");
      String[] rawColumns = StringUtils.splitByWholeSeparatorPreserveAllTokens(rawLine, "\t");
      columnsCount = rawColumns.length;
      columns = new ArrayList<>();
      for (int i = 0; i < rawColumns.length; i++) {
        columns.add(rawColumns[i]);
      }
    }

    public String getColumn(int index) {
      if (index < this.columns.size()) {
        return this.columns.get(index).trim();
      } else {
        return "";
      }
    }

    public String getColumn(Column column, int groupIndex) {
      if (column.ordinal() + groupMultiplier * groupIndex < this.columns.size()) {
        return this.columns.get(column.ordinal() + groupMultiplier * groupIndex).trim();
      } else {
        return "";
      }
    }

    public int getColumnCount() {
      return columnsCount;
    }
  }

  @Inject
  private Charset charset;

  protected DataParser() {
  }

  protected DataParser(Charset charset) {
    this.charset = charset;
  }

  /**
   * Parses data group file.
   *
   * @param file
   *          file
   * @return data groups
   * @throws IOException
   *           could not read file
   */
  public List<DataGroup> parse(File file) throws IOException {
    int columnCount = Column.values().length;

    List<DataGroup> groups = new ArrayList<>();
    int groupCount = 0;
    // Find number of groups.
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String rawLine;
      while ((rawLine = reader.readLine()) != null) {
        if (!rawLine.startsWith("#")) {
          Line line = new Line(rawLine);
          groupCount = line.getColumnCount() / columnCount;
          break;
        }
      }
    }
    for (int i = 0; i < groupCount; i++) {
      DataGroup group = new DataGroup();
      group.setDatas(new ArrayList<Data>());
      groups.add(group);
    }
    // Find group names.
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String rawLine;
      while ((rawLine = reader.readLine()) != null) {
        Line line = new Line(rawLine);
        if (rawLine.startsWith("#")) {
          boolean foundAllNames = true;
          for (int i = 0; i < groupCount; i++) {
            DataGroup group = groups.get(i);
            if (!line.getColumn(Column.AVERAGE, i).equals("")) {
              group.setName(line.getColumn(Column.AVERAGE, i));
            } else {
              foundAllNames = false;
            }
          }
          if (foundAllNames) {
            break;
          }
        }
      }
    }
    // Find index of X's 0.
    int zeroIndex = 0;
    double step = 1.0;
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String rawLine;
      int dataIndex = 0;
      boolean foundZero = false;
      while ((rawLine = reader.readLine()) != null) {
        Line line = new Line(rawLine);
        if (rawLine.startsWith("#INV") || !rawLine.startsWith("#")) {
          Double xvalue = parseDouble(line.getColumn(0));
          if (xvalue == 0.0) {
            foundZero = true;
            zeroIndex = dataIndex;
          } else if (foundZero) {
            foundZero = false;
            step = xvalue;
          }
          dataIndex++;
        }
      }
    }
    // Parse data.
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String rawLine;
      int dataIndex = 0;
      while ((rawLine = reader.readLine()) != null) {
        Line line = new Line(rawLine);
        if (rawLine.startsWith("#INV")) {
          for (int i = 0; i < groupCount; i++) {
            DataGroup group = groups.get(i);
            Data lastData = lastData(group);
            if (lastData != null && lastData.getType() == Data.Type.SEPARATOR) {
              lastData.setLength(lastData.getLength() + step);
            } else {
              Data data = new Data();
              data.setX((dataIndex - zeroIndex) * step - step);
              data.setType(Data.Type.SEPARATOR);
              data.setAverage(Double.NaN);
              data.setStandardDeviation(Double.NaN);
              data.setProp(Double.NaN);
              data.setLength(step * 2);
              group.getDatas().add(data);
            }
          }
          dataIndex++;
        } else if (!rawLine.startsWith("#")) {
          for (int i = 0; i < groupCount; i++) {
            final DataGroup group = groups.get(i);
            Data data = new Data();
            data.setX((dataIndex - zeroIndex) * step);
            data.setOriginalX(parseDouble(line.getColumn(Column.POSITION, i)));
            data.setType(Data.Type.NORMAL);
            data.setAverage(parseDouble(line.getColumn(Column.AVERAGE, i)));
            data.setStandardDeviation(parseDouble(line.getColumn(Column.STANDARD_ERROR, i)));
            if (Double.isNaN(data.getStandardDeviation()) && !Double.isNaN(data.getAverage())) {
              data.setStandardDeviation(0.0);
            }
            data.setProp(parseDouble(line.getColumn(Column.SPLIT, i)));
            data.setLength(0.0);
            group.getDatas().add(data);
          }
          dataIndex++;
        }
      }
    }
    return groups;
  }

  private Data lastData(DataGroup group) {
    if (!group.getDatas().isEmpty()) {
      return group.getDatas().get(group.getDatas().size() - 1);
    } else {
      return null;
    }
  }

  private Double parseDouble(String input) {
    try {
      return Double.valueOf(input);
    } catch (NumberFormatException e) {
      return Double.NaN;
    }
  }
}

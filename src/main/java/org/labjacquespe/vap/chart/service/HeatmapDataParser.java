/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import javax.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapWindow;
import org.labjacquespe.vap.message.MessageResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Parses group of samples inside a file.
 */
@Component
public class HeatmapDataParser {
  private static final String BELOW_MINIMUM_COLUMNS = "BELOW_MINIMUM_COLUMNS";
  private static final String EMPTY_COLUMN = "EMPTY_COLUMN";
  private static final String INVALID_WINDOW_NAME = "INVALID_WINDOW_NAME";
  private static final String INVALID_VALUE = "INVALID_VALUE";

  private static final Logger logger = LoggerFactory.getLogger(HeatmapDataParser.class);
  @Inject
  private Charset charset;

  protected HeatmapDataParser() {
  }

  protected HeatmapDataParser(Charset charset) {
    this.charset = charset;
  }

  private Pattern getWindowSeparatorNamePattern(MessageResources resources) {
    return Pattern.compile(resources.message("window.name.separator.pattern"));
  }

  private Pattern getWindowNamePattern(MessageResources resources) {
    return Pattern.compile(resources.message("window.name.pattern"));
  }

  private Pattern getFeatureAggregatePattern(MessageResources resources) {
    return Pattern.compile(resources.message("feature.aggregate.comment.pattern"));
  }

  private Pattern getNullValuePattern(MessageResources resources) {
    return Pattern.compile(resources.message("null.value.pattern"));
  }

  /**
   * Validates heatmap data file.
   *
   * @param file
   *          heatmap data file
   * @param locale
   *          locale
   * @param errorHandler
   *          validation error handler
   * @throws IOException
   *           could not read heatmap data
   */
  public void validate(File file, Locale locale, Consumer<String> errorHandler) throws IOException {
    final MessageResources resources = new MessageResources(HeatmapDataParser.class, locale);
    parseAndValidateMetadata(file, resources, errorHandler);
  }

  /**
   * Parses heatmap data file.
   *
   * @param file
   *          heatmap data file
   * @return heatmap meta-data consumer
   * @throws IOException
   *           could not parse heatmap data file
   */
  public HeatmapMetadata parseMetadata(File file) throws IOException {
    final MessageResources resources =
        new MessageResources(HeatmapDataParser.class, Locale.getDefault());
    return parseAndValidateMetadata(file, resources, error -> {
    });
  }

  /**
   * Parses heatmap data file.
   *
   * @param file
   *          heatmap data file
   * @param dataConsumer
   *          data consumer
   * @throws IOException
   *           could not parse heatmap data file
   */
  public void parseData(File file, Consumer<HeatmapFeature> dataConsumer) throws IOException {
    final MessageResources resources =
        new MessageResources(HeatmapDataParser.class, Locale.getDefault());
    parseAndValidateFeatures(file, resources, dataConsumer, error -> {
    });
  }

  private HeatmapMetadata parseAndValidateMetadata(File file, MessageResources resources,
      Consumer<String> errorHandler) throws IOException {
    HeatmapMetadata metadata = new HeatmapMetadata();
    metadata.name = FilenameUtils.getBaseName(file.getName());
    try (LineNumberReader reader =
        new LineNumberReader(Files.newBufferedReader(file.toPath(), charset))) {
      String line = findWindowsLine(reader);
      metadata.windows =
          parseAndValidateWindows(line, reader.getLineNumber(), resources, errorHandler);
      metadata.windowLength = findWindowsLength(metadata.windows);
    }
    DoubleSummaryStatistics statistics = new DoubleSummaryStatistics();
    IntSummaryStatistics featureCount = new IntSummaryStatistics();
    parseAndValidateFeatures(file, resources, data -> {
      featureCount.accept(0);
      data.heats.stream().filter(h -> h != null).forEach(h -> statistics.accept(h));
    }, errorHandler);
    metadata.featureCount = (int) featureCount.getCount();
    metadata.minHeat = statistics.getMin();
    metadata.maxHeat = statistics.getMax();
    return metadata;
  }

  private List<HeatmapWindow> parseAndValidateWindows(String windowsLine, int lineNumber,
      MessageResources resources, Consumer<String> errorHandler) throws IOException {
    List<HeatmapWindow> windows = new ArrayList<>();
    Pattern separatorPattern = getWindowSeparatorNamePattern(resources);
    Pattern namePattern = getWindowNamePattern(resources);
    int firstWindowIndex = firstWindowIndex(windowsLine);
    String[] columns = windowsLine.split("\t", -1);
    for (int i = firstWindowIndex; i < columns.length; i++) {
      String name = columns[i];
      if (name.isEmpty()) {
        logger.debug("Window name cannot be empty");
        errorHandler.accept(resources.message(EMPTY_COLUMN, i + 1, lineNumber));
      } else {
        Matcher separatorMatcher = separatorPattern.matcher(name);
        Matcher nameMatcher = namePattern.matcher(name);
        if (!separatorMatcher.matches() && !nameMatcher.matches()) {
          logger.debug("Window name {} does not match pattern {}", name, namePattern);
          errorHandler.accept(resources.message(INVALID_WINDOW_NAME, i + 1, lineNumber,
              getWindowNamePattern(resources)));
        }
        HeatmapWindow window = new HeatmapWindow();
        window.setName(name);
        Matcher matcher = namePattern.matcher(name);
        if (matcher.matches()) {
          double xvalue = Double.valueOf(matcher.group(2));
          window.setOriginalX(xvalue);
        }
        window.setSeparator(separatorPattern.matcher(name).matches());
        windows.add(window);
      }
    }
    fixWindowsXvalue(windows, findWindowsLength(windows));
    return windows;
  }

  private int zeroWindowsIndex(List<HeatmapWindow> windows) {
    return IntStream.range(0, windows.size())
        .filter(i -> windows.get(i).getOriginalX() != null && windows.get(i).getOriginalX() == 0)
        .findFirst().orElse(-1);
  }

  private double findWindowsLength(List<HeatmapWindow> windows) {
    double length = 1.0;
    int zeroIndex = zeroWindowsIndex(windows);
    if (zeroIndex > -1) {
      HeatmapWindow window = windows.get(zeroIndex + 1);
      if (!window.isSeparator() && window.getOriginalX() != null) {
        length = window.getOriginalX();
      }
    }
    return length;
  }

  private void fixWindowsXvalue(List<HeatmapWindow> windows, double windowsLength) {
    double xvalue = -zeroWindowsIndex(windows) * windowsLength;
    for (HeatmapWindow window : windows) {
      window.setX(xvalue);
      xvalue += windowsLength;
    }
  }

  private void parseAndValidateFeatures(File file, MessageResources resources,
      Consumer<HeatmapFeature> dataConsumer, Consumer<String> errorHandler) throws IOException {
    try (LineNumberReader reader =
        new LineNumberReader(Files.newBufferedReader(file.toPath(), charset))) {
      String windowsLine = findWindowsLine(reader);
      int firstWindowIndex = firstWindowIndex(windowsLine);
      int windowsSize = parseAndValidateWindows(windowsLine, 0, resources, error -> {
      }).size();
      int expectedColumnCount = firstWindowIndex + windowsSize;
      Pattern featureAggregatePattern = getFeatureAggregatePattern(resources);
      String line;
      final Pattern nullValuePattern = getNullValuePattern(resources);
      boolean aggregate = false;
      while (!aggregate && (line = reader.readLine()) != null) {
        if (line.isEmpty()) {
          continue;
        }
        if (line.startsWith("#")) {
          if (featureAggregatePattern.matcher(line).matches()) {
            aggregate = true;
          }
          continue;
        }

        String[] columns = line.split("\t", -1);
        if (columns.length < expectedColumnCount) {
          logger.debug("Expected {} columns, but found {}", expectedColumnCount, columns.length);
          errorHandler.accept(resources.message(BELOW_MINIMUM_COLUMNS, expectedColumnCount,
              reader.getLineNumber()));
        } else {
          String feature = columns[0];
          if (feature.isEmpty()) {
            logger.debug("Feature name cannot be empty");
            errorHandler.accept(resources.message(EMPTY_COLUMN, 1, reader.getLineNumber()));
          }
          List<Double> heats = new ArrayList<>();
          for (int i = firstWindowIndex; i < expectedColumnCount; i++) {
            String value = columns[i];
            Double heat = null;
            if (value.isEmpty()) {
              logger.debug("Value cannot be empty");
              errorHandler.accept(resources.message(EMPTY_COLUMN, i + 1, reader.getLineNumber()));
            } else {
              if (!nullValuePattern.matcher(value).matches()) {
                try {
                  heat = Double.valueOf(value);
                } catch (NumberFormatException e) {
                  logger.debug("Value {} is not a valid double", value);
                  errorHandler
                      .accept(resources.message(INVALID_VALUE, i + 1, reader.getLineNumber()));
                }
              }
            }
            heats.add(heat);
          }
          dataConsumer.accept(new HeatmapFeature(feature, heats));
        }
      }
    }
  }

  private String findWindowsLine(BufferedReader reader) throws IOException {
    String line = null;
    boolean windowsLineFound = false;
    while (!windowsLineFound && (line = reader.readLine()) != null) {
      windowsLineFound = !line.isEmpty() && !line.startsWith("#");
    }
    return line;
  }

  private int firstWindowIndex(String windowsLine) throws IOException {
    String[] columns = windowsLine.split("\t", -1);
    return IntStream.range(0, windowsLine.length()).filter(i -> !columns[i].isEmpty()).findFirst()
        .orElse(-1);
  }
}

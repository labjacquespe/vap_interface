/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.labjacquespe.vap.chart.ChartGroup;
import org.labjacquespe.vap.message.MessageResources;
import org.springframework.stereotype.Component;

/**
 * Parses group of samples inside a file.
 */
@Component
public class ChartGroupParser {
  private static final String BELOW_MINIMUM_COLUMNS = "BELOW_MINIMUM_COLUMNS";
  private static final String ABOVE_MAXIMUM_COLUMNS = "ABOVE_MAXIMUM_COLUMNS";
  private static final String EMPTY_COLUMN = "EMPTY_COLUMN";

  @Inject
  private Charset charset;

  protected ChartGroupParser() {
  }

  protected ChartGroupParser(Charset charset) {
    this.charset = charset;
  }

  /**
   * Validates chart group file.
   *
   * @param file
   *          file
   * @param locale
   *          locale
   * @param errorHandler
   *          handles validation errors
   * @throws IOException
   *           could not parse file
   */
  public void validate(File file, Locale locale, Consumer<String> errorHandler) throws IOException {
    final MessageResources resources = new MessageResources(ChartGroupParser.class, locale);
    try (LineNumberReader reader =
        new LineNumberReader(Files.newBufferedReader(file.toPath(), charset))) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (!line.equals("") && !line.startsWith("#")) {
          String[] columns = StringUtils.splitPreserveAllTokens(line, "\t");
          if (columns.length < 2) {
            errorHandler
                .accept(resources.message(BELOW_MINIMUM_COLUMNS, 2, reader.getLineNumber()));
          } else if (columns.length > 2) {
            errorHandler
                .accept(resources.message(ABOVE_MAXIMUM_COLUMNS, 2, reader.getLineNumber()));
          } else {
            for (int i = 0; i < columns.length; i++) {
              if (columns[i].isEmpty()) {
                errorHandler.accept(resources.message(EMPTY_COLUMN, i + 1, reader.getLineNumber()));
              }
            }
          }
        }
      }
    }
  }

  /**
   * Parses chart group file.
   *
   * @param file
   *          file
   * @return chart groups
   * @throws IOException
   *           could not parse file
   */
  public List<ChartGroup> parse(File file) throws IOException {
    List<ChartGroup> groups = new ArrayList<>();
    Map<String, ChartGroup> groupsMap = new HashMap<>();
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (!line.equals("") && !line.startsWith("#")) {
          String[] columns = line.split("\\t");
          if (!groupsMap.containsKey(columns[0])) {
            ChartGroup chartGroup = new ChartGroup();
            chartGroup.setName(columns[0]);
            chartGroup.setResources(new ArrayList<String>());
            groups.add(chartGroup);
            groupsMap.put(chartGroup.getName(), chartGroup);
          }
          ChartGroup chartGroup = groupsMap.get(columns[0]);
          chartGroup.getResources().add(columns[1]);
        }
      }
    }
    return groups;
  }
}

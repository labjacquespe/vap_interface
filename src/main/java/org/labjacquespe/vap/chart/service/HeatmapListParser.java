/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.labjacquespe.vap.chart.HeatmapFile;
import org.springframework.stereotype.Component;

/**
 * Parses group of samples inside a file.
 */
@Component
public class HeatmapListParser {
  @Inject
  private Charset charset;

  protected HeatmapListParser() {
  }

  protected HeatmapListParser(Charset charset) {
    this.charset = charset;
  }

  /**
   * Validates heatmap list file.
   *
   * @param file
   *          heatmap list file
   * @param locale
   *          locale
   * @param errorHandler
   *          handlers validation errors
   * @throws IOException
   *           could not parse file
   */
  public void validate(File file, Locale locale, Consumer<String> errorHandler) throws IOException {
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      // Heatmap list file has no specific requirements.
    }
  }

  /**
   * Parses heatmap list file.
   *
   * @param file
   *          heatmap list file
   * @return parsed heatmap data
   * @throws IOException
   *           could not parse file
   */
  public List<HeatmapFile> parse(File file) throws IOException {
    List<HeatmapFile> heatmaps = new ArrayList<>();
    try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (!line.equals("") && !line.startsWith("#")) {
          String[] columns = StringUtils.splitPreserveAllTokens(line, "\t");
          heatmaps.add(new HeatmapFile(columns[0], new File(columns[1])));
        }
      }
    }
    return heatmaps;
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.Axis;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import org.labjacquespe.vap.chart.ChartGroup;
import org.labjacquespe.vap.chart.DataGroup;
import org.labjacquespe.vap.chart.GeneChart;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.HeatmapChart;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapFile;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapParameters;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.progressbar.ProgressBar;
import org.labjacquespe.vap.util.ExceptionUtils;
import org.labjacquespe.vap.util.PackagedRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Create and save charts based on analysis.
 */
@Component
public class AnalysisChartService {
  private static final int DPI = 300;
  private static final double INCH_TO_MM = 25.4;
  private static final double SCENE_WIDTH_MM = 130;
  private static final double SCENE_HEIGHT_MM = 70;
  private static final double X_AXIS_HEIGHT_MM = 4;
  private static final double Y_AXIS_WIDTH_MM = 5;
  private static final double CUSTOM_COLOR_LEGEND_HEIGHT_MM = 6;
  private static final double CUSTOM_GENE_AXIS_HEIGHT_MM = 8;
  private static final double CUSTOM_GENE_AXIS_PADDING_TOP_MM = 4;
  private static final Logger logger = LoggerFactory.getLogger(AnalysisChartService.class);

  private abstract class ChartSaver implements Callable<WritableImage> {
    private final GeneChart<?, ?>[] charts;

    private ChartSaver(GeneChart<?, ?>... charts) {
      this.charts = charts;
    }

    protected Stage createStage() {
      Stage stage = new Stage();
      VBox layout = new VBox();
      Scene scene = new Scene(layout, getWidth(), getHeight());
      stage.setScene(scene);
      stage.setTitle("Chart");
      scene.getStylesheets().add("application.css");
      for (GeneChart<?, ?> chart : charts) {
        chart.getNode().setPrefSize(getWidth(), getHeight());
        chart.getChart().setAnimated(false);
        chart.getChart().getXAxis().setAnimated(false);
        chart.getChart().getYAxis().setAnimated(false);
        layout.getChildren().add(chart.getNode());
      }
      HBox versionLayout = new HBox();
      versionLayout.setAlignment(Pos.BASELINE_RIGHT);
      Label label = new Label(resources.message("version"));
      versionLayout.getChildren().add(label);
      layout.getChildren().add(versionLayout);
      return stage;
    }

    protected abstract double getWidth();

    protected abstract double getHeight();
  }

  private class GeneChartSaver extends ChartSaver implements Callable<WritableImage> {
    private final GeneChart<?, ?> geneChart;
    private final GeneChart<?, ?> proportionChart;
    private final double width;
    private final double height;
    private final double xaxisHeight;
    private final double yaxisWidth;

    private GeneChartSaver(GeneChart<?, ?> geneChart, GeneChart<?, ?> proportionChart) {
      super(geneChart, proportionChart);
      this.geneChart = geneChart;
      this.proportionChart = proportionChart;
      this.width = mmToPixel(SCENE_WIDTH_MM);
      this.height = mmToPixel(SCENE_HEIGHT_MM);
      this.xaxisHeight = mmToPixel(X_AXIS_HEIGHT_MM);
      this.yaxisWidth = mmToPixel(Y_AXIS_WIDTH_MM);
    }

    @Override
    public WritableImage call() {
      final Stage stage = createStage();
      geneChart.getNode().setPrefHeight(getHeight() / 3 * 2);
      proportionChart.getNode().setPrefHeight(getHeight() / 3);
      geneChart.getChart().getXAxis().setPrefHeight(xaxisHeight);
      geneChart.getChart().getYAxis().setPrefWidth(yaxisWidth);
      proportionChart.getChart().getXAxis().setPrefHeight(xaxisHeight);
      proportionChart.getChart().getYAxis().setPrefWidth(yaxisWidth);
      return stage.getScene().snapshot(null);
    }

    @Override
    protected double getWidth() {
      return width;
    }

    @Override
    protected double getHeight() {
      return height;
    }
  }

  private class HeatmapChartSaver extends ChartSaver implements Callable<WritableImage> {
    private final GeneChart<?, ?> heatmapChart;
    private final HeatmapParameters parameters;
    private double width;
    private double height;
    private double xaxisHeight;
    private double yaxisWidth;

    private HeatmapChartSaver(GeneChart<?, ?> heatmapChart, HeatmapParameters parameters) {
      super(heatmapChart);
      this.heatmapChart = heatmapChart;
      this.parameters = parameters;
      this.width = mmToPixel(SCENE_WIDTH_MM);
      this.height = mmToPixel(SCENE_HEIGHT_MM);
      this.xaxisHeight = mmToPixel(X_AXIS_HEIGHT_MM);
      this.yaxisWidth = mmToPixel(Y_AXIS_WIDTH_MM);
      if (parameters != null) {
        this.width = mmToPixel(parameters.imageWidth);
        this.height = mmToPixel(parameters.imageHeight);
      }
    }

    @Override
    public WritableImage call() {
      final Stage stage = createStage();
      heatmapChart.getChart().getXAxis().setPrefHeight(xaxisHeight);
      heatmapChart.getChart().getYAxis().setPrefWidth(yaxisWidth);
      if (parameters != null) {
        if (!parameters.axisVisible) {
          heatmapChart.getChart().getStyleClass().add("noaxis");
          heatmapChart.getNode().getStyleClass().add("noaxis");
          heatmapChart.getChart().getXAxis().setPrefHeight(0.0);
          heatmapChart.getChart().getYAxis().setPrefWidth(0.0);
        }
      }
      final WritableImage image = stage.getScene().snapshot(null);
      heatmapChart.getChart().getStyleClass().remove("noaxis");
      heatmapChart.getNode().getStyleClass().remove("noaxis");
      heatmapChart.getChart().getXAxis().setPrefHeight(xaxisHeight);
      heatmapChart.getChart().getYAxis().setPrefWidth(yaxisWidth);
      return image;
    }

    @Override
    protected double getWidth() {
      return width;
    }

    @Override
    protected double getHeight() {
      return height;
    }
  }

  private class HeatmapLegendSaver extends HeatmapChartSaver implements Callable<WritableImage> {
    private final GeneChart<?, ?> heatmapChart;
    private double height;

    private HeatmapLegendSaver(GeneChart<?, ?> heatmapChart, HeatmapParameters parameters) {
      super(heatmapChart, parameters);
      this.heatmapChart = heatmapChart;
      this.height = mmToPixel(CUSTOM_COLOR_LEGEND_HEIGHT_MM);
    }

    @Override
    public WritableImage call() {
      Stage stage = createStage();
      Scene scene = stage.getScene();
      Pane parent = (Pane) scene.getRoot();
      parent.getChildren().clear();
      Region colorLegend = ((HeatmapChart<?, ?>) heatmapChart.getChart()).getColorLegend();
      colorLegend.setPrefWidth(getWidth());
      parent.getChildren().add(colorLegend);
      WritableImage image = scene.snapshot(null);
      return image;
    }

    @Override
    protected double getHeight() {
      return height;
    }
  }

  private class HeatmapXAxisSaver extends HeatmapChartSaver implements Callable<WritableImage> {
    private final GeneChart<?, ?> heatmapChart;
    private double height;
    private double paddingTop;

    private HeatmapXAxisSaver(GeneChart<?, ?> heatmapChart, HeatmapParameters parameters) {
      super(heatmapChart, parameters);
      this.heatmapChart = heatmapChart;
      this.height = mmToPixel(CUSTOM_GENE_AXIS_HEIGHT_MM);
      this.paddingTop = mmToPixel(CUSTOM_GENE_AXIS_PADDING_TOP_MM);
    }

    @Override
    public WritableImage call() {
      Stage stage = createStage();
      Scene scene = stage.getScene();
      Pane parent = (Pane) scene.getRoot();
      parent.getChildren().clear();
      Axis<?> xaxis = heatmapChart.getChart().getXAxis();
      xaxis.getStyleClass().add("saveaxis");
      parent.setPadding(new Insets(paddingTop, 0, 0, 0));
      parent.getChildren().add(xaxis);
      WritableImage image = scene.snapshot(null);
      xaxis.getStyleClass().remove("saveaxis");
      return image;
    }

    @Override
    protected double getHeight() {
      return height;
    }
  }

  private final MessageResources resources;
  @Inject
  private ChartGroupParser chartGroupParser;
  @Inject
  private DataParser dataParser;
  @Inject
  private HeatmapListParser heatmapListParser;
  @Inject
  private HeatmapDataParser heatmapParser;
  @Inject
  private GeneChartService geneChartService;

  protected AnalysisChartService() {
    resources = new MessageResources(AnalysisChartService.class, Locale.getDefault());
  }

  protected AnalysisChartService(ChartGroupParser chartGroupParser, DataParser dataParser,
      HeatmapListParser heatmapListParser, HeatmapDataParser heatmapParser,
      GeneChartService geneChartService) {
    resources = new MessageResources(AnalysisChartService.class, Locale.getDefault());
    this.chartGroupParser = chartGroupParser;
    this.dataParser = dataParser;
    this.heatmapListParser = heatmapListParser;
    this.heatmapParser = heatmapParser;
    this.geneChartService = geneChartService;
  }

  /**
   * Validates that file is a chart map.
   *
   * @param chartMap
   *          chart map file
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   * @throws IOException
   *           could not read file
   */
  public void validateChartMap(File chartMap, Locale locale, Consumer<String> errorHandler)
      throws IOException {
    chartGroupParser.validate(chartMap, locale, errorHandler);
  }

  /**
   * Validates that file is a heatmap list.
   *
   * @param heatmapList
   *          heatmap list file
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   * @throws IOException
   *           could not read file
   */
  public void validateHeatmapList(File heatmapList, Locale locale, Consumer<String> errorHandler)
      throws IOException {
    heatmapListParser.validate(heatmapList, locale, errorHandler);
  }

  /**
   * Returns chart map file inside folder.
   *
   * @param folder
   *          folder containing chart map
   * @param prefix
   *          file prefix
   * @return chart map file inside folder
   */
  public File chartMap(File folder, String prefix) {
    String chartMapFilename =
        resources.message("chartMap.filename", prefix == null || prefix.isEmpty() ? 0 : 1, prefix);
    return new File(folder, chartMapFilename);
  }

  /**
   * Returns heatmap list file inside output folder.
   *
   * @param folder
   *          folder containing chart map
   * @param prefix
   *          file prefix
   * @return heatmap list file inside output folder
   */
  public File heatmapList(File folder, String prefix) {
    String heatmapListFilename = resources.message("heatmapList.filename",
        prefix == null || prefix.isEmpty() ? 0 : 1, prefix);
    return new File(folder, heatmapListFilename);
  }

  /**
   * Creates and saves charts for analysis.
   *
   * @param chartMap
   *          mapping file for charts
   * @param heatmapList
   *          heatmap list file
   * @param parameters
   *          gene chart parameters
   * @param progressBar
   *          records progression
   * @throws IOException
   *           could not read data or save chart
   * @throws InterruptedException
   *           charts creation was interrupted
   */
  public void createCharts(File chartMap, File heatmapList, GeneChartParameters parameters,
      ProgressBar progressBar) throws IOException, InterruptedException {
    List<ChartGroup> chartGroups;
    if (parameters.generateAggregateGraphs) {
      logger.debug("Parse chart map file {}", chartMap);
      chartGroups = chartGroupParser.parse(chartMap);
    } else {
      chartGroups = Collections.emptyList();
    }
    List<HeatmapFile> heatmapFiles;
    if (parameters.generateHeatmap) {
      logger.debug("Parse heatmap list file {}", heatmapList);
      heatmapFiles = heatmapListParser.parse(heatmapList);
    } else {
      heatmapFiles = Collections.emptyList();
    }
    progressBar.setProgress(0.05);
    double step = 1.0 / Math.max(chartGroups.size() + heatmapFiles.size(), 1.0);
    if (parameters.generateAggregateGraphs && !chartGroups.isEmpty()) {
      for (ChartGroup chartGroup : chartGroups) {
        createGeneChart(chartGroup, chartMap.getParentFile(), parameters, progressBar.step(step));
      }
    }
    if (parameters.generateHeatmap && !heatmapFiles.isEmpty()) {
      for (HeatmapFile heatmapFile : heatmapFiles) {
        createHeatmapCharts(heatmapFile, parameters, progressBar.step(step));
      }
    }
  }

  private double mmToPixel(double mm) {
    return mm * DPI / INCH_TO_MM;
  }

  private void createGeneChart(ChartGroup chartGroup, File folder, GeneChartParameters parameters,
      ProgressBar progressBar) throws IOException, InterruptedException {
    ExceptionUtils.throwIfInterrupted("Interrupted chart creation");
    progressBar.setMessage(resources.message("chartGroup.message", chartGroup.getName()));
    // Parse data.
    List<DataGroup> dataGroups = new ArrayList<>();
    for (String resource : chartGroup.getResources()) {
      logger.debug("Parse data file {}", resource);
      List<DataGroup> resourceGroups = dataParser.parse(new File(resource));
      dataGroups.addAll(resourceGroups);
    }

    // Create charts.
    logger.debug("Create charts for group {}", chartGroup.getName());
    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    GeneChart<Number, Number> proportionChart =
        geneChartService.proportionChart(dataGroups, parameters);

    // Save charts.
    String extension = resources.message("chartMap.extension");
    File outputFile = new File(folder, chartGroup.getName() + "." + extension);
    FutureTask<WritableImage> chartSaver =
        new FutureTask<>(new GeneChartSaver(geneChart, proportionChart));
    Platform.runLater(chartSaver);
    try {
      WritableImage image = chartSaver.get();
      saveNodeOnDisk(image, extension, outputFile);
    } catch (ExecutionException e) {
      throw new PackagedRuntimeException(e);
    }
    progressBar.setProgress(1.0);
  }

  private void createHeatmapCharts(HeatmapFile heatmapFile, GeneChartParameters parameters,
      ProgressBar progressBar) throws IOException, InterruptedException {
    logger.debug("Parse heatmap file {}", heatmapFile);
    progressBar.setMessage(resources.message("heatmap.message", heatmapFile.getName()));
    final HeatmapMetadata metadata = heatmapParser.parseMetadata(heatmapFile.getFile());
    metadata.name = heatmapFile.getName();
    ExceptionUtils.throwIfInterrupted("Interrupted chart creation");
    progressBar.setProgress(0.05);

    logger.debug("Generate heatmaps for {}", heatmapFile.getName());
    try {
      List<HeatmapFeature> features = new ArrayList<HeatmapFeature>();
      heatmapParser.parseData(heatmapFile.getFile(), feature -> features.add(feature));
      String extension = resources.message("heatmap.extension");
      File output = heatmapOutput(heatmapFile.getFile(), extension, metadata, features, parameters);
      GeneChart<?, ?> heatmapChart = geneChartService.heatmapChart(metadata, features, parameters);
      try {
        saveHeatmapChart(output, heatmapChart, parameters);
      } catch (IOException | InterruptedException e) {
        throw new PackagedRuntimeException(e);
      }
      progressBar.setProgress(1.0);
    } catch (PackagedRuntimeException e) {
      ExceptionUtils.throwExceptionIfMatch(e.getCause(), IOException.class);
      ExceptionUtils.throwExceptionIfMatch(e.getCause(), InterruptedException.class);
      throw e;
    }
  }

  private File heatmapOutput(File input, String extension, HeatmapMetadata metadata,
      List<HeatmapFeature> features, GeneChartParameters parameters) {
    StringBuilder heatmapFilename = new StringBuilder(FilenameUtils.getBaseName(metadata.name));
    if (parameters.heatmapParameters != null && !parameters.heatmapParameters.axisVisible) {
      heatmapFilename.append("_image");
    } else {
      heatmapFilename.append("_heatmap");
    }
    heatmapFilename.append("_");
    heatmapFilename.append(features.get(0).name.replaceAll("\\W", "_"));
    heatmapFilename.append("-");
    heatmapFilename.append(features.get(features.size() - 1).name.replaceAll("\\W", "_"));
    heatmapFilename.append(".");
    heatmapFilename.append(extension);
    return new File(input.getParentFile(), heatmapFilename.toString());
  }

  private void saveHeatmapChart(File output, GeneChart<?, ?> heatmapChart,
      GeneChartParameters parameters) throws IOException, InterruptedException {
    // Save chart.
    String extension = FilenameUtils.getExtension(output.getName());
    FutureTask<WritableImage> chartSaver =
        new FutureTask<>(new HeatmapChartSaver(heatmapChart, parameters.heatmapParameters));
    Platform.runLater(chartSaver);
    try {
      WritableImage image = chartSaver.get();
      saveNodeOnDisk(image, extension, output);
    } catch (ExecutionException e) {
      throw new PackagedRuntimeException(e);
    }
    if (parameters.heatmapParameters != null && !parameters.heatmapParameters.axisVisible) {
      // Save legend.
      File legendOutput = new File(output.getParentFile(),
          FilenameUtils.getBaseName(output.getName()) + "_legend." + extension);
      chartSaver =
          new FutureTask<>(new HeatmapLegendSaver(heatmapChart, parameters.heatmapParameters));
      Platform.runLater(chartSaver);
      try {
        WritableImage image = chartSaver.get();
        saveNodeOnDisk(image, extension, legendOutput);
      } catch (ExecutionException e) {
        throw new PackagedRuntimeException(e);
      }
      // Save X axis.
      File xaxisOutput = new File(output.getParentFile(),
          FilenameUtils.getBaseName(output.getName()) + "_X-axis." + extension);
      chartSaver =
          new FutureTask<>(new HeatmapXAxisSaver(heatmapChart, parameters.heatmapParameters));
      Platform.runLater(chartSaver);
      try {
        WritableImage image = chartSaver.get();
        saveNodeOnDisk(image, extension, xaxisOutput);
      } catch (ExecutionException e) {
        throw new PackagedRuntimeException(e);
      }
    }
  }

  private void saveNodeOnDisk(WritableImage image, String type, File file) {
    try {
      BufferedImage awtImage = SwingFXUtils.fromFXImage(image, null);

      boolean saved = false;
      Iterator<ImageWriter> writerIterator = ImageIO.getImageWritersByFormatName(type);
      while (writerIterator.hasNext()) {
        ImageWriter writer = writerIterator.next();
        ImageWriteParam writeParam = writer.getDefaultWriteParam();
        ImageTypeSpecifier typeSpecifier =
            ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_ARGB);
        IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);
        if (metadata.isReadOnly() || !metadata.isStandardMetadataFormatSupported()) {
          continue;
        }

        setDpi(metadata);

        try (ImageOutputStream stream =
            ImageIO.createImageOutputStream(Files.newOutputStream(file.toPath()))) {
          writer.setOutput(stream);
          writer.write(metadata, new IIOImage(awtImage, null, metadata), writeParam);
        }
        saved = true;
      }
      if (!saved) {
        throw new IllegalStateException("Could not find a proper ImageWriter for PNG");
      }

      //ImageIO.write(awtImage, type, file);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void setDpi(IIOMetadata metadata) throws IIOInvalidTreeException {
    double dotsPerMilli = 1.0 * DPI / INCH_TO_MM;

    IIOMetadataNode horiz = new IIOMetadataNode("HorizontalPixelSize");
    horiz.setAttribute("value", Double.toString(dotsPerMilli));

    IIOMetadataNode vert = new IIOMetadataNode("VerticalPixelSize");
    vert.setAttribute("value", Double.toString(dotsPerMilli));

    IIOMetadataNode dim = new IIOMetadataNode("Dimension");
    dim.appendChild(horiz);
    dim.appendChild(vert);

    IIOMetadataNode root = new IIOMetadataNode("javax_imageio_1.0");
    root.appendChild(dim);

    metadata.mergeTree("javax_imageio_1.0", root);
  }
}

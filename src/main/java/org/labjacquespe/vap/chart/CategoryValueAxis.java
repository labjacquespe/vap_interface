/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.ObservableMap;
import javafx.geometry.Dimension2D;
import javafx.scene.chart.ValueAxis;
import javafx.util.Duration;

/**
 * {@link ValueAxis} that allows text for ticks.
 */
public class CategoryValueAxis extends ValueAxis<Integer> {
  private MapProperty<Integer, String> tickLabels = new SimpleMapProperty<>();

  public final ObservableMap<Integer, String> getTickLabels() {
    return tickLabels.get();
  }

  public final void setTickLabels(ObservableMap<Integer, String> tickLabels) {
    this.tickLabels.set(tickLabels);
  }

  private IntegerProperty tickUnit = new SimpleIntegerProperty(5);

  public final int getTickUnit() {
    return tickUnit.get();
  }

  public final void setTickUnit(int value) {
    tickUnit.set(value);
  }

  /**
   * Creates a new CategoricalValueAxis.
   */
  public CategoryValueAxis() {
  }

  /**
   * Creates a new CategoricalValueAxis.
   *
   * @param tickLabels
   *          labels for ticks
   */
  public CategoryValueAxis(ObservableMap<Integer, String> tickLabels) {
    setTickLabels(tickLabels);
  }

  /**
   * Creates a new CategoricalValueAxis.
   *
   * @param lowerBound
   *          lower bound
   * @param upperBound
   *          upper bound
   * @param tickUnit
   *          tick unit
   */
  public CategoryValueAxis(int lowerBound, int upperBound, int tickUnit) {
    super(lowerBound, upperBound);
    setTickUnit(tickUnit);
  }

  /**
   * Creates a new CategoricalValueAxis.
   *
   * @param lowerBound
   *          lower bound
   * @param upperBound
   *          upper bound
   * @param tickUnit
   *          tick unit
   * @param tickLabels
   *          labels for ticks
   */
  public CategoryValueAxis(int lowerBound, int upperBound, int tickUnit,
      ObservableMap<Integer, String> tickLabels) {
    super(lowerBound, upperBound);
    setTickUnit(tickUnit);
    setTickLabels(tickLabels);
  }

  /**
   * Creates a new CategoricalValueAxis.
   *
   * @param axisLabel
   *          axis label
   * @param lowerBound
   *          lower bound
   * @param upperBound
   *          upper bound
   * @param tickUnit
   *          tick unit
   */
  public CategoryValueAxis(String axisLabel, int lowerBound, int upperBound, int tickUnit) {
    super(lowerBound, upperBound);
    setTickUnit(tickUnit);
    setLabel(axisLabel);
  }

  /**
   * Creates a new CategoricalValueAxis.
   *
   * @param axisLabel
   *          axis label
   * @param lowerBound
   *          lower bound
   * @param upperBound
   *          upper bound
   * @param tickUnit
   *          tick unit
   * @param tickLabels
   *          labels for ticks
   */
  public CategoryValueAxis(String axisLabel, int lowerBound, int upperBound, int tickUnit,
      ObservableMap<Integer, String> tickLabels) {
    super(lowerBound, upperBound);
    setTickUnit(tickUnit);
    setLabel(axisLabel);
    setTickLabels(tickLabels);
  }

  @Override
  protected List<Integer> calculateMinorTickMarks() {
    final List<Integer> minorTickMarks = new ArrayList<>();
    final int lowerBound = (int) getLowerBound();
    final int upperBound = (int) getUpperBound();
    final int tickUnit = getTickUnit();
    final int minorUnit = Math.max(1, tickUnit / Math.max(1, getMinorTickCount()));
    if (tickUnit > 0) {
      if (((upperBound - lowerBound) / minorUnit) > 10000) {
        // This is a ridiculous amount of major tick marks, something has probably gone wrong
        System.err
            .println("Warning we tried to create more than 10000 minor tick marks on a NumberAxis. "
                + "Lower Bound=" + getLowerBound() + ", Upper Bound=" + getUpperBound()
                + ", Tick Unit=" + tickUnit);
        return minorTickMarks;
      }
      int major = lowerBound;
      int count = 1 + (upperBound - major) / tickUnit;
      for (int i = 0; major < upperBound && i < count; major += tickUnit, i++) {
        final int next = Math.min(major + tickUnit, upperBound);
        int minor = major + minorUnit;
        int minorCount = (next - minor) / minorUnit;
        for (int j = 0; minor < next && j < minorCount; minor += minorUnit, j++) {
          minorTickMarks.add(minor);
        }
      }
    }
    return minorTickMarks;
  }

  @Override
  protected void setRange(Object range, boolean animate) {
    Object[] rangeProperties = (Object[]) range;
    double lowerBound = (double) rangeProperties[0];
    double upperBound = (double) rangeProperties[1];
    double scale = (double) rangeProperties[3];
    final double oldLowerBound = getLowerBound();
    setLowerBound(lowerBound);
    setUpperBound(upperBound);
    setScale(scale);
    if (animate) {
      Timeline timeline = new Timeline();
      timeline.getKeyFrames().addAll(
          new KeyFrame(Duration.ZERO, new KeyValue(currentLowerBound, oldLowerBound)),
          new KeyFrame(Duration.millis(700), new KeyValue(currentLowerBound, lowerBound)));
      timeline.play();
    } else {
      currentLowerBound.set(lowerBound);
    }
  }

  @Override
  protected Object getRange() {
    return new Object[] { getLowerBound(), getUpperBound(), getTickUnit(), getScale() };
  }

  @Override
  @SuppressWarnings("checkstyle:linelength")
  protected Object autoRange(double minValue, double maxValue, double length, double labelSize) {
    // calculate the number of tick-marks we can fit in the given length
    int numOfTickMarks = (int) Math.floor(length / labelSize);
    // can never have less than 2 tick marks one for each end
    numOfTickMarks = Math.max(numOfTickMarks, 2);
    int minorTickCount = Math.max(getMinorTickCount(), 1);

    double range = maxValue - minValue;

    if (range != 0 && range / (numOfTickMarks * minorTickCount) <= Math.ulp(minValue)) {
      range = 0;
    }
    // pad min and max by 2%, checking if the range is zero
    final double paddedRange =
        (range == 0) ? minValue == 0 ? 2 : Math.abs(minValue) * 0.02 : Math.abs(range) * 1.02;
    final double padding = (paddedRange - range) / 2;
    // if min and max are not zero then add padding to them
    double paddedMin = minValue - padding;
    double paddedMax = maxValue + padding;
    // check padding has not pushed min or max over zero line
    if ((paddedMin < 0 && minValue >= 0) || (paddedMin > 0 && minValue <= 0)) {
      // padding pushed min above or below zero so clamp to 0
      paddedMin = 0;
    }
    if ((paddedMax < 0 && maxValue >= 0) || (paddedMax > 0 && maxValue <= 0)) {
      // padding pushed min above or below zero so clamp to 0
      paddedMax = 0;
    }
    // calculate tick unit for the number of ticks can have in the given data range
    double tickUnit = paddedRange / numOfTickMarks;
    // search for the best tick unit that fits
    double tickUnitRounded = 0;
    double minRounded = 0;
    double maxRounded = 0;
    int count = 0;
    double reqLength = Double.MAX_VALUE;
    String formatter = "0.00000000";
    // loop till we find a set of ticks that fit length and result in a total of less than 20 tick marks
    while (reqLength > length || count > 20) {
      int exp = (int) Math.floor(Math.log10(tickUnit));
      final double mant = tickUnit / Math.pow(10, exp);
      double ratio = mant;
      if (mant > 5d) {
        exp++;
        ratio = 1;
      } else if (mant > 1d) {
        ratio = mant > 2.5 ? 5 : 2.5;
      }
      if (exp > 1) {
        formatter = "#,##0";
      } else if (exp == 1) {
        formatter = "0";
      } else {
        final boolean ratioHasFrac = Math.rint(ratio) != ratio;
        final StringBuilder formatterB = new StringBuilder("0");
        int n = ratioHasFrac ? Math.abs(exp) + 1 : Math.abs(exp);
        if (n > 0) {
          formatterB.append(".");
        }
        for (int i = 0; i < n; ++i) {
          formatterB.append("0");
        }
        formatter = formatterB.toString();

      }
      tickUnitRounded = ratio * Math.pow(10, exp);
      // move min and max to nearest tick mark
      minRounded = Math.floor(paddedMin / tickUnitRounded) * tickUnitRounded;
      maxRounded = Math.ceil(paddedMax / tickUnitRounded) * tickUnitRounded;
      // calculate the required length to display the chosen tick marks for real, this will handle if there are
      // huge numbers involved etc or special formatting of the tick mark label text
      double maxReqTickGap = 0;
      double last = 0;
      count = (int) Math.ceil((maxRounded - minRounded) / tickUnitRounded);
      double major = minRounded;
      for (int i = 0; major <= maxRounded && i < count; major += tickUnitRounded, i++) {
        Dimension2D markSize = measureTickMarkSize(major, getTickLabelRotation(), formatter);
        double size = getSide().isVertical() ? markSize.getHeight() : markSize.getWidth();
        if (i == 0) { // first
          last = size / 2;
        } else {
          maxReqTickGap = Math.max(maxReqTickGap, last + 6 + (size / 2));
        }
      }
      reqLength = (count - 1) * maxReqTickGap;
      tickUnit = tickUnitRounded;

      // fix for RT-35600 where a massive tick unit was being selected
      // unnecessarily. There is probably a better solution, but this works
      // well enough for now.
      if (numOfTickMarks == 2 && reqLength > length) {
        break;
      }
      if (reqLength > length || count > 20) {
        tickUnit *= 2; // This is just for the while loop, if there are still too many ticks
      }
    }
    // calculate new scale
    final double newScale = calculateNewScale(length, minRounded, maxRounded);
    // return new range
    return new Object[] { minRounded, maxRounded, (int) tickUnitRounded, newScale };
  }

  @Override
  protected List<Integer> calculateTickValues(double length, Object range) {
    final Object[] rangeProps = (Object[]) range;
    final int lowerBound = ((Double) rangeProps[0]).intValue();
    final int upperBound = ((Double) rangeProps[1]).intValue();
    final int tickUnit = (int) rangeProps[2];
    List<Integer> tickValues = new ArrayList<>();
    if (lowerBound == upperBound) {
      tickValues.add(lowerBound);
    } else if (tickUnit <= 0) {
      tickValues.add(lowerBound);
      tickValues.add(upperBound);
    } else if (tickUnit > 0) {
      tickValues.add(lowerBound);
      if (((upperBound - lowerBound) / tickUnit) > 2000) {
        // This is a ridiculous amount of major tick marks, something has probably gone wrong
        System.err
            .println("Warning we tried to create more than 2000 major tick marks on a NumberAxis. "
                + "Lower Bound=" + lowerBound + ", Upper Bound=" + upperBound + ", Tick Unit="
                + tickUnit);
      } else {
        if (lowerBound + tickUnit < upperBound) {
          int major = lowerBound + tickUnit;
          int count = (upperBound - lowerBound) / tickUnit;
          for (int i = 0; major < upperBound && i < count; major += tickUnit, i++) {
            tickValues.add(major);
          }
        }
      }
      tickValues.add(upperBound);
    }
    return tickValues;
  }

  private Dimension2D measureTickMarkSize(Number value, double rotation, String numFormatter) {
    String labelText =
        value != null ? Objects.toString(getTickMarkLabel(value.intValue()), "") : "";
    return measureTickMarkLabelSize(labelText, rotation);
  }

  protected Dimension2D measureTickMarkSize(Number value, Object range) {
    final Object[] rangeProps = (Object[]) range;
    final String formatter = (String) rangeProps[4];
    return measureTickMarkSize(value, getTickLabelRotation(), formatter);
  }

  @Override
  protected String getTickMarkLabel(Integer value) {
    String label = tickLabels.get(value);
    return Objects.toString(label, "");
  }
}

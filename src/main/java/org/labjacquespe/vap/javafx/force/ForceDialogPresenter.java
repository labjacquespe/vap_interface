/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.javafx.force;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.labjacquespe.vap.gui.SelectableLabel;
import org.labjacquespe.vap.javafx.force.ForceDialog.ForceOption;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Force dialog presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ForceDialogPresenter {
  private final ObjectProperty<ForceOption> optionProperty = new SimpleObjectProperty<>();
  @FXML
  private BorderPane view;
  @FXML
  private ScrollPane scrollPane;
  @FXML
  private Pane messageBox;
  @FXML
  private Label forceMessage;
  @FXML
  private Button cancel;

  @FXML
  private void initialize() {
    messageBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

    cancel.requestFocus();
  }

  /**
   * Sets messages.
   * 
   * @param messages
   *          messages
   */
  public void setMessages(ObservableList<String> messages) {
    for (String message : messages) {
      messageBox.getChildren().add(new SelectableLabel(message));
    }
  }

  public void setForceMessage(String message) {
    forceMessage.setText(message);
  }

  public ObjectProperty<ForceOption> optionProperty() {
    return optionProperty;
  }

  @FXML
  private void force(ActionEvent event) {
    optionProperty.set(ForceOption.FORCE);
    view.getScene().getWindow().hide();
  }

  @FXML
  private void cancel(ActionEvent event) {
    optionProperty.set(ForceOption.CANCEL);
    view.getScene().getWindow().hide();
  }

  void computeMessageBoxBounds() {
    scrollPane.setContent(null);
    Stage messageBoundsStage = new Stage();
    view.setCenter(messageBox);
    Scene messageBoundsScene = new Scene(view);
    messageBoundsStage.setScene(messageBoundsScene);
    messageBoundsScene.getStylesheets().add("application.css");
    view.snapshot(null, null);
    Bounds bounds = view.getBoundsInLocal();
    view.setPrefWidth(bounds.getWidth() + 15);
    view.setPrefHeight(bounds.getHeight() + 15);
    view.setCenter(scrollPane);
    messageBoundsScene.setRoot(new Label());
    scrollPane.setContent(messageBox);
  }

  void focusOnDefault() {
    cancel.requestFocus();
  }
}

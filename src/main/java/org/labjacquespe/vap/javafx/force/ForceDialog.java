/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.javafx.force;

import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.labjacquespe.vap.gui.JavafxUtils;

/**
 * Force dialog.
 */
public class ForceDialog {
  public static enum ForceOption {
    FORCE, CANCEL
  }

  private Stage stage;
  private ForceDialogPresenter presenter;

  public ForceDialog(Window owner, String title, String forceMessage, String... messages) {
    this(owner, title, forceMessage, Arrays.asList(messages));
  }

  /**
   * Creates confirm dialog.
   *
   * @param owner
   *          dialog's owner
   * @param title
   *          dialog's title
   * @param forceMessage
   *          force message
   * @param messages
   *          dialog's messages
   */
  public ForceDialog(Window owner, String title, String forceMessage, List<String> messages) {
    ForceDialogView view = new ForceDialogView();
    presenter = (ForceDialogPresenter) view.getPresenter();
    presenter.setForceMessage(forceMessage);
    presenter.setMessages(FXCollections.observableList(messages));
    presenter.computeMessageBoxBounds();

    stage = new Stage();
    stage.initOwner(owner);
    stage.initModality(Modality.WINDOW_MODAL);
    JavafxUtils.setMaxSizeForScreen(stage);
    Parent root = view.getView();
    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setTitle(title);
    scene.getStylesheets().add("application.css");
    Image icon = getIcon();
    if (icon != null) {
      stage.getIcons().add(icon);
    }
    presenter.focusOnDefault();
    stage.showAndWait();
  }

  private Image getIcon() {
    Image image = new Image(getResourcePath("dialog-warning.png"));
    return image;
  }

  private String getResourcePath(String resource) {
    String resourceFolder = getClass().getPackage().getName().replaceAll("\\.", "/");
    return resourceFolder + "/" + resource;
  }

  public ForceOption getOption() {
    return presenter.optionProperty().get();
  }
}

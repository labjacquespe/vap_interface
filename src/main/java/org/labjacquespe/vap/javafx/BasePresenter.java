/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.javafx;

import java.util.ResourceBundle;
import javafx.fxml.FXML;
import org.labjacquespe.vap.message.Messages;

/**
 * Basic functionalities for JavaFX presenter.
 */
public abstract class BasePresenter implements Messages {
  @FXML
  private ResourceBundle resources;

  @Override
  public ResourceBundle getResources() {
    return resources;
  }

  public void setMammalianDefaults() {
  }

  public void setYeastDefaults() {
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.javafx.progress;

import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.labjacquespe.vap.gui.JavafxUtils;

/**
 * Progress dialog for file filtering.
 */
public class ProgressDialog {
  private Stage stage;
  private Worker<?> worker;

  /**
   * Creates a progress dialog.
   * 
   * @param owner
   *          progress dialog owner
   * @param worker
   *          worker to link to this progress dialog
   */
  public ProgressDialog(Window owner, final Worker<?> worker) {
    this.worker = worker;
    stage = new Stage();
    stage.initOwner(owner);
    stage.initModality(Modality.WINDOW_MODAL);

    ProgressDialogView view = new ProgressDialogView();
    final ProgressDialogPresenter presenter = (ProgressDialogPresenter) view.getPresenter();
    ResourceBundle resources = view.getResourceBundle();
    Parent root = view.getView();
    JavafxUtils.setMaxSizeForScreen(stage);

    Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.setTitle(resources.getString("title"));

    presenter.progressProperty().bind(worker.progressProperty());
    presenter.messageProperty().bind(worker.messageProperty());
    presenter.titleProperty().bind(worker.titleProperty());
    presenter.cancelledProperty().addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> obv, Boolean ov, Boolean nv) {
        worker.cancel();
      }
    });

    stage.show();
  }

  public void hide() {
    worker.cancel();
    stage.close();
  }

  public void show() {
    stage.show();
  }
}

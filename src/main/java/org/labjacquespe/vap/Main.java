/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import com.beust.jcommander.JCommander;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import javafx.application.Application;
import org.apache.commons.io.IOUtils;
import org.labjacquespe.vap.chart.CreateGeneChartApplication;
import org.labjacquespe.vap.core.AnalysisApplication;
import org.labjacquespe.vap.gui.MainApplication;
import org.labjacquespe.vap.gui.MainPreloader;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Starts VAP.
 */
@SpringBootApplication
public class Main {
  /**
   * Starts VAP.
   *
   * @param args
   *          command line arguments
   */
  public static void main(String[] args) {
    MainCommand mainCommand = new MainCommand();
    JCommander jc = JCommander.newBuilder().addObject(mainCommand).addCommand(new RunCommand())
        .addCommand(new CreateGraphCommand()).build();
    jc.parse(args);
    if (mainCommand.isHelp()) {
      showHelp();
      System.exit(0);
    } else if (args.length == 0 || contains(args, "gui")) {
      System.setProperty("javafx.preloader", MainPreloader.class.getName());
      Application.launch(MainApplication.class, args);
    } else if (contains(args, CreateGraphCommand.CREATE_GRAPH_COMMAND)) {
      Application.launch(CreateGeneChartApplication.class, args);
    } else if (contains(args, RunCommand.RUN_COMMAND)) {
      Application.launch(AnalysisApplication.class, args);
    } else {
      showHelp();
      System.exit(0);
    }
  }

  private static boolean contains(String[] elements, String toFind) {
    boolean found = false;
    for (String element : elements) {
      found |= element.equals(toFind);
    }
    return found;
  }

  private static void showHelp() {
    try {
      try (Reader reader = new BufferedReader(
          new InputStreamReader(Main.class.getResourceAsStream("/commandLineHelp.txt"), "UTF-8"))) {
        IOUtils.copy(reader, System.out, Charset.defaultCharset());
      }
    } catch (IOException e) {
      System.err.print("Error: could not open help file");
    }
  }
}

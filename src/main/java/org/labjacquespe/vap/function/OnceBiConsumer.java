/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.function;

import java.util.function.BiConsumer;

/**
 * {@link BiConsumer} that runs only once.
 */
public class OnceBiConsumer<T, U> implements BiConsumer<T, U> {
  private BiConsumer<T, U> delegate;
  private boolean consumed = false;

  public OnceBiConsumer(BiConsumer<T, U> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void accept(T firstValue, U secondValue) {
    if (!consumed) {
      delegate.accept(firstValue, secondValue);
    }
    consumed = true;
  }

  public static <T, U> OnceBiConsumer<T, U> once(BiConsumer<T, U> delegate) {
    return new OnceBiConsumer<>(delegate);
  }
}

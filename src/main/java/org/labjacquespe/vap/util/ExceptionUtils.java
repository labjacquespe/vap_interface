/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.util;

/**
 * Utilities for exceptions.
 */
public class ExceptionUtils {
  /**
   * Throws an {@link InterruptedException} if current thread is interrupted.
   *
   * @param message
   *          exception's message
   * @throws InterruptedException
   *           if current thread is interrupted
   */
  public static void throwIfInterrupted(String message) throws InterruptedException {
    if (Thread.currentThread().isInterrupted()) {
      throw new InterruptedException(message);
    }
  }

  /**
   * Throws exception if it's assignable to clazz. Otherwise, this method does nothing.
   *
   * @param <T>
   *          exception type
   * @param exception
   *          exception
   * @param clazz
   *          expected exception's clazz
   * @throws T
   *           if exception is assignable to clazz
   */
  public static <T extends Throwable> void throwExceptionIfMatch(Throwable exception,
      Class<? extends T> clazz) throws T {
    if (exception != null && clazz.isAssignableFrom(exception.getClass())) {
      @SuppressWarnings("unchecked")
      T exceptionCast = (T) exception;
      throw exceptionCast;
    }
  }
}

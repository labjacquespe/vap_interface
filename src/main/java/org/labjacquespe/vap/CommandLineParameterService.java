/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import static org.labjacquespe.vap.chart.CreateGraphFilesType.LIST_FILES;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.labjacquespe.vap.chart.CreateGraphFiles;
import org.labjacquespe.vap.chart.CreateGraphFilesType;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.validation.ConsumerMarker;
import org.springframework.stereotype.Component;

/**
 * Service for command line parameters.
 */
@Component
public class CommandLineParameterService {
  public static interface ParsedGeneChartArguments {
    public CreateGraphFiles getFiles();

    public GeneChartParameters getParameters();
  }

  private static class ParsedGeneChartArgumentsDefault implements ParsedGeneChartArguments {
    private CreateGraphFiles files = new CreateGraphFiles();
    private GeneChartParameters parameters;

    @Override
    public CreateGraphFiles getFiles() {
      return files;
    }

    @Override
    public GeneChartParameters getParameters() {
      return parameters;
    }
  }

  private static final String MISSING_PARAMETER = "MISSING_PARAMETER";
  private static final String MISSING_FILE_TYPE = "MISSING_FILE_TYPE";
  private static final String MULTIPLE_FILE_TYPE = "MULTIPLE_FILE_TYPE";
  private static final String FILE_NOT_EXISTS = "FILE_NOT_EXISTS";
  private static final String INVALID_PARAMETER = "INVALID_PARAMETER";
  private static final String MISSING_FILE = "MISSING_FILE";
  private static final String INVALID_MAP = "INVALID_MAP";
  private static final String INVALID_HEATMAP_LIST = "INVALID_HEATMAP_LIST";
  private static final String INVALID_GENERATE_AGGREGATE_GRAPHS =
      "INVALID_GENERATE_AGGREGATE_GRAPHS";
  private static final String INVALID_OUTPUT_HEATMAP = "INVALID_OUTPUT_HEATMAP";
  private static final String INVALID_DISPERSION = "INVALID_DISPERSION";
  private static final String INVALID_YAXIS_SCALE = "INVALID_YAXIS_SCALE";
  @Inject
  private ParameterFileService parameterFileService;
  @Inject
  private AnalysisChartService analysisChartService;

  protected CommandLineParameterService() {
  }

  protected CommandLineParameterService(ParameterFileService parameterFileService,
      AnalysisChartService analysisChartService) {
    this.parameterFileService = parameterFileService;
    this.analysisChartService = analysisChartService;
  }

  /**
   * Validates arguments for gene chart creation.
   *
   * @param args
   *          arguments for gene chart creation
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles validation error
   * @throws IOException
   *           could not parse parameters/map file
   */
  public void validateGeneChartArguments(String[] args, Locale locale,
      Consumer<String> errorHandler) throws IOException {
    final MessageResources resources =
        new MessageResources(CommandLineParameterService.class.getName(), locale);
    MainCommand mainCommand = new MainCommand();
    CreateGraphCommand createGraphCommand = new CreateGraphCommand();
    JCommander jc =
        JCommander.newBuilder().addObject(mainCommand).addCommand(createGraphCommand).build();
    try {
      jc.parse(args);
    } catch (ParameterException e) {
      errorHandler.accept(e.getMessage());
      return;
    }

    if (!jc.getParsedCommand().equals(CreateGraphCommand.CREATE_GRAPH_COMMAND)) {
      errorHandler
          .accept(resources.message(MISSING_PARAMETER, CreateGraphCommand.CREATE_GRAPH_COMMAND));
    }
    boolean parameterFilePresent = createGraphCommand.getParameterFile() != null;
    boolean listFilePresent = createGraphCommand.getMapGraphsFile() != null
        || createGraphCommand.getListHeatmapFile() != null;
    int fileTypeCount = 0;
    if (parameterFilePresent) {
      fileTypeCount++;
    }
    if (listFilePresent) {
      fileTypeCount++;
    }
    boolean generateAggregateGraphs = true;
    boolean generateHeatmap = false;
    if (createGraphCommand.getGenerateAggregateGraphs() != null) {
      generateAggregateGraphs = parameterFileService
          .parseGenerateAggregateGraphs(createGraphCommand.getGenerateAggregateGraphs());
    }
    if (createGraphCommand.getGenerateHeatmap() != null) {
      generateHeatmap =
          parameterFileService.parseGenerateHeatmap(createGraphCommand.getGenerateHeatmap());
    }

    if (fileTypeCount == 0) {
      errorHandler.accept(resources.message(MISSING_FILE_TYPE));
    } else if (fileTypeCount > 1) {
      errorHandler.accept(resources.message(MULTIPLE_FILE_TYPE));
    } else {
      if (parameterFilePresent) {
        String fileArgument = createGraphCommand.getParameterFile();
        File file = new File(fileArgument);
        if (!file.isFile()) {
          errorHandler.accept(resources.message(FILE_NOT_EXISTS, fileArgument));
        } else {
          ConsumerMarker<String> parametersErrorHandler = new ConsumerMarker<>(
              error -> errorHandler.accept(resources.message(INVALID_PARAMETER, error)));
          parameterFileService.validate(file, locale, parametersErrorHandler);
          if (!parametersErrorHandler.isConsumed()) {
            AnalysisParameters analysisParameters = parameterFileService.parse(file);
            if (generateAggregateGraphs) {
              File chartMap = analysisChartService.chartMap(analysisParameters.outputFolder,
                  analysisParameters.ouputFilePrefix);
              if (!chartMap.isFile()) {
                errorHandler.accept(resources.message(MISSING_FILE, chartMap));
              } else {
                analysisChartService.validateChartMap(chartMap, locale,
                    error -> errorHandler.accept(resources.message(INVALID_MAP, error)));
              }
            }
            if (generateHeatmap) {
              File heatmapList = analysisChartService.heatmapList(analysisParameters.outputFolder,
                  analysisParameters.ouputFilePrefix);
              if (!heatmapList.isFile()) {
                errorHandler.accept(resources.message(MISSING_FILE, heatmapList));
              } else {
                analysisChartService.validateHeatmapList(heatmapList, locale,
                    error -> errorHandler.accept(resources.message(INVALID_MAP, error)));
              }
            }
          }
        }
      } else if (listFilePresent) {
        if (createGraphCommand.getMapGraphsFile() != null) {
          String fileArgument = createGraphCommand.getMapGraphsFile();
          File file = new File(fileArgument);
          if (!file.isFile()) {
            errorHandler.accept(resources.message(FILE_NOT_EXISTS, fileArgument));
          } else if (generateAggregateGraphs) {
            analysisChartService.validateChartMap(file, locale,
                error -> errorHandler.accept(resources.message(INVALID_MAP, error)));
          }
        }
        if (createGraphCommand.getListHeatmapFile() != null) {
          String fileArgument = createGraphCommand.getListHeatmapFile();
          File file = new File(fileArgument);
          if (!file.isFile()) {
            errorHandler.accept(resources.message(FILE_NOT_EXISTS, fileArgument));
          } else if (generateHeatmap) {
            analysisChartService.validateHeatmapList(file, locale,
                error -> errorHandler.accept(resources.message(INVALID_HEATMAP_LIST, error)));
          }
        }
      }
    }

    if (createGraphCommand.getGenerateAggregateGraphs() != null) {
      parameterFileService.validateGenerateHeatmap(createGraphCommand.getGenerateAggregateGraphs(),
          locale, error -> errorHandler
              .accept(resources.message(INVALID_GENERATE_AGGREGATE_GRAPHS, error)));
    }
    if (createGraphCommand.getGenerateHeatmap() != null) {
      parameterFileService.validateGenerateHeatmap(createGraphCommand.getGenerateHeatmap(), locale,
          error -> errorHandler.accept(resources.message(INVALID_OUTPUT_HEATMAP, error)));
    }
    if (createGraphCommand.getDipersion() != null) {
      parameterFileService.validateDispersion(createGraphCommand.getDipersion(), locale,
          error -> errorHandler.accept(resources.message(INVALID_DISPERSION, error)));
    }
    if (createGraphCommand.getYaxisScale() != null) {
      parameterFileService.validateYaxisScale(createGraphCommand.getYaxisScale(), locale,
          error -> errorHandler.accept(resources.message(INVALID_YAXIS_SCALE, error)));
    }
  }

  private GeneChartParameters defaultGeneChartParameters() {
    GeneChartParameters chartParameters = new GeneChartParameters();
    chartParameters.generateAggregateGraphs = true;
    chartParameters.referenceType = null;
    chartParameters.representationType = null;
    chartParameters.boundary = null;
    chartParameters.blocks = null;
    return chartParameters;
  }

  /**
   * Parse arguments for gene chart creation.
   *
   * @param args
   *          arguments for gene chart creation
   * @return parsed arguments for gene chart creation
   * @throws IOException
   *           could not parse parameters/map file
   */
  public ParsedGeneChartArguments parseGeneChartArguments(String[] args) throws IOException {
    ParsedGeneChartArgumentsDefault parsedArguments = new ParsedGeneChartArgumentsDefault();
    GeneChartParameters chartParameters = defaultGeneChartParameters();

    MainCommand mainCommand = new MainCommand();
    CreateGraphCommand createGraphCommand = new CreateGraphCommand();
    JCommander.newBuilder().addObject(mainCommand).addCommand(createGraphCommand).build()
        .parse(args);
    if (createGraphCommand.getMapGraphsFile() != null) {
      File file = new File(createGraphCommand.getMapGraphsFile());
      parsedArguments.files.type(LIST_FILES);
      parsedArguments.files.chartMapFile(file);
    }
    if (createGraphCommand.getListHeatmapFile() != null) {
      File file = new File(createGraphCommand.getListHeatmapFile());
      parsedArguments.files.type(LIST_FILES);
      parsedArguments.files.listHeatmapFile(file);
    }
    if (createGraphCommand.getParameterFile() != null) {
      File file = new File(createGraphCommand.getParameterFile());
      AnalysisParameters analysisParameters = parameterFileService.parse(file);
      parsedArguments.files.type(CreateGraphFilesType.PARAMETER_FILE);
      parsedArguments.files.parameterFile(file);
      chartParameters = analysisParameters.geneChartParameters;
    }

    parsedArguments.parameters = chartParameters;
    if (createGraphCommand.getGenerateAggregateGraphs() != null) {
      chartParameters.generateAggregateGraphs = parameterFileService
          .parseGenerateAggregateGraphs(createGraphCommand.getGenerateAggregateGraphs());
    }
    if (createGraphCommand.getGenerateHeatmap() != null) {
      chartParameters.generateHeatmap =
          parameterFileService.parseGenerateHeatmap(createGraphCommand.getGenerateHeatmap());
    }
    if (createGraphCommand.getDipersion() != null) {
      chartParameters.dispersion =
          parameterFileService.parseDispersion(createGraphCommand.getDipersion());
    }
    if (createGraphCommand.getYaxisScale() != null) {
      chartParameters.yaxisScale =
          parameterFileService.parseYaxisScale(createGraphCommand.getYaxisScale());
    }
    return parsedArguments;
  }

  /**
   * Validates arguments for automatic execution.
   *
   * @param args
   *          arguments for automatic execution
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles validation error
   * @throws IOException
   *           could not parse parameters file
   */
  public void validateRunArguments(String[] args, Locale locale, Consumer<String> errorHandler)
      throws IOException {
    final MessageResources resources =
        new MessageResources(CommandLineParameterService.class.getName(), locale);
    MainCommand mainCommand = new MainCommand();
    RunCommand runCommand = new RunCommand();
    JCommander jc = JCommander.newBuilder().addObject(mainCommand).addCommand(runCommand).build();
    try {
      jc.parse(args);
    } catch (ParameterException e) {
      errorHandler.accept(e.getMessage());
      return;
    }

    if (!jc.getParsedCommand().equals(RunCommand.RUN_COMMAND)) {
      errorHandler.accept(resources.message(MISSING_PARAMETER, RunCommand.RUN_COMMAND));
    }
    String fileArgument = runCommand.getParameterFile();
    File file = new File(fileArgument);
    if (!file.isFile()) {
      errorHandler.accept(resources.message(FILE_NOT_EXISTS, fileArgument));
    } else {
      parameterFileService.validate(file, locale,
          error -> errorHandler.accept(resources.message(INVALID_PARAMETER, error)));
    }
  }

  /**
   * Parse arguments for automatic execution.
   *
   * @param args
   *          arguments for automatic execution
   * @return analysis parameters
   * @throws IOException
   *           could not parse parameters file
   */
  public AnalysisParameters parseRunArguments(String[] args) throws IOException {
    MainCommand mainCommand = new MainCommand();
    RunCommand runCommand = new RunCommand();
    JCommander.newBuilder().addObject(mainCommand).addCommand(runCommand).build().parse(args);

    if (runCommand.getParameterFile() != null) {
      File file = new File(runCommand.getParameterFile());
      return parameterFileService.parse(file);
    } else {
      return new AnalysisParameters();
    }
  }
}

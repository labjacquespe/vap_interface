/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import javax.inject.Inject;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.springframework.stereotype.Component;

/**
 * Factory for {@link AnalysisTask}.
 */
@Component
public class AnalysisTaskFactory {
  @Inject
  private AnalysisService analysisService;
  @Inject
  private AnalysisChartService analysisChartService;

  public AnalysisTask create(AnalysisParameters parameters) {
    return new AnalysisTask(analysisService, analysisChartService, parameters);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.inject.Provider;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.output.TeeOutputStream;
import org.labjacquespe.vap.OperatingSystem;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.message.MessageResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link ExecutableService}.
 */
@Component
public class ExecutableService {
  private static final String MISSING_NATIVE_EXECUTABLE = "MISSING_NATIVE_EXECUTABLE";
  private static final Pattern GROUP_PATTERN = Pattern.compile("Reading group: (.+)");
  private static final Pattern DATASET_PATTERN = Pattern.compile("Processing dataset: (.+)");
  private static final Pattern ERROR_PATTERN = Pattern.compile("ERROR: (.+)");
  private final Logger logger = LoggerFactory.getLogger(ExecutableService.class);
  @Inject
  private OperatingSystemService operatingSystemService;
  @Inject
  private Provider<Executor> executorProvider;
  @Inject
  private VapCoreConfiguration vapCoreConfiguration;
  protected ExecutableService() {
  }
  protected ExecutableService(OperatingSystemService operatingSystemService,
      Provider<Executor> executorProvider, VapCoreConfiguration vapCoreConfiguration) {
    this.operatingSystemService = operatingSystemService;
    this.executorProvider = executorProvider;
    this.vapCoreConfiguration = vapCoreConfiguration;
  }

  /**
   * Validates that VAP can be executed.
   *
   * @param directory
   *          directory where vap should be run
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles validation errors
   */
  public void validateVap(File directory, Locale locale, Consumer<String> errorHandler) {
    MessageResources resources = new MessageResources(ExecutableService.class, locale);
    OperatingSystem currentOs = operatingSystemService.currentOs();
    File nativeExecutable = nativeExecutable(currentOs, directory);
    if (currentOs == OperatingSystem.OTHER && !nativeExecutable.exists()) {
      errorHandler.accept(resources.message(MISSING_NATIVE_EXECUTABLE, nativeExecutable.getName()));
    }
  }

  /**
   * Runs vap external program.
   *
   * @param directory
   *          directory where vap should be run
   * @param parameters
   *          parameters
   * @throws IOException
   *           could not run program
   */
  public void runVap(File directory, File parameters) throws IOException {
    runVap(directory, parameters, null);
  }

  /**
   * Runs vap external program.
   *
   * @param directory
   *          directory where vap should be run
   * @param parameters
   *          parameters
   * @param listener
   *          listens for vap event
   * @throws IOException
   *           could not run program
   */
  public void runVap(File directory, File parameters, VapEventListener listener)
      throws IOException {
    OperatingSystem operatingSystem = operatingSystemService.currentOs();
    File nativeExecutable = nativeExecutable(operatingSystem, directory);
    File exec;
    if (nativeExecutable.exists()) {
      // Use native executable.
      exec = nativeExecutable;
    } else {
      // Extract executable.
      exec = extractResource(operatingSystem, directory);
      if (!exec.canExecute()) {
        exec.setExecutable(true);
      }
    }
    CommandLine commandLine = new CommandLine(exec);
    commandLine.addArgument("-p");
    commandLine.addArgument(parameters.toString(), false);
    Executor executor = executorProvider.get();
    executor.setWorkingDirectory(directory);
    if (listener != null) {
      OutputStream output = new TeeOutputStream(System.out, new VapEventGenerator(listener));
      OutputStream errorOutput = new TeeOutputStream(System.err, new VapEventGenerator(listener));
      executor.setStreamHandler(new PumpStreamHandler(output, errorOutput));
    }
    logger.info("Executing: {}", commandLine);
    int exitValue = executor.execute(commandLine);
    if (executor.isFailure(exitValue)) {
      throw new IOException("Incorrect return value " + exitValue);
    }
  }

  private File nativeExecutable(OperatingSystem operatingSystem, File directory) {
    File nativeExecutableFilename = vapCoreConfiguration.nativeVap(operatingSystem);
    return new File(directory, nativeExecutableFilename.getPath());
  }

  private File extractResource(OperatingSystem operatingSystem, File directory) throws IOException {
    String resource = vapCoreConfiguration.resource(operatingSystem);
    if (getClass().getResource(resource) == null) {
      throw new IOException(
          "Resource " + resource + " not found, check if you have the right jar for your OS");
    }
    File outputFilename = vapCoreConfiguration.output(operatingSystem);
    File file = new File(directory, outputFilename.getPath());
    try (InputStream input = new BufferedInputStream(getClass().getResourceAsStream(resource))) {
      Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
    return file;
  }

  /**
   * Listens for events coming from vap program.
   */
  public static class VapEventListener {
    public Consumer<String> groupHandler = group -> {
    };
    public Consumer<String> datasetHandler = dataset -> {
    };
    public Consumer<String> errorHandler = error -> {
    };
  }

  private static class VapEventGenerator extends LogOutputStream {
    private final VapEventListener listener;

    public VapEventGenerator(VapEventListener listener) {
      this.listener = listener;
    }

    @Override
    protected void processLine(String line, int level) {
      Matcher matcher = GROUP_PATTERN.matcher(line);
      if (matcher.matches()) {
        listener.groupHandler.accept(matcher.group(1));
      }
      matcher = DATASET_PATTERN.matcher(line);
      if (matcher.matches()) {
        listener.datasetHandler.accept(matcher.group(1));
      }
      matcher = ERROR_PATTERN.matcher(line);
      if (matcher.matches()) {
        listener.errorHandler.accept(matcher.group(1));
      }
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.labjacquespe.vap.chart.HeatmapParameters;
import org.springframework.stereotype.Component;

/**
 * Test data.
 */
@Component
public class TestData extends AnalysisParameters {
  private final Set<File> testFiles;
  private final List<File> coordinatesReferenceFiles;

  protected TestData() throws IOException {
    testFiles = findTestFiles();
    outputFolder = null;
    ArrayList<File> dataFiles = new ArrayList<>();
    dataFiles.add(new File("H3K36me3-vs-H3_Young_sacCer1.bigwig"));
    dataFiles.add(new File("H2AZ-vs-H2B_Robert_sacCer1.bigwig"));
    this.dataFiles = Collections.unmodifiableList(dataFiles);
    geneChartParameters.referenceType = ReferenceType.ANNOTATIONS;
    List<File> referenceFiles = new ArrayList<>();
    referenceFiles.add(new File("group_genes_trxFreq_16-50.txt"));
    referenceFiles.add(new File("group_genes_trxFreq_2-4.txt"));
    referenceFiles.add(new File("group_genes_trxFreq_-1.txt"));
    this.referenceFiles = Collections.unmodifiableList(referenceFiles);
    List<File> coordinatesReferenceFiles = new ArrayList<>();
    coordinatesReferenceFiles.add(new File("coord_group_genes_trxFreq_16-50.coord4"));
    coordinatesReferenceFiles.add(new File("coord_group_genes_trxFreq_2-4.coord4"));
    coordinatesReferenceFiles.add(new File("coord_group_genes_trxFreq_-1.coord4"));
    this.coordinatesReferenceFiles = Collections.unmodifiableList(coordinatesReferenceFiles);
    processDataByChunk = false;
    dataChunkSize = 10000000;
    this.genomeAnnotations = new File("SGDannot_20080202_on_sacCer1.genepred");
    selectionAnnotationFilter = null;
    exclusionAnnotationFilter = null;
    coordinatesType = CoordinatesType.TX;
    mergeMiddleIntron = null;
    geneChartParameters.representationType = RepresentationType.ABSOLUTE;
    geneChartParameters.referencePoints = 4;
    geneChartParameters.boundary = null;
    geneChartParameters.windowSize = 50;
    List<Block> blocks = new ArrayList<>();
    Block block = new Block();
    block.windows = 20L;
    blocks.add(block);
    block = new Block();
    block.windows = 10L;
    block.alignment = BlockAlignment.SPLIT;
    block.splitType = SplitType.PERCENTAGE;
    block.split = 50L;
    block.splitAlignment = SplitAlignment.LEFT;
    blocks.add(block);
    block = new Block();
    block.windows = 30L;
    block.alignment = BlockAlignment.SPLIT;
    block.splitType = SplitType.PERCENTAGE;
    block.split = 50L;
    block.splitAlignment = SplitAlignment.LEFT;
    blocks.add(block);
    block = new Block();
    block.windows = 10L;
    block.alignment = BlockAlignment.SPLIT;
    block.splitType = SplitType.PERCENTAGE;
    block.split = 50L;
    block.splitAlignment = SplitAlignment.LEFT;
    blocks.add(block);
    block = new Block();
    block.windows = 20L;
    block.alignment = BlockAlignment.SPLIT;
    block.splitType = SplitType.PERCENTAGE;
    block.split = 50L;
    block.splitAlignment = SplitAlignment.LEFT;
    blocks.add(block);
    geneChartParameters.blocks = Collections.unmodifiableList(blocks);
    smoothDataWindows = 6;
    aggregateValueType = AggregateValueType.MEAN;
    missingDataReplacement = MissingDataReplacement.ZERO;
    ouputFilePrefix = "test";
    outputGraphs = Collections.nCopies(1, Graph.ANY_ANY);
    dispersionType = DispersionType.SEM;
    geneChartParameters.dispersion = true;
    geneChartParameters.generateAggregateGraphs = true;
    oneDataFilePerGraph = false;
    oneReferenceGroupPerGraph = false;
    oneOrientationPerGraph = false;
    outputData = true;
    geneChartParameters.generateHeatmap = false;
    geneChartParameters.heatmapParameters = new HeatmapParameters();
    geneChartParameters.yaxisScale = new YAxisScale();
    testData = true;
  }

  private Set<File> findTestFiles() throws IOException {
    Set<File> testFiles = new HashSet<>();
    try (ZipInputStream zipInput =
        new ZipInputStream(getClass().getResourceAsStream("/test/test_files.zip"))) {
      ZipEntry entry;
      while ((entry = zipInput.getNextEntry()) != null) {
        try {
          testFiles.add(new File(entry.getName()));
        } finally {
          zipInput.closeEntry();
        }
      }
    }
    return testFiles;
  }

  public boolean isTestFile(File file) {
    return testFiles.contains(file);
  }

  public List<File> getCoordinatesReferenceFiles() {
    return coordinatesReferenceFiles;
  }
}

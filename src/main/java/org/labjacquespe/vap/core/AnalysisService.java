/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.inject.Inject;
import org.labjacquespe.vap.core.ExecutableService.VapEventListener;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.progressbar.ProgressBar;
import org.labjacquespe.vap.util.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Analyzes data files using VAP core.
 */
@Component
public class AnalysisService {
  public static class VapException extends IOException {
    private static final long serialVersionUID = 6855020309991147776L;
    private final List<String> errors;

    protected VapException(List<String> errors) {
      this.errors = errors;
    }

    public VapException(List<String> errors, Throwable cause) {
      super(cause);
      this.errors = errors;
    }

    public List<String> getErrors() {
      return errors;
    }
  }

  private final Logger logger = LoggerFactory.getLogger(AnalysisService.class);
  @Inject
  private ParameterFileService parameterFileService;
  @Inject
  private ExecutableService executableService;

  protected AnalysisService() {
  }

  protected AnalysisService(ParameterFileService parameterFileService,
      ExecutableService executableService) {
    this.parameterFileService = parameterFileService;
    this.executableService = executableService;
  }

  /**
   * Launches vap executable program using parameters.
   *
   * @param parameters
   *          parameters
   * @param progressBar
   *          records progression
   * @throws IOException
   *           vap executable could not complete
   * @throws InterruptedException
   *           vap execution was interrupted
   */
  public void mapGenes(final AnalysisParameters parameters, final ProgressBar progressBar)
      throws IOException, InterruptedException {
    logger.trace("Analyse data");
    MessageResources resources = new MessageResources(AnalysisService.class, Locale.getDefault());
    progressBar.setTitle(resources.message("write.parameters"));
    File outputFolder = parameters.outputFolder;
    String prefix = parameters.ouputFilePrefix;
    final File parametersFile = new File(outputFolder, resources.message("parameter.filename",
        prefix == null || prefix.isEmpty() ? 0 : 1, prefix));
    logger.debug("Export parameters to {}", parametersFile);
    parameterFileService.export(parametersFile, parameters, false);
    progressBar.setProgress(0.05);
    ExceptionUtils.throwIfInterrupted("Interrupted analysis");
    if (parameters.testData) {
      logger.debug("Extract test data to {}", parameters.outputFolder);
      extractTestFiles(parameters.outputFolder);
    }
    logger.debug("Run executable");
    progressBar.setTitle(resources.message("analyse"));
    VapEventListener listener = new VapEventListener();
    listener.groupHandler = group -> progressBar.setMessage(resources.message("group", group));
    listener.datasetHandler =
        dataset -> progressBar.setMessage(resources.message("dataset", dataset));
    List<String> errors = new ArrayList<>();
    listener.errorHandler = error -> errors.add(error);

    class ProgressUpdater {
      private int totalGroupCount = parameters.referenceFiles.size();
      private int totalDatasetCount = parameters.dataFiles.size();
      private int processedGroupCount = 0;
      private int processedDatasetCount = 0;
      private double progressStart = 0.05;

      private void updateProgress() {
        double progress = 0.1 * processedGroupCount / totalGroupCount;
        progress += 0.9 * processedDatasetCount / totalDatasetCount;
        progressBar.setProgress(progressStart + progress * (1.0 - progressStart));
      }
    }

    ProgressUpdater progressUpdater = new ProgressUpdater();
    listener.groupHandler.andThen(group -> {
      progressUpdater.processedGroupCount++;
      progressUpdater.updateProgress();
    });
    listener.datasetHandler.andThen(dataset -> {
      progressUpdater.processedDatasetCount++;
      progressUpdater.updateProgress();
    });

    try {
      executableService.runVap(outputFolder, parametersFile, listener);
    } catch (IOException e) {
      if (!errors.isEmpty()) {
        throw new VapException(errors, e);
      } else {
        throw e;
      }
    }
    progressBar.setProgress(1.0);
    logger.debug("Executable completed");
  }

  private void extractTestFiles(final File outputFolder) throws IOException {
    URL resource = getClass().getResource("/test/test_files.zip");
    if (resource != null) {
      try (ZipInputStream zipInput = new ZipInputStream(resource.openStream())) {
        ZipEntry entry;
        while ((entry = zipInput.getNextEntry()) != null) {
          try {
            File outputFile = new File(outputFolder, entry.getName());
            if (!outputFile.exists()) {
              Files.copy(zipInput, outputFile.toPath());
            }
          } finally {
            zipInput.closeEntry();
          }
        }
      }
    }
  }
}

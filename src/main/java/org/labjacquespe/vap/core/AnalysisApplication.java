/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.stage.Stage;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.labjacquespe.vap.AbstractSpringBootJavafxApplication;
import org.labjacquespe.vap.CommandLineParameterService;
import org.labjacquespe.vap.javafx.SpringAfterburnerInstanceSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application that creates gene charts.
 */
public class AnalysisApplication extends AbstractSpringBootJavafxApplication {
  private class AnalysisErrorHandler implements Consumer<String> {
    private final ResourceBundle bundle;
    private boolean printedHeader;
    private boolean hasError;

    private AnalysisErrorHandler(ResourceBundle bundle) {
      this.bundle = bundle;
    }

    @Override
    public void accept(String error) {
      hasError = true;
      if (!printedHeader) {
        printedHeader = true;
        printErrorTitle(bundle);
      }

      System.err.println(error);
    }

    public boolean hasError() {
      return hasError;
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(AnalysisApplication.class);
  @Inject
  private CommandLineParameterService commandLineParameterService;
  @Inject
  private AnalysisTaskFactory analysisTaskFactory;
  private final ResourceBundle bundle;

  public AnalysisApplication() {
    bundle = ResourceBundle.getBundle(getClass().getName(), Locale.getDefault());
  }

  @Override
  public void init() throws Exception {
    super.init();
    com.airhacks.afterburner.injection.Injector
        .setInstanceSupplier(new SpringAfterburnerInstanceSupplier(applicationContext));
  }

  @Override
  public void start(final Stage stage) throws Exception {
    try {
      AnalysisErrorHandler errorHandler = new AnalysisErrorHandler(bundle);
      commandLineParameterService.validateRunArguments(
          getParameters().getRaw().toArray(new String[] {}), Locale.getDefault(), errorHandler);
      if (errorHandler.hasError()) {
        showHelp();
        Platform.exit();
      } else {
        AnalysisParameters analysisParameters = commandLineParameterService
            .parseRunArguments(getParameters().getRaw().toArray(new String[] {}));
        final AnalysisTask task = analysisTaskFactory.create(analysisParameters);
        task.messageProperty().addListener(new ChangeListener<String>() {
          @Override
          public void changed(ObservableValue<? extends String> observableValue, String oldValue,
              String newValue) {
            System.out.println(MessageFormat.format(bundle.getString("message"), newValue));
          }
        });
        task.stateProperty().addListener(new ChangeListener<State>() {
          @Override
          public void changed(ObservableValue<? extends State> observableValue, State oldValue,
              State newValue) {
            if (newValue == State.CANCELLED) {
              System.err.println(bundle.getString("cancelled"));
              Platform.exit();
            } else if (newValue == State.FAILED) {
              // Show error message.
              Throwable error = task.getException();
              logger.error("Could not analyse data", error);
              System.err.println(bundle.getString("fail"));
              error.printStackTrace();
              Platform.exit();
            } else if (newValue == State.SUCCEEDED) {
              // Show confirm message.
              System.out.println(bundle.getString("success"));
              Platform.exit();
            }
          }
        });
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        Platform.setImplicitExit(false);
        thread.start();
      }
    } catch (IOException e) {
      printErrorTitle(bundle);
      e.printStackTrace();
    }
  }

  private void showHelp() {
    try {
      try (Reader reader = new BufferedReader(
          new InputStreamReader(getClass().getResourceAsStream("/commandLineHelp.txt"), "UTF-8"))) {
        IOUtils.copy(reader, System.out, Charset.defaultCharset());
      }
    } catch (IOException e) {
      System.err.print("Error: could not open help file");
    }
  }

  private void printErrorTitle(ResourceBundle bundle) {
    System.err.println(bundle.getString("error.title"));
    System.err.println();
  }
}

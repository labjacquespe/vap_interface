/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_OUTPUT_DATA;
import static org.labjacquespe.vap.core.service.ParameterFileService.WRITE_INDIVIDUAL_REFERENCES;

import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Report reference feature values presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReportReferenceFeatureValuesPresenter extends BasePresenter {
  private final BooleanProperty outputDataProperty = new SimpleBooleanProperty();
  private final BooleanProperty outputHeatmapProperty = new SimpleBooleanProperty();
  @FXML
  private Pane view;
  @FXML
  private CheckBox checkBox;

  @FXML
  private void initialize() {
    outputDataProperty.bindBidirectional(checkBox.selectedProperty());
    checkBox.disableProperty().bind(outputHeatmapProperty);
    outputHeatmapProperty.addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> obv, Boolean ov, Boolean nv) {
        if (nv) {
          outputDataProperty.set(true);
        }
      }
    });
    outputDataProperty.set(DEFAULT_OUTPUT_DATA);
  }

  public BooleanProperty outputDataProperty() {
    return outputDataProperty;
  }

  public BooleanProperty outputHeatmapProperty() {
    return outputHeatmapProperty;
  }

  public boolean isOutputData() {
    return outputDataProperty.get();
  }

  public void setOutputData(boolean outputData) {
    outputDataProperty.set(outputData);
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(WRITE_INDIVIDUAL_REFERENCES);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

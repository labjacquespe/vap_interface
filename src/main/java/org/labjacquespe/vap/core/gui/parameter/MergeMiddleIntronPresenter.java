/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_MERGE_MIDDLE_INTRON;
import static org.labjacquespe.vap.core.service.ParameterFileService.MERGE_MID_INTRONS;

import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.MergeMiddleIntron;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Merge middle intron presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MergeMiddleIntronPresenter extends BasePresenter {
  private final ObjectProperty<MergeMiddleIntron> mergeMiddleIntronProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton no;
  @FXML
  private RadioButton first;
  @FXML
  private RadioButton last;

  @FXML
  private void initialize() {
    no.setUserData(null);
    first.setUserData(MergeMiddleIntron.FIRST);
    last.setUserData(MergeMiddleIntron.LAST);
    group.selectedToggleProperty().addListener((ov, oldValue, newValue) -> mergeMiddleIntronProperty
        .set((MergeMiddleIntron) newValue.getUserData()));
    mergeMiddleIntronProperty.addListener((ov, oldValue, newValue) -> updateMergeMiddleIntron());
    mergeMiddleIntronProperty.set(DEFAULT_MERGE_MIDDLE_INTRON);
  }

  public ObjectProperty<MergeMiddleIntron> mergeMiddleIntronProperty() {
    return mergeMiddleIntronProperty;
  }

  public MergeMiddleIntron getMergeMiddleIntron() {
    return mergeMiddleIntronProperty.get();
  }

  public void setMergeMiddleIntron(MergeMiddleIntron mergeMiddleIntron) {
    mergeMiddleIntronProperty.set(mergeMiddleIntron);
  }

  private void updateMergeMiddleIntron() {
    MergeMiddleIntron mergeMiddleIntron = mergeMiddleIntronProperty.get();
    if (mergeMiddleIntron == null) {
      no.setSelected(true);
    } else {
      switch (mergeMiddleIntron) {
        case FIRST:
          first.setSelected(true);
          break;
        case LAST:
          last.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(MERGE_MID_INTRONS);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

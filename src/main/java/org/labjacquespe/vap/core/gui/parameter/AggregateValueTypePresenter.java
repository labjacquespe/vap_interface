/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_AGGREGATE_VALUE_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.AGGREGATE_DATA_TYPE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Aggregate value type presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AggregateValueTypePresenter extends BasePresenter {
  private final ObjectProperty<AggregateValueType> aggregateValueTypeProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton mean;
  @FXML
  private RadioButton median;
  @FXML
  private RadioButton max;
  @FXML
  private RadioButton min;

  @FXML
  private void initialize() {
    mean.setUserData(AggregateValueType.MEAN);
    median.setUserData(AggregateValueType.MEDIAN);
    max.setUserData(AggregateValueType.MAX);
    min.setUserData(AggregateValueType.MIN);
    group.selectedToggleProperty().addListener((ov, oldValue,
        newValue) -> aggregateValueTypeProperty.set((AggregateValueType) newValue.getUserData()));
    aggregateValueTypeProperty.addListener((ov, oldValue, newValue) -> updateAggregateValueType());
    aggregateValueTypeProperty.set(DEFAULT_AGGREGATE_VALUE_TYPE);
  }

  public ObjectProperty<AggregateValueType> aggregateValueTypeProperty() {
    return aggregateValueTypeProperty;
  }

  public AggregateValueType getAggregateValueType() {
    return aggregateValueTypeProperty.get();
  }

  /**
   * Sets aggregate value type.
   *
   * @param aggregateValueType
   *          aggregate value type
   */
  public void setAggregateValueType(AggregateValueType aggregateValueType) {
    if (aggregateValueType != null) {
      aggregateValueTypeProperty.set(aggregateValueType);
    }
  }

  private void updateAggregateValueType() {
    AggregateValueType aggregateValueType = aggregateValueTypeProperty.get();
    if (aggregateValueType != null) {
      switch (aggregateValueType) {
        case MEAN:
          mean.setSelected(true);
          break;
        case MEDIAN:
          median.setSelected(true);
          break;
        case MAX:
          max.setSelected(true);
          break;
        case MIN:
          min.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(AGGREGATE_DATA_TYPE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates aggregate value type.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (aggregateValueTypeProperty.get() == null) {
      errorHandler.accept(message("error.required"), null);
    }
  }
}

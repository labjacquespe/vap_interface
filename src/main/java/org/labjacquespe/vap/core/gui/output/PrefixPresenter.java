/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.service.ParameterFileService.PREFIX_FILENAME;

import java.util.function.BiConsumer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Output file prefix presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrefixPresenter extends BasePresenter {
  private final StringProperty prefixProperty = new SimpleStringProperty();
  @FXML
  private Pane view;
  @FXML
  private TextField prefix;

  @FXML
  private void initialize() {
    prefix.textProperty().bindBidirectional(prefixProperty);
  }

  public StringProperty prefixProperty() {
    return prefixProperty;
  }

  public String getPrefix() {
    return prefixProperty.get();
  }

  public void setPrefix(String prefix) {
    prefixProperty.set(prefix);
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(PREFIX_FILENAME);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

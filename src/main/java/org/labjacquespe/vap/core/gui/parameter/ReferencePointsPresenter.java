/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_REFERENCE_POINTS;
import static org.labjacquespe.vap.chart.GeneChartParameters.EXON_REFERENCE_POINTS;
import static org.labjacquespe.vap.core.service.ParameterFileService.REFERENCE_POINTS;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Reference points presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReferencePointsPresenter extends BasePresenter {
  private final IntegerProperty referencePointsProperty = new SimpleIntegerProperty();
  private final IntegerProperty mainReferencePointsProperties = new SimpleIntegerProperty();
  private final IntegerProperty exonReferencePointsProperties = new SimpleIntegerProperty();
  private final IntegerProperty maximumReferencePointsProperty = new SimpleIntegerProperty();
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private TextField referencePoints;
  @FXML
  private TextField exonsReferencePoints;
  @FXML
  private Label maximum;

  @FXML
  private void initialize() {
    mainReferencePointsProperties
        .addListener((ov, oldValue, newValue) -> referencePointsProperty.setValue(newValue));
    exonReferencePointsProperties
        .addListener((ov, oldValue, newValue) -> referencePointsProperty.setValue(newValue));

    maximum.textProperty()
        .addListener((ov, oldValue, newValue) -> message("maximumLabel", maximum));
    referenceTypeProperty.addListener((ov, oldValue, newValue) -> toggleReferencePoints());
    referencePoints.textProperty().bindBidirectional(mainReferencePointsProperties,
        new DefaultValueOnExceptionAndNullConverter<>(new NumberStringConverter(),
            DEFAULT_REFERENCE_POINTS));
    exonsReferencePoints.textProperty().bindBidirectional(exonReferencePointsProperties,
        new DefaultValueOnExceptionAndNullConverter<>(new NumberStringConverter(),
            EXON_REFERENCE_POINTS));

    // Default values.
    mainReferencePointsProperties.set(DEFAULT_REFERENCE_POINTS);
    exonReferencePointsProperties.set(EXON_REFERENCE_POINTS);
  }

  public IntegerProperty referencePointsProperty() {
    return referencePointsProperty;
  }

  public int getReferencePoints() {
    return referencePointsProperty.get();
  }

  /**
   * Sets number of reference points.
   *
   * @param referencePoints
   *          number of reference points
   */
  public void setReferencePoints(int referencePoints) {
    if (referencePoints > 0) {
      ReferenceType referenceType = referenceTypeProperty.get();
      if (referenceType == ReferenceType.EXONS) {
        exonReferencePointsProperties.set(referencePoints);
      } else if (referenceType != null) {
        mainReferencePointsProperties.set(referencePoints);
      }
    } else {
      if (referenceTypeProperty.get() != ReferenceType.EXONS) {
        // Exon field is disabled and cannot be set.
        this.referencePoints.setText("");
      }
    }
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  private void toggleReferencePoints() {
    ReferenceType referenceType = referenceTypeProperty.get();
    view.setDisable(referenceType == ReferenceType.EXONS);
    if (referenceType != null) {
      referencePoints.setVisible(false);
      exonsReferencePoints.setVisible(false);
      if (referenceTypeProperty.get() == ReferenceType.EXONS) {
        referencePointsProperty.set(exonReferencePointsProperties.get());
        exonsReferencePoints.setVisible(true);
        maximumReferencePointsProperty.set(6);
      } else {
        referencePointsProperty.set(mainReferencePointsProperties.get());
        referencePoints.setVisible(true);
        maximumReferencePointsProperty.set(6);
      }
    }
  }

  /**
   * Show help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(REFERENCE_POINTS);
    helpPopup.show((Node) event.getSource());
  }

  @Override
  public void setMammalianDefaults() {
    mainReferencePointsProperties.set(2);
  }

  @Override
  public void setYeastDefaults() {
    mainReferencePointsProperties.set(4);
  }

  /**
   * Validate number of reference points.
   *
   * @param errorHandler
   *          handles validation errors.
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    referencePoints.getStyleClass().remove("error");
    exonsReferencePoints.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
      referencePoints.getStyleClass().add("error");
      exonsReferencePoints.getStyleClass().add("error");
    }));
    int max = maximumReferencePointsProperty.get();
    try {
      ReferenceType referenceType = referenceTypeProperty.get();
      NumberStringConverter converter = new NumberStringConverter();
      Integer value = null;
      if (referenceType == ReferenceType.EXONS) {
        value = converter.fromString(exonsReferencePoints.getText()).intValue();
      } else {
        value = converter.fromString(referencePoints.getText()).intValue();
      }
      if (value < 1) {
        errorHandler.accept(message("error.underMinimum"), null);
      } else if (value > max) {
        errorHandler.accept(message("error.overMaximum", max), null);
      }
    } catch (Exception e) {
      errorHandler.accept(message("error.invalidNumber"), null);
    }
  }
}

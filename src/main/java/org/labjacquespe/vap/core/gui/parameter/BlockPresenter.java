/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOWS_PER_BLOCK;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.util.converter.LongStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.service.GeneChartService;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Block presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BlockPresenter extends BasePresenter {
  private static final int BLOCK_START_ROW = 0;
  private static final int BLOCK_START_COLUMN = 2;
  private static final int MAX_ELEMENTS = 7;

  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final IntegerProperty referencePointsProperty = new SimpleIntegerProperty();
  private final BlockFx[] blocks = new BlockFx[MAX_ELEMENTS];
  @FXML
  private GridPane view;
  @FXML
  private Label label;
  private Label[] blockLabels = new Label[MAX_ELEMENTS];
  private TextField[] windows = new TextField[MAX_ELEMENTS];
  @Autowired
  private GeneChartService geneChartService;

  @FXML
  private void initialize() {
    referenceTypeProperty.addListener((ov, oldValue, newValue) -> updateBlocksType());
    referencePointsProperty.addListener((ov, oldValue, newValue) -> updateBlocksType());
    referencePointsProperty.addListener((ov, oldValue, newValue) -> updateVisible());
    for (int i = 0; i < blocks.length; i++) {
      blocks[i] = new BlockFx();
      blocks[i].typeProperty().addListener(updateBlockLabelListener(i));
    }

    for (int i = 0; i < blockLabels.length; i++) {
      StackPane pane = new StackPane();
      pane.getStyleClass().addAll("stack-block");
      view.add(pane, BLOCK_START_COLUMN + i, BLOCK_START_ROW);
      blockLabels[i] = new Label();
      blockLabels[i].getStyleClass().addAll("label-block");
      pane.getChildren().add(blockLabels[i]);
    }
    for (int i = 0; i < windows.length; i++) {
      windows[i] = new TextField();
      windows[i].setPrefColumnCount(8);
      windows[i].textProperty().bindBidirectional(blocks[i].windowsProperty(),
          new DefaultValueOnExceptionAndNullConverter<>(new LongStringConverter(), 0L));
      view.add(windows[i], BLOCK_START_COLUMN + i, BLOCK_START_ROW + 1);
    }

    // Default values.
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].setWindows(20L);
    }
    blocks[2].setWindows(60L);
  }

  private void updateBlocksType() {
    ReferenceType referenceType = referenceTypeProperty.get();
    Integer referencePoints = Math.min(referencePointsProperty.get(), MAX_ELEMENTS - 1);

    for (int index = 0; index < blockLabels.length; index++) {
      BlockType blockType = geneChartService.regionType(index, referenceType, referencePoints);
      blocks[index].setType(blockType);
    }
  }

  public List<Block> getBlocks() {
    return Arrays.asList(blocks).stream().map(b -> b.toBlock()).collect(Collectors.toList());
  }

  /**
   * Sets blocks.
   *
   * @param blocks
   *          blocks
   */
  public void setBlocks(List<Block> blocks) {
    if (blocks != null) {
      int max = Math.min(blocks.size(), this.blocks.length);
      for (int i = 0; i < max; i++) {
        this.blocks[i].copy(blocks.get(i));
      }
    } else {
      for (int i = 0; i < this.blocks.length; i++) {
        this.blocks[i].copy(new Block());
      }
    }
    updateBlocksType();
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public IntegerProperty referencePointsProperty() {
    return referencePointsProperty;
  }

  private void updateVisible() {
    Integer referencePoints = referencePointsProperty.get();
    for (int index = 0; index < MAX_ELEMENTS; index++) {
      boolean visible = index < 2 || (referencePoints != null && index <= referencePoints);
      blockLabels[index].setVisible(visible);
      windows[index].setVisible(visible);
    }
  }

  private ChangeListener<BlockType> updateBlockLabelListener(int index) {
    return (ov, oldValue, newValue) -> {
      Label blockLabel = blockLabels[index];
      for (BlockType blockType : BlockType.values()) {
        blockLabel.getStyleClass().remove(blockType.name());
      }
      if (newValue != null) {
        blockLabel.setText(message("blockLabels." + newValue.name()));
        blockLabel.getStyleClass().add(newValue.name());
      } else {
        blockLabel.setText("");
      }
    };
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(WINDOWS_PER_BLOCK);
    helpPopup.show((Node) event.getSource());
  }

  @Override
  public void setMammalianDefaults() {
    blocks[0].setWindows(200L);
    blocks[1].setWindows(600L);
    blocks[2].setWindows(200L);
  }

  @Override
  public void setYeastDefaults() {
    blocks[0].setWindows(20L);
    blocks[1].setWindows(10L);
    blocks[2].setWindows(30L);
    blocks[3].setWindows(10L);
    blocks[4].setWindows(20L);
  }

  /**
   * Validate block window count.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    label.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      label.getStyleClass().add("error");
    }));
    for (TextField window : windows) {
      window.getStyleClass().remove("error");
      BiConsumer<String, String> windowErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            window.getStyleClass().add("error");
          }));
      if (window.isVisible()) {
        try {
          NumberStringConverter converter = new NumberStringConverter();
          long value = converter.fromString(window.getText()).longValue();
          if (value <= 0) {
            windowErrorHandler.accept(message("error.windowCount.underMinimum"), null);
          }
        } catch (Exception e) {
          windowErrorHandler.accept(message("error.windowCount.invalidNumber"), null);
        }
      }
    }
  }
}

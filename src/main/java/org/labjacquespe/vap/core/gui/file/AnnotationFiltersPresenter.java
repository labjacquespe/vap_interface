/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.file;

import static org.labjacquespe.vap.core.service.ParameterFileService.SELECTION_PATH;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;
import static org.labjacquespe.vap.gui.SelectAllListener.selectAllListener;

import java.io.File;
import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.core.TestData;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.gui.SelectableLabel;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileOverHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Annotation filters presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AnnotationFiltersPresenter extends BasePresenter {
  private ObjectProperty<File> relativeFolderProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private SelectableLabel label;
  @FXML
  private Pane selectionPane;
  @FXML
  private Pane exclusionPane;
  @FXML
  private TextField selectionFilter;
  @FXML
  private TextField exclusionFilter;
  @Autowired
  private OperatingSystemService operatingSystemService;
  @Autowired
  private TestData testData;
  private FileChooser selectionChooser = new FileChooser();
  private FileChooser exclusionChooser = new FileChooser();

  @FXML
  private void initialize() {
    selectionChooser.setTitle(message("selectionChooser.title"));
    selectionChooser.getExtensionFilters()
        .add(new ExtensionFilter(message("selectionChooser.description"),
            operatingSystemService.dataFileExtensions(operatingSystemService.currentOs())));
    moveCaretToEndOnLostFocus(selectionFilter);
    selectionFilter.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());
    exclusionChooser.setTitle(message("exclusionChooser.title"));
    exclusionChooser.getExtensionFilters()
        .add(new ExtensionFilter(message("exclusionChooser.description"),
            operatingSystemService.dataFileExtensions(operatingSystemService.currentOs())));
    moveCaretToEndOnLostFocus(exclusionFilter);
    exclusionFilter.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());
    exclusionChooser.initialDirectoryProperty()
        .bindBidirectional(selectionChooser.initialDirectoryProperty());

    // Enable drag and drop.
    selectionFilter.setOnDragDetected(new DragFileDetectedHandler(selectionFilter));
    selectionFilter.setOnDragDone(new DragFileDoneHandler(selectionFilter));
    selectionPane.setOnDragOver(new DragFileOverHandler(selectionPane, selectionFilter));
    selectionPane.setOnDragExited(new DragExitedHandler(selectionPane));
    selectionPane.setOnDragDropped(new DragFileDroppedHandler(selectionFilter));
    exclusionFilter.setOnDragDetected(new DragFileDetectedHandler(exclusionFilter));
    exclusionFilter.setOnDragDone(new DragFileDoneHandler(exclusionFilter));
    exclusionPane.setOnDragOver(new DragFileOverHandler(exclusionPane, exclusionFilter));
    exclusionPane.setOnDragExited(new DragExitedHandler(exclusionPane));
    exclusionPane.setOnDragDropped(new DragFileDroppedHandler(exclusionFilter));
  }

  public ObjectProperty<File> relativeFolderProperty() {
    return relativeFolderProperty;
  }

  public ObjectProperty<File> initialDirectoryProperty() {
    return selectionChooser.initialDirectoryProperty();
  }

  /**
   * Returns selection filter.
   *
   * @return selection filter
   */
  public File getSelectionFilter() {
    if (selectionFilter.getText() != null && !selectionFilter.getText().isEmpty()) {
      File file = new File(selectionFilter.getText());
      if (!file.isAbsolute() && relativeFolderProperty.get() != null && !isSelectionTest(file)) {
        return new File(relativeFolderProperty.get(), selectionFilter.getText());
      } else {
        return file;
      }
    } else {
      return null;
    }
  }

  /**
   * Returns exclusion filter.
   *
   * @return exclusion filter
   */
  public File getExclusionFilter() {
    if (exclusionFilter.getText() != null && !exclusionFilter.getText().isEmpty()) {
      File file = new File(exclusionFilter.getText());
      if (!file.isAbsolute() && relativeFolderProperty.get() != null && !isExclusionTest(file)) {
        return new File(relativeFolderProperty.get(), exclusionFilter.getText());
      } else {
        return file;
      }
    } else {
      return null;
    }
  }

  /**
   * Sets selection filter.
   *
   * @param selectionFilter
   *          selection filter
   */
  public void setSelectionFilter(File selectionFilter) {
    if (selectionFilter != null) {
      this.selectionFilter.setText(selectionFilter.toString());
      this.selectionFilter.positionCaret(this.selectionFilter.getText().length());
    } else {
      this.selectionFilter.setText(null);
    }
  }

  /**
   * Sets exclusion filter.
   *
   * @param exclusionFilter
   *          exclusion filter
   */
  public void setExclusionFilter(File exclusionFilter) {
    if (exclusionFilter != null) {
      this.exclusionFilter.setText(exclusionFilter.toString());
      this.exclusionFilter.positionCaret(this.exclusionFilter.getText().length());
    } else {
      this.exclusionFilter.setText(null);
    }
  }

  /**
   * Shows file selection windows for selection filter.
   *
   * @param event
   *          event
   */
  public void browseSelectionFilter(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(selectionChooser);
    File selection = selectionChooser.showOpenDialog(selectionFilter.getScene().getWindow());
    if (selection != null) {
      selectionChooser.setInitialDirectory(selection.getParentFile());
      setSelectionFilter(selection);
    }
  }

  public void clearSelectionFilter(ActionEvent event) {
    selectionFilter.setText("");
  }

  /**
   * Shows file selection windows for exclusion filter.
   *
   * @param event
   *          event
   */
  public void browseExclusionFilter(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(exclusionChooser);
    File selection = exclusionChooser.showOpenDialog(exclusionFilter.getScene().getWindow());
    if (selection != null) {
      exclusionChooser.setInitialDirectory(selection.getParentFile());
      setExclusionFilter(selection);
    }
  }

  public void clearExclusionFilter(ActionEvent event) {
    exclusionFilter.setText("");
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(SELECTION_PATH);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates annotation filters.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    label.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      label.getStyleClass().add("error");
    }));
    selectionPane.getStyleClass().remove("error");
    exclusionPane.getStyleClass().remove("error");
    if (selectionFilter.getText() != null && !selectionFilter.getText().isEmpty()) {
      File file = getSelectionFilter();
      if (!file.isFile() && file.isAbsolute() && !isSelectionTest(file)) {
        errorHandler.accept(message("error.selectionFile.notExists", file.getName()),
            file.getPath());
        selectionPane.getStyleClass().add("error");
      }
    }
    if (exclusionFilter.getText() != null && !exclusionFilter.getText().isEmpty()) {
      File file = getExclusionFilter();
      if (!file.isFile() && file.isAbsolute() && !isExclusionTest(file)) {
        errorHandler.accept(message("error.exclusionFile.notExists", file.getName()),
            file.getPath());
        exclusionPane.getStyleClass().add("error");
      }
    }
  }

  private boolean isSelectionTest(File file) {
    return testData.isTestFile(file);
  }

  private boolean isExclusionTest(File file) {
    return testData.isTestFile(file);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * Help popup for parameters.
 */
public class HelpParameterPopup {
  private Popup popup;
  private StringProperty hashtagProperty = new SimpleStringProperty();

  /**
   * Creates an help pop-up.
   */
  public HelpParameterPopup() {
    popup = new Popup();
    HelpParameterView helpParameterView = new HelpParameterView();
    HelpParameterPresenter helpParameterPresenter =
        (HelpParameterPresenter) helpParameterView.getPresenter();

    helpParameterPresenter.hashtagProperty().bind(hashtagProperty);
    helpParameterPresenter.buttonsVisibleProperty().setValue(false);

    popup.getScene().setRoot(helpParameterView.getView());
    popup.setAutoHide(true);
  }

  public StringProperty getHashtagProperty() {
    return hashtagProperty;
  }

  public String getHashtag() {
    return hashtagProperty.get();
  }

  public void setHashtag(String hashtag) {
    hashtagProperty.set(hashtag);
  }

  /**
   * Shows pop-up next to owner node.
   *
   * @param ownerNode
   *          owner node
   */
  public void show(Node ownerNode) {
    Window window = ownerNode.getScene().getWindow();
    Bounds bounds = ownerNode.localToScene(ownerNode.getBoundsInLocal());
    double screenX = window.getX() + bounds.getMaxX();
    double screenY = window.getY() + bounds.getMaxY();
    popup.show(ownerNode, screenX, screenY);
  }

  public void hide() {
    popup.hide();
  }
}

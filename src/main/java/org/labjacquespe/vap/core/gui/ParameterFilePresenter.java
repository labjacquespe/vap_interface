/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_REFERENCE_TYPE;
import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_REPRESENTATION_TYPE;
import static org.labjacquespe.vap.core.ReferenceType.ANNOTATIONS;
import static org.labjacquespe.vap.core.ReferenceType.EXONS;
import static org.labjacquespe.vap.core.RepresentationType.RELATIVE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_METHOD;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_MODE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.stage.Window;
import org.apache.commons.exec.ExecuteException;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.AnalysisService.VapException;
import org.labjacquespe.vap.core.AnalysisTask;
import org.labjacquespe.vap.core.AnalysisTaskFactory;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.TestData;
import org.labjacquespe.vap.core.gui.file.AnnotationFiltersPresenter;
import org.labjacquespe.vap.core.gui.file.AnnotationFiltersView;
import org.labjacquespe.vap.core.gui.file.DatasetPresenter;
import org.labjacquespe.vap.core.gui.file.DatasetView;
import org.labjacquespe.vap.core.gui.file.GenomeAnnotationsPresenter;
import org.labjacquespe.vap.core.gui.file.GenomeAnnotationsView;
import org.labjacquespe.vap.core.gui.file.ReferenceGroupFilesPresenter;
import org.labjacquespe.vap.core.gui.file.ReferenceGroupFilesView;
import org.labjacquespe.vap.core.gui.output.GenerateAggregateGraphsPresenter;
import org.labjacquespe.vap.core.gui.output.GenerateAggregateGraphsView;
import org.labjacquespe.vap.core.gui.output.OutputFolderPresenter;
import org.labjacquespe.vap.core.gui.output.OutputFolderView;
import org.labjacquespe.vap.core.gui.output.OutputHeatmapPresenter;
import org.labjacquespe.vap.core.gui.output.OutputHeatmapView;
import org.labjacquespe.vap.core.gui.output.PrefixPresenter;
import org.labjacquespe.vap.core.gui.output.PrefixView;
import org.labjacquespe.vap.core.gui.output.ReportReferenceFeatureValuesPresenter;
import org.labjacquespe.vap.core.gui.output.ReportReferenceFeatureValuesView;
import org.labjacquespe.vap.core.gui.output.YAxisScalePresenter;
import org.labjacquespe.vap.core.gui.output.YAxisScaleView;
import org.labjacquespe.vap.core.gui.parameter.AbsoluteBlockPresenter;
import org.labjacquespe.vap.core.gui.parameter.AbsoluteBlockView;
import org.labjacquespe.vap.core.gui.parameter.AggregateValueTypePresenter;
import org.labjacquespe.vap.core.gui.parameter.AggregateValueTypeView;
import org.labjacquespe.vap.core.gui.parameter.BlockPresenter;
import org.labjacquespe.vap.core.gui.parameter.BlockView;
import org.labjacquespe.vap.core.gui.parameter.BoundaryPresenter;
import org.labjacquespe.vap.core.gui.parameter.BoundaryView;
import org.labjacquespe.vap.core.gui.parameter.CoordinatesTypePresenter;
import org.labjacquespe.vap.core.gui.parameter.CoordinatesTypeView;
import org.labjacquespe.vap.core.gui.parameter.DataChunkSizePresenter;
import org.labjacquespe.vap.core.gui.parameter.DataChunkSizeView;
import org.labjacquespe.vap.core.gui.parameter.DispersionTypePresenter;
import org.labjacquespe.vap.core.gui.parameter.DispersionTypeView;
import org.labjacquespe.vap.core.gui.parameter.MergeMiddleIntronPresenter;
import org.labjacquespe.vap.core.gui.parameter.MergeMiddleIntronView;
import org.labjacquespe.vap.core.gui.parameter.MissingDataReplacementPresenter;
import org.labjacquespe.vap.core.gui.parameter.MissingDataReplacementView;
import org.labjacquespe.vap.core.gui.parameter.ReferencePointsPresenter;
import org.labjacquespe.vap.core.gui.parameter.ReferencePointsView;
import org.labjacquespe.vap.core.gui.parameter.SmoothDataPresenter;
import org.labjacquespe.vap.core.gui.parameter.SmoothDataView;
import org.labjacquespe.vap.core.gui.parameter.WindowSizePresenter;
import org.labjacquespe.vap.core.gui.parameter.WindowSizeView;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.message.MessageDialog;
import org.labjacquespe.vap.javafx.message.MessageDialog.MessageDialogType;
import org.labjacquespe.vap.javafx.progress.ProgressDialog;
import org.labjacquespe.vap.validation.JavafxValidationErrorAccumulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Parameter file presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ParameterFilePresenter extends BasePresenter {
  private static final Logger logger = LoggerFactory.getLogger(ParameterFilePresenter.class);
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<RepresentationType> representationTypeProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup referenceTypeGroup;
  @FXML
  private Toggle annotations;
  @FXML
  private Toggle exons;
  @FXML
  private Toggle coordinates;
  @FXML
  private Pane fileSelectionElements;
  @FXML
  private ToggleGroup representationTypeGroup;
  @FXML
  private Toggle absolute;
  @FXML
  private Toggle relative;
  @FXML
  private Pane parametersElements;
  @FXML
  private Node relativeWarning;
  @FXML
  private Pane outputElements;
  @Autowired
  private AnalysisTaskFactory analysisTaskFactory;
  @Autowired
  private TestData testData;
  private AnnotationFiltersPresenter annotationFiltersPresenter;
  private AnnotationFiltersView annotationFiltersView;
  private DatasetPresenter datasetPresenter;
  private DatasetView datasetView;
  private GenomeAnnotationsPresenter genomeAnnotationsPresenter;
  private GenomeAnnotationsView genomeAnnotationsView;
  private ReferenceGroupFilesPresenter referenceGroupFilesPresenter;
  private ReferenceGroupFilesView referenceGroupFilesView;
  private AbsoluteBlockPresenter absoluteBlockPresenter;
  private AbsoluteBlockView absoluteBlockView;
  private AggregateValueTypePresenter aggregateValueTypePresenter;
  private AggregateValueTypeView aggregateValueTypeView;
  private BlockPresenter relativeBlockPresenter;
  private BlockView relativeBlockView;
  private BoundaryPresenter boundaryPresenter;
  private BoundaryView boundaryView;
  private CoordinatesTypePresenter coordinatesTypePresenter;
  private CoordinatesTypeView coordinatesTypeView;
  private DataChunkSizePresenter dataChunkSizePresenter;
  private DataChunkSizeView dataChunkSizeView;
  private DispersionTypePresenter dispersionTypePresenter;
  private DispersionTypeView dispersionTypeView;
  private MergeMiddleIntronPresenter mergeMiddleIntronPresenter;
  private MergeMiddleIntronView mergeMiddleIntronView;
  private MissingDataReplacementPresenter missingDataReplacementPresenter;
  private MissingDataReplacementView missingDataReplacementView;
  private ReferencePointsPresenter referencePointsPresenter;
  private ReferencePointsView referencePointsView;
  private SmoothDataPresenter smoothDataPresenter;
  private SmoothDataView smoothDataView;
  private WindowSizePresenter windowSizePresenter;
  private WindowSizeView windowSizeView;
  private GenerateAggregateGraphsPresenter generateAggregateGraphsPresenter;
  private GenerateAggregateGraphsView generateAggregateGraphsView;
  private OutputFolderPresenter outputFolderPresenter;
  private OutputFolderView outputFolderView;
  private OutputHeatmapPresenter outputHeatmapPresenter;
  private OutputHeatmapView outputHeatmapView;
  private PrefixPresenter prefixPresenter;
  private PrefixView prefixView;
  private ReportReferenceFeatureValuesPresenter reportReferenceFeatureValuesPresenter;
  private ReportReferenceFeatureValuesView reportReferenceFeatureValuesView;
  private YAxisScalePresenter yaxisScalePresenter;
  private YAxisScaleView yaxisScaleView;

  @FXML
  private void initialize() {
    annotationFiltersView = new AnnotationFiltersView();
    annotationFiltersPresenter = (AnnotationFiltersPresenter) annotationFiltersView.getPresenter();
    datasetView = new DatasetView();
    datasetPresenter = (DatasetPresenter) datasetView.getPresenter();
    genomeAnnotationsView = new GenomeAnnotationsView();
    genomeAnnotationsPresenter = (GenomeAnnotationsPresenter) genomeAnnotationsView.getPresenter();
    referenceGroupFilesView = new ReferenceGroupFilesView();
    referenceGroupFilesPresenter =
        (ReferenceGroupFilesPresenter) referenceGroupFilesView.getPresenter();
    absoluteBlockView = new AbsoluteBlockView();
    absoluteBlockPresenter = (AbsoluteBlockPresenter) absoluteBlockView.getPresenter();
    aggregateValueTypeView = new AggregateValueTypeView();
    aggregateValueTypePresenter =
        (AggregateValueTypePresenter) aggregateValueTypeView.getPresenter();
    relativeBlockView = new BlockView();
    relativeBlockPresenter = (BlockPresenter) relativeBlockView.getPresenter();
    boundaryView = new BoundaryView();
    boundaryPresenter = (BoundaryPresenter) boundaryView.getPresenter();
    coordinatesTypeView = new CoordinatesTypeView();
    coordinatesTypePresenter = (CoordinatesTypePresenter) coordinatesTypeView.getPresenter();
    dataChunkSizeView = new DataChunkSizeView();
    dataChunkSizePresenter = (DataChunkSizePresenter) dataChunkSizeView.getPresenter();
    dispersionTypeView = new DispersionTypeView();
    dispersionTypePresenter = (DispersionTypePresenter) dispersionTypeView.getPresenter();
    mergeMiddleIntronView = new MergeMiddleIntronView();
    mergeMiddleIntronPresenter = (MergeMiddleIntronPresenter) mergeMiddleIntronView.getPresenter();
    missingDataReplacementView = new MissingDataReplacementView();
    missingDataReplacementPresenter =
        (MissingDataReplacementPresenter) missingDataReplacementView.getPresenter();
    referencePointsView = new ReferencePointsView();
    referencePointsPresenter = (ReferencePointsPresenter) referencePointsView.getPresenter();
    smoothDataView = new SmoothDataView();
    smoothDataPresenter = (SmoothDataPresenter) smoothDataView.getPresenter();
    windowSizeView = new WindowSizeView();
    windowSizePresenter = (WindowSizePresenter) windowSizeView.getPresenter();
    generateAggregateGraphsView = new GenerateAggregateGraphsView();
    generateAggregateGraphsPresenter =
        (GenerateAggregateGraphsPresenter) generateAggregateGraphsView.getPresenter();
    generateAggregateGraphsPresenter.setTab(GuiTabs.PARAMETERS);
    outputFolderView = new OutputFolderView();
    outputFolderPresenter = (OutputFolderPresenter) outputFolderView.getPresenter();
    outputHeatmapView = new OutputHeatmapView();
    outputHeatmapPresenter = (OutputHeatmapPresenter) outputHeatmapView.getPresenter();
    outputHeatmapPresenter.setTab(GuiTabs.PARAMETERS);
    prefixView = new PrefixView();
    prefixPresenter = (PrefixPresenter) prefixView.getPresenter();
    reportReferenceFeatureValuesView = new ReportReferenceFeatureValuesView();
    reportReferenceFeatureValuesPresenter =
        (ReportReferenceFeatureValuesPresenter) reportReferenceFeatureValuesView.getPresenter();
    yaxisScaleView = new YAxisScaleView();
    yaxisScalePresenter = (YAxisScalePresenter) yaxisScaleView.getPresenter();
    yaxisScalePresenter.setTab(GuiTabs.PARAMETERS);

    referenceTypeProperty.addListener(referenceTypeListener());
    annotations.setUserData(ReferenceType.ANNOTATIONS);
    exons.setUserData(ReferenceType.EXONS);
    coordinates.setUserData(ReferenceType.COORDINATES);
    referenceTypeGroup.selectedToggleProperty().addListener(referenceTypeToggleListener());
    outputFolderPresenter.initialDirectoryProperty()
        .bindBidirectional(datasetPresenter.initialDirectoryProperty());
    datasetPresenter.relativeFolderProperty().bind(outputFolderPresenter.outputFolderProperty());
    outputFolderPresenter.initialDirectoryProperty()
        .bindBidirectional(referenceGroupFilesPresenter.initialDirectoryProperty());
    referenceGroupFilesPresenter.relativeFolderProperty()
        .bind(outputFolderPresenter.outputFolderProperty());
    referenceGroupFilesPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    outputFolderPresenter.initialDirectoryProperty()
        .bindBidirectional(genomeAnnotationsPresenter.initialDirectoryProperty());
    genomeAnnotationsPresenter.relativeFolderProperty()
        .bind(outputFolderPresenter.outputFolderProperty());
    outputFolderPresenter.initialDirectoryProperty()
        .bindBidirectional(annotationFiltersPresenter.initialDirectoryProperty());
    annotationFiltersPresenter.relativeFolderProperty()
        .bind(outputFolderPresenter.outputFolderProperty());
    absolute.setUserData(RepresentationType.ABSOLUTE);
    relative.setUserData(RepresentationType.RELATIVE);
    representationTypeProperty.addListener(representationTypeListener());
    representationTypeGroup.selectedToggleProperty()
        .addListener(representationTypeToggleListener());
    referencePointsPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    boundaryPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    boundaryPresenter.referencePointsProperty()
        .bind(referencePointsPresenter.referencePointsProperty());
    smoothDataPresenter.representationTypeProperty().bind(representationTypeProperty);
    smoothDataPresenter.windowSizeProperty().bind(windowSizePresenter.windowSizeProperty());
    reportReferenceFeatureValuesPresenter.outputHeatmapProperty()
        .bind(outputHeatmapPresenter.outputHeatmapProperty());
    windowSizePresenter.representationTypeProperty().bind(representationTypeProperty);
    absoluteBlockPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    absoluteBlockPresenter.referencePointsProperty()
        .bind(referencePointsPresenter.referencePointsProperty());
    absoluteBlockPresenter.windowSizeProperty().bind(windowSizePresenter.windowSizeProperty());
    relativeBlockPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    relativeBlockPresenter.referencePointsProperty()
        .bind(referencePointsPresenter.referencePointsProperty());
    dispersionTypePresenter.aggregateValueTypeProperty()
        .bind(aggregateValueTypePresenter.aggregateValueTypeProperty());
    generateAggregateGraphsPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    generateAggregateGraphsPresenter.aggregateValueTypeProperty()
        .bind(aggregateValueTypePresenter.aggregateValueTypeProperty());

    // Default values.
    referenceTypeProperty.set(DEFAULT_REFERENCE_TYPE);
    representationTypeProperty.set(DEFAULT_REPRESENTATION_TYPE);

    updateChildren();
  }

  /**
   * Returns analysis parameters.
   *
   * @return analysis parameters
   */
  public AnalysisParameters getAnalysisParameters() {
    AnalysisParameters analysisParameters = new AnalysisParameters();
    analysisParameters.geneChartParameters.referenceType = referenceTypeProperty.get();
    analysisParameters.outputFolder = outputFolderPresenter.getOutputFolder();
    analysisParameters.dataFiles = datasetPresenter.getFiles();
    analysisParameters.referenceFiles = referenceGroupFilesPresenter.getFiles();
    analysisParameters.processDataByChunk = dataChunkSizePresenter.isProcessDataByChunk();
    analysisParameters.dataChunkSize = dataChunkSizePresenter.getDataChunkSize();
    analysisParameters.genomeAnnotations = genomeAnnotationsPresenter.getGenomeAnnotations();
    analysisParameters.selectionAnnotationFilter = annotationFiltersPresenter.getSelectionFilter();
    analysisParameters.exclusionAnnotationFilter = annotationFiltersPresenter.getExclusionFilter();
    analysisParameters.coordinatesType = coordinatesTypePresenter.getCoordinatesType();
    analysisParameters.mergeMiddleIntron = mergeMiddleIntronPresenter.getMergeMiddleIntron();
    analysisParameters.geneChartParameters.representationType = representationTypeProperty.get();
    analysisParameters.geneChartParameters.referencePoints =
        referencePointsPresenter.getReferencePoints();
    analysisParameters.geneChartParameters.boundary = boundaryPresenter.getBoundary();
    analysisParameters.geneChartParameters.windowSize = windowSizePresenter.getWindowSize();
    switch (analysisParameters.geneChartParameters.representationType) {
      case ABSOLUTE:
        analysisParameters.geneChartParameters.blocks = absoluteBlockPresenter.getBlocks();
        break;
      case RELATIVE:
        analysisParameters.geneChartParameters.blocks = relativeBlockPresenter.getBlocks();
        break;
      default:
        throw new AssertionError("RepresentationType "
            + analysisParameters.geneChartParameters.referenceType + " not covered in switch");
    }
    analysisParameters.smoothDataWindows = smoothDataPresenter.getWindowCount();
    analysisParameters.aggregateValueType = aggregateValueTypePresenter.getAggregateValueType();
    analysisParameters.missingDataReplacement =
        missingDataReplacementPresenter.getMissingDataReplacement();
    analysisParameters.ouputFilePrefix = prefixPresenter.getPrefix();
    analysisParameters.outputGraphs = generateAggregateGraphsPresenter.getOutputGraphs();
    analysisParameters.dispersionType = dispersionTypePresenter.getDispersionType();
    analysisParameters.geneChartParameters.dispersion =
        generateAggregateGraphsPresenter.isDispersion();
    analysisParameters.geneChartParameters.generateAggregateGraphs =
        generateAggregateGraphsPresenter.isGenerateAggregateGraphs();
    analysisParameters.oneDataFilePerGraph =
        generateAggregateGraphsPresenter.isOneDataFilePerGraph();
    analysisParameters.oneReferenceGroupPerGraph =
        generateAggregateGraphsPresenter.isOneReferenceGroupPerGraph();
    analysisParameters.oneOrientationPerGraph =
        generateAggregateGraphsPresenter.isOneOrientationPerGraph();
    analysisParameters.outputData = reportReferenceFeatureValuesPresenter.isOutputData();
    analysisParameters.geneChartParameters.generateHeatmap =
        outputHeatmapPresenter.isOutputHeatmap();
    analysisParameters.geneChartParameters.heatmapParameters =
        outputHeatmapPresenter.getHeatmapParameters();
    analysisParameters.geneChartParameters.yaxisScale = yaxisScalePresenter.getYAxisScale();
    boolean testDataUsed = false;
    for (File file : analysisParameters.dataFiles) {
      testDataUsed |= testData.isTestFile(file);
    }
    for (File file : analysisParameters.referenceFiles) {
      testDataUsed |= testData.isTestFile(file);
    }
    testDataUsed |= testData.isTestFile(analysisParameters.genomeAnnotations);
    testDataUsed |= testData.isTestFile(analysisParameters.selectionAnnotationFilter);
    testDataUsed |= testData.isTestFile(analysisParameters.exclusionAnnotationFilter);
    analysisParameters.testData = testDataUsed;
    return analysisParameters;
  }

  /**
   * Sets analysis parameters.
   *
   * @param parameters
   *          analysis parameters
   */
  public void setAnalysisParameters(AnalysisParameters parameters) {
    // Set general parameter.
    if (parameters.geneChartParameters.referenceType != null) {
      referenceTypeProperty.set(parameters.geneChartParameters.referenceType);
    }
    outputFolderPresenter.setOutputFolder(parameters.outputFolder);
    datasetPresenter.setFiles(parameters.dataFiles);
    dataChunkSizePresenter.setProcessDataByChunk(parameters.processDataByChunk);
    dataChunkSizePresenter.setDataChunkSize(parameters.dataChunkSize);
    if (parameters instanceof TestData) {
      TestData testData = (TestData) parameters;
      referenceGroupFilesPresenter.setMainFiles(parameters.referenceFiles);
      referenceGroupFilesPresenter.setCoordinatesFiles(testData.getCoordinatesReferenceFiles());
    } else {
      referenceGroupFilesPresenter.setFiles(parameters.referenceFiles);
    }
    genomeAnnotationsPresenter.setGenomeAnnotations(parameters.genomeAnnotations);
    coordinatesTypePresenter.setCoordinatesType(parameters.coordinatesType);
    mergeMiddleIntronPresenter.setMergeMiddleIntron(parameters.mergeMiddleIntron);
    annotationFiltersPresenter.setSelectionFilter(parameters.selectionAnnotationFilter);
    annotationFiltersPresenter.setExclusionFilter(parameters.exclusionAnnotationFilter);

    // Set representation parameter.
    if (parameters.geneChartParameters.representationType != null) {
      representationTypeProperty.set(parameters.geneChartParameters.representationType);
    }
    referencePointsPresenter.setReferencePoints(parameters.geneChartParameters.referencePoints);
    boundaryPresenter.setBoundary(parameters.geneChartParameters.boundary);
    windowSizePresenter.setWindowSize(parameters.geneChartParameters.windowSize);
    switch (representationTypeProperty.get()) {
      case ABSOLUTE:
        absoluteBlockPresenter.setBlocks(parameters.geneChartParameters.blocks);
        break;
      case RELATIVE:
        relativeBlockPresenter.setBlocks(parameters.geneChartParameters.blocks);
        break;
      default:
        break;
    }
    smoothDataPresenter.setWindowCount(parameters.smoothDataWindows);
    aggregateValueTypePresenter.setAggregateValueType(parameters.aggregateValueType);
    prefixPresenter.setPrefix(parameters.ouputFilePrefix);
    dispersionTypePresenter.setDispersionType(parameters.dispersionType);
    missingDataReplacementPresenter.setMissingDataReplacement(parameters.missingDataReplacement);
    generateAggregateGraphsPresenter.setDispersion(parameters.geneChartParameters.dispersion);
    generateAggregateGraphsPresenter
        .setGenerateAggregateGraphs(parameters.geneChartParameters.generateAggregateGraphs);
    generateAggregateGraphsPresenter.setOneDataFilePerGraph(parameters.oneDataFilePerGraph);
    generateAggregateGraphsPresenter
        .setOneReferenceGroupPerGraph(parameters.oneReferenceGroupPerGraph);
    generateAggregateGraphsPresenter.setOneOrientationPerGraph(parameters.oneOrientationPerGraph);
    reportReferenceFeatureValuesPresenter.setOutputData(parameters.outputData);
    outputHeatmapPresenter.setOutputHeatmap(parameters.geneChartParameters.generateHeatmap);
    outputHeatmapPresenter.setHeatmapParameters(parameters.geneChartParameters.heatmapParameters);
    generateAggregateGraphsPresenter.setOutputGraphs(parameters.outputGraphs);
    yaxisScalePresenter.setYAxisScale(parameters.geneChartParameters.yaxisScale);
  }

  @FXML
  private void run(ActionEvent event) {
    JavafxValidationErrorAccumulator errorHandler = new JavafxValidationErrorAccumulator();
    validate(errorHandler);
    if (!errorHandler.errors().isEmpty()) {
      // TODO Add tooltip.
      new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
          message("validationError.title"),
          errorHandler.errors().stream().map(error -> error.error).collect(Collectors.toList()));
    } else {
      final Window window = view.getScene().getWindow();
      AnalysisParameters parameters = getAnalysisParameters();
      final AnalysisTask task = analysisTaskFactory.create(parameters);
      final ProgressDialog progressDialog = new ProgressDialog(view.getScene().getWindow(), task);
      task.stateProperty().addListener(new ChangeListener<State>() {
        @Override
        public void changed(ObservableValue<? extends State> observable, State oldValue,
            State newValue) {
          if (newValue == State.FAILED || newValue == State.SUCCEEDED
              || newValue == State.CANCELLED) {
            progressDialog.hide();
          }
          if (newValue == State.FAILED) {
            // Show error message.
            Throwable error = task.getException();
            logger.error("Could not analyse data", error);
            if (error instanceof VapException) {
              List<String> errors = new ArrayList<>();
              errors.add(message("analysis.failed.message"));
              errors.addAll(((VapException) error).getErrors());
              new MessageDialog(window, MessageDialogType.ERROR, message("analysis.failed.title"),
                  errors);
            } else if (error instanceof ExecuteException) {
              new MessageDialog(window, MessageDialogType.ERROR, message("analysis.failed.title"),
                  message("analysis.failed.message"), error.getMessage(),
                  message("analysis.failed.execute.message"));
            } else {
              new MessageDialog(window, MessageDialogType.ERROR, message("analysis.failed.title"),
                  message("analysis.failed.message"), error.getMessage());
            }
          } else if (newValue == State.SUCCEEDED) {
            // Show confirm message.
            new MessageDialog(window, MessageDialogType.INFORMATION,
                message("analysis.succeeded.title"), message("analysis.succeeded.message"));
          }
        }
      });
      Thread thread = new Thread(task);
      thread.setDaemon(true);
      thread.start();
    }
  }

  @Override
  public void setMammalianDefaults() {
    referenceTypeProperty.set(ReferenceType.ANNOTATIONS);
    referencePointsPresenter.setMammalianDefaults();
    if (referenceTypeProperty.get() != ReferenceType.EXONS) {
      boundaryPresenter.setMammalianDefaults();
    }
    windowSizePresenter.setMammalianDefaults();
    absoluteBlockPresenter.setMammalianDefaults();
    relativeBlockPresenter.setMammalianDefaults();
    smoothDataPresenter.setMammalianDefaults();
    aggregateValueTypePresenter.setMammalianDefaults();
    prefixPresenter.setMammalianDefaults();
    dispersionTypePresenter.setMammalianDefaults();
    generateAggregateGraphsPresenter.setMammalianDefaults();
    reportReferenceFeatureValuesPresenter.setMammalianDefaults();
    outputHeatmapPresenter.setMammalianDefaults();
    yaxisScalePresenter.setMammalianDefaults();
  }

  @Override
  public void setYeastDefaults() {
    referenceTypeProperty.set(ReferenceType.ANNOTATIONS);
    referencePointsPresenter.setYeastDefaults();
    if (referenceTypeProperty.get() != ReferenceType.EXONS) {
      boundaryPresenter.setYeastDefaults();
    }
    windowSizePresenter.setYeastDefaults();
    absoluteBlockPresenter.setYeastDefaults();
    relativeBlockPresenter.setYeastDefaults();
    smoothDataPresenter.setYeastDefaults();
    aggregateValueTypePresenter.setYeastDefaults();
    prefixPresenter.setYeastDefaults();
    dispersionTypePresenter.setYeastDefaults();
    generateAggregateGraphsPresenter.setYeastDefaults();
    reportReferenceFeatureValuesPresenter.setYeastDefaults();
    outputHeatmapPresenter.setYeastDefaults();
    yaxisScalePresenter.setYeastDefaults();
  }

  public void useTestData() {
    setAnalysisParameters(testData);
  }

  public ObjectProperty<File> initialDirectoryProperty() {
    return outputFolderPresenter.initialDirectoryProperty();
  }

  public BooleanProperty outputHeatmapProperty() {
    return outputHeatmapPresenter.outputHeatmapProperty();
  }

  public ObjectProperty<Double> yaxisScaleFromProperty() {
    return yaxisScalePresenter.fromProperty();
  }

  public ObjectProperty<Double> yaxisScaleToProperty() {
    return yaxisScalePresenter.toProperty();
  }

  private ChangeListener<ReferenceType> referenceTypeListener() {
    return (obv, ov, nv) -> {
      switch (nv) {
        case ANNOTATIONS:
          annotations.setSelected(true);
          break;
        case EXONS:
          exons.setSelected(true);
          break;
        case COORDINATES:
          coordinates.setSelected(true);
          break;
        default:
          break;
      }
      updateChildren();
    };
  }

  private ChangeListener<Toggle> referenceTypeToggleListener() {
    return (obv, ov, nv) -> {
      if (nv != null) {
        referenceTypeProperty.set((ReferenceType) nv.getUserData());
      } else {
        referenceTypeGroup.selectToggle(ov);
      }
    };
  }

  private ChangeListener<RepresentationType> representationTypeListener() {
    return (obv, ov, nv) -> {
      switch (nv) {
        case ABSOLUTE:
          absolute.setSelected(true);
          break;
        case RELATIVE:
          relative.setSelected(true);
          break;
        default:
          break;
      }
      updateChildren();
    };
  }

  private ChangeListener<Toggle> representationTypeToggleListener() {
    return (obv, ov, nv) -> {
      if (nv != null) {
        representationTypeProperty.set((RepresentationType) nv.getUserData());
      } else {
        representationTypeGroup.selectToggle(ov);
      }
    };
  }

  private void updateChildren() {
    // File selection elements.
    List<Node> children = fileSelectionElements.getChildren();
    children.clear();
    ReferenceType referenceType = referenceTypeProperty.get();
    if (referenceType == null) {
      // Sanity check.
      return;
    }
    children.add(datasetView.getView());
    children.add(referenceGroupFilesView.getView());
    if (referenceType == ANNOTATIONS || referenceType == EXONS) {
      children.add(genomeAnnotationsView.getView());
    }
    children.add(annotationFiltersView.getView());

    // Parameters elements.
    children = parametersElements.getChildren();
    children.clear();
    RepresentationType representationType = representationTypeProperty.get();
    if (representationType == null) {
      // Sanity check.
      return;
    }
    if (representationType == RELATIVE) {
      children.add(relativeWarning);
    }
    if (referenceType == ANNOTATIONS) {
      children.add(coordinatesTypeView.getView());
    }
    children.add(referencePointsView.getView());
    if (referenceType == ANNOTATIONS) {
      children.add(boundaryView.getView());
    }
    children.add(windowSizeView.getView());
    switch (representationTypeProperty.get()) {
      case ABSOLUTE:
        children.add(absoluteBlockView.getView());
        break;
      case RELATIVE:
        children.add(relativeBlockView.getView());
        break;
      default:
        break;
    }
    if (referenceType == EXONS) {
      children.add(mergeMiddleIntronView.getView());
    }
    children.add(aggregateValueTypeView.getView());
    children.add(dispersionTypeView.getView());
    children.add(smoothDataView.getView());
    children.add(missingDataReplacementView.getView());
    children.add(dataChunkSizeView.getView());

    // Output elements,
    children = outputElements.getChildren();
    children.clear();
    children.add(outputFolderView.getView());
    children.add(prefixView.getView());
    children.add(generateAggregateGraphsView.getView());
    children.add(reportReferenceFeatureValuesView.getView());
    children.add(outputHeatmapView.getView());
    children.add(yaxisScaleView.getView());
  }

  @FXML
  private void referenceTypeHelp(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ANALYSIS_MODE);
    helpPopup.show((Node) event.getSource());
  }

  @FXML
  private void representationTypeHelp(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ANALYSIS_METHOD);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates analysis parameters.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    // Validate file selection.
    ReferenceType referenceType = referenceTypeProperty.get();
    datasetPresenter.validate(errorHandler);
    referenceGroupFilesPresenter.validate(errorHandler);
    if (referenceType == ANNOTATIONS || referenceType == EXONS) {
      genomeAnnotationsPresenter.validate(errorHandler);
    }
    annotationFiltersPresenter.validate(errorHandler);

    // Validate parameters.
    if (referenceType == ANNOTATIONS) {
      coordinatesTypePresenter.validate(errorHandler);
    }
    referencePointsPresenter.validate(errorHandler);
    if (referenceType == ANNOTATIONS) {
      boundaryPresenter.validate(errorHandler);
    }
    windowSizePresenter.validate(errorHandler);
    RepresentationType representationType = representationTypeProperty.get();
    switch (representationType) {
      case ABSOLUTE:
        absoluteBlockPresenter.validate(errorHandler);
        break;
      case RELATIVE:
        relativeBlockPresenter.validate(errorHandler);
        break;
      default:
        break;
    }
    if (referenceType == EXONS) {
      mergeMiddleIntronPresenter.validate(errorHandler);
    }
    aggregateValueTypePresenter.validate(errorHandler);
    dispersionTypePresenter.validate(errorHandler);
    smoothDataPresenter.validate(errorHandler);
    missingDataReplacementPresenter.validate(errorHandler);
    dataChunkSizePresenter.validate(errorHandler);

    // Validate output.
    outputFolderPresenter.validate(errorHandler);
    prefixPresenter.validate(errorHandler);
    if (!reportReferenceFeatureValuesPresenter.isOutputData()
        && generateAggregateGraphsPresenter.getOutputGraphs().isEmpty()) {
      errorHandler.accept(message("error.outputGraphRequired"), null);
    }
    generateAggregateGraphsPresenter.validate(errorHandler);
    reportReferenceFeatureValuesPresenter.validate(errorHandler);
    outputHeatmapPresenter.validate(errorHandler);
    yaxisScalePresenter.validate(errorHandler);
  }
}

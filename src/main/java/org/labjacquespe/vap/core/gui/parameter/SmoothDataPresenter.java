/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_SMOOTH_DATA_WINDOWS;
import static org.labjacquespe.vap.core.service.ParameterFileService.SMOOTHING_WINDOWS;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Smooth data presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SmoothDataPresenter extends BasePresenter {
  private final ObjectProperty<Integer> windowCountProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<RepresentationType> representationTypeProperty =
      new SimpleObjectProperty<>();
  private final LongProperty windowSizeProperty = new SimpleLongProperty();
  @FXML
  private Pane view;
  @FXML
  private TextField windowCount;
  @FXML
  private Pane totalBpPane;
  @FXML
  private Label totalBpLabel;
  private int totalBpPaneIndex;

  @FXML
  private void initialize() {
    totalBpPaneIndex = view.getChildren().indexOf(totalBpPane);
    Bindings.bindBidirectional(windowCount.textProperty(), windowCountProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new IntegerStringConverter(), 0));
    windowSizeProperty.addListener((ov, oldValue, newValue) -> updateTotalBpLabel());
    representationTypeProperty.addListener((ov, oldValue, newValue) -> toggleTotalBpLabel());
    windowCountProperty.addListener((ov, oldValue, newValue) -> updateTotalBpLabel());

    windowCountProperty.set(DEFAULT_SMOOTH_DATA_WINDOWS);
  }

  public LongProperty windowSizeProperty() {
    return windowSizeProperty;
  }

  public ObjectProperty<RepresentationType> representationTypeProperty() {
    return representationTypeProperty;
  }

  public ObjectProperty<Integer> windowCountProperty() {
    return windowCountProperty;
  }

  public Integer getWindowCount() {
    return windowCountProperty.get();
  }

  /**
   * Sets number of windows to smooth data.
   *
   * @param windowCount
   *          number of windows
   */
  public void setWindowCount(Integer windowCount) {
    if (windowCount != null && windowCount >= 0) {
      windowCountProperty.set(windowCount);
    } else {
      this.windowCount.setText("");
    }
  }

  private void toggleTotalBpLabel() {
    RepresentationType representationType = representationTypeProperty.get();
    if (representationType != null) {
      switch (representationType) {
        case ABSOLUTE:
          if (!view.getChildren().contains(totalBpPane)) {
            view.getChildren().add(totalBpPaneIndex, totalBpPane);
          }
          break;
        case RELATIVE:
          view.getChildren().remove(totalBpPane);
          break;
        default:
          break;
      }
    }
  }

  private void updateTotalBpLabel() {
    long windowSize = windowSizeProperty.get();
    long windowCount = windowCountProperty.get();

    totalBpLabel.setText(String.valueOf(windowSize * windowCount));
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(SMOOTHING_WINDOWS);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates number of windows to smooth data.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    windowCount.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
      windowCount.getStyleClass().add("error");
    }));
    try {
      NumberStringConverter converter = new NumberStringConverter();
      int value = converter.fromString(windowCount.getText()).intValue();
      if (value < 0) {
        errorHandler.accept(message("error.underMinimum"), null);
      } else if (value % 2 != 0) {
        errorHandler.accept(message("error.notEven"), null);
      }
    } catch (Exception e) {
      errorHandler.accept(message("error.invalidNumber"), null);
    }
  }
}

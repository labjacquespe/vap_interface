/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_VALUE;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOWS_PER_BLOCK;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.converter.LongStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.service.GeneChartService;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.BlockAlignment;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.SplitAlignment;
import org.labjacquespe.vap.core.SplitType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Control for absolute blocks.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbsoluteBlockPresenter extends BasePresenter {
  private static final int BLOCK_START_COLUMN = 2;
  private static final int MAX_ELEMENTS = 7;
  private static final BlockAlignment DEFAULT_BLOCK_ALIGNMENT = BlockAlignment.SPLIT;
  private static final SplitType DEFAULT_SPLIT_TYPE = SplitType.PERCENTAGE;
  private static final SplitAlignment DEFAULT_SPLIT_ALIGNMENT = SplitAlignment.LEFT;
  private static final int SPLIT_TYPE_ROW = 8;
  private static final int SPLIT_ALIGNMENT_ROW = 9;
  private static final int SPLIT_VALUE_ROW = 10;
  private static final int TOTAL_SPLIT_BP_ROW = 11;

  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final IntegerProperty referencePointsProperty = new SimpleIntegerProperty();
  private final LongProperty windowSizeProperty = new SimpleLongProperty();
  private final BooleanProperty showAdvancedProperty = new SimpleBooleanProperty();
  @FXML
  private GridPane view;
  private final BlockFx[] blocks = new BlockFx[MAX_ELEMENTS];
  private Label[] blockLabels = new Label[MAX_ELEMENTS];
  @FXML
  private Label windowCountLabel;
  private TextField[] windows = new TextField[MAX_ELEMENTS];
  private HBox[] totalBpsBox = new HBox[MAX_ELEMENTS];
  private Label[] totalBps = new Label[MAX_ELEMENTS];
  @FXML
  private Label blockAlignmentLabel;
  private Label[] alignmentImageLabels = new Label[MAX_ELEMENTS];
  private Label[] forcedAlignmentImageLabels = new Label[MAX_ELEMENTS];
  private ToggleGroup[] alignmentGroups = new ToggleGroup[MAX_ELEMENTS];
  private RadioButton[] lefts = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] rights = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] splits = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedLefts = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedRights = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedSplits = new RadioButton[MAX_ELEMENTS];
  @FXML
  private ToggleButton showAdvanced;
  @FXML
  private Label splitTypeLabel;
  @FXML
  private Button splitTypeHelp;
  private ToggleGroup[] splitTypeGroups = new ToggleGroup[MAX_ELEMENTS];
  private HBox[] splitTypeBoxes = new HBox[MAX_ELEMENTS];
  private RadioButton[] percentages = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] absolutes = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedPercentages = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedAbsolutes = new RadioButton[MAX_ELEMENTS];
  @FXML
  private Label splitAlignmentLabel;
  @FXML
  private Button splitAlignmentHelp;
  private ToggleGroup[] splitAlignmentGroups = new ToggleGroup[MAX_ELEMENTS];
  private HBox[] splitAlignmentBoxes = new HBox[MAX_ELEMENTS];
  private RadioButton[] splitLefts = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] splitRights = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedSplitLefts = new RadioButton[MAX_ELEMENTS];
  private RadioButton[] forcedSplitRights = new RadioButton[MAX_ELEMENTS];
  @FXML
  private Label splitValueLabel;
  @FXML
  private Button splitValueHelp;
  private StackPane[] splitStacks = new StackPane[MAX_ELEMENTS];
  private TextField[] splitValues = new TextField[MAX_ELEMENTS];
  private TextField[] forcedSplitValues = new TextField[MAX_ELEMENTS];
  private HBox[] totalSplitBpsBox = new HBox[MAX_ELEMENTS];
  private Label[] totalSplitBps = new Label[MAX_ELEMENTS];
  @Autowired
  private GeneChartService geneChartService;

  @FXML
  private void initialize() {
    referenceTypeProperty.addListener((ov, oldValue, newValue) -> updateBlocksType());
    referencePointsProperty.addListener((ov, oldValue, newValue) -> updateBlocksType());
    referencePointsProperty.addListener((ov, oldValue, newValue) -> updateVisible());
    windowSizeProperty.addListener(updateTotalBpLabelListener(null));
    windowSizeProperty.addListener(updateTotalSplitBpLabelListener(null));
    showAdvancedProperty.addListener((ov, oldValue, newValue) -> showAdvancedListener());
    for (int i = 0; i < blocks.length; i++) {
      blocks[i] = new BlockFx();
      blocks[i].windowsProperty().addListener(updateTotalBpLabelListener(i));
      blocks[i].windowsProperty().addListener((ov, oldValue, newValue) -> updateVisible());
      blocks[i].alignmentProperty().addListener(blockAlignmentListener(i));
      blocks[i].alignmentProperty().addListener(disableSplitListener(i));
      blocks[i].splitTypeProperty().addListener((ov, oldValue, newValue) -> updateVisible());
      blocks[i].splitTypeProperty().addListener(splitTypeListener(i));
      blocks[i].splitProperty().addListener(splitValueListener(i));
      blocks[i].splitProperty().addListener(updateTotalSplitBpLabelListener(i));
      blocks[i].splitAlignmentProperty().addListener(splitAlignmentListener(i));
      blocks[i].typeProperty().addListener(updateBlockLabelListener(i));
    }
    showAdvanced.selectedProperty().bindBidirectional(showAdvancedProperty);

    for (int i = 0; i < blockLabels.length; i++) {
      StackPane pane = new StackPane();
      pane.getStyleClass().addAll("stack-block");
      view.add(pane, BLOCK_START_COLUMN + i, 0);
      blockLabels[i] = new Label();
      blockLabels[i].getStyleClass().addAll("label-block");
      pane.getChildren().add(blockLabels[i]);
    }
    for (int i = 0; i < windows.length; i++) {
      windows[i] = new TextField();
      windows[i].setPrefColumnCount(8);
      windows[i].textProperty().bindBidirectional(blocks[i].windowsProperty(),
          new DefaultValueOnExceptionAndNullConverter<>(new LongStringConverter(), 0L));
      view.add(windows[i], BLOCK_START_COLUMN + i, 1);
    }
    for (int i = 0; i < totalBps.length; i++) {
      totalBpsBox[i] = new HBox();
      totalBpsBox[i].getStyleClass().addAll("value-unit");
      view.add(totalBpsBox[i], BLOCK_START_COLUMN + i, 2);
      totalBps[i] = new Label();
      totalBpsBox[i].getChildren().add(totalBps[i]);
      Label unit = new Label();
      unit.setText(message("totalBps.unit"));
      totalBpsBox[i].getChildren().add(unit);
    }
    StackPane[] blockAlignmentImageStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < blockAlignmentImageStacks.length; i++) {
      blockAlignmentImageStacks[i] = new StackPane();
      blockAlignmentImageStacks[i].getStyleClass().addAll("stack-forced");
      view.add(blockAlignmentImageStacks[i], BLOCK_START_COLUMN + i, 3);
    }
    for (int i = 0; i < alignmentImageLabels.length; i++) {
      alignmentImageLabels[i] = new Label();
      alignmentImageLabels[i].getStyleClass().add(BLOCK_ALIGNMENT);
      blockAlignmentImageStacks[i].getChildren().add(alignmentImageLabels[i]);
    }
    for (int i = 0; i < forcedAlignmentImageLabels.length; i++) {
      forcedAlignmentImageLabels[i] = new Label();
      forcedAlignmentImageLabels[i].setVisible(false);
      forcedAlignmentImageLabels[i].getStyleClass().add(BLOCK_ALIGNMENT);
      blockAlignmentImageStacks[i].getChildren().add(forcedAlignmentImageLabels[i]);
    }
    for (int i = 1; i < forcedAlignmentImageLabels.length; i++) {
      BlockAlignment blockAlignment = BlockAlignment.LEFT;
      forcedAlignmentImageLabels[i].setText(message("blockAlignment." + blockAlignment.name()));
      forcedAlignmentImageLabels[i].getStyleClass().addAll(blockAlignment.name(), "forced");
    }
    forcedAlignmentImageLabels[0].setText(message("blockAlignment." + BlockAlignment.RIGHT.name()));
    forcedAlignmentImageLabels[0].getStyleClass().addAll(BlockAlignment.RIGHT.name(), "forced");
    for (int i = 0; i < alignmentGroups.length; i++) {
      int index = i;
      alignmentGroups[i] = new ToggleGroup();
      alignmentGroups[i].selectedToggleProperty().addListener((ov, oldValue,
          newValue) -> blocks[index].setAlignment((BlockAlignment) newValue.getUserData()));
    }
    StackPane[] leftStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < leftStacks.length; i++) {
      leftStacks[i] = new StackPane();
      leftStacks[i].getStyleClass().addAll("stack-forced");
      view.add(leftStacks[i], BLOCK_START_COLUMN + i, 4);
    }
    for (int i = 0; i < lefts.length; i++) {
      lefts[i] = new RadioButton();
      lefts[i].setToggleGroup(alignmentGroups[i]);
      lefts[i].setText(message("blockAlignment.LEFT"));
      lefts[i].setUserData(BlockAlignment.LEFT);
      leftStacks[i].getChildren().add(lefts[i]);
    }
    for (int i = 0; i < forcedLefts.length; i++) {
      forcedLefts[i] = new RadioButton();
      forcedLefts[i].setDisable(true);
      forcedLefts[i].setVisible(false);
      forcedLefts[i].setText(message("blockAlignment.LEFT"));
      leftStacks[i].getChildren().add(forcedLefts[i]);
    }
    StackPane[] rightStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < rightStacks.length; i++) {
      rightStacks[i] = new StackPane();
      rightStacks[i].getStyleClass().addAll("stack-forced");
      view.add(rightStacks[i], BLOCK_START_COLUMN + i, 5);
    }
    for (int i = 0; i < rights.length; i++) {
      rights[i] = new RadioButton();
      rights[i].setToggleGroup(alignmentGroups[i]);
      rights[i].setText(message("blockAlignment.RIGHT"));
      rights[i].setUserData(BlockAlignment.RIGHT);
      rightStacks[i].getChildren().add(rights[i]);
    }
    for (int i = 0; i < forcedRights.length; i++) {
      forcedRights[i] = new RadioButton();
      forcedRights[i].setDisable(true);
      forcedRights[i].setVisible(false);
      forcedRights[i].setText(message("blockAlignment.RIGHT"));
      rightStacks[i].getChildren().add(forcedRights[i]);
    }
    StackPane[] splitsStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < splitsStacks.length; i++) {
      splitsStacks[i] = new StackPane();
      splitsStacks[i].getStyleClass().addAll("stack-forced");
      view.add(splitsStacks[i], BLOCK_START_COLUMN + i, 6);
    }
    for (int i = 0; i < splits.length; i++) {
      splits[i] = new RadioButton();
      splits[i].setToggleGroup(alignmentGroups[i]);
      splits[i].setText(message("blockAlignment.SPLIT"));
      splits[i].setUserData(BlockAlignment.SPLIT);
      splitsStacks[i].getChildren().add(splits[i]);
    }
    for (int i = 0; i < forcedSplits.length; i++) {
      forcedSplits[i] = new RadioButton();
      forcedSplits[i].setDisable(true);
      forcedSplits[i].setVisible(false);
      forcedSplits[i].setText(message("blockAlignment.SPLIT"));
      splitsStacks[i].getChildren().add(forcedSplits[i]);
    }

    for (int i = 0; i < splitTypeGroups.length; i++) {
      splitTypeGroups[i] = new ToggleGroup();
      splitTypeGroups[i].selectedToggleProperty().addListener(splitTypeToggleListener(i));
    }
    for (int i = 0; i < splitTypeBoxes.length; i++) {
      splitTypeBoxes[i] = new HBox();
      splitTypeBoxes[i].getStyleClass().add("split-type");
      view.add(splitTypeBoxes[i], BLOCK_START_COLUMN + i, SPLIT_TYPE_ROW);
    }
    StackPane[] percentageSplitTypeStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < percentageSplitTypeStacks.length; i++) {
      percentageSplitTypeStacks[i] = new StackPane();
      percentageSplitTypeStacks[i].getStyleClass().addAll("stack-forced");
      splitTypeBoxes[i].getChildren().add(percentageSplitTypeStacks[i]);
    }
    StackPane[] absoluteSplitTypeStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < absoluteSplitTypeStacks.length; i++) {
      absoluteSplitTypeStacks[i] = new StackPane();
      absoluteSplitTypeStacks[i].getStyleClass().addAll("stack-forced");
      splitTypeBoxes[i].getChildren().add(absoluteSplitTypeStacks[i]);
    }
    for (int i = 0; i < percentages.length; i++) {
      percentages[i] = new RadioButton();
      percentages[i].setToggleGroup(splitTypeGroups[i]);
      percentages[i].setText(message("splitType.PERCENTAGE"));
      percentages[i].setUserData(SplitType.PERCENTAGE);
      percentageSplitTypeStacks[i].getChildren().add(percentages[i]);
    }
    for (int i = 0; i < absolutes.length; i++) {
      absolutes[i] = new RadioButton();
      absolutes[i].setToggleGroup(splitTypeGroups[i]);
      absolutes[i].setText(message("splitType.ABSOLUTE"));
      absolutes[i].setUserData(SplitType.ABSOLUTE);
      absoluteSplitTypeStacks[i].getChildren().add(absolutes[i]);
    }
    for (int i = 0; i < forcedPercentages.length; i++) {
      forcedPercentages[i] = new RadioButton();
      forcedPercentages[i].setDisable(true);
      forcedPercentages[i].setVisible(false);
      forcedPercentages[i].setText(message("splitType.PERCENTAGE"));
      percentageSplitTypeStacks[i].getChildren().add(forcedPercentages[i]);
    }
    for (int i = 0; i < forcedAbsolutes.length; i++) {
      forcedAbsolutes[i] = new RadioButton();
      forcedAbsolutes[i].setDisable(true);
      forcedAbsolutes[i].setVisible(false);
      forcedAbsolutes[i].setText(message("splitType.ABSOLUTE"));
      absoluteSplitTypeStacks[i].getChildren().add(forcedAbsolutes[i]);
    }
    for (int i = 0; i < splitAlignmentGroups.length; i++) {
      splitAlignmentGroups[i] = new ToggleGroup();
      splitAlignmentGroups[i].selectedToggleProperty().addListener(splitAlignmentToggleListener(i));
    }
    for (int i = 0; i < splitAlignmentBoxes.length; i++) {
      splitAlignmentBoxes[i] = new HBox();
      splitAlignmentBoxes[i].getStyleClass().add("split-alignment");
      view.add(splitAlignmentBoxes[i], BLOCK_START_COLUMN + i, SPLIT_ALIGNMENT_ROW);
    }
    StackPane[] leftSplitAlignmentStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < leftSplitAlignmentStacks.length; i++) {
      leftSplitAlignmentStacks[i] = new StackPane();
      leftSplitAlignmentStacks[i].getStyleClass().addAll("stack-forced");
      splitAlignmentBoxes[i].getChildren().add(leftSplitAlignmentStacks[i]);
    }
    StackPane[] rightSplitAlignmentStacks = new StackPane[MAX_ELEMENTS];
    for (int i = 0; i < rightSplitAlignmentStacks.length; i++) {
      rightSplitAlignmentStacks[i] = new StackPane();
      rightSplitAlignmentStacks[i].getStyleClass().addAll("stack-forced");
      splitAlignmentBoxes[i].getChildren().add(rightSplitAlignmentStacks[i]);
    }
    for (int i = 0; i < splitLefts.length; i++) {
      splitLefts[i] = new RadioButton();
      splitLefts[i].setToggleGroup(splitAlignmentGroups[i]);
      splitLefts[i].setText(message("splitAlignment.LEFT"));
      splitLefts[i].setUserData(SplitAlignment.LEFT);
      leftSplitAlignmentStacks[i].getChildren().add(splitLefts[i]);
    }
    for (int i = 0; i < splitRights.length; i++) {
      splitRights[i] = new RadioButton();
      splitRights[i].setToggleGroup(splitAlignmentGroups[i]);
      splitRights[i].setText(message("splitAlignment.RIGHT"));
      splitRights[i].setUserData(SplitAlignment.RIGHT);
      rightSplitAlignmentStacks[i].getChildren().add(splitRights[i]);
    }
    for (int i = 0; i < forcedSplitLefts.length; i++) {
      forcedSplitLefts[i] = new RadioButton();
      forcedSplitLefts[i].setDisable(true);
      forcedSplitLefts[i].setVisible(false);
      forcedSplitLefts[i].setText(message("splitAlignment.LEFT"));
      leftSplitAlignmentStacks[i].getChildren().add(forcedSplitLefts[i]);
    }
    for (int i = 0; i < forcedSplitRights.length; i++) {
      forcedSplitRights[i] = new RadioButton();
      forcedSplitRights[i].setDisable(true);
      forcedSplitRights[i].setVisible(false);
      forcedSplitRights[i].setText(message("splitAlignment.RIGHT"));
      rightSplitAlignmentStacks[i].getChildren().add(forcedSplitRights[i]);
    }
    for (int i = 0; i < splitStacks.length; i++) {
      splitStacks[i] = new StackPane();
      splitStacks[i].getStyleClass().addAll("stack-forced");
      view.add(splitStacks[i], BLOCK_START_COLUMN + i, SPLIT_VALUE_ROW);
    }
    for (int i = 0; i < splitValues.length; i++) {
      splitValues[i] = new TextField();
      splitValues[i].setPrefColumnCount(8);
      splitStacks[i].getChildren().add(splitValues[i]);
      splitValues[i].textProperty().bindBidirectional(blocks[i].splitProperty(),
          new DefaultValueOnExceptionAndNullConverter<>(new LongStringConverter(), 0L));
    }
    for (int i = 0; i < forcedSplitValues.length; i++) {
      forcedSplitValues[i] = new TextField();
      forcedSplitValues[i].setPrefColumnCount(8);
      forcedSplitValues[i].setDisable(true);
      forcedSplitValues[i].setVisible(false);
      splitStacks[i].getChildren().add(forcedSplitValues[i]);
    }
    for (int i = 0; i < totalSplitBps.length; i++) {
      totalSplitBpsBox[i] = new HBox();
      totalSplitBpsBox[i].getStyleClass().addAll("value-unit");
      view.add(totalSplitBpsBox[i], BLOCK_START_COLUMN + i, TOTAL_SPLIT_BP_ROW);
      totalSplitBps[i] = new Label();
      totalSplitBpsBox[i].getChildren().add(totalSplitBps[i]);
      Label unit = new Label();
      unit.setText(message("totalSplitBps.unit"));
      totalSplitBpsBox[i].getChildren().add(unit);
    }

    // Initialise default values.
    showAdvanced.setSelected(false);
    for (int i = 0; i < windows.length; i++) {
      blocks[i].setWindows(20L);
    }
    for (int i = 0; i < splits.length; i++) {
      blocks[i].setAlignment(DEFAULT_BLOCK_ALIGNMENT);
    }
    forcedRights[0].setSelected(true);
    for (int i = 1; i < forcedLefts.length; i++) {
      forcedLefts[i].setSelected(true);
    }
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].setSplitType(DEFAULT_SPLIT_TYPE);
      forcedPercentages[i].setSelected(true);
    }
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].setSplitAlignment(DEFAULT_SPLIT_ALIGNMENT);
      forcedSplitLefts[i].setSelected(true);
    }
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].setSplit(50L);
      forcedSplitValues[i].setText("100");
    }
    blocks[2].setWindows(60L);
    updateChildren();
  }

  private void updateBlocksType() {
    ReferenceType referenceType = referenceTypeProperty.get();
    Integer referencePoints = Math.min(referencePointsProperty.get(), MAX_ELEMENTS - 1);

    for (int index = 0; index < blockLabels.length; index++) {
      BlockType blockType = geneChartService.regionType(index, referenceType, referencePoints);
      blocks[index].setType(blockType);
    }
  }

  public List<Block> getBlocks() {
    return Arrays.asList(blocks).stream().map(b -> b.toBlock()).collect(Collectors.toList());
  }

  /**
   * Sets block.
   *
   * @param blocks
   *          blocks
   */
  public void setBlocks(List<Block> blocks) {
    if (blocks != null) {
      int max = Math.min(blocks.size(), this.blocks.length);
      for (int i = 0; i < max; i++) {
        this.blocks[i].copy(blocks.get(i));
      }
    } else {
      for (int i = 0; i < this.blocks.length; i++) {
        this.blocks[i].copy(new Block());
      }
    }
    updateBlocksType();
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public IntegerProperty referencePointsProperty() {
    return referencePointsProperty;
  }

  public LongProperty windowSizeProperty() {
    return windowSizeProperty;
  }

  private void updateChildren() {
    view.getChildren().remove(splitTypeLabel);
    view.getChildren().remove(splitTypeHelp);
    view.getChildren().removeAll(splitTypeBoxes);
    view.getChildren().remove(splitAlignmentLabel);
    view.getChildren().remove(splitAlignmentHelp);
    view.getChildren().removeAll(splitAlignmentBoxes);
    view.getChildren().remove(splitValueLabel);
    view.getChildren().remove(splitValueHelp);
    view.getChildren().removeAll(splitStacks);
    view.getChildren().removeAll(totalSplitBpsBox);
    if (showAdvancedProperty.get()) {
      view.add(splitTypeLabel, 0, SPLIT_TYPE_ROW);
      view.add(splitTypeHelp, 1, SPLIT_TYPE_ROW);
      for (int i = 0; i < splitTypeBoxes.length; i++) {
        view.add(splitTypeBoxes[i], BLOCK_START_COLUMN + i, SPLIT_TYPE_ROW);
      }
      view.add(splitAlignmentLabel, 0, SPLIT_ALIGNMENT_ROW);
      view.add(splitAlignmentHelp, 1, SPLIT_ALIGNMENT_ROW);
      for (int i = 0; i < splitAlignmentBoxes.length; i++) {
        view.add(splitAlignmentBoxes[i], BLOCK_START_COLUMN + i, SPLIT_ALIGNMENT_ROW);
      }
      view.add(splitValueLabel, 0, SPLIT_VALUE_ROW);
      view.add(splitValueHelp, 1, SPLIT_VALUE_ROW);
      for (int i = 0; i < splitStacks.length; i++) {
        view.add(splitStacks[i], BLOCK_START_COLUMN + i, SPLIT_VALUE_ROW);
      }
      for (int i = 0; i < totalSplitBpsBox.length; i++) {
        view.add(totalSplitBpsBox[i], BLOCK_START_COLUMN + i, TOTAL_SPLIT_BP_ROW);
      }
    }
  }

  private ChangeListener<BlockType> updateBlockLabelListener(int index) {
    return (ov, oldValue, newValue) -> {
      Label blockLabel = blockLabels[index];
      for (BlockType blockType : BlockType.values()) {
        blockLabel.getStyleClass().remove(blockType.name());
      }
      if (newValue != null) {
        blockLabel.setText(message("blockLabels." + newValue.name()));
        blockLabel.getStyleClass().add(newValue.name());
      } else {
        blockLabel.setText("");
      }
    };
  }

  private void updateVisible() {
    Integer referencePoints = referencePointsProperty.get();

    // Update visibility of inputs.
    for (int index = 0; index < MAX_ELEMENTS; index++) {
      boolean visible = index < 2 || (referencePoints != null && index <= referencePoints);
      boolean blockAlignmentVisible =
          index != 0 && referencePoints != null && index < referencePoints;
      boolean forcedLeft = blocks[index].getWindows() != null && blocks[index].getWindows() == 1;
      blockLabels[index].setVisible(visible);
      windows[index].setVisible(visible);
      totalBpsBox[index].setVisible(visible);
      alignmentImageLabels[index].setVisible(blockAlignmentVisible && visible);
      forcedAlignmentImageLabels[index].setVisible(!blockAlignmentVisible && visible);
      lefts[index].setVisible(blockAlignmentVisible && !forcedLeft && visible);
      rights[index].setVisible(blockAlignmentVisible && !forcedLeft && visible);
      splits[index].setVisible(blockAlignmentVisible && !forcedLeft && visible);
      forcedLefts[index].setVisible((!blockAlignmentVisible || forcedLeft) && visible);
      forcedRights[index].setVisible((!blockAlignmentVisible || forcedLeft) && visible);
      forcedSplits[index].setVisible((!blockAlignmentVisible || forcedLeft) && visible);
      percentages[index].setVisible(blockAlignmentVisible && visible);
      absolutes[index].setVisible(blockAlignmentVisible && visible);
      forcedPercentages[index].setVisible(!blockAlignmentVisible && visible);
      forcedAbsolutes[index].setVisible(!blockAlignmentVisible && visible);
      splitLefts[index].setVisible(blockAlignmentVisible && visible);
      splitRights[index].setVisible(blockAlignmentVisible && visible);
      forcedSplitLefts[index].setVisible(!blockAlignmentVisible && visible);
      forcedSplitRights[index].setVisible(!blockAlignmentVisible && visible);
      splitValues[index].setVisible(blockAlignmentVisible && visible);
      forcedSplitValues[index].setVisible(!blockAlignmentVisible && visible);
      totalSplitBpsBox[index].setVisible(
          blockAlignmentVisible && visible && blocks[index].getSplitType() == SplitType.ABSOLUTE);
    }

    // If reference points is 1, disable split labels.
    splitTypeLabel.setDisable(referencePoints == null || referencePoints == 1);
    splitValueLabel.setDisable(referencePoints == null || referencePoints == 1);
    splitAlignmentLabel.setDisable(referencePoints == null || referencePoints == 1);
  }

  private ChangeListener<Object> updateTotalBpLabelListener(Integer index) {
    return (ov, oldValue, newValue) -> {
      if (index != null) {
        updateTotalBpLabel(index);
      } else {
        for (int i = 0; i < MAX_ELEMENTS; i++) {
          updateTotalBpLabel(i);
        }
      }
    };
  }

  private void updateTotalBpLabel(int index) {
    long windowSize = windowSizeProperty.get();

    Label totalBp = totalBps[index];
    Long windowCount = blocks[index].getWindows();

    if (windowCount != null) {
      totalBp.setText(String.valueOf(windowCount * windowSize));
    } else {
      totalBp.setText("");
    }
  }

  private ChangeListener<BlockAlignment> blockAlignmentListener(int index) {
    return (ov, oldValue, newValue) -> {
      BlockAlignment blockAlignment = blocks[index].getAlignment();

      // Update radio button.
      if (newValue == null) {
        // Invalid value.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            blocks[index].setAlignment(DEFAULT_BLOCK_ALIGNMENT);
          }
        });
      } else {
        switch (newValue) {
          case LEFT:
            lefts[index].setSelected(true);
            break;
          case RIGHT:
            rights[index].setSelected(true);
            break;
          case SPLIT:
            splits[index].setSelected(true);
            break;
          default:
            break;
        }
      }

      // Update representation type label.
      Label representationTypeLabel = alignmentImageLabels[index];
      for (BlockAlignment removeClass : BlockAlignment.values()) {
        representationTypeLabel.getStyleClass().removeAll(removeClass.name());
      }
      if (blockAlignment != null) {
        representationTypeLabel.setText(message("blockAlignment." + blockAlignment.name()));
        representationTypeLabel.getStyleClass().add(blockAlignment.name());
      }
    };
  }

  private void showAdvancedListener() {
    showAdvanced.setText(message("showAdvanced." + showAdvancedProperty.get()));
    updateChildren();
  }

  private ChangeListener<BlockAlignment> disableSplitListener(int index) {
    return (ov, oldValue, newValue) -> {
      boolean disabled = newValue != BlockAlignment.SPLIT;
      percentages[index].setDisable(disabled);
      absolutes[index].setDisable(disabled);
      splitValues[index].setDisable(disabled);
      splitLefts[index].setDisable(disabled);
      splitRights[index].setDisable(disabled);
    };
  }

  private ChangeListener<SplitType> splitTypeListener(int index) {
    return (ov, oldValue, newValue) -> {
      if (newValue == null) {
        // Invalid value.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            blocks[index].setSplitType(DEFAULT_SPLIT_TYPE);
          }
        });
      } else {
        switch (newValue) {
          case ABSOLUTE:
            absolutes[index].setSelected(true);
            break;
          case PERCENTAGE:
            percentages[index].setSelected(true);
            break;
          default:
            break;
        }
      }

      // Update representation type label.
      Label representationTypeLabel = alignmentImageLabels[index];
      for (SplitType removeClass : SplitType.values()) {
        representationTypeLabel.getStyleClass().removeAll(removeClass.name());
      }
      if (newValue != null) {
        representationTypeLabel.getStyleClass().add(newValue.name());
      }
    };
  }

  private ChangeListener<Toggle> splitTypeToggleListener(int index) {
    return (ov, oldValue, newValue) -> {
      if (newValue == null) {
        blocks[index].setSplitType(null);
      } else {
        blocks[index].setSplitType((SplitType) newValue.getUserData());
      }
    };
  }

  private ChangeListener<SplitAlignment> splitAlignmentListener(int index) {
    return (ov, oldValue, newValue) -> {
      if (newValue == null) {
        // Invalid value.
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            blocks[index].setSplitAlignment(DEFAULT_SPLIT_ALIGNMENT);
          }
        });
      } else {
        switch (newValue) {
          case LEFT:
            splitLefts[index].setSelected(true);
            break;
          case RIGHT:
            splitRights[index].setSelected(true);
            break;
          default:
            break;
        }
      }

      // Update representation type label.
      Label representationTypeLabel = alignmentImageLabels[index];
      for (SplitAlignment removeClass : SplitAlignment.values()) {
        representationTypeLabel.getStyleClass().removeAll("PA_" + removeClass.name());
      }
      if (newValue != null) {
        representationTypeLabel.getStyleClass().add("PA_" + newValue.name());
      }
    };
  }

  private ChangeListener<Toggle> splitAlignmentToggleListener(int index) {
    return (ov, oldValue, newValue) -> {
      if (newValue == null) {
        blocks[index].setSplitAlignment(null);
      } else {
        blocks[index].setSplitAlignment((SplitAlignment) newValue.getUserData());
      }
    };
  }

  private ChangeListener<Number> splitValueListener(int index) {
    return (ov, oldValue, newValue) -> {
      // Update representation type label.
      Label representationTypeLabel = alignmentImageLabels[index];
      representationTypeLabel.getStyleClass().removeAll("P_50");
      representationTypeLabel.getStyleClass().removeAll("P_under_50");
      representationTypeLabel.getStyleClass().removeAll("P_over_50");
      if (newValue != null) {
        if (newValue.longValue() == 50) {
          representationTypeLabel.getStyleClass().add("P_50");
        } else if (newValue.longValue() < 50) {
          representationTypeLabel.getStyleClass().add("P_under_50");
        } else {
          representationTypeLabel.getStyleClass().add("P_over_50");
        }
      }
    };
  }

  private <O> ChangeListener<O> updateTotalSplitBpLabelListener(Integer index) {
    return (ov, oldValue, newValue) -> {
      if (index != null) {
        updateTotalSplitBpLabel(index);
      } else {
        for (int i = 0; i < MAX_ELEMENTS; i++) {
          updateTotalSplitBpLabel(i);
        }
      }
    };
  }

  private void updateTotalSplitBpLabel(int index) {
    long windowSize = windowSizeProperty.get();

    Label totalSplitBp = totalSplitBps[index];
    Long split = blocks[index].getSplit();

    if (split != null) {
      totalSplitBp.setText(String.valueOf(split * windowSize));
    } else {
      totalSplitBp.setText("");
    }
  }

  /**
   * Shows help pop-up for windows.
   *
   * @param event
   *          event
   */
  public void helpWindows(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(WINDOWS_PER_BLOCK);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Shows help pop-up for block alignment.
   *
   * @param event
   *          event
   */
  public void helpBlockAlignment(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(BLOCK_ALIGNMENT);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Shows help pop-up for split type.
   *
   * @param event
   *          event
   */
  public void helpSplitType(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(BLOCK_SPLIT_TYPE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Shows help pop-up for split size.
   *
   * @param event
   *          event
   */
  public void helpSplit(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(BLOCK_SPLIT_VALUE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Shows help pop-up for split alignment.
   *
   * @param event
   *          event
   */
  public void helpSplitAlignment(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(BLOCK_SPLIT_ALIGNMENT);
    helpPopup.show((Node) event.getSource());
  }

  @Override
  public void setMammalianDefaults() {
    blocks[0].setWindows(200L);
    blocks[1].setWindows(600L);
    blocks[2].setWindows(200L);
    blocks[1].setAlignment(BlockAlignment.SPLIT);
    for (BlockFx block : blocks) {
      block.setSplitType(SplitType.PERCENTAGE);
      block.setSplit(50L);
      block.setSplitAlignment(SplitAlignment.LEFT);
    }
  }

  @Override
  public void setYeastDefaults() {
    blocks[0].setWindows(20L);
    blocks[1].setWindows(10L);
    blocks[2].setWindows(30L);
    blocks[3].setWindows(10L);
    blocks[4].setWindows(20L);
    blocks[1].setAlignment(BlockAlignment.SPLIT);
    blocks[2].setAlignment(BlockAlignment.SPLIT);
    blocks[3].setAlignment(BlockAlignment.SPLIT);
    for (BlockFx block : blocks) {
      block.setSplitType(SplitType.PERCENTAGE);
      block.setSplit(50L);
      block.setSplitAlignment(SplitAlignment.LEFT);
    }
  }

  /**
   * Validates blocks.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    validateWindowCounts(errorHandler);
    validateBlockAlignments(errorHandler);
    validateBlockSplitTypes(errorHandler);
    validateBlockSplitAlignments(errorHandler);
    validateBlockSplits(errorHandler);
  }

  private void validateWindowCounts(BiConsumer<String, String> errorHandler) {
    windowCountLabel.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      windowCountLabel.getStyleClass().add("error");
    }));
    for (int i = 0; i < windows.length; i++) {
      TextField window = windows[i];
      window.getStyleClass().remove("error");
      BiConsumer<String, String> windowErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            window.getStyleClass().add("error");
          }));
      if (window.isVisible()) {
        try {
          NumberStringConverter converter = new NumberStringConverter();
          long value = converter.fromString(window.getText()).longValue();
          if (value <= 0) {
            windowErrorHandler.accept(message("error.windowCount.underMinimum"), null);
          }
        } catch (Exception e) {
          windowErrorHandler.accept(message("error.windowCount.invalidNumber"), null);
        }
      }
    }
  }

  private void validateBlockAlignments(BiConsumer<String, String> errorHandler) {
    blockAlignmentLabel.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      blockAlignmentLabel.getStyleClass().add("error");
    }));
    for (int i = 0; i < blocks.length && i < lefts.length; i++) {
      RadioButton left = lefts[i];
      RadioButton right = rights[i];
      RadioButton split = splits[i];
      left.getStyleClass().remove("error");
      right.getStyleClass().remove("error");
      split.getStyleClass().remove("error");
      BiConsumer<String, String> blockAlignmentErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            left.getStyleClass().add("error");
            right.getStyleClass().add("error");
            split.getStyleClass().add("error");
          }));
      if (left.isVisible()) {
        BlockAlignment alignment = blocks[i].getAlignment();
        if (alignment == null) {
          blockAlignmentErrorHandler.accept(message("error.alignment.required"), null);
        }
      }
    }
  }

  private void validateBlockSplitTypes(BiConsumer<String, String> errorHandler) {
    splitTypeLabel.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      splitTypeLabel.getStyleClass().add("error");
    }));
    for (int i = 0; i < blocks.length && i < percentages.length; i++) {
      RadioButton percentage = percentages[i];
      RadioButton absolute = absolutes[i];
      percentage.getStyleClass().remove("error");
      absolute.getStyleClass().remove("error");
      BiConsumer<String, String> splitTypeErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            percentage.getStyleClass().add("error");
            absolute.getStyleClass().add("error");
          }));
      if (percentage.isVisible()) {
        SplitType splitType = blocks[i].getSplitType();
        if (splitType == null) {
          splitTypeErrorHandler.accept(message("error.splitType.required"), null);
        }
      }
    }
  }

  private void validateBlockSplitAlignments(BiConsumer<String, String> errorHandler) {
    splitAlignmentLabel.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      splitAlignmentLabel.getStyleClass().add("error");
    }));
    for (int i = 0; i < blocks.length && i < splitLefts.length; i++) {
      RadioButton splitLeft = splitLefts[i];
      RadioButton splitRight = splitRights[i];
      splitLeft.getStyleClass().remove("error");
      splitRight.getStyleClass().remove("error");
      BiConsumer<String, String> splitAlignmentErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            splitLeft.getStyleClass().add("error");
            splitRight.getStyleClass().add("error");
          }));
      if (splitLeft.isVisible()) {
        SplitAlignment splitAlignment = blocks[i].getSplitAlignment();
        if (splitAlignment == null) {
          splitAlignmentErrorHandler.accept(message("error.splitAlignment.required"), null);
        }
      }
    }
  }

  private void validateBlockSplits(BiConsumer<String, String> errorHandler) {
    splitValueLabel.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      splitValueLabel.getStyleClass().add("error");
    }));
    for (int i = 0; i < splitValues.length; i++) {
      TextField splitValue = splitValues[i];
      splitValue.getStyleClass().remove("error");
      BiConsumer<String, String> splitValueErrorHandler =
          errorHandler.andThen(once((error, tooltip) -> {
            splitValue.getStyleClass().add("error");
          }));
      if (splitValue.isVisible()) {
        try {
          NumberStringConverter converter = new NumberStringConverter();
          long value = converter.fromString(splitValue.getText()).longValue();
          if (value < 0) {
            splitValueErrorHandler.accept(message("error.split.underMinimum"), null);
          } else if (value > 100 && blocks[i].getSplitType() == SplitType.PERCENTAGE) {
            splitValueErrorHandler.accept(message("error.split.overMaximum"), null);
          }
        } catch (Exception e) {
          splitValueErrorHandler.accept(message("error.split.invalidNumber"), null);
        }
      }
    }
  }
}

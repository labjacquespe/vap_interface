/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.service.ParameterFileService.OUTPUT_DIRECTORY;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;
import static org.labjacquespe.vap.gui.SelectAllListener.selectAllListener;

import java.io.File;
import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragDirectoryDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragDirectoryOverHandler;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Output folder presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OutputFolderPresenter extends BasePresenter {
  private ObjectProperty<File> outputFolderProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private TextField outputFolder;
  private DirectoryChooser outputFolderChooser = new DirectoryChooser();

  @FXML
  private void initialize() {
    outputFolderChooser.setTitle(message("outputFolderChooser.title"));
    outputFolderProperty.addListener((ov, oldValue, newValue) -> {
      if (newValue != null) {
        outputFolder.setText(newValue.toString());
      }
    });
    outputFolder.textProperty().addListener((obv, ov, nv) -> updateTextListener());
    moveCaretToEndOnLostFocus(outputFolder);
    outputFolder.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());

    // Enable drag and drop.
    outputFolder.setOnDragDetected(new DragFileDetectedHandler(outputFolder, TransferMode.COPY));
    outputFolder.setOnDragDone(new DragFileDoneHandler(outputFolder));
    view.setOnDragOver(new DragDirectoryOverHandler(view, outputFolder));
    view.setOnDragExited(new DragExitedHandler(view));
    view.setOnDragDropped(new DragDirectoryDroppedHandler(outputFolder));
  }

  public ObjectProperty<File> outputFolderProperty() {
    return outputFolderProperty;
  }

  public final ObjectProperty<File> initialDirectoryProperty() {
    return outputFolderChooser.initialDirectoryProperty();
  }

  public File getOutputFolder() {
    return outputFolderProperty.get();
  }

  /**
   * Sets output folder.
   *
   * @param outputFolder
   *          output folder
   */
  public void setOutputFolder(File outputFolder) {
    if (outputFolder != null) {
      outputFolderProperty.set(outputFolder);
      this.outputFolder.positionCaret(this.outputFolder.getText().length());
    } else {
      this.outputFolder.setText("");
    }
  }

  /**
   * Shows directory selection window.
   *
   * @param event
   *          event
   */
  public void browseOutputFolder(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(outputFolderChooser);
    File selection = outputFolderChooser.showDialog(outputFolder.getScene().getWindow());
    if (selection != null) {
      outputFolderProperty.set(selection);
      outputFolderChooser.setInitialDirectory(selection);
    }
  }

  private void updateTextListener() {
    String text = outputFolder.getText();
    if (text != null) {
      File file = new File(text);
      if (file.isAbsolute()) {
        outputFolderProperty.set(file);
      } else {
        outputFolderProperty.set(null);
      }
    } else {
      outputFolderProperty.set(null);
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(OUTPUT_DIRECTORY);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates output folder.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    File outputFolder = outputFolderProperty.get();
    if (outputFolder == null) {
      if (this.outputFolder.getText() == null || this.outputFolder.getText().isEmpty()) {
        errorHandler.accept(message("error.required"), null);
      } else {
        errorHandler.accept(message("error.absolute"), null);
      }
    } else if (!outputFolder.exists()) {
      errorHandler.accept(message("error.notExists", outputFolder.getName()),
          outputFolder.getName());
    } else if (!outputFolder.isDirectory()) {
      errorHandler.accept(message("error.notFolder", outputFolder.getName()),
          outputFolder.getName());
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_DISPERSION;
import static org.labjacquespe.vap.core.service.ParameterFileService.DISPLAY_DISPERSION_VALUES;

import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.chart.gui.HelpChartParameterPopup;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Dispersion presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DispersionPresenter extends BasePresenter {
  private final ObjectProperty<AggregateValueType> aggregateValueTypeProperty =
      new SimpleObjectProperty<>();
  private final BooleanProperty generateAggregateGraphsProperty = new SimpleBooleanProperty();
  private final BooleanProperty dispersionProperty = new SimpleBooleanProperty();
  private final ObjectProperty<GuiTabs> tabProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private CheckBox checkBox;
  @FXML
  private Node help;

  @FXML
  private void initialize() {
    checkBox.selectedProperty().bindBidirectional(dispersionProperty);
    aggregateValueTypeProperty.addListener((ov, oldValue, newValue) -> updateDisable());
    generateAggregateGraphsProperty.addListener((ov, oldValue, newValue) -> updateDisable());

    // Default values.
    checkBox.setSelected(DEFAULT_DISPERSION);
  }

  public ObjectProperty<AggregateValueType> aggregateValueTypeProperty() {
    return aggregateValueTypeProperty;
  }

  public BooleanProperty generateAggregateGraphsProperty() {
    return generateAggregateGraphsProperty;
  }

  public BooleanProperty dispersionProperty() {
    return dispersionProperty;
  }

  public ObjectProperty<GuiTabs> tabProperty() {
    return tabProperty;
  }

  public boolean isDispersion() {
    return dispersionProperty.get();
  }

  public void setDispersion(boolean dispersion) {
    dispersionProperty.set(dispersion);
  }

  private void updateDisable() {
    AggregateValueType aggregateValueType = aggregateValueTypeProperty.get();
    boolean generateAggregateGraphs = generateAggregateGraphsProperty.get();
    boolean disable = !generateAggregateGraphs
        || (aggregateValueType != null && aggregateValueType != AggregateValueType.MEAN);
    view.setDisable(disable);
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    GuiTabs tab = tabProperty.get();
    if (tab == null) {
      tab = GuiTabs.PARAMETERS;
    }
    switch (tab) {
      case PARAMETERS: {
        HelpParameterPopup helpPopup = new HelpParameterPopup();
        helpPopup.setHashtag(DISPLAY_DISPERSION_VALUES);
        helpPopup.show((Node) event.getSource());
        break;
      }
      case CREATE_GRAPHS: {
        HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
        helpPopup.setHashtag(DISPLAY_DISPERSION_VALUES);
        helpPopup.show((Node) event.getSource());
        break;
      }
      default:
        break;
    }
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }

  public GuiTabs getTab() {
    return tabProperty.get();
  }

  public void setTab(GuiTabs tab) {
    tabProperty.set(tab);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_BOUNDARY;
import static org.labjacquespe.vap.core.service.ParameterFileService.FIRST_PT_BOUNDARY;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Boundary presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BoundaryPresenter extends BasePresenter {
  private final ObjectProperty<Boundary> boundaryProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final IntegerProperty referencePointsProperty = new SimpleIntegerProperty();
  @FXML
  private Pane view;
  @FXML
  private Label label;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton fivePrime;
  @FXML
  private RadioButton threePrime;

  @FXML
  private void initialize() {
    fivePrime.setUserData(Boundary.FIVE_PRIME);
    threePrime.setUserData(Boundary.THREE_PRIME);
    group.selectedToggleProperty().addListener(
        (ov, oldValue, newValue) -> boundaryProperty.set((Boundary) newValue.getUserData()));
    boundaryProperty.addListener((ov, oldValue, newValue) -> updateBoundary());
    referenceTypeProperty.addListener((ov, oldValue, newValue) -> updateDisabled());
    referencePointsProperty.addListener((ov, oldValue, newValue) -> updateDisabled());

    // Default values.
    boundaryProperty.set(DEFAULT_BOUNDARY);
    fivePrime.setSelected(true);
  }

  public ObjectProperty<Boundary> boundaryProperty() {
    return boundaryProperty;
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public IntegerProperty referencePointsProperty() {
    return referencePointsProperty;
  }

  public Boundary getBoundary() {
    return boundaryProperty.get();
  }

  /**
   * Sets boundary.
   *
   * @param boundary
   *          boundary
   */
  public void setBoundary(Boundary boundary) {
    if (boundary != null) {
      boundaryProperty.set(boundary);
    }
  }

  private void updateDisabled() {
    ReferenceType referenceType = referenceTypeProperty.get();
    Integer referencePoints = referencePointsProperty.getValue();
    boolean disable =
        referenceType == ReferenceType.EXONS || referencePoints == null || referencePoints != 1;
    label.setDisable(disable);
    fivePrime.setDisable(disable);
    threePrime.setDisable(disable);
  }

  private void updateBoundary() {
    Boundary boundary = boundaryProperty.get();
    if (boundary == null) {
      fivePrime.setSelected(false);
      threePrime.setSelected(false);
    } else {
      switch (boundary) {
        case FIVE_PRIME:
          fivePrime.setSelected(true);
          break;
        case THREE_PRIME:
          threePrime.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag("a" + FIRST_PT_BOUNDARY);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validate boundary.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (boundaryProperty.get() == null) {
      errorHandler.accept(message("error.required"), null);
    }
  }
}

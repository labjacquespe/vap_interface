/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_MISSING_DATA_REPLACEMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_MISSING_DATA;

import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.MissingDataReplacement;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Missing data replacement presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MissingDataReplacementPresenter extends BasePresenter {
  private final ObjectProperty<MissingDataReplacement> missingDataReplacementProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Label label;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton zero;
  @FXML
  private RadioButton ignore;
  @FXML
  private Node help;

  @FXML
  private void initialize() {
    zero.setUserData(MissingDataReplacement.ZERO);
    ignore.setUserData(MissingDataReplacement.IGNORE);
    group.selectedToggleProperty()
        .addListener((ov, oldValue, newValue) -> missingDataReplacementProperty
            .set((MissingDataReplacement) newValue.getUserData()));
    missingDataReplacementProperty
        .addListener((ov, oldValue, newValue) -> updateMissingDataReplacement());

    // Default values.
    missingDataReplacementProperty.set(DEFAULT_MISSING_DATA_REPLACEMENT);
  }

  public ObjectProperty<MissingDataReplacement> missingDataReplacementProperty() {
    return missingDataReplacementProperty;
  }

  public MissingDataReplacement getMissingDataReplacement() {
    return missingDataReplacementProperty.get();
  }

  /**
   * Sets missing data replacement.
   *
   * @param missingDataReplacement
   *          missing data replacement
   */
  public void setMissingDataReplacement(MissingDataReplacement missingDataReplacement) {
    if (missingDataReplacement != null) {
      missingDataReplacementProperty.set(missingDataReplacement);
    }
  }

  private void updateMissingDataReplacement() {
    MissingDataReplacement missingDataReplacement = missingDataReplacementProperty.get();
    if (missingDataReplacement != null) {
      switch (missingDataReplacement) {
        case ZERO:
          zero.setSelected(true);
          break;
        case IGNORE:
          ignore.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(PROCESS_MISSING_DATA);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

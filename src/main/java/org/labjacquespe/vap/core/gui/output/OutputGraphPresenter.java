/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_OUTPUT_GRAPHS;
import static org.labjacquespe.vap.core.service.ParameterFileService.ORIENTATION_SUBGROUPS;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.SetChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.Graph;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Output graph presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OutputGraphPresenter extends BasePresenter {
  private final BooleanProperty showAdvancedProperty = new SimpleBooleanProperty();
  private final SetProperty<Graph> outputGraphsProperty =
      new SimpleSetProperty<>(FXCollections.observableSet(new HashSet<Graph>()));
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final BooleanProperty generateAggregateGraphsProperty = new SimpleBooleanProperty();
  @FXML
  private Pane view;
  @FXML
  private ToggleButton showAdvanced;
  @FXML
  private Pane advancedBox;
  @FXML
  private ToggleButton anyAny;
  @FXML
  private ToggleButton anyConv;
  @FXML
  private ToggleButton divAny;
  @FXML
  private ToggleButton anyTand;
  @FXML
  private ToggleButton tandAny;
  @FXML
  private ToggleButton tandTand;
  @FXML
  private ToggleButton tandConv;
  @FXML
  private ToggleButton divTand;
  @FXML
  private ToggleButton divConv;

  @FXML
  private void initialize() {
    outputGraphsProperty.addListener(outputGraphsListener());
    referenceTypeProperty.addListener((obv, ov, nv) -> updateDisable());
    generateAggregateGraphsProperty.addListener((obv, ov, nv) -> updateDisable());
    showAdvancedProperty.addListener((obv, ov, nv) -> updateShowAdvanced());
    showAdvanced.selectedProperty().bindBidirectional(showAdvancedProperty);

    anyAny.selectedProperty().addListener(toggleButtonListener(Graph.ANY_ANY));
    anyConv.selectedProperty().addListener(toggleButtonListener(Graph.ANY_CONV));
    divAny.selectedProperty().addListener(toggleButtonListener(Graph.DIV_ANY));
    anyTand.selectedProperty().addListener(toggleButtonListener(Graph.ANY_TAND));
    tandAny.selectedProperty().addListener(toggleButtonListener(Graph.TAND_ANY));
    tandTand.selectedProperty().addListener(toggleButtonListener(Graph.TAND_TAND));
    tandConv.selectedProperty().addListener(toggleButtonListener(Graph.TAND_CONV));
    divTand.selectedProperty().addListener(toggleButtonListener(Graph.DIV_TAND));
    divConv.selectedProperty().addListener(toggleButtonListener(Graph.DIV_CONV));

    // Default values.
    showAdvancedProperty.set(false);
    outputGraphsProperty.addAll(DEFAULT_OUTPUT_GRAPHS);
    updateChildren();
  }

  public SetProperty<Graph> outputGraphsProperty() {
    return outputGraphsProperty;
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public BooleanProperty generateAggregateGraphsProperty() {
    return generateAggregateGraphsProperty;
  }

  public Collection<Graph> getOutputGraphs() {
    return outputGraphsProperty.get();
  }

  /**
   * Sets graphs to output.
   *
   * @param outputGraphs
   *          graphs to output
   */
  public void setOutputGraphs(Collection<Graph> outputGraphs) {
    if (outputGraphs != null) {
      outputGraphsProperty.set(FXCollections.observableSet(new HashSet<>(outputGraphs)));
    } else {
      outputGraphsProperty.set(FXCollections.observableSet(new HashSet<Graph>()));
    }
  }

  private void updateChildren() {
    view.getChildren().remove(advancedBox);
    if (showAdvancedProperty.get()) {
      view.getChildren().add(advancedBox);
    }
  }

  private void updateDisable() {
    ReferenceType referenceType = referenceTypeProperty.get();
    boolean generateAggregateGraphs = generateAggregateGraphsProperty.get();
    view.setDisable(!generateAggregateGraphs || referenceType != ReferenceType.ANNOTATIONS);
  }

  private void updateShowAdvanced() {
    showAdvanced.setText(message("showAdvanced." + showAdvancedProperty.get()));
    updateChildren();
  }

  private SetChangeListener<Graph> outputGraphsListener() {
    return (change) -> {
      if (change.wasAdded()) {
        setToggleSelected(change.getElementAdded(), true);
      } else if (change.wasRemoved()) {
        setToggleSelected(change.getElementRemoved(), false);
      }
    };
  }

  private void setToggleSelected(Graph graph, boolean value) {
    switch (graph) {
      case ANY_ANY:
        anyAny.setSelected(value);
        break;
      case ANY_CONV:
        anyConv.setSelected(value);
        break;
      case DIV_ANY:
        divAny.setSelected(value);
        break;
      case ANY_TAND:
        anyTand.setSelected(value);
        break;
      case TAND_ANY:
        tandAny.setSelected(value);
        break;
      case TAND_TAND:
        tandTand.setSelected(value);
        break;
      case TAND_CONV:
        tandConv.setSelected(value);
        break;
      case DIV_TAND:
        divTand.setSelected(value);
        break;
      case DIV_CONV:
        divConv.setSelected(value);
        break;
      default:
        break;
    }
  }

  private ChangeListener<Boolean> toggleButtonListener(Graph graph) {
    return (obv, ov, nv) -> {
      if (nv) {
        outputGraphsProperty.add(graph);
      } else {
        outputGraphsProperty.remove(graph);
      }
    };
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event.
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ORIENTATION_SUBGROUPS);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

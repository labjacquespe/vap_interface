/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_ONE_REFERENCE_GROUP_PER_GRAPH;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_GROUP;

import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Reference group per graph presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReferenceGroupPerGraphPresenter extends BasePresenter {
  private final BooleanProperty generateAggregateGraphsProperty = new SimpleBooleanProperty();
  private final BooleanProperty oneReferenceGroupPerGraphProperty = new SimpleBooleanProperty();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton one;
  @FXML
  private RadioButton all;

  @FXML
  private void initialize() {
    one.setUserData(true);
    all.setUserData(false);
    group.selectedToggleProperty().addListener((ov, oldValue, newValue) -> {
      oneReferenceGroupPerGraphProperty.set((boolean) newValue.getUserData());
    });
    oneReferenceGroupPerGraphProperty.addListener((ov, oldValue, newValue) -> {
      System.out.println("oneReferenceGroupPerGraphProperty.addListener new value:" + newValue);
      if (newValue) {
        one.setSelected(true);
      } else {
        all.setSelected(true);
      }
    });

    view.disableProperty().bind(generateAggregateGraphsProperty.not());

    // Default values.
    all.setSelected(true);
    oneReferenceGroupPerGraphProperty.set(DEFAULT_ONE_REFERENCE_GROUP_PER_GRAPH);
  }

  public BooleanProperty oneReferenceGroupPerGraphProperty() {
    return oneReferenceGroupPerGraphProperty;
  }

  public BooleanProperty generateAggregateGraphsProperty() {
    return generateAggregateGraphsProperty;
  }

  public boolean isOneReferenceGroupPerGraph() {
    return oneReferenceGroupPerGraphProperty.get();
  }

  public void setOneReferenceGroupPerGraph(boolean oneReferenceGroupPerGraph) {
    oneReferenceGroupPerGraphProperty.set(oneReferenceGroupPerGraph);
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ONE_GRAPH_PER_GROUP);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

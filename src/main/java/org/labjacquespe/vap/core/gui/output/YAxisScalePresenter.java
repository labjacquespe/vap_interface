/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.core.service.ParameterFileService.Y_AXIS_SCALE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.chart.gui.HelpChartParameterPopup;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Y axis scale presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class YAxisScalePresenter extends BasePresenter {
  private final ObjectProperty<Double> fromProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<Double> toProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<GuiTabs> tabProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Pane fromPane;
  @FXML
  private Pane toPane;
  @FXML
  private TextField from;
  @FXML
  private TextField to;

  @FXML
  private void initialize() {
    Bindings.bindBidirectional(from.textProperty(), fromProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new DoubleStringConverter(), 0.0));
    Bindings.bindBidirectional(to.textProperty(), toProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new DoubleStringConverter(), 0.0));
  }

  public ObjectProperty<Double> fromProperty() {
    return fromProperty;
  }

  public ObjectProperty<Double> toProperty() {
    return toProperty;
  }

  public ObjectProperty<GuiTabs> tabProperty() {
    return tabProperty;
  }

  public YAxisScale getYAxisScale() {
    return new YAxisScale(fromProperty.get(), toProperty.get());
  }

  /**
   * Sets Y axis scale.
   *
   * @param yaxisScale
   *          Y axis scale
   */
  public void setYAxisScale(YAxisScale yaxisScale) {
    if (yaxisScale != null) {
      fromProperty.set(yaxisScale.from);
      toProperty.set(yaxisScale.to);
    } else {
      fromProperty.set(null);
      toProperty.set(null);
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    GuiTabs tab = tabProperty.get();
    if (tab == null) {
      tab = GuiTabs.PARAMETERS;
    }
    switch (tab) {
      case PARAMETERS: {
        HelpParameterPopup helpPopup = new HelpParameterPopup();
        helpPopup.setHashtag(Y_AXIS_SCALE);
        helpPopup.show((Node) event.getSource());
        break;
      }
      case CREATE_GRAPHS: {
        HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
        helpPopup.setHashtag(Y_AXIS_SCALE);
        helpPopup.show((Node) event.getSource());
        break;
      }
      default:
        break;
    }
  }

  /**
   * Validates Y axis scale.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    fromPane.getStyleClass().remove("error");
    toPane.getStyleClass().remove("error");
    from.getStyleClass().remove("error");
    to.getStyleClass().remove("error");
    if (from.getText() != null && !from.getText().isEmpty()) {
      try {
        NumberStringConverter converter = new NumberStringConverter();
        converter.fromString(from.getText());
      } catch (Exception e) {
        errorHandler.accept(message("error.from.invalidNumber"), null);
        fromPane.getStyleClass().add("error");
        from.getStyleClass().add("error");
      }
    }
    if (to.getText() != null && !to.getText().isEmpty()) {
      try {
        NumberStringConverter converter = new NumberStringConverter();
        converter.fromString(to.getText());
      } catch (Exception e) {
        errorHandler.accept(message("error.to.invalidNumber"), null);
        toPane.getStyleClass().add("error");
        to.getStyleClass().add("error");
      }
    }
  }

  public GuiTabs getTab() {
    return tabProperty.get();
  }

  public void setTab(GuiTabs tab) {
    tabProperty.set(tab);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_DATA_CHUNK;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_PROCESS_DATA_BY_CHUNK;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_DATA_BY_CHUNK;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Data chunk size presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DataChunkSizePresenter extends BasePresenter {
  private final BooleanProperty processDataByChunkProperty = new SimpleBooleanProperty();
  private final IntegerProperty dataChunkSizeProperty = new SimpleIntegerProperty();
  @FXML
  private Pane view;
  @FXML
  private CheckBox checkbox;
  @FXML
  private TextField dataChunkSize;
  @FXML
  private Label unit;

  @FXML
  private void initialize() {
    processDataByChunkProperty.bindBidirectional(checkbox.selectedProperty());
    dataChunkSize.textProperty().bindBidirectional(dataChunkSizeProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new NumberStringConverter(), 0));
    dataChunkSize.disableProperty().bind(checkbox.selectedProperty().not());

    // Default values.
    checkbox.setSelected(DEFAULT_PROCESS_DATA_BY_CHUNK);
    dataChunkSizeProperty.set(DEFAULT_DATA_CHUNK);
  }

  public BooleanProperty processDataByChunkProperty() {
    return processDataByChunkProperty;
  }

  public IntegerProperty dataChunkSizeProperty() {
    return dataChunkSizeProperty;
  }

  public boolean isProcessDataByChunk() {
    return processDataByChunkProperty.get();
  }

  public void setProcessDataByChunk(boolean processDataByChunk) {
    processDataByChunkProperty.set(processDataByChunk);
  }

  public int getDataChunkSize() {
    return dataChunkSizeProperty.get();
  }

  /**
   * Sets data chunk size.
   *
   * @param dataChunkSize
   *          data chunk size
   */
  public void setDataChunkSize(int dataChunkSize) {
    if (dataChunkSize > 0) {
      dataChunkSizeProperty.set(dataChunkSize);
    } else {
      this.dataChunkSize.setText("");
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(PROCESS_DATA_BY_CHUNK);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates data chunk size.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (checkbox.isSelected()) {
      try {
        NumberStringConverter converter = new NumberStringConverter();
        int value = converter.fromString(dataChunkSize.getText()).intValue();
        if (value < 1) {
          errorHandler.accept(message("error.underMinimum"), null);
        }
      } catch (Exception e) {
        errorHandler.accept(message("error.invalidNumber"), null);
      }
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.file;

import static org.labjacquespe.vap.core.service.ParameterFileService.REFGROUP_PATH;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.commons.io.FilenameUtils;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.TestData;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.FileListView;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.gui.ResizablePane;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Reference group files presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReferenceGroupFilesPresenter extends BasePresenter {
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<File> relativeFolderProperty = new SimpleObjectProperty<>();
  private final ListProperty<File> filesProperty = new SimpleListProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ResizablePane filesContainer;
  @FXML
  private TextField file;
  @FXML
  private Label label;
  @Autowired
  private OperatingSystemService operatingSystemService;
  @Autowired
  private TestData testData;
  private FileChooser fileChooser = new FileChooser();
  private FileListView mainFiles = new FileListView();
  private FileListView coordinatesFiles = new FileListView();

  @FXML
  private void initialize() {
    filesProperty.set(FXCollections.observableArrayList(new ArrayList<File>()));
    mainFiles.setPrefHeight(120.0);
    coordinatesFiles.setPrefHeight(120.0);
    mainFiles.setEditable(true);
    coordinatesFiles.setEditable(true);
    mainFiles.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    coordinatesFiles.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    StackPane stackFilesContainer = new StackPane();
    filesContainer.setContent(stackFilesContainer);
    stackFilesContainer.getChildren().add(mainFiles);
    stackFilesContainer.getChildren().add(coordinatesFiles);
    fileChooser.setTitle(message("fileChooser.title"));
    fileChooser.getExtensionFilters()
        .add(new ExtensionFilter(message("fileChooser.description"), "*"));
    referenceTypeProperty.addListener(referenceTypeListener());
  }

  public ListProperty<File> filesProperty() {
    return filesProperty;
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public final ObjectProperty<File> initialDirectoryProperty() {
    return fileChooser.initialDirectoryProperty();
  }

  public ObjectProperty<File> relativeFolderProperty() {
    return relativeFolderProperty;
  }

  /**
   * Returns reference group files.
   *
   * @return reference group files
   */
  public List<File> getFiles() {
    List<File> originalFiles = filesProperty.get();
    List<File> convertedFiles = new ArrayList<>(originalFiles.size());
    for (File file : originalFiles) {
      if (!file.isAbsolute() && relativeFolderProperty.get() != null && !isTest(file)) {
        file = new File(relativeFolderProperty.get(), file.getPath());
      }
      convertedFiles.add(file);
    }
    return convertedFiles;
  }

  /**
   * Sets reference group files.
   *
   * @param files
   *          reference group files
   */
  public void setFiles(Collection<File> files) {
    if (files != null) {
      filesProperty.set(FXCollections.observableArrayList(files));
    } else {
      filesProperty.set(FXCollections.observableArrayList(new ArrayList<File>()));
    }
  }

  /**
   * Sets reference group files for {@link ReferenceType#ANNOTATIONS} and
   * {@link ReferenceType#EXONS} only.
   *
   * @param files
   *          reference group files
   */
  public void setMainFiles(Collection<File> files) {
    if (files != null) {
      mainFiles.setItems(FXCollections.observableArrayList(files));
    } else {
      mainFiles.setItems(FXCollections.observableArrayList(new ArrayList<File>()));
    }
  }

  /**
   * Sets reference group files for {@link ReferenceType#COORDINATES} only.
   *
   * @param files
   *          reference group files
   */
  public void setCoordinatesFiles(Collection<File> files) {
    if (files != null) {
      coordinatesFiles.setItems(FXCollections.observableArrayList(files));
    } else {
      coordinatesFiles.setItems(FXCollections.observableArrayList(new ArrayList<File>()));
    }
  }

  private FileListView getFileListView() {
    if (referenceTypeProperty.get() == ReferenceType.COORDINATES) {
      return coordinatesFiles;
    } else {
      return mainFiles;
    }
  }

  @FXML
  private void addFiles(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(fileChooser);
    FileListView files = getFileListView();
    List<File> selections = fileChooser.showOpenMultipleDialog(files.getScene().getWindow());
    if (selections != null) {
      if (!selections.isEmpty()) {
        fileChooser.setInitialDirectory(selections.get(0).getParentFile());
      }
      for (File selection : selections) {
        if (!files.getItems().contains(selection)) {
          files.getItems().add(selection);
        }
      }
    }
  }

  @FXML
  private void addFile(ActionEvent event) {
    FileListView files = getFileListView();
    if (file.getText() != null && !file.getText().isEmpty()) {
      files.getItems().add(new File(file.getText()));
    }
  }

  public void removeFiles(ActionEvent event) {
    FileListView files = getFileListView();
    files.removeSelectedFiles();
  }

  @FXML
  private void upFiles(ActionEvent event) {
    FileListView files = getFileListView();
    List<Integer> selections = new ArrayList<>(files.getSelectionModel().getSelectedIndices());
    Collections.sort(selections);
    int numberOfElementsToSkip = 0;
    while (numberOfElementsToSkip < selections.size()
        && numberOfElementsToSkip == selections.get(numberOfElementsToSkip)) {
      numberOfElementsToSkip++;
    }

    if (numberOfElementsToSkip < selections.size()) {
      for (int i = numberOfElementsToSkip; i < selections.size(); i++) {
        int sourceIndex = selections.get(i);

        File element = files.getItems().get(sourceIndex);
        files.getItems().remove(sourceIndex);
        files.getItems().add(sourceIndex - 1, element);

        selections.set(i, selections.get(i) - 1);
      }
    }
    files.getSelectionModel().clearSelection();
    for (Integer selection : selections) {
      files.getSelectionModel().select(selection.intValue());
    }
  }

  @FXML
  private void downFiles(ActionEvent event) {
    FileListView files = getFileListView();
    List<Integer> selections = new ArrayList<>(files.getSelectionModel().getSelectedIndices());
    Collections.sort(selections);
    Collections.reverse(selections);
    int numberOfElementsToSkip = 0;
    while (numberOfElementsToSkip < selections.size() && files.getItems().size()
        - numberOfElementsToSkip - 1 == selections.get(numberOfElementsToSkip)) {
      numberOfElementsToSkip++;
    }

    if (numberOfElementsToSkip < selections.size()) {
      for (int i = numberOfElementsToSkip; i < selections.size(); i++) {
        int sourceIndex = selections.get(i);

        File element = files.getItems().get(sourceIndex);
        files.getItems().remove(sourceIndex);
        files.getItems().add(sourceIndex + 1, element);

        selections.set(i, selections.get(i) + 1);
      }
    }
    files.getSelectionModel().clearSelection();
    for (Integer selection : selections) {
      files.getSelectionModel().select(selection.intValue());
    }
  }

  public void remvoveStyleClassFromCells(String classStyle) {
    FileListView files = getFileListView();
    files.remvoveStyleClassFromCells(classStyle);
  }

  public void addStyleClass(File file, String styleClass) {
    FileListView files = getFileListView();
    files.addStyleClass(file, styleClass);
  }

  private ChangeListener<ReferenceType> referenceTypeListener() {
    return (obv, ov, nv) -> {
      label.setText(message("label." + nv.name()));
      fileChooser.setTitle(message("fileChooser.title." + nv.name()));
      fileChooser.getExtensionFilters().clear();
      fileChooser.getExtensionFilters()
          .add(new ExtensionFilter(message("fileChooser.description." + nv.name()),
              operatingSystemService.dataFileExtensions(operatingSystemService.currentOs())));
      mainFiles.setVisible(false);
      coordinatesFiles.setVisible(false);
      mainFiles.itemsProperty().unbindBidirectional(filesProperty);
      coordinatesFiles.itemsProperty().unbindBidirectional(filesProperty);
      if (nv == ReferenceType.COORDINATES) {
        coordinatesFiles.setVisible(true);
        filesProperty.set(coordinatesFiles.itemsProperty().get());
        coordinatesFiles.itemsProperty().bindBidirectional(filesProperty);
      } else {
        mainFiles.setVisible(true);
        filesProperty.set(mainFiles.itemsProperty().get());
        mainFiles.itemsProperty().bindBidirectional(filesProperty);
      }
    };
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(REFGROUP_PATH);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates reference group files.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    remvoveStyleClassFromCells("error");
    Collection<File> files = getFiles();
    if (files == null || files.isEmpty()) {
      errorHandler.accept(message("error.required", referenceTypeProperty.get().ordinal()), null);
    } else {
      for (File file : files) {
        if (!file.isFile() && file.isAbsolute() && !isTest(file)) {
          errorHandler.accept(
              message("error.notExists", referenceTypeProperty.get().ordinal(), file.getName()),
              file.getPath());
          addStyleClass(file, "error");
        } else if (referenceTypeProperty.get() == ReferenceType.COORDINATES
            && !FilenameUtils.getExtension(file.getName()).startsWith("coord")) {
          errorHandler.accept(message("error.notCoordExtension",
              referenceTypeProperty.get().ordinal(), file.getName()), file.getPath());
          addStyleClass(file, "error");
        }
      }
    }
  }

  private boolean isTest(File file) {
    return testData.isTestFile(file);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_WINDOW_SIZE;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOW_SIZE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.converter.NumberStringConverter;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Window size presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WindowSizePresenter extends BasePresenter {
  private final LongProperty windowSizeProperty = new SimpleLongProperty();
  private final ObjectProperty<RepresentationType> representationTypeProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Label label;
  @FXML
  private TextField windowSize;

  @FXML
  private void initialize() {
    representationTypeProperty.addListener(
        (ov, oldValue, newValue) -> label.setText(message("label." + newValue.name())));
    Bindings.bindBidirectional(windowSize.textProperty(), windowSizeProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new NumberStringConverter(),
            DEFAULT_WINDOW_SIZE));

    // Default values.
    windowSizeProperty.setValue(DEFAULT_WINDOW_SIZE);
  }

  public LongProperty windowSizeProperty() {
    return windowSizeProperty;
  }

  public ObjectProperty<RepresentationType> representationTypeProperty() {
    return representationTypeProperty;
  }

  public long getWindowSize() {
    return windowSizeProperty.get();
  }

  /**
   * Sets window size.
   *
   * @param windowSize
   *          window size
   */
  public void setWindowSize(long windowSize) {
    if (windowSize >= 0) {
      windowSizeProperty.set(windowSize);
    } else {
      this.windowSize.setText("");
    }
  }

  /**
   * Show help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(WINDOW_SIZE);
    helpPopup.show((Node) event.getSource());
  }

  @Override
  public void setMammalianDefaults() {
    windowSizeProperty.set(50);
  }

  @Override
  public void setYeastDefaults() {
    windowSizeProperty.set(50);
  }

  /**
   * Validates window size.
   *
   * @param errorHandler
   *          handles validation error.
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    windowSize.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
      windowSize.getStyleClass().add("error");
    }));
    try {
      NumberStringConverter converter = new NumberStringConverter();
      long value = converter.fromString(windowSize.getText()).longValue();
      if (value < 0) {
        errorHandler.accept(message("error.underMinimum"), null);
      }
    } catch (Exception e) {
      errorHandler.accept(message("error.invalidNumber"), null);
    }
  }
}

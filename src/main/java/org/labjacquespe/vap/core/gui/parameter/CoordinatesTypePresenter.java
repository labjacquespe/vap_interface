/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_COORDINATES_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANNOTATION_COORDINATES_TYPE;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.CoordinatesType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Coordinates type presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CoordinatesTypePresenter extends BasePresenter {
  private final ObjectProperty<CoordinatesType> coordinatesTypeProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton tx;
  @FXML
  private RadioButton cds;

  @FXML
  private void initialize() {
    tx.setUserData(CoordinatesType.TX);
    cds.setUserData(CoordinatesType.CDS);
    group.selectedToggleProperty().addListener((ov, oldValue, newValue) -> coordinatesTypeProperty
        .set((CoordinatesType) newValue.getUserData()));
    coordinatesTypeProperty.addListener((ov, oldValue, newValue) -> updateCoordinatesType());
    coordinatesTypeProperty.set(DEFAULT_COORDINATES_TYPE);
  }

  public ObjectProperty<CoordinatesType> coordinatesTypeProperty() {
    return coordinatesTypeProperty;
  }

  public CoordinatesType getCoordinatesType() {
    return coordinatesTypeProperty.get();
  }

  /**
   * Sets coordinates type.
   *
   * @param coordinatesType
   *          coordinates type
   */
  public void setCoordinatesType(CoordinatesType coordinatesType) {
    if (coordinatesType != null) {
      coordinatesTypeProperty.set(coordinatesType);
    }
  }

  private void updateCoordinatesType() {
    CoordinatesType coordinatesType = coordinatesTypeProperty.get();
    if (coordinatesType == null) {
      tx.setSelected(false);
      cds.setSelected(false);
    } else {
      switch (coordinatesType) {
        case TX:
          tx.setSelected(true);
          break;
        case CDS:
          cds.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ANNOTATION_COORDINATES_TYPE);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates coordinates type.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (coordinatesTypeProperty.get() == null) {
      errorHandler.accept(message("error.required"), null);
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_GENERATE_AGGREGATE_GRAPHS;
import static org.labjacquespe.vap.core.ReferenceType.ANNOTATIONS;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_AGGREGATE_GRAPHS;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.chart.CreateGraphFilesType;
import org.labjacquespe.vap.chart.gui.HelpChartParameterPopup;
import org.labjacquespe.vap.chart.gui.MapGraphFilePresenter;
import org.labjacquespe.vap.chart.gui.MapGraphFileView;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.Graph;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Generate aggregate graphs presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenerateAggregateGraphsPresenter extends BasePresenter {
  private final BooleanProperty generateAggregateGraphsProperty = new SimpleBooleanProperty();
  private final BooleanProperty showAdvancedProperty = new SimpleBooleanProperty();
  private final ObjectProperty<ReferenceType> referenceTypeProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<AggregateValueType> aggregateValueTypeProperty =
      new SimpleObjectProperty<>();
  private final ObjectProperty<CreateGraphFilesType> createGraphFilesTypeProperty =
      new SimpleObjectProperty<>();
  private final ObjectProperty<GuiTabs> tabProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Pane subView;
  @FXML
  private CheckBox checkBox;
  @FXML
  private ToggleButton showAdvanced;
  @FXML
  private Pane advancedBox;
  private DatasetPerGraphPresenter datasetPerGraphPresenter;
  private DatasetPerGraphView datasetPerGraphView;
  private DispersionPresenter dispersionPresenter;
  private DispersionView dispersionView;
  private MapGraphFilePresenter mapGraphFilePresenter;
  private MapGraphFileView mapGraphFileView;
  private OrientationPerGraphPresenter orientationPerGraphPresenter;
  private OrientationPerGraphView orientationPerGraphView;
  private OutputGraphPresenter outputGraphPresenter;
  private OutputGraphView outputGraphView;
  private ReferenceGroupPerGraphPresenter referenceGroupPerGraphPresenter;
  private ReferenceGroupPerGraphView referenceGroupPerGraphView;

  @FXML
  private void initialize() {
    showAdvancedProperty.addListener((ov, oldValue, newValue) -> updateShowAdvanced());
    checkBox.selectedProperty().bindBidirectional(generateAggregateGraphsProperty);
    showAdvanced.selectedProperty().bindBidirectional(showAdvancedProperty);
    referenceTypeProperty.addListener((ov, oldValue, newValue) -> updateChildren());
    tabProperty.addListener((ov, oldValue, newValue) -> updateChildren());
    createGraphFilesTypeProperty.addListener((ov, oldValue, newValue) -> updateChildren());

    datasetPerGraphView = new DatasetPerGraphView();
    datasetPerGraphPresenter = (DatasetPerGraphPresenter) datasetPerGraphView.getPresenter();
    dispersionView = new DispersionView();
    dispersionPresenter = (DispersionPresenter) dispersionView.getPresenter();
    mapGraphFileView = new MapGraphFileView();
    mapGraphFilePresenter = (MapGraphFilePresenter) mapGraphFileView.getPresenter();
    orientationPerGraphView = new OrientationPerGraphView();
    orientationPerGraphPresenter =
        (OrientationPerGraphPresenter) orientationPerGraphView.getPresenter();
    outputGraphView = new OutputGraphView();
    outputGraphPresenter = (OutputGraphPresenter) outputGraphView.getPresenter();
    referenceGroupPerGraphView = new ReferenceGroupPerGraphView();
    referenceGroupPerGraphPresenter =
        (ReferenceGroupPerGraphPresenter) referenceGroupPerGraphView.getPresenter();
    outputGraphPresenter.referenceTypeProperty().bind(referenceTypeProperty);
    dispersionPresenter.aggregateValueTypeProperty().bind(aggregateValueTypeProperty());
    dispersionPresenter.generateAggregateGraphsProperty().bind(generateAggregateGraphsProperty());
    dispersionPresenter.tabProperty().bind(tabProperty());
    mapGraphFilePresenter.generateAggregateGraphsProperty().bind(generateAggregateGraphsProperty());
    datasetPerGraphPresenter.generateAggregateGraphsProperty()
        .bind(generateAggregateGraphsProperty());
    referenceGroupPerGraphPresenter.generateAggregateGraphsProperty()
        .bind(generateAggregateGraphsProperty());
    outputGraphPresenter.generateAggregateGraphsProperty().bind(generateAggregateGraphsProperty());
    orientationPerGraphPresenter.generateAggregateGraphsProperty()
        .bind(generateAggregateGraphsProperty());

    // Default values.
    showAdvancedProperty.set(true);
    generateAggregateGraphsProperty.set(DEFAULT_GENERATE_AGGREGATE_GRAPHS);
    updateChildren();
  }

  public BooleanProperty generateAggregateGraphsProperty() {
    return generateAggregateGraphsProperty;
  }

  public ObjectProperty<ReferenceType> referenceTypeProperty() {
    return referenceTypeProperty;
  }

  public ObjectProperty<AggregateValueType> aggregateValueTypeProperty() {
    return aggregateValueTypeProperty;
  }

  public BooleanProperty oneDataFilePerGraphProperty() {
    return datasetPerGraphPresenter.oneDataFilePerGraphProperty();
  }

  public BooleanProperty dispersionProperty() {
    return dispersionPresenter.dispersionProperty();
  }

  public BooleanProperty oneOrientationPerGraphProperty() {
    return orientationPerGraphPresenter.oneOrientationPerGraphProperty();
  }

  public SetProperty<Graph> outputGraphsProperty() {
    return outputGraphPresenter.outputGraphsProperty();
  }

  public BooleanProperty oneReferenceGroupPerGraphProperty() {
    return referenceGroupPerGraphPresenter.oneReferenceGroupPerGraphProperty();
  }

  public boolean isGenerateAggregateGraphs() {
    return generateAggregateGraphsProperty.get();
  }

  public void setGenerateAggregateGraphs(boolean generateAggregateGraphs) {
    generateAggregateGraphsProperty.set(generateAggregateGraphs);
  }

  public boolean isOneDataFilePerGraph() {
    return datasetPerGraphPresenter.isOneDataFilePerGraph();
  }

  public void setOneDataFilePerGraph(boolean oneDataFilePerGraph) {
    datasetPerGraphPresenter.setOneDataFilePerGraph(oneDataFilePerGraph);
  }

  public boolean isDispersion() {
    return dispersionPresenter.isDispersion();
  }

  public void setDispersion(boolean dispersion) {
    dispersionPresenter.setDispersion(dispersion);
  }

  public boolean isOneOrientationPerGraph() {
    return orientationPerGraphPresenter.isOneOrientationPerGraph();
  }

  public void setOneOrientationPerGraph(boolean oneOrientationPerGraph) {
    orientationPerGraphPresenter.setOneOrientationPerGraph(oneOrientationPerGraph);
  }

  public Collection<Graph> getOutputGraphs() {
    return outputGraphPresenter.getOutputGraphs();
  }

  public void setOutputGraphs(Collection<Graph> outputGraphs) {
    outputGraphPresenter.setOutputGraphs(outputGraphs);
  }

  public boolean isOneReferenceGroupPerGraph() {
    return referenceGroupPerGraphPresenter.isOneReferenceGroupPerGraph();
  }

  public void setOneReferenceGroupPerGraph(boolean oneReferenceGroupPerGraph) {
    referenceGroupPerGraphPresenter.setOneReferenceGroupPerGraph(oneReferenceGroupPerGraph);
  }

  public File getMapGraph() {
    return mapGraphFilePresenter.getMapGraph();
  }

  public void setMapGraph(File file) {
    mapGraphFilePresenter.setMapGraph(file);
  }

  public ObjectProperty<CreateGraphFilesType> createGraphFilesTypeProperty() {
    return createGraphFilesTypeProperty;
  }

  public CreateGraphFilesType isCreateGraphFilesType() {
    return createGraphFilesTypeProperty.get();
  }

  public void setCreateGraphFilesType(CreateGraphFilesType createGraphFilesType) {
    createGraphFilesTypeProperty.set(createGraphFilesType);
  }

  public ObjectProperty<GuiTabs> tabProperty() {
    return tabProperty;
  }

  public GuiTabs getTab() {
    return tabProperty.get();
  }

  public void setTab(GuiTabs tab) {
    tabProperty.set(tab);
  }

  private void updateShowAdvanced() {
    showAdvanced.setText(message("showAdvanced." + showAdvancedProperty.get()));
    updateChildren();
  }

  /**
   * Show help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    GuiTabs tab = tabProperty.get();
    if (tab == null) {
      tab = GuiTabs.PARAMETERS;
    }
    switch (tab) {
      case PARAMETERS: {
        HelpParameterPopup helpPopup = new HelpParameterPopup();
        helpPopup.setHashtag(GENERATE_AGGREGATE_GRAPHS);
        helpPopup.show((Node) event.getSource());
        break;
      }
      case CREATE_GRAPHS: {
        HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
        helpPopup.setHashtag(GENERATE_AGGREGATE_GRAPHS);
        helpPopup.show((Node) event.getSource());
        break;
      }
      default:
        break;
    }
  }

  @Override
  public void setMammalianDefaults() {
    dispersionPresenter.setMammalianDefaults();
    datasetPerGraphPresenter.setMammalianDefaults();
    referenceGroupPerGraphPresenter.setMammalianDefaults();
    orientationPerGraphPresenter.setMammalianDefaults();
    if (referenceTypeProperty.get() != ReferenceType.EXONS) {
      orientationPerGraphPresenter.setMammalianDefaults();
      outputGraphPresenter.setMammalianDefaults();
    }
  }

  @Override
  public void setYeastDefaults() {
    dispersionPresenter.setYeastDefaults();
    datasetPerGraphPresenter.setYeastDefaults();
    referenceGroupPerGraphPresenter.setYeastDefaults();
    orientationPerGraphPresenter.setYeastDefaults();
    if (referenceTypeProperty.get() != ReferenceType.EXONS) {
      orientationPerGraphPresenter.setMammalianDefaults();
      outputGraphPresenter.setYeastDefaults();
    }
  }

  private void updateChildren() {
    List<Node> subViewChildren = subView.getChildren();
    subViewChildren.clear();
    if (tabProperty.get() == GuiTabs.CREATE_GRAPHS
        && createGraphFilesTypeProperty.get() == CreateGraphFilesType.LIST_FILES) {
      subViewChildren.add(mapGraphFileView.getView());
    }
    subViewChildren.add(advancedBox);
    List<Node> advancedChildren = advancedBox.getChildren();
    advancedChildren.clear();
    ReferenceType referenceType = referenceTypeProperty.get();

    if (showAdvancedProperty.get()) {
      advancedChildren.add(dispersionView.getView());
      if (tabProperty.get() != GuiTabs.CREATE_GRAPHS) {
        advancedChildren.add(datasetPerGraphView.getView());
        advancedChildren.add(referenceGroupPerGraphView.getView());
        if (referenceType == ANNOTATIONS) {
          advancedChildren.add(outputGraphView.getView());
        }
        if (referenceType == ANNOTATIONS) {
          advancedChildren.add(orientationPerGraphView.getView());
        }
      }
    }
  }

  /**
   * Validates generates aggregate graphs parameters.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    if (tabProperty.get() == GuiTabs.CREATE_GRAPHS
        && createGraphFilesTypeProperty.get() == CreateGraphFilesType.LIST_FILES) {
      mapGraphFilePresenter.validate(errorHandler);
    }
    dispersionPresenter.validate(errorHandler);
    datasetPerGraphPresenter.validate(errorHandler);
    referenceGroupPerGraphPresenter.validate(errorHandler);
    ReferenceType referenceType = referenceTypeProperty.get();
    if (referenceType == ANNOTATIONS) {
      outputGraphPresenter.validate(errorHandler);
    }
    if (referenceType == ANNOTATIONS) {
      orientationPerGraphPresenter.validate(errorHandler);
    }
  }
}

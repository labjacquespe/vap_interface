/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_DISPERSION_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.MEAN_DISPERSION_VALUE;

import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.DispersionType;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Dispersion type presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DispersionTypePresenter extends BasePresenter {
  private final ObjectProperty<AggregateValueType> aggregateValueTypeProperty =
      new SimpleObjectProperty<>();
  private final ObjectProperty<DispersionType> dispersionTypeProperty =
      new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Label label;
  @FXML
  private ToggleGroup group;
  @FXML
  private RadioButton sem;
  @FXML
  private RadioButton sd;
  @FXML
  private Node help;

  @FXML
  private void initialize() {
    sem.setUserData(DispersionType.SEM);
    sd.setUserData(DispersionType.SD);
    group.selectedToggleProperty().addListener((ov, oldValue, newValue) -> dispersionTypeProperty
        .set((DispersionType) newValue.getUserData()));
    dispersionTypeProperty.addListener((ov, oldValue, newValue) -> updateDispersion());
    aggregateValueTypeProperty.addListener((ov, oldValue, newValue) -> updateAggregateValueType());

    // Default values.
    dispersionTypeProperty.set(DEFAULT_DISPERSION_TYPE);
  }

  public ObjectProperty<AggregateValueType> aggregateValueTypeProperty() {
    return aggregateValueTypeProperty;
  }

  public ObjectProperty<DispersionType> dispersionTypeProperty() {
    return dispersionTypeProperty;
  }

  public DispersionType getDispersionType() {
    return dispersionTypeProperty.get();
  }

  /**
   * Sets dispersion type.
   *
   * @param dispersionType
   *          dispersion type
   */
  public void setDispersionType(DispersionType dispersionType) {
    if (dispersionType != null) {
      dispersionTypeProperty.set(dispersionType);
    }
  }

  private void updateAggregateValueType() {
    AggregateValueType aggregateValueType = aggregateValueTypeProperty.get();
    boolean disable = aggregateValueType != AggregateValueType.MEAN;
    label.setDisable(disable);
    sem.setDisable(disable);
    sd.setDisable(disable);
  }

  private void updateDispersion() {
    DispersionType dispersionType = dispersionTypeProperty.get();
    if (dispersionType != null) {
      switch (dispersionType) {
        case SEM:
          sem.setSelected(true);
          break;
        case SD:
          sd.setSelected(true);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Show help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(MEAN_DISPERSION_VALUE);
    helpPopup.show((Node) event.getSource());
  }

  public void validate(BiConsumer<String, String> errorHandler) {
  }
}

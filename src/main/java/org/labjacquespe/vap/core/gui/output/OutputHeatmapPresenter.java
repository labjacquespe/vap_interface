/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.output;

import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_GENERATE_HEATMAP;
import static org.labjacquespe.vap.chart.HeatmapParameters.DEFAULT_AXIS_VISIBLE;
import static org.labjacquespe.vap.chart.HeatmapParameters.DEFAULT_COLORS;
import static org.labjacquespe.vap.chart.HeatmapParameters.DEFAULT_HEIGHT;
import static org.labjacquespe.vap.chart.HeatmapParameters.DEFAULT_WIDTH;
import static org.labjacquespe.vap.chart.HeatmapParameters.MINIMAL_HEIGHT;
import static org.labjacquespe.vap.chart.HeatmapParameters.MINIMAL_WIDTH;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_HEATMAP;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.io.File;
import java.util.List;
import java.util.function.BiConsumer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.converter.IntegerStringConverter;
import org.labjacquespe.vap.chart.CreateGraphFilesType;
import org.labjacquespe.vap.chart.HeatmapColors;
import org.labjacquespe.vap.chart.HeatmapParameters;
import org.labjacquespe.vap.chart.gui.HeatmapColorsConverter;
import org.labjacquespe.vap.chart.gui.HeatmapColorsGradient;
import org.labjacquespe.vap.chart.gui.HelpChartParameterPopup;
import org.labjacquespe.vap.chart.gui.ListHeatmapFilePresenter;
import org.labjacquespe.vap.chart.gui.ListHeatmapFileView;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.DefaultValueOnExceptionAndNullConverter;
import org.labjacquespe.vap.gui.GuiTabs;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Output heatmap presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OutputHeatmapPresenter extends BasePresenter {
  private static final boolean DEFAULT_USE_DEFAULT = true;
  private final BooleanProperty outputHeatmapProperty = new SimpleBooleanProperty();
  private final BooleanProperty showAdvancedProperty = new SimpleBooleanProperty();
  private final BooleanProperty useDefaultProperty = new SimpleBooleanProperty();
  private final BooleanProperty noAxisProperty = new SimpleBooleanProperty();
  private final ObjectProperty<Integer> imageWidthProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<Integer> imageHeightProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<HeatmapColors> heatmapColorsProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<CreateGraphFilesType> createGraphFilesTypeProperty = new SimpleObjectProperty<>();
  private final ObjectProperty<GuiTabs> tabProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private CheckBox checkBox;
  @FXML
  private ToggleButton showAdvanced;
  @FXML
  private Pane subView;
  @FXML
  private Pane advancedBox;
  @FXML
  private Pane advancedCustomBox;
  @FXML
  private ToggleGroup defaultHeatmapGroup;
  @FXML
  private RadioButton defaultHeatmap;
  @FXML
  private RadioButton customHeatmap;
  @FXML
  private CheckBox noAxis;
  @FXML
  private Pane imageWidthPane;
  @FXML
  private TextField imageWidth;
  @FXML
  private Pane imageHeightPane;
  @FXML
  private TextField imageHeight;
  @FXML
  private ChoiceBox<HeatmapColors> colors;
  @FXML
  private GridPane colorsGradients;
  @FXML
  private Pane maxFeaturesPane;
  @FXML
  private TextField maxFeatures;
  private ListHeatmapFilePresenter listHeatmapFilePresenter;
  private ListHeatmapFileView listHeatmapFileView;

  @FXML
  private void initialize() {
    listHeatmapFileView = new ListHeatmapFileView();
    listHeatmapFilePresenter = (ListHeatmapFilePresenter) listHeatmapFileView.getPresenter();

    tabProperty.addListener((obv, ov, nv) -> updateChildren());
    createGraphFilesTypeProperty.addListener((obv, ov, nv) -> updateChildren());
    listHeatmapFilePresenter.outputHeatmapProperty().bind(outputHeatmapProperty);
    showAdvancedProperty.addListener((obv, ov, nv) -> {
      showAdvanced.setText(message("showAdvanced." + nv));
      updateChildren();
    });
    outputHeatmapProperty.bindBidirectional(checkBox.selectedProperty());
    showAdvanced.selectedProperty().bindBidirectional(showAdvancedProperty);
    advancedBox.disableProperty().bind(outputHeatmapProperty.not());
    advancedCustomBox.disableProperty().bind(outputHeatmapProperty.not().or(useDefaultProperty));
    defaultHeatmap.setUserData(true);
    customHeatmap.setUserData(false);
    defaultHeatmapGroup.selectedToggleProperty().addListener(defaultHeatmapToggleListener());
    useDefaultProperty.addListener(defaultHeatmapListener());
    noAxis.selectedProperty().bindBidirectional(noAxisProperty);
    Bindings.bindBidirectional(imageWidth.textProperty(), imageWidthProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new IntegerStringConverter(), DEFAULT_WIDTH));
    Bindings.bindBidirectional(imageHeight.textProperty(), imageHeightProperty,
        new DefaultValueOnExceptionAndNullConverter<>(new IntegerStringConverter(),
            DEFAULT_HEIGHT));
    final HeatmapColorsConverter colorsConverter = new HeatmapColorsConverter();
    colors.setConverter(colorsConverter);
    colors.setItems(FXCollections.observableArrayList(HeatmapColors.values()));
    colors.valueProperty().bindBidirectional(heatmapColorsProperty);
    {
      int row = 0;
      for (HeatmapColors value : colors.getItems()) {
        colorsGradients.add(new Label(colorsConverter.toString(value)), 0, row);
        colorsGradients.add(new HeatmapColorsGradient(value), 1, row);
        row++;
      }
    }

    // Default values.
    showAdvancedProperty.set(DEFAULT_GENERATE_HEATMAP);
    defaultHeatmap.setSelected(true);
    useDefaultProperty.set(DEFAULT_USE_DEFAULT);
    imageWidthProperty.set(DEFAULT_WIDTH);
    imageHeightProperty.set(DEFAULT_HEIGHT);
    noAxisProperty.set(DEFAULT_AXIS_VISIBLE);
    heatmapColorsProperty.set(DEFAULT_COLORS);
    updateChildren();
  }

  public BooleanProperty outputHeatmapProperty() {
    return outputHeatmapProperty;
  }

  public BooleanProperty useDefaultProperty() {
    return useDefaultProperty;
  }

  public BooleanProperty noAxisProperty() {
    return noAxisProperty;
  }

  public ObjectProperty<Integer> imageWidthProperty() {
    return imageWidthProperty;
  }

  public ObjectProperty<Integer> imageHeightProperty() {
    return imageHeightProperty;
  }

  public ObjectProperty<HeatmapColors> heatmapColorsProperty() {
    return heatmapColorsProperty;
  }

  public boolean isOutputHeatmap() {
    return outputHeatmapProperty.get();
  }

  public void setOutputHeatmap(boolean outputHeatmap) {
    outputHeatmapProperty.set(outputHeatmap);
  }

  public File getListHeatmap() {
    return listHeatmapFilePresenter.getListHeatmap();
  }

  public void setListHeatmap(File file) {
    listHeatmapFilePresenter.setListHeatmap(file);
  }

  public ObjectProperty<CreateGraphFilesType> createGraphFilesTypeProperty() {
    return createGraphFilesTypeProperty;
  }

  public CreateGraphFilesType isCreateGraphFilesType() {
    return createGraphFilesTypeProperty.get();
  }

  public void setCreateGraphFilesType(CreateGraphFilesType createGraphFilesType) {
    createGraphFilesTypeProperty.set(createGraphFilesType);
  }

  public ObjectProperty<GuiTabs> tabProperty() {
    return tabProperty;
  }

  public GuiTabs getTab() {
    return tabProperty.get();
  }

  public void setTab(GuiTabs tab) {
    tabProperty.set(tab);
  }

  /**
   * Returns heatmap parameters.
   *
   * @return heatmap parameters
   */
  public HeatmapParameters getHeatmapParameters() {
    HeatmapParameters heatmapParameters = new HeatmapParameters();
    if (!useDefaultProperty.get()) {
      heatmapParameters.imageWidth = imageWidthProperty.get();
      heatmapParameters.imageHeight = imageHeightProperty.get();
      heatmapParameters.axisVisible = !noAxisProperty.get();
      heatmapParameters.heatmapColors = heatmapColorsProperty.get();
    }
    return heatmapParameters;
  }

  /**
   * Sets heatmap parameters.
   *
   * @param heatmapParameters
   *          heatmap parameters
   */
  public void setHeatmapParameters(HeatmapParameters heatmapParameters) {
    if (heatmapParameters != null) {
      imageWidthProperty.set(heatmapParameters.imageWidth);
      imageHeightProperty.set(heatmapParameters.imageHeight);
      noAxisProperty.set(!heatmapParameters.axisVisible);
      heatmapColorsProperty
          .set(heatmapParameters.heatmapColors != null ? heatmapParameters.heatmapColors
              : DEFAULT_COLORS);
    } else {
      useDefaultProperty.set(DEFAULT_USE_DEFAULT);
      imageWidthProperty.set(DEFAULT_WIDTH);
      imageHeightProperty.set(DEFAULT_HEIGHT);
      noAxisProperty.set(!DEFAULT_AXIS_VISIBLE);
      heatmapColorsProperty.set(DEFAULT_COLORS);
    }
  }

  private void updateChildren() {
    List<Node> subViewChildren = subView.getChildren();
    subViewChildren.clear();
    if (tabProperty.get() == GuiTabs.CREATE_GRAPHS
        && createGraphFilesTypeProperty.get() == CreateGraphFilesType.LIST_FILES) {
      subViewChildren.add(listHeatmapFileView.getView());
    }
    if (showAdvancedProperty.get()) {
      subViewChildren.add(advancedBox);
    }
  }

  private ChangeListener<Boolean> defaultHeatmapListener() {
    return (obv, ov, nv) -> {
      if (nv) {
        defaultHeatmap.setSelected(true);
      } else {
        customHeatmap.setSelected(true);
      }
    };
  }

  private ChangeListener<Toggle> defaultHeatmapToggleListener() {
    return (obv, ov, nv) -> {
      useDefaultProperty.set((boolean) nv.getUserData());
    };
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    GuiTabs tab = tabProperty.get();
    if (tab == null) {
      tab = GuiTabs.PARAMETERS;
    }
    switch (tab) {
    case PARAMETERS: {
      HelpParameterPopup helpPopup = new HelpParameterPopup();
      helpPopup.setHashtag(GENERATE_HEATMAP);
      helpPopup.show((Node) event.getSource());
      break;
    }
    case CREATE_GRAPHS: {
      HelpChartParameterPopup helpPopup = new HelpChartParameterPopup();
      helpPopup.setHashtag(GENERATE_HEATMAP);
      helpPopup.show((Node) event.getSource());
      break;
    }
    default:
      break;
    }
  }

  /**
   * Validates output heatmap parameters.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    if (tabProperty.get() == GuiTabs.CREATE_GRAPHS
        && createGraphFilesTypeProperty.get() == CreateGraphFilesType.LIST_FILES) {
      listHeatmapFilePresenter.validate(errorHandler);
    }
    imageWidthPane.getStyleClass().remove("error");
    imageHeightPane.getStyleClass().remove("error");
    BiConsumer<String, String> widthErrorHandler = errorHandler.andThen(once((error, tooltip) -> {
      imageWidthPane.getStyleClass().add("error");
    }));
    BiConsumer<String, String> heightErrorHandler = errorHandler.andThen(once((error, tooltip) -> {
      imageHeightPane.getStyleClass().add("error");
    }));
    if (outputHeatmapProperty.get() && !useDefaultProperty.get()) {
      try {
        IntegerStringConverter converter = new IntegerStringConverter();
        int value = converter.fromString(imageWidth.getText()).intValue();
        if (value < MINIMAL_WIDTH) {
          widthErrorHandler.accept(message("error.imageWidth.underMinimum", MINIMAL_WIDTH), null);
        }
      } catch (Exception e) {
        widthErrorHandler.accept(message("error.imageWidth.invalidNumber"), null);
      }
      try {
        IntegerStringConverter converter = new IntegerStringConverter();
        int value = converter.fromString(imageHeight.getText()).intValue();
        if (value < MINIMAL_HEIGHT) {
          heightErrorHandler.accept(message("error.imageHeight.underMinimum", MINIMAL_HEIGHT),
              null);
        }
      } catch (Exception e) {
        heightErrorHandler.accept(message("error.imageHeight.invalidNumber"), null);
      }
    }
  }
}

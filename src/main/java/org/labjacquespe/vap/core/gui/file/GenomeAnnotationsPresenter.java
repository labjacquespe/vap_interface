/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.file;

import static org.labjacquespe.vap.core.service.ParameterFileService.ANNOTATIONS_PATH;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;
import static org.labjacquespe.vap.gui.MoveCaretToEndOnLostFocus.moveCaretToEndOnLostFocus;
import static org.labjacquespe.vap.gui.SelectAllListener.selectAllListener;

import java.io.File;
import java.util.function.BiConsumer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.core.TestData;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.JavafxUtils;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.labjacquespe.vap.javafx.drag.DragExitedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDetectedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDoneHandler;
import org.labjacquespe.vap.javafx.drag.DragFileDroppedHandler;
import org.labjacquespe.vap.javafx.drag.DragFileOverHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Genome annotations presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenomeAnnotationsPresenter extends BasePresenter {
  private ObjectProperty<File> relativeFolderProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private TextField genomeAnnotations;
  @Autowired
  private OperatingSystemService operatingSystemService;
  @Autowired
  private TestData testData;
  private FileChooser chooser = new FileChooser();

  @FXML
  private void initialize() {
    chooser.setTitle(message("chooser.title"));
    chooser.getExtensionFilters().add(new ExtensionFilter(message("chooser.description"),
        operatingSystemService.dataFileExtensions(operatingSystemService.currentOs())));
    moveCaretToEndOnLostFocus(genomeAnnotations);
    genomeAnnotations.addEventHandler(MouseEvent.MOUSE_CLICKED, selectAllListener());

    // Enable drag and drop.
    genomeAnnotations.setOnDragDetected(new DragFileDetectedHandler(genomeAnnotations));
    genomeAnnotations.setOnDragDone(new DragFileDoneHandler(genomeAnnotations));
    view.setOnDragOver(new DragFileOverHandler(view, genomeAnnotations));
    view.setOnDragExited(new DragExitedHandler(view));
    view.setOnDragDropped(new DragFileDroppedHandler(genomeAnnotations));
  }

  public ObjectProperty<File> relativeFolderProperty() {
    return relativeFolderProperty;
  }

  public final ObjectProperty<File> initialDirectoryProperty() {
    return chooser.initialDirectoryProperty();
  }

  /**
   * Returns genome annotations file.
   *
   * @return genome annotations file
   */
  public File getGenomeAnnotations() {
    if (genomeAnnotations.getText() != null) {
      File file = new File(genomeAnnotations.getText());
      if (!file.isAbsolute() && relativeFolderProperty.get() != null && !isTest(file)) {
        return new File(relativeFolderProperty.get(), genomeAnnotations.getText());
      } else {
        return file;
      }
    } else {
      return null;
    }
  }

  /**
   * Sets genome annotations file.
   *
   * @param genomeAnnotations
   *          genome annotations file
   */
  public void setGenomeAnnotations(File genomeAnnotations) {
    if (genomeAnnotations != null) {
      this.genomeAnnotations.setText(genomeAnnotations.getPath());
      this.genomeAnnotations.positionCaret(this.genomeAnnotations.getText().length());
    } else {
      this.genomeAnnotations.setText("");
    }
  }

  @FXML
  private void browseGenomeAnnotations(ActionEvent event) {
    JavafxUtils.setValidInitialDirectory(chooser);
    File selection = chooser.showOpenDialog(genomeAnnotations.getScene().getWindow());
    if (selection != null) {
      chooser.setInitialDirectory(selection.getParentFile());
      setGenomeAnnotations(selection);
    }
  }

  @FXML
  private void clearGenomeAnnotations() {
    genomeAnnotations.setText("");
  }

  @FXML
  private void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(ANNOTATIONS_PATH);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validate genome annotations file.
   *
   * @param errorHandler
   *          handles validation error
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().remove("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    if (genomeAnnotations.getText() == null || genomeAnnotations.getText().isEmpty()) {
      errorHandler.accept(message("error.required"), null);
      view.getStyleClass().add("error");
    } else {
      File file = getGenomeAnnotations();
      if (!file.isFile() && file.isAbsolute() && !isTest(file)) {
        errorHandler.accept(message("error.notExists", file.getName()), file.getPath());
        view.getStyleClass().add("error");
      }
    }
  }

  private boolean isTest(File file) {
    return testData.isTestFile(file);
  }
}

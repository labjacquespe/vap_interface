/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Help parameter presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HelpParameterPresenter {
  private static final String DOCUMENT =
      HelpParameterPresenter.class.getResource("/parameterDescription.html").toString();

  private StringProperty hashtagProperty = new SimpleStringProperty();
  private BooleanProperty buttonsVisibleProperty = new SimpleBooleanProperty(true);
  @FXML
  private Pane view;
  @FXML
  private WebView webView;
  @FXML
  private Pane buttons;
  @FXML
  private Button back;
  @FXML
  private Button forward;

  @FXML
  private void initialize() {
    webView.getEngine().getLoadWorker().stateProperty()
        .addListener((obv, ov, nv) -> updateHashtag());

    hashtagProperty.addListener((obv, ov, nv) -> updateHashtag());

    webView.getEngine().load(DOCUMENT);
    webView.getEngine().getHistory().currentIndexProperty().addListener((ob, ov, nv) -> {
      int index = nv.intValue();
      back.setDisable(index == 0);
      forward.setDisable(index == webView.getEngine().getHistory().getEntries().size() - 1);
    });
    buttons.visibleProperty().bind(buttonsVisibleProperty);
    back.setDisable(true);
    forward.setDisable(true);
  }

  public StringProperty hashtagProperty() {
    return hashtagProperty;
  }

  public BooleanProperty buttonsVisibleProperty() {
    return buttonsVisibleProperty;
  }

  public String getHashtag() {
    return hashtagProperty.get();
  }

  public void setHashtag(String hashtag) {
    hashtagProperty.set(hashtag);
  }

  private State getState() {
    return webView.getEngine().getLoadWorker().getState();
  }

  private void updateHashtag() {
    State state = getState();
    if (state == State.SUCCEEDED) {
      Platform.runLater(() -> {
        String hashtag = hashtagProperty.get();
        if (hashtag != null && !hashtag.equals("")) {
          webView.getEngine().executeScript("window.location.hash = '" + hashtag + "'");
        } else {
          webView.getEngine().executeScript("window.location.hash = ''");
        }
      });
    }
  }

  @FXML
  private void back() {
    Platform.runLater(() -> {
      webView.getEngine().executeScript("history.back()");
    });
  }

  @FXML
  private void forward() {
    Platform.runLater(() -> {
      webView.getEngine().executeScript("history.forward()");
    });
  }
}

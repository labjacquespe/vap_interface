/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.file;

import static org.labjacquespe.vap.core.service.ParameterFileService.DATASET_PATH;
import static org.labjacquespe.vap.function.OnceBiConsumer.once;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.core.TestData;
import org.labjacquespe.vap.core.gui.HelpParameterPopup;
import org.labjacquespe.vap.gui.MultipleFilesSelectionPresenter;
import org.labjacquespe.vap.gui.MultipleFilesSelectionView;
import org.labjacquespe.vap.javafx.BasePresenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Dataset presenter.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DatasetPresenter extends BasePresenter {
  private final ObjectProperty<File> relativeFolderProperty = new SimpleObjectProperty<>();
  @FXML
  private Pane view;
  @FXML
  private Pane multipleFilesBox;
  @Autowired
  private OperatingSystemService operatingSystemService;
  @Autowired
  private TestData testData;
  private MultipleFilesSelectionPresenter multipleFiles;
  private FileChooser fileChooser = new FileChooser();

  @FXML
  private void initialize() {
    MultipleFilesSelectionView multipleFilesView = new MultipleFilesSelectionView();
    multipleFiles = (MultipleFilesSelectionPresenter) multipleFilesView.getPresenter();
    multipleFilesBox.getChildren().add(multipleFilesView.getView());
    fileChooser.setTitle(message("fileChooser.title"));
    fileChooser.getExtensionFilters().add(new ExtensionFilter(message("fileChooser.description"),
        operatingSystemService.dataFileExtensions(operatingSystemService.currentOs())));
    multipleFiles.setFileChooser(fileChooser);
  }

  public ListProperty<File> filesProperty() {
    return multipleFiles.filesProperty();
  }

  public ObjectProperty<File> initialDirectoryProperty() {
    return multipleFiles.initialDirectoryProperty();
  }

  public ObjectProperty<File> relativeFolderProperty() {
    return relativeFolderProperty;
  }

  /**
   * Returns dataset files.
   *
   * @return dataset files
   */
  public List<File> getFiles() {
    List<File> originalFiles = multipleFiles.getFiles();
    List<File> convertedFiles = new ArrayList<>(originalFiles.size());
    for (File file : originalFiles) {
      if (!file.isAbsolute() && relativeFolderProperty.get() != null && !isTest(file)) {
        file = new File(relativeFolderProperty.get(), file.getPath());
      }
      convertedFiles.add(file);
    }
    return convertedFiles;
  }

  public void setFiles(List<File> files) {
    multipleFiles.setFiles(files);
  }

  /**
   * Shows help pop-up.
   *
   * @param event
   *          event
   */
  public void help(ActionEvent event) {
    HelpParameterPopup helpPopup = new HelpParameterPopup();
    helpPopup.setHashtag(DATASET_PATH);
    helpPopup.show((Node) event.getSource());
  }

  /**
   * Validates dataset files.
   *
   * @param errorHandler
   *          handles validation errors
   */
  public void validate(BiConsumer<String, String> errorHandler) {
    view.getStyleClass().removeAll("error");
    errorHandler = errorHandler.andThen(once((error, tooltip) -> {
      view.getStyleClass().add("error");
    }));
    multipleFiles.remvoveStyleClassFromCells("error");
    Collection<File> files = getFiles();
    if (files == null || files.isEmpty()) {
      errorHandler.accept(message("error.required"), null);
    } else {
      for (File file : files) {
        if (!file.isFile() && file.isAbsolute() && !isTest(file)) {
          errorHandler.accept(message("error.notExists", file.getName()), file.getPath());
          multipleFiles.addStyleClass(file, "error");
        }
      }
    }
  }

  private boolean isTest(File file) {
    return testData.isTestFile(file);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.gui.parameter;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.BlockAlignment;
import org.labjacquespe.vap.core.SplitAlignment;
import org.labjacquespe.vap.core.SplitType;

/**
 * {@link Block} implementation for JavaFX.
 */
public class BlockFx {
  private ObjectProperty<Long> windowsProperty = new SimpleObjectProperty<>();
  private ObjectProperty<BlockAlignment> alignmentProperty = new SimpleObjectProperty<>();
  private ObjectProperty<SplitType> splitTypeProperty = new SimpleObjectProperty<>();
  private ObjectProperty<Long> splitProperty = new SimpleObjectProperty<>();
  private ObjectProperty<SplitAlignment> splitAlignmentProperty = new SimpleObjectProperty<>();
  private ObjectProperty<BlockType> typeProperty = new SimpleObjectProperty<>();

  public BlockFx() {
  }

  /**
   * Copies properties from block.
   * 
   * @param block
   *          block
   */
  public void copy(Block block) {
    windowsProperty.set(block.windows);
    alignmentProperty.set(block.alignment);
    splitTypeProperty.set(block.splitType);
    splitProperty.set(block.split);
    splitAlignmentProperty.set(block.splitAlignment);
    typeProperty.set(block.type);
  }

  /**
   * Converts this block into a {@link Block}.
   * 
   * @return block
   */
  public Block toBlock() {
    Block block = new Block();
    block.windows = getWindows();
    block.alignment = getAlignment();
    block.splitType = getSplitType();
    block.split = getSplit();
    block.splitAlignment = getSplitAlignment();
    block.type = getType();
    return block;
  }

  public ObjectProperty<Long> windowsProperty() {
    return windowsProperty;
  }

  public ObjectProperty<BlockAlignment> alignmentProperty() {
    return alignmentProperty;
  }

  public ObjectProperty<SplitType> splitTypeProperty() {
    return splitTypeProperty;
  }

  public ObjectProperty<Long> splitProperty() {
    return splitProperty;
  }

  public ObjectProperty<SplitAlignment> splitAlignmentProperty() {
    return splitAlignmentProperty;
  }

  public ObjectProperty<BlockType> typeProperty() {
    return typeProperty;
  }

  public Long getWindows() {
    return windowsProperty.get();
  }

  public void setWindows(Long windows) {
    windowsProperty.set(windows);
  }

  public BlockAlignment getAlignment() {
    return alignmentProperty.get();
  }

  public void setAlignment(BlockAlignment alignment) {
    alignmentProperty.set(alignment);
  }

  public SplitType getSplitType() {
    return splitTypeProperty.get();
  }

  public void setSplitType(SplitType splitType) {
    splitTypeProperty.set(splitType);
  }

  public Long getSplit() {
    return splitProperty.get();
  }

  public void setSplit(Long split) {
    splitProperty.set(split);
  }

  public SplitAlignment getSplitAlignment() {
    return splitAlignmentProperty.get();
  }

  public void setSplitAlignment(SplitAlignment splitAlignment) {
    splitAlignmentProperty.set(splitAlignment);
  }

  public BlockType getType() {
    return typeProperty.get();
  }

  public void setType(BlockType type) {
    typeProperty.set(type);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;

/**
 * Writes parameter files.
 */
public class ParameterWriter implements Closeable {
  private final Writer writer;

  public ParameterWriter(Writer writer) {
    this.writer = writer;
  }

  public void writeVersion(String version) throws IOException {
    writer.write("~~version " + version);
    writer.write(System.lineSeparator());
  }

  public void writeParameter(String name, String value) throws IOException {
    writer.write("~~@" + name + "=" + value);
    writer.write(System.lineSeparator());
  }

  public void writeOther(String line) throws IOException {
    writer.write(line);
    writer.write(System.lineSeparator());
  }

  @Override
  public void close() throws IOException {
    writer.close();
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

/**
 * Parses parameters file.
 */
@Component
public class ParameterParser {
  private static final Pattern VERSION_PATTERN = Pattern.compile("~~version (.*)");
  private static final Pattern PARAMETER_PATTERN = Pattern.compile("~~@(\\w+)=(.*)");

  public static class ParameterHandlers {
    public Consumer<String> versionHandler = version -> {
    };

    public BiConsumer<String, String> parameterHandler = (name, value) -> {
    };

    public Consumer<String> otherHandler = line -> {
    };
  }

  public ParameterParser() {
  }

  /**
   * Parses parameter file as Reader.
   *
   * @param reader
   *          parameter file as Reader
   * @param handlers
   *          handles file elements
   * @throws IOException
   *           could not parse file
   */
  public void parse(Reader reader, ParameterHandlers handlers) throws IOException {
    List<String> lines = new ArrayList<>();
    try (BufferedReader bufferedReader = new BufferedReader(reader)) {
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        lines.add(line);
      }
    }
    for (String line : lines) {
      Matcher matcher = VERSION_PATTERN.matcher(line);
      if (matcher.matches()) {
        handlers.versionHandler.accept(matcher.group(1));
      } else {
        matcher = PARAMETER_PATTERN.matcher(line);
        if (matcher.matches()) {
          handlers.parameterHandler.accept(matcher.group(1), matcher.group(2));
        } else {
          handlers.otherHandler.accept(line);
        }
      }
    }
  }
}

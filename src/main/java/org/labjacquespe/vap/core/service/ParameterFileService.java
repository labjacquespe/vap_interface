/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import static org.labjacquespe.vap.core.Block.firstBlock;
import static org.labjacquespe.vap.core.Block.lastBlock;
import static org.labjacquespe.vap.core.Block.relativeBlock;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.service.GeneChartService;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.BlockAlignment;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.CoordinatesType;
import org.labjacquespe.vap.core.DispersionType;
import org.labjacquespe.vap.core.Graph;
import org.labjacquespe.vap.core.MergeMiddleIntron;
import org.labjacquespe.vap.core.MissingDataReplacement;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.SplitAlignment;
import org.labjacquespe.vap.core.SplitType;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;
import org.labjacquespe.vap.core.version.ParameterFileVersionService;
import org.labjacquespe.vap.message.MessageResources;
import org.springframework.stereotype.Component;

/**
 * Service for parameter file.
 */
@Component
public class ParameterFileService {
  public static final String VERSION_EXPORT = "~~version %1$s";
  public static final String PARAMETER_EXPORT = "~~@%1$s=%2$s";
  public static final String DATA_ALIAS = "R1:=:";
  public static final String ANALYSIS_MODE = "analysis_mode";
  public static final String DATASET_PATH = "dataset_path";
  public static final String REFGROUP_PATH = "refgroup_path";
  public static final String ANNOTATIONS_PATH = "annotations_path";
  public static final String SELECTION_PATH = "selection_path";
  public static final String EXCLUSION_PATH = "exclusion_path";
  public static final String ANALYSIS_METHOD = "analysis_method";
  public static final String ANNOTATION_COORDINATES_TYPE = "annotation_coordinates_type";
  public static final String REFERENCE_POINTS = "reference_points";
  public static final String FIRST_PT_BOUNDARY = "1pt_boundary";
  public static final String WINDOW_SIZE = "window_size";
  public static final String WINDOWS_PER_BLOCK = "windows_per_block";
  public static final String BLOCK_ALIGNMENT = "block_alignment";
  public static final String BLOCK_SPLIT_TYPE = "block_split_type";
  public static final String BLOCK_SPLIT_VALUE = "block_split_value";
  public static final String BLOCK_SPLIT_ALIGNMENT = "block_split_alignment";
  public static final String MERGE_MID_INTRONS = "merge_mid_introns";
  public static final String AGGREGATE_DATA_TYPE = "aggregate_data_type";
  public static final String SMOOTHING_WINDOWS = "smoothing_windows";
  public static final String MEAN_DISPERSION_VALUE = "mean_dispersion_value";
  public static final String PROCESS_MISSING_DATA = "process_missing_data";
  public static final String PROCESS_DATA_BY_CHUNK = "process_data_by_chunk";
  public static final String DATASET_CHUNK_SIZE = "dataset_chunk_size";
  public static final String OUTPUT_DIRECTORY = "output_directory";
  public static final String PREFIX_FILENAME = "prefix_filename";
  public static final String WRITE_INDIVIDUAL_REFERENCES = "write_individual_references";
  public static final String GENERATE_HEATMAP = "generate_heatmaps";
  public static final String DISPLAY_DISPERSION_VALUES = "display_dispersion_values";
  public static final String GENERATE_AGGREGATE_GRAPHS = "generate_aggregate_graphs";
  public static final String ONE_GRAPH_PER_DATASET = "one_graph_per_dataset";
  public static final String ONE_GRAPH_PER_GROUP = "one_graph_per_group";
  public static final String ORIENTATION_SUBGROUPS = "orientation_subgroups";
  public static final String ONE_GRAPH_PER_ORIENTATION = "one_graph_per_orientation";
  public static final String Y_AXIS_SCALE = "Y_axis_scale";

  private static final String MISSING_PARAMETER = "MISSING_PARAMETER";
  private static final String INVALID_REFERENCE_TYPE = "INVALID_REFERENCE_TYPE";
  private static final String RELATIVE_OUTPUT_FOLDER = "RELATIVE_OUTPUT_FOLDER";
  private static final String INVALID_PROCESS_MISSING_DATA = "INVALID_PROCESS_MISSING_DATA";
  private static final String INVALID_PROCESS_DATA_BY_CHUNK = "INVALID_PROCESS_DATA_BY_CHUNK";
  private static final String INVALID_DATA_CHUNK_SIZE = "INVALID_DATA_CHUNK_SIZE";
  private static final String BELOW_MINIMUM_DATA_CHUNK_SIZE = "BELOW_MINIMUM_DATA_CHUNK_SIZE";
  private static final String INVALID_COORDINATES_TYPE = "INVALID_COORDINATES_TYPE";
  private static final String INVALID_MERGE_MIDDLE_INTRON = "INVALID_MERGE_MIDDLE_INTRON";
  private static final String INVALID_REPRESENTATION_TYPE = "INVALID_REPRESENTATION_TYPE";
  private static final String INVALID_REFERENCE_POINTS = "INVALID_REFERENCE_POINTS";
  private static final String BELOW_MINIMUM_REFERENCE_POINTS = "BELOW_MINIMUM_REFERENCE_POINTS";
  private static final String ABOVE_MAXIMUM_REFERENCE_POINTS = "ABOVE_MAXIMUM_REFERENCE_POINTS";
  private static final String INVALID_BOUNDARY = "INVALID_BOUNDARY";
  private static final String INVALID_WINDOW_SIZE = "INVALID_WINDOW_SIZE";
  private static final String BELOW_MINIMUM_WINDOW_SIZE = "BELOW_MINIMUM_WINDOW_SIZE";
  private static final String INVALID_BLOCK_WINDOW = "INVALID_BLOCK_WINDOW";
  private static final String BLOCK_WINDOW_COUNT_MISSMATCH = "BLOCK_WINDOW_COUNT_MISSMATCH";
  private static final String BELOW_MINIMUM_BLOCK_WINDOW = "BELOW_MINIMUM_BLOCK_WINDOW";
  private static final String INVALID_BLOCK_ALIGNMENT = "INVALID_BLOCK_ALIGNMENT";
  private static final String BLOCK_ALIGNMENT_COUNT_MISSMATCH = "BLOCK_ALIGNMENT_COUNT_MISSMATCH";
  private static final String INVALID_BLOCK_SPLIT_TYPE = "INVALID_BLOCK_SPLIT_TYPE";
  private static final String BLOCK_SPLIT_TYPE_COUNT_MISSMATCH = "BLOCK_SPLIT_TYPE_COUNT_MISSMATCH";
  private static final String INVALID_BLOCK_SPLIT = "INVALID_BLOCK_SPLIT";
  private static final String BLOCK_SPLIT_COUNT_MISSMATCH = "BLOCK_SPLIT_COUNT_MISSMATCH";
  private static final String BELOW_MINIMUM_BLOCK_SPLIT = "BELOW_MINIMUM_BLOCK_SPLIT";
  private static final String INVALID_BLOCK_SPLIT_ALIGNMENT = "INVALID_BLOCK_SPLIT_ALIGNMENT";
  private static final String BLOCK_SPLIT_ALIGNMENT_COUNT_MISSMATCH =
      "BLOCK_SPLIT_ALIGNMENT_COUNT_MISSMATCH";
  private static final String INVALID_SMOOTH_CURVE_WINDOW = "INVALID_SMOOTH_CURVE_WINDOW";
  private static final String BELOW_MINIMUM_SMOOTH_CURVE_WINDOW =
      "BELOW_MINIMUM_SMOOTH_CURVE_WINDOW";
  private static final String SMOOTH_CURVE_WINDOW_NOT_EVEN = "SMOOTH_CURVE_WINDOW_NOT_EVEN";
  private static final String INVALID_CALCULATED_TYPE = "INVALID_CALCULATED_TYPE";
  private static final String INVALID_DISPERSION_TYPE = "INVALID_DISPERSION_TYPE";
  private static final String INVALID_DISPERSION = "INVALID_DISPERSION";
  private static final String INVALID_GENERATE_AGGREGATE_GRAPHS =
      "INVALID_GENERATE_AGGREGATE_GRAPHS";
  private static final String INVALID_ONE_DATA_FILE_PER_GRAPH = "INVALID_ONE_DATA_FILE_PER_GRAPH";
  private static final String INVALID_ONE_REFERENCE_GROUP_PER_GRAPH =
      "INVALID_ONE_REFERENCE_GROUP_PER_GRAPH";
  private static final String INVALID_ONE_ORIENTATION_PER_GRAPH =
      "INVALID_ONE_ORIENTATION_PER_GRAPH";
  private static final String INVALID_OUTPUT_DATA = "INVALID_OUTPUT_DATA";
  private static final String INVALID_GENERATE_HEATMAP = "INVALID_GENERATE_HEATMAP";
  private static final String INVALID_OUTPUT_GRAPH = "INVALID_OUTPUT_GRAPH";
  private static final String OUTPUT_GRAPH_COUNT_MISSMATCH = "OUTPUT_GRAPH_COUNT_MISSMATCH";
  private static final String INVALID_YAXIS_SCALE = "INVALID_YAXIS_SCALE";

  private static class ParserProperties {
    private File relativeDirectory;
    private String dataAlias;
    private Integer blockWindowCount;
    private Integer blockAlignmentCount;
    private Integer blockSplitTypeCount;
    private Integer blockSplitCount;
    private Integer blockSplitAlignmentCount;
  }

  @Inject
  private ParameterFileVersionService parameterFileVersionService;
  @Inject
  private ParameterParser parameterParser;
  @Inject
  private GeneChartService geneChartService;
  @Inject
  private Charset charset;

  protected ParameterFileService() {
  }

  protected ParameterFileService(ParameterFileVersionService textParameterVersionService,
      ParameterParser parameterParser, GeneChartService geneChartService, Charset charset) {
    this.parameterFileVersionService = textParameterVersionService;
    this.parameterParser = parameterParser;
    this.geneChartService = geneChartService;
    this.charset = charset;
  }

  private String updateToCurrentVersion(File file, File relativeDirectory) throws IOException {
    StringWriter versionOutput = new StringWriter();
    try (Reader input = Files.newBufferedReader(file.toPath(), charset)) {
      parameterFileVersionService.updateParameters(input, versionOutput, relativeDirectory);
    }
    return versionOutput.toString();
  }

  /**
   * Validates parameter file.
   *
   * @param file
   *          file containing parameters
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   * @throws IOException
   *           could not read file
   */
  public void validate(final File file, Locale locale, final Consumer<String> errorHandler)
      throws IOException {
    parseAndValidate(file, locale, errorHandler);
  }

  private AnalysisParameters parseAndValidate(final File file, Locale locale,
      final Consumer<String> errorHandler) throws IOException {
    final MessageResources resources = new MessageResources(ParameterFileService.class, locale);
    final String updatedFile = updateToCurrentVersion(file, file.getParentFile());

    final AnalysisParameters analysisParameters = new AnalysisParameters();
    analysisParameters.referenceFiles = new ArrayList<>();
    analysisParameters.dataFiles = new ArrayList<>();
    // Set invalid values in case parameters are incorrect.
    analysisParameters.geneChartParameters.referenceType = null;
    analysisParameters.geneChartParameters.representationType = null;
    analysisParameters.dataChunkSize = -1;
    analysisParameters.geneChartParameters.referencePoints = -1;
    analysisParameters.geneChartParameters.windowSize = -1L;
    final List<Block> blocks = new ArrayList<Block>() {
      private static final long serialVersionUID = 3899948203191647414L;

      @Override
      public Block get(int index) {
        while (index >= size()) {
          add(new Block());
        }
        return super.get(index);
      }
    };
    analysisParameters.geneChartParameters.blocks = blocks;
    analysisParameters.geneChartParameters.yaxisScale = new YAxisScale(null, null);

    final Map<String, Boolean> parameterNames = new HashMap<>();
    try (Reader template = new InputStreamReader(
        getClass().getResourceAsStream("/parameters_template.txt"), charset)) {
      ParameterHandlers parserHandlers = new ParameterHandlers();
      parserHandlers.parameterHandler = (name, value) -> parameterNames.put(name, false);
      parameterParser.parse(template, parserHandlers);
    }
    ParameterHandlers missingParserHandlers = new ParameterHandlers();
    missingParserHandlers.parameterHandler = (name, value) -> parameterNames.put(name, true);
    parameterParser.parse(new StringReader(updatedFile), missingParserHandlers);
    for (Map.Entry<String, Boolean> entry : parameterNames.entrySet()) {
      if (!entry.getValue()) {
        errorHandler.accept(resources.message(MISSING_PARAMETER, entry.getKey()));
      }
    }

    ParserProperties properties = new ParserProperties();
    properties.relativeDirectory = parseRelativeFolder(updatedFile);
    ParameterHandlers parserHandlers = new ParameterHandlers();
    parserHandlers.parameterHandler = (name, value) -> parseAndValidateParameter(name, value,
        analysisParameters, properties, resources, errorHandler);
    parameterParser.parse(new StringReader(updatedFile), parserHandlers);
    updateBlockTypes(analysisParameters.geneChartParameters.blocks,
        analysisParameters.geneChartParameters.referenceType,
        analysisParameters.geneChartParameters.referencePoints);
    return analysisParameters;
  }

  private void parseAndValidateParameter(String name, String value,
      AnalysisParameters analysisParameters, ParserProperties properties,
      MessageResources resources, Consumer<String> errorHandler) {
    switch (name) {
      case OUTPUT_DIRECTORY:
        File outputFolder = new File(value);
        if (!outputFolder.isAbsolute()) {
          errorHandler.accept(resources.message(RELATIVE_OUTPUT_FOLDER, name));
        } else {
          analysisParameters.outputFolder =
              parseFile(value, properties.relativeDirectory, resources);
        }
        break;
      case ANALYSIS_MODE:
        if (!Pattern.matches(pattern(resources, ANALYSIS_MODE), value)) {
          errorHandler.accept(resources.message(INVALID_REFERENCE_TYPE, name));
        } else {
          switch (value) {
            case "A":
              analysisParameters.geneChartParameters.referenceType = ReferenceType.ANNOTATIONS;
              break;
            case "E":
              analysisParameters.geneChartParameters.referenceType = ReferenceType.EXONS;
              break;
            case "C":
              analysisParameters.geneChartParameters.referenceType = ReferenceType.COORDINATES;
              break;
            default:
              break;
          }
        }
        validateReferencePointsLimits(analysisParameters.geneChartParameters.referenceType,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      case REFGROUP_PATH:
        if (properties.dataAlias == null) {
          properties.dataAlias = parseFileAlias(value, resources);
        }
        if (parseFileAlias(value, resources).equals(properties.dataAlias)) {
          File refGroupFile = parseFile(value, properties.relativeDirectory, resources);
          if (refGroupFile != null) {
            analysisParameters.referenceFiles.add(refGroupFile);
          }
        }
        break;
      case DATASET_PATH:
        if (properties.dataAlias == null) {
          properties.dataAlias = parseFileAlias(value, resources);
        }
        if (parseFileAlias(value, resources).equals(properties.dataAlias)) {
          File datasetFile = parseFile(value, properties.relativeDirectory, resources);
          if (datasetFile != null) {
            analysisParameters.dataFiles
                .add(parseFile(value, properties.relativeDirectory, resources));
          }
        }
        break;
      case PROCESS_MISSING_DATA:
        if (!Pattern.matches(pattern(resources, PROCESS_MISSING_DATA), value)) {
          errorHandler.accept(resources.message(INVALID_PROCESS_MISSING_DATA, name));
        } else {
          switch (value) {
            case "1":
              analysisParameters.missingDataReplacement = MissingDataReplacement.ZERO;
              break;
            case "0":
              analysisParameters.missingDataReplacement = MissingDataReplacement.IGNORE;
              break;
            default:
              break;
          }
        }
        break;
      case PROCESS_DATA_BY_CHUNK:
        if (!Pattern.matches(pattern(resources, PROCESS_DATA_BY_CHUNK), value)) {
          errorHandler.accept(resources.message(INVALID_PROCESS_DATA_BY_CHUNK, name));
        } else {
          analysisParameters.processDataByChunk = value.equals("1");
        }
        break;
      case DATASET_CHUNK_SIZE:
        try {
          Integer datasetChunkSize = Integer.parseInt(value);
          if (datasetChunkSize < 1) {
            errorHandler.accept(resources.message(BELOW_MINIMUM_DATA_CHUNK_SIZE, name, 1));
          } else {
            analysisParameters.dataChunkSize = parseInteger(value, 0);
          }
        } catch (NumberFormatException e) {
          errorHandler.accept(resources.message(INVALID_DATA_CHUNK_SIZE, name));
        }
        break;
      case ANNOTATIONS_PATH:
        if (!value.isEmpty()) {
          analysisParameters.genomeAnnotations =
              parseFile(value, properties.relativeDirectory, resources);
        }
        break;
      case SELECTION_PATH:
        if (!value.isEmpty()) {
          analysisParameters.selectionAnnotationFilter =
              parseFile(value, properties.relativeDirectory, resources);
        }
        break;
      case EXCLUSION_PATH:
        if (!value.isEmpty()) {
          analysisParameters.exclusionAnnotationFilter =
              parseFile(value, properties.relativeDirectory, resources);
        }
        break;
      case ANNOTATION_COORDINATES_TYPE:
        if (!Pattern.matches(pattern(resources, ANNOTATION_COORDINATES_TYPE), value)) {
          errorHandler.accept(resources.message(INVALID_COORDINATES_TYPE, name));
        } else {
          switch (value) {
            case "N":
              analysisParameters.coordinatesType = null;
              break;
            case "T":
              analysisParameters.coordinatesType = CoordinatesType.TX;
              break;
            case "C":
              analysisParameters.coordinatesType = CoordinatesType.CDS;
              break;
            default:
              break;
          }
        }
        break;
      case MERGE_MID_INTRONS:
        if (!Pattern.matches(pattern(resources, MERGE_MID_INTRONS), value)) {
          errorHandler.accept(resources.message(INVALID_MERGE_MIDDLE_INTRON, name));
        } else {
          switch (value) {
            case "N":
              analysisParameters.mergeMiddleIntron = null;
              break;
            case "F":
              analysisParameters.mergeMiddleIntron = MergeMiddleIntron.FIRST;
              break;
            case "L":
              analysisParameters.mergeMiddleIntron = MergeMiddleIntron.LAST;
              break;
            default:
              break;
          }
        }
        break;
      case ANALYSIS_METHOD:
        if (!Pattern.matches(pattern(resources, ANALYSIS_METHOD), value)) {
          errorHandler.accept(resources.message(INVALID_REPRESENTATION_TYPE, name));
        } else {
          switch (value) {
            case "A":
              analysisParameters.geneChartParameters.representationType =
                  RepresentationType.ABSOLUTE;
              break;
            case "R":
              analysisParameters.geneChartParameters.representationType =
                  RepresentationType.RELATIVE;
              break;
            default:
              break;
          }
        }
        break;
      case REFERENCE_POINTS:
        try {
          Integer.parseInt(value);
          analysisParameters.geneChartParameters.referencePoints = parseInteger(value, 0);
        } catch (NumberFormatException e) {
          errorHandler.accept(resources.message(INVALID_REFERENCE_POINTS, name));
        }
        validateReferencePointsLimits(analysisParameters.geneChartParameters.referenceType,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        validateBlockWindowCount(properties.blockWindowCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        validateBlockAlignmentCount(properties.blockAlignmentCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        validateBlockSplitTypeCount(properties.blockSplitTypeCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        validateBlockSplitCount(properties.blockSplitCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        validateBlockSplitAlignmentCount(properties.blockSplitAlignmentCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      case FIRST_PT_BOUNDARY:
        if (!Pattern.matches(pattern(resources, FIRST_PT_BOUNDARY), value)) {
          errorHandler.accept(resources.message(INVALID_BOUNDARY, name));
        } else {
          switch (value) {
            case "5":
              analysisParameters.geneChartParameters.boundary = Boundary.FIVE_PRIME;
              break;
            case "3":
              analysisParameters.geneChartParameters.boundary = Boundary.THREE_PRIME;
              break;
            default:
              break;
          }
        }
        break;
      case WINDOW_SIZE:
        try {
          Long windowSize = Long.parseLong(value);
          if (windowSize < 0) {
            errorHandler.accept(resources.message(BELOW_MINIMUM_WINDOW_SIZE, name, 0));
          } else {
            analysisParameters.geneChartParameters.windowSize = parseLong(value, 0L);
          }
        } catch (NumberFormatException e) {
          errorHandler.accept(resources.message(INVALID_WINDOW_SIZE, name));
        }
        break;
      case WINDOWS_PER_BLOCK: {
        value = value.replaceFirst(";\\s*$", "");
        String[] windows = value.split(";");
        properties.blockWindowCount = windows.length;
        boolean invalid = false;
        boolean belowMinimum = false;
        for (String windowAsString : windows) {
          try {
            Long window = Long.parseLong(windowAsString);
            if (window < 0) {
              belowMinimum = true;
            }
          } catch (NumberFormatException e) {
            invalid = true;
          }
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_BLOCK_WINDOW, name));
        } else if (belowMinimum) {
          errorHandler.accept(resources.message(BELOW_MINIMUM_BLOCK_WINDOW, name, 0));
        } else {
          for (int i = 0; i < windows.length; i++) {
            Block block = analysisParameters.geneChartParameters.blocks.get(i);
            block.windows = parseLong(windows[i], null);
          }
        }
        validateBlockWindowCount(properties.blockWindowCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      }
      case BLOCK_ALIGNMENT: {
        value = value.replaceFirst(";\\s*$", "");
        String[] alignments = value.split(";");
        properties.blockAlignmentCount = alignments.length;
        boolean invalid = false;
        for (String alignment : alignments) {
          invalid |= !Pattern.matches(pattern(resources, BLOCK_ALIGNMENT), alignment);
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_BLOCK_ALIGNMENT, name));
        } else {
          for (int i = 0; i < alignments.length; i++) {
            Block block = analysisParameters.geneChartParameters.blocks.get(i);
            switch (alignments[i]) {
              case "S":
                block.alignment = BlockAlignment.SPLIT;
                break;
              case "L":
                block.alignment = BlockAlignment.LEFT;
                break;
              case "R":
                block.alignment = BlockAlignment.RIGHT;
                break;
              default:
                break;
            }
          }
        }
        validateBlockAlignmentCount(properties.blockAlignmentCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      }
      case BLOCK_SPLIT_TYPE: {
        value = value.replaceFirst(";\\s*$", "");
        String[] splitTypes = value.split(";");
        properties.blockSplitTypeCount = splitTypes.length;
        boolean invalid = false;
        for (String splitType : splitTypes) {
          invalid |= !Pattern.matches(pattern(resources, BLOCK_SPLIT_TYPE), splitType);
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_BLOCK_SPLIT_TYPE, name));
        } else {
          for (int i = 0; i < splitTypes.length; i++) {
            Block block = analysisParameters.geneChartParameters.blocks.get(i);
            switch (splitTypes[i]) {
              case "A":
                block.splitType = SplitType.ABSOLUTE;
                break;
              case "P":
                block.splitType = SplitType.PERCENTAGE;
                break;
              case "N":
                block.splitType = null;
                break;
              default:
                break;
            }
          }
        }
        validateBlockSplitTypeCount(properties.blockSplitTypeCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      }
      case BLOCK_SPLIT_ALIGNMENT: {
        value = value.replaceFirst(";\\s*$", "");
        String[] splitAlignments = value.split(";");
        properties.blockSplitAlignmentCount = splitAlignments.length;
        boolean invalid = false;
        for (String splitAlignment : splitAlignments) {
          invalid |= !Pattern.matches(pattern(resources, BLOCK_SPLIT_ALIGNMENT), splitAlignment);
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_BLOCK_SPLIT_ALIGNMENT, name));
        } else {
          for (int i = 0; i < splitAlignments.length; i++) {
            Block block = analysisParameters.geneChartParameters.blocks.get(i);
            switch (splitAlignments[i]) {
              case "L":
                block.splitAlignment = SplitAlignment.LEFT;
                break;
              case "R":
                block.splitAlignment = SplitAlignment.RIGHT;
                break;
              case "N":
                block.splitAlignment = null;
                break;
              default:
                break;
            }
          }
        }
        validateBlockSplitAlignmentCount(properties.blockSplitAlignmentCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      }
      case BLOCK_SPLIT_VALUE: {
        value = value.replaceFirst(";\\s*$", "");
        String[] splits = value.split(";");
        properties.blockSplitCount = splits.length;
        boolean invalid = false;
        boolean belowMinimum = false;
        for (String splitAsString : splits) {
          try {
            Long split = Long.parseLong(splitAsString);
            if (split < 0) {
              belowMinimum = true;
            }
          } catch (NumberFormatException e) {
            invalid = true;
          }
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_BLOCK_SPLIT, name));
        } else if (belowMinimum) {
          errorHandler.accept(resources.message(BELOW_MINIMUM_BLOCK_SPLIT, name, 0));
        } else {
          for (int i = 0; i < splits.length; i++) {
            Block block = analysisParameters.geneChartParameters.blocks.get(i);
            block.split = parseLong(splits[i], null);
          }
        }
        validateBlockSplitCount(properties.blockSplitCount,
            analysisParameters.geneChartParameters.referencePoints, resources, errorHandler);
        break;
      }
      case SMOOTHING_WINDOWS:
        try {
          Long smoothDataWindow = Long.parseLong(value);
          if (smoothDataWindow < 0) {
            errorHandler.accept(resources.message(BELOW_MINIMUM_SMOOTH_CURVE_WINDOW, name, 0));
          } else if (smoothDataWindow % 2 != 0) {
            errorHandler.accept(resources.message(SMOOTH_CURVE_WINDOW_NOT_EVEN, name));
          } else {
            analysisParameters.smoothDataWindows = Integer.parseInt(value);
          }
        } catch (NumberFormatException e) {
          errorHandler.accept(resources.message(INVALID_SMOOTH_CURVE_WINDOW, name));
        }
        break;
      case PREFIX_FILENAME:
        analysisParameters.ouputFilePrefix = value;
        break;
      case AGGREGATE_DATA_TYPE:
        if (!Pattern.matches(pattern(resources, AGGREGATE_DATA_TYPE), value)) {
          errorHandler.accept(resources.message(INVALID_CALCULATED_TYPE, name));
        } else {
          switch (value) {
            case "E":
              analysisParameters.aggregateValueType = AggregateValueType.MEAN;
              break;
            case "D":
              analysisParameters.aggregateValueType = AggregateValueType.MEDIAN;
              break;
            case "A":
              analysisParameters.aggregateValueType = AggregateValueType.MAX;
              break;
            case "I":
              analysisParameters.aggregateValueType = AggregateValueType.MIN;
              break;
            default:
              break;
          }
        }
        break;
      case ORIENTATION_SUBGROUPS: {
        value = value.replaceFirst(";\\s*$", "");
        String[] graphs = value.split(";");
        boolean invalid = false;
        for (String graph : graphs) {
          invalid |= !Pattern.matches(pattern(resources, ORIENTATION_SUBGROUPS), graph);
        }
        if (invalid) {
          errorHandler.accept(resources.message(INVALID_OUTPUT_GRAPH, name));
        } else if (graphs.length != 9) {
          errorHandler.accept(resources.message(OUTPUT_GRAPH_COUNT_MISSMATCH, name, 9));
        } else {
          analysisParameters.outputGraphs = new HashSet<>();
          if (graphs[0].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.ANY_ANY);
          }
          if (graphs[1].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.ANY_CONV);
          }
          if (graphs[2].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.DIV_ANY);
          }
          if (graphs[3].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.ANY_TAND);
          }
          if (graphs[4].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.TAND_ANY);
          }
          if (graphs[5].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.TAND_TAND);
          }
          if (graphs[6].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.TAND_CONV);
          }
          if (graphs[7].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.DIV_TAND);
          }
          if (graphs[8].equals("1")) {
            analysisParameters.outputGraphs.add(Graph.DIV_CONV);
          }
        }
        break;
      }
      case WRITE_INDIVIDUAL_REFERENCES:
        if (!Pattern.matches(pattern(resources, WRITE_INDIVIDUAL_REFERENCES), value)) {
          errorHandler.accept(resources.message(INVALID_OUTPUT_DATA, name));
        } else {
          analysisParameters.outputData = value.equals("1");
        }
        break;
      case GENERATE_HEATMAP:
        if (validateGenerateHeatmap(value, errorHandler, resources)) {
          analysisParameters.geneChartParameters.generateHeatmap = value.equals("1");
        }
        break;
      case GENERATE_AGGREGATE_GRAPHS:
        if (validateGenerateAggregateGraphs(value, errorHandler, resources)) {
          analysisParameters.geneChartParameters.generateAggregateGraphs = value.equals("1");
        }
        break;
      case ONE_GRAPH_PER_GROUP:
        if (!Pattern.matches(pattern(resources, ONE_GRAPH_PER_GROUP), value)) {
          errorHandler.accept(resources.message(INVALID_ONE_REFERENCE_GROUP_PER_GRAPH, name));
        } else {
          analysisParameters.oneReferenceGroupPerGraph = value.equals("1");
        }
        break;
      case ONE_GRAPH_PER_DATASET:
        if (!Pattern.matches(pattern(resources, ONE_GRAPH_PER_DATASET), value)) {
          errorHandler.accept(resources.message(INVALID_ONE_DATA_FILE_PER_GRAPH, name));
        } else {
          analysisParameters.oneDataFilePerGraph = value.equals("1");
        }
        break;
      case ONE_GRAPH_PER_ORIENTATION:
        if (!Pattern.matches(pattern(resources, ONE_GRAPH_PER_ORIENTATION), value)) {
          errorHandler.accept(resources.message(INVALID_ONE_ORIENTATION_PER_GRAPH, name));
        } else {
          analysisParameters.oneOrientationPerGraph = value.equals("1");
        }
        break;
      case MEAN_DISPERSION_VALUE:
        if (!Pattern.matches(pattern(resources, MEAN_DISPERSION_VALUE), value)) {
          errorHandler.accept(resources.message(INVALID_DISPERSION_TYPE, name));
        } else {
          switch (value) {
            case "D":
              analysisParameters.dispersionType = DispersionType.SD;
              break;
            case "E":
              analysisParameters.dispersionType = DispersionType.SEM;
              break;
            default:
              break;
          }
        }
        break;
      case DISPLAY_DISPERSION_VALUES:
        if (validateDispersion(value, errorHandler, resources)) {
          analysisParameters.geneChartParameters.dispersion = value.equals("1");
        }
        break;
      case Y_AXIS_SCALE: {
        if (validateYaxisScale(value, errorHandler, resources)) {
          if (value.contains(";")) {
            int separatorIndex = value.indexOf(";");
            Double from = parseDouble(value.substring(0, separatorIndex), null);
            Double to = parseDouble(value.substring(separatorIndex + 1), null);
            analysisParameters.geneChartParameters.yaxisScale = new YAxisScale(from, to);
          } else {
            analysisParameters.geneChartParameters.yaxisScale = new YAxisScale(null, null);
          }
        }
        break;
      }
      default:
        // Ignored.
        break;
    }
  }

  private void validateReferencePointsLimits(ReferenceType referenceType, int referencePoints,
      MessageResources resources, Consumer<String> errorHandler) {
    if (referenceType != null && referencePoints >= 0) {
      Integer minimum = null;
      Integer maximum = null;
      switch (referenceType) {
        case ANNOTATIONS:
          minimum = 1;
          maximum = 6;
          break;
        case EXONS:
          minimum = 6;
          maximum = 6;
          break;
        case COORDINATES:
          minimum = 1;
          maximum = 6;
          break;
        default:
          break;
      }
      if (minimum == null || maximum == null) {
        throw new AssertionError("ReferenceType " + referenceType + " not covered in switch");
      }
      if (referencePoints < minimum) {
        errorHandler
            .accept(resources.message(BELOW_MINIMUM_REFERENCE_POINTS, REFERENCE_POINTS, minimum));
      } else if (referencePoints > maximum) {
        errorHandler
            .accept(resources.message(ABOVE_MAXIMUM_REFERENCE_POINTS, REFERENCE_POINTS, maximum));
      }
    }
  }

  private void validateBlockWindowCount(Integer blockWindowCount, int referencePoints,
      MessageResources resources, Consumer<String> errorHandler) {
    if (referencePoints >= 0 && blockWindowCount != null && blockWindowCount <= referencePoints) {
      errorHandler.accept(
          resources.message(BLOCK_WINDOW_COUNT_MISSMATCH, WINDOWS_PER_BLOCK, referencePoints));
    }
  }

  private void validateBlockAlignmentCount(Integer blockAlignmentCount, int referencePoints,
      MessageResources resources, Consumer<String> errorHandler) {
    if (referencePoints >= 0 && blockAlignmentCount != null
        && blockAlignmentCount <= referencePoints) {
      errorHandler.accept(
          resources.message(BLOCK_ALIGNMENT_COUNT_MISSMATCH, BLOCK_ALIGNMENT, referencePoints));
    }
  }

  private void validateBlockSplitTypeCount(Integer blockSplitTypeCount, int referencePoints,
      MessageResources resources, Consumer<String> errorHandler) {
    if (referencePoints >= 0 && blockSplitTypeCount != null
        && blockSplitTypeCount <= referencePoints) {
      errorHandler.accept(
          resources.message(BLOCK_SPLIT_TYPE_COUNT_MISSMATCH, BLOCK_SPLIT_TYPE, referencePoints));
    }
  }

  private void validateBlockSplitCount(Integer blockSplitCount, int referencePoints,
      MessageResources resources, Consumer<String> errorHandler) {
    if (referencePoints >= 0 && blockSplitCount != null && blockSplitCount <= referencePoints) {
      errorHandler.accept(
          resources.message(BLOCK_SPLIT_COUNT_MISSMATCH, BLOCK_SPLIT_VALUE, referencePoints));
    }
  }

  private void validateBlockSplitAlignmentCount(Integer blockSplitAlignmentCount,
      int referencePoints, MessageResources resources, Consumer<String> errorHandler) {
    if (referencePoints >= 0 && blockSplitAlignmentCount != null
        && blockSplitAlignmentCount <= referencePoints) {
      errorHandler.accept(resources.message(BLOCK_SPLIT_ALIGNMENT_COUNT_MISSMATCH,
          BLOCK_SPLIT_ALIGNMENT, referencePoints));
    }
  }

  private String pattern(MessageResources resources, String parameterName) {
    return resources.message(parameterName + ".expression");
  }

  /**
   * Validates generate aggregate graphs.
   *
   * @param generateAggregateGraphs
   *          generate aggregate graphs value
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   */
  public void validateGenerateAggregateGraphs(String generateAggregateGraphs, Locale locale,
      Consumer<String> errorHandler) {
    final MessageResources resources = new MessageResources(ParameterFileService.class, locale);
    validateGenerateAggregateGraphs(generateAggregateGraphs, errorHandler, resources);
  }

  private boolean validateGenerateAggregateGraphs(String generateAggregateGraphs,
      Consumer<String> errorHandler, MessageResources resources) {
    if (generateAggregateGraphs == null) {
      generateAggregateGraphs = "";
    }
    if (!Pattern.matches(pattern(resources, GENERATE_AGGREGATE_GRAPHS), generateAggregateGraphs)) {
      errorHandler
          .accept(resources.message(INVALID_GENERATE_AGGREGATE_GRAPHS, GENERATE_AGGREGATE_GRAPHS));
      return false;
    }
    return true;
  }

  /**
   * Validates generate heatmap.
   *
   * @param generateHeatmap
   *          generate heatmap value
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   */
  public void validateGenerateHeatmap(String generateHeatmap, Locale locale,
      Consumer<String> errorHandler) {
    final MessageResources resources = new MessageResources(ParameterFileService.class, locale);
    validateGenerateHeatmap(generateHeatmap, errorHandler, resources);
  }

  private boolean validateGenerateHeatmap(String generateHeatmap, Consumer<String> errorHandler,
      MessageResources resources) {
    if (generateHeatmap == null) {
      generateHeatmap = "";
    }
    if (!Pattern.matches(pattern(resources, GENERATE_HEATMAP), generateHeatmap)) {
      errorHandler.accept(resources.message(INVALID_GENERATE_HEATMAP, GENERATE_HEATMAP));
      return false;
    }
    return true;
  }

  /**
   * Validates dispersion.
   *
   * @param dispersion
   *          dispersion value
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   */
  public void validateDispersion(String dispersion, Locale locale, Consumer<String> errorHandler) {
    final MessageResources resources = new MessageResources(ParameterFileService.class, locale);
    validateDispersion(dispersion, errorHandler, resources);
  }

  private boolean validateDispersion(String dispersion, Consumer<String> errorHandler,
      MessageResources resources) {
    if (dispersion == null) {
      dispersion = "";
    }
    if (!Pattern.matches(pattern(resources, DISPLAY_DISPERSION_VALUES), dispersion)) {
      errorHandler.accept(resources.message(INVALID_DISPERSION, DISPLAY_DISPERSION_VALUES));
      return false;
    }
    return true;
  }

  /**
   * Validates y axis scale.
   *
   * @param yaxisScale
   *          y axis scale value
   * @param locale
   *          locale for error messages
   * @param errorHandler
   *          handles errors
   */
  public void validateYaxisScale(String yaxisScale, Locale locale, Consumer<String> errorHandler) {
    final MessageResources resources = new MessageResources(ParameterFileService.class, locale);
    validateYaxisScale(yaxisScale, errorHandler, resources);
  }

  private boolean validateYaxisScale(String yaxisScale, Consumer<String> errorHandler,
      MessageResources resources) {
    if (yaxisScale == null) {
      yaxisScale = "";
    }
    if (!Pattern.matches(pattern(resources, Y_AXIS_SCALE), yaxisScale)) {
      errorHandler.accept(resources.message(INVALID_YAXIS_SCALE, Y_AXIS_SCALE));
      return false;
    }
    return true;
  }

  /**
   * Parses parameter file.
   *
   * @param file
   *          file containing parameters
   * @return parsed parameters
   * @throws IOException
   *           could not read file
   */
  public AnalysisParameters parse(File file) throws IOException {
    return parseAndValidate(file, Locale.getDefault(), error -> {
    });
  }

  private File parseRelativeFolder(String parameterFileContent) throws IOException {
    class OutputFolder {
      File value;
    }

    OutputFolder outputFolder = new OutputFolder();
    ParameterHandlers handlers = new ParameterHandlers();
    handlers.parameterHandler = (name, value) -> {
      if (OUTPUT_DIRECTORY.equals(name)) {
        outputFolder.value = new File(value);
      }
    };
    parameterParser.parse(new StringReader(parameterFileContent), handlers);
    File relativeFolder = outputFolder.value;
    if (relativeFolder != null && relativeFolder.isAbsolute()) {
      return relativeFolder;
    } else {
      return null;
    }
  }

  private void updateBlockTypes(List<Block> blocks, ReferenceType referenceType,
      int referencePoints) {
    int index = 0;
    for (Block block : blocks) {
      BlockType blockType = geneChartService.regionType(index++, referenceType, referencePoints);
      block.type = blockType;
    }
  }

  private Integer parseInteger(String input, Integer defaultValue) {
    try {
      return Integer.valueOf(input);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  private Long parseLong(String input, Long defaultValue) {
    try {
      return Long.valueOf(input);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  private Double parseDouble(String input, Double defaultValue) {
    try {
      return Double.valueOf(input);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  private String parseFileAlias(String input, MessageResources resources) {
    if (input == null || input.equals("")) {
      return "";
    }
    Pattern pattern = Pattern.compile(resources.message("data.alias.expression"));
    Matcher matcher = pattern.matcher(input);
    if (matcher.matches()) {
      return matcher.group(1);
    } else {
      return "";
    }
  }

  private File parseFile(String input, File relativeDirectory, MessageResources resources) {
    if (input == null || input.equals("")) {
      return null;
    }
    Pattern pattern = Pattern.compile(resources.message("data.alias.expression"));
    Matcher matcher = pattern.matcher(input);
    if (matcher.matches()) {
      input = matcher.group(2);
    }
    String path = FilenameUtils.getFullPath(input);
    File file;
    if (path.equals("") || path.startsWith(".")) {
      file = relativeDirectory != null ? new File(relativeDirectory, input) : new File(input);
    } else {
      file = new File(input);
    }
    String normalized = FilenameUtils.normalize(file.getPath(), true);
    if (normalized != null) {
      file = new File(normalized);
    }
    return file;
  }

  /**
   * Parses generate aggregate graphs.
   *
   * @param generateAggregateGraphs
   *          generate aggregate graphs value
   * @return generate aggregate graphs
   */
  public boolean parseGenerateAggregateGraphs(String generateAggregateGraphs) {
    return generateAggregateGraphs != null && generateAggregateGraphs.equals("1");
  }

  /**
   * Parses generate heatmap.
   *
   * @param generateHeatmap
   *          generate heatmap value
   * @return generate heatmap
   */
  public boolean parseGenerateHeatmap(String generateHeatmap) {
    return generateHeatmap != null && generateHeatmap.equals("1");
  }

  /**
   * Parses dispersion.
   *
   * @param dispersion
   *          dispersion value
   * @return dispersion
   */
  public boolean parseDispersion(String dispersion) {
    return dispersion != null && dispersion.equals("1");
  }

  /**
   * Parses y axis scale.
   *
   * @param yaxisScale
   *          y axis scale value
   * @return y axis scale
   */
  public YAxisScale parseYaxisScale(String yaxisScale) {
    if (yaxisScale != null && yaxisScale.contains(";")) {
      int separatorIndex = yaxisScale.indexOf(";");
      Double from = parseDouble(yaxisScale.substring(0, separatorIndex), null);
      Double to = parseDouble(yaxisScale.substring(separatorIndex + 1), null);
      return new YAxisScale(from, to);
    } else {
      return new YAxisScale(null, null);
    }
  }

  /**
   * Exports parameters to a file.
   *
   * @param file
   *          where to export parameters
   * @param parameters
   *          parameters to export
   * @param useParameterFilenameForBatchDataFile
   *          if true, file parameter's name will be used as a base for batch data filename instead
   *          of the prefix
   * @throws IOException
   *           could not write to file
   */
  public void export(final File file, final AnalysisParameters parameters,
      boolean useParameterFilenameForBatchDataFile) throws IOException {
    if (parameters.geneChartParameters.representationType != null) {
      switch (parameters.geneChartParameters.representationType) {
        case ABSOLUTE:
          // Replace first and last block.
          if (parameters.geneChartParameters.blocks.size() > 0) {
            Block original = parameters.geneChartParameters.blocks.get(0);
            parameters.geneChartParameters.blocks.set(0, firstBlock(original.windows));
          }
          int referencePoints = parameters.geneChartParameters.referencePoints;
          if (parameters.geneChartParameters.blocks.size() > referencePoints) {
            Block original = parameters.geneChartParameters.blocks.get(referencePoints);
            parameters.geneChartParameters.blocks.set(referencePoints, lastBlock(original.windows));
          }
          break;
        case RELATIVE:
          // Replace by relative blocks.
          for (int i = 0; i < parameters.geneChartParameters.blocks.size(); i++) {
            Block original = parameters.geneChartParameters.blocks.get(i);
            parameters.geneChartParameters.blocks.set(i,
                relativeBlock(original.windows, original.type));
          }
          break;
        default:
          break;
      }
    }

    // Export parameters.
    List<String> lines = new ArrayList<>();
    try (Reader template = new InputStreamReader(
        getClass().getResourceAsStream("/parameters_template.txt"), charset)) {
      ParameterHandlers handlers = new ParameterHandlers();
      handlers.versionHandler = version -> lines.add(String.format(VERSION_EXPORT, version));
      handlers.parameterHandler =
          (name, value) -> lines.addAll(exportParameter(name, value, parameters));
      handlers.otherHandler = line -> lines.add(line);
      parameterParser.parse(template, handlers);
    }
    Files.write(file.toPath(), lines);
  }

  private List<String> exportParameter(String name, String value, AnalysisParameters parameters) {
    File outputFolder = parameters.outputFolder;
    List<String> exportValues = new ArrayList<>();
    switch (name) {
      case OUTPUT_DIRECTORY:
        exportValues.add(path(outputFolder));
        break;
      case ANALYSIS_MODE:
        if (parameters.geneChartParameters.referenceType != null) {
          switch (parameters.geneChartParameters.referenceType) {
            case ANNOTATIONS:
              exportValues.add("A");
              break;
            case EXONS:
              exportValues.add("E");
              break;
            case COORDINATES:
              exportValues.add("C");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("");
        }
        break;
      case REFGROUP_PATH: {
        for (File referenceFile : parameters.referenceFiles) {
          exportValues.add(DATA_ALIAS + relativize(outputFolder, referenceFile));
        }
        break;
      }
      case DATASET_PATH: {
        for (File dataFile : parameters.dataFiles) {
          exportValues.add(DATA_ALIAS + relativize(outputFolder, dataFile));
        }
        break;
      }
      case PROCESS_MISSING_DATA:
        exportValues
            .add(parameters.missingDataReplacement == MissingDataReplacement.ZERO ? "1" : "0");
        break;
      case PROCESS_DATA_BY_CHUNK:
        exportValues.add(parameters.processDataByChunk ? "1" : "0");
        break;
      case DATASET_CHUNK_SIZE:
        exportValues.add(String.valueOf(parameters.dataChunkSize));
        break;
      case ANNOTATIONS_PATH:
        if (parameters.geneChartParameters.referenceType != ReferenceType.COORDINATES
            && parameters.genomeAnnotations != null) {
          exportValues.add(relativize(outputFolder, parameters.genomeAnnotations));
        } else {
          exportValues.add("");
        }
        break;
      case SELECTION_PATH:
        if (parameters.selectionAnnotationFilter != null) {
          exportValues.add(relativize(outputFolder, parameters.selectionAnnotationFilter));
        } else {
          exportValues.add("");
        }
        break;
      case EXCLUSION_PATH:
        if (parameters.exclusionAnnotationFilter != null) {
          exportValues.add(relativize(outputFolder, parameters.exclusionAnnotationFilter));
        } else {
          exportValues.add("");
        }
        break;
      case ANNOTATION_COORDINATES_TYPE:
        if (parameters.geneChartParameters.referenceType != ReferenceType.ANNOTATIONS) {
          exportValues.add("N");
        } else if (parameters.coordinatesType != null) {
          switch (parameters.coordinatesType) {
            case TX:
              exportValues.add("T");
              break;
            case CDS:
              exportValues.add("C");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("T");
        }
        break;
      case MERGE_MID_INTRONS:
        if (parameters.mergeMiddleIntron != null) {
          switch (parameters.mergeMiddleIntron) {
            case FIRST:
              exportValues.add("F");
              break;
            case LAST:
              exportValues.add("L");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("N");
        }
        break;
      case ANALYSIS_METHOD:
        if (parameters.geneChartParameters.representationType != null) {
          switch (parameters.geneChartParameters.representationType) {
            case ABSOLUTE:
              exportValues.add("A");
              break;
            case RELATIVE:
              exportValues.add("R");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("");
        }
        break;
      case REFERENCE_POINTS:
        exportValues.add(String.valueOf(parameters.geneChartParameters.referencePoints));
        break;
      case FIRST_PT_BOUNDARY:
        if (parameters.geneChartParameters.referenceType != ReferenceType.ANNOTATIONS
            || parameters.geneChartParameters.referencePoints > 1) {
          exportValues.add("N");
        } else if (parameters.geneChartParameters.boundary != null) {
          switch (parameters.geneChartParameters.boundary) {
            case FIVE_PRIME:
              exportValues.add("5");
              break;
            case THREE_PRIME:
              exportValues.add("3");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("N");
        }
        break;
      case WINDOW_SIZE:
        exportValues.add(String.valueOf(parameters.geneChartParameters.windowSize));
        break;
      case WINDOWS_PER_BLOCK: {
        StringBuilder builder = new StringBuilder();
        if (parameters.geneChartParameters.blocks != null) {
          for (int i = 0; i < parameters.geneChartParameters.blocks.size()
              && i <= parameters.geneChartParameters.referencePoints; i++) {
            Block block = parameters.geneChartParameters.blocks.get(i);
            builder.append(defaultString(block.windows, ""));
            builder.append(";");
          }
        }
        exportValues.add(builder.toString());
        break;
      }
      case BLOCK_ALIGNMENT: {
        StringBuilder builder = new StringBuilder();
        if (parameters.geneChartParameters.blocks != null) {
          for (int i = 0; i < parameters.geneChartParameters.blocks.size()
              && i <= parameters.geneChartParameters.referencePoints; i++) {
            Block block = parameters.geneChartParameters.blocks.get(i);
            if (block.alignment != null) {
              switch (block.alignment) {
                case SPLIT:
                  builder.append("S");
                  break;
                case LEFT:
                  builder.append("L");
                  break;
                case RIGHT:
                  builder.append("R");
                  break;
                default:
                  break;
              }
            }
            builder.append(";");
          }
          if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
          }
        }
        exportValues.add(builder.toString());
        break;
      }
      case BLOCK_SPLIT_TYPE: {
        StringBuilder builder = new StringBuilder();
        if (parameters.geneChartParameters.blocks != null) {
          for (int i = 0; i < parameters.geneChartParameters.blocks.size()
              && i <= parameters.geneChartParameters.referencePoints; i++) {
            Block block = parameters.geneChartParameters.blocks.get(i);
            if (block.splitType != null) {
              switch (block.splitType) {
                case ABSOLUTE:
                  builder.append("A");
                  break;
                case PERCENTAGE:
                  builder.append("P");
                  break;
                default:
                  break;
              }
            } else {
              builder.append("N");
            }
            builder.append(";");
          }
          if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
          }
        }
        exportValues.add(builder.toString());
        break;
      }
      case BLOCK_SPLIT_ALIGNMENT: {
        StringBuilder builder = new StringBuilder();
        if (parameters.geneChartParameters.blocks != null) {
          for (int i = 0; i < parameters.geneChartParameters.blocks.size()
              && i <= parameters.geneChartParameters.referencePoints; i++) {
            Block block = parameters.geneChartParameters.blocks.get(i);
            if (block.splitAlignment != null) {
              switch (block.splitAlignment) {
                case LEFT:
                  builder.append("L");
                  break;
                case RIGHT:
                  builder.append("R");
                  break;
                default:
                  break;
              }
            } else {
              builder.append("N");
            }
            builder.append(";");
          }
          if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
          }
        }
        exportValues.add(builder.toString());
        break;
      }
      case BLOCK_SPLIT_VALUE: {
        StringBuilder builder = new StringBuilder();
        if (parameters.geneChartParameters.blocks != null) {
          for (int i = 0; i < parameters.geneChartParameters.blocks.size()
              && i <= parameters.geneChartParameters.referencePoints; i++) {
            Block block = parameters.geneChartParameters.blocks.get(i);
            builder.append(defaultString(block.split, "0"));
            builder.append(";");
          }
          if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
          }
        }
        exportValues.add(builder.toString());
        break;
      }
      case SMOOTHING_WINDOWS:
        exportValues.add(String.valueOf(parameters.smoothDataWindows));
        break;
      case PREFIX_FILENAME:
        exportValues.add(defaultString(parameters.ouputFilePrefix, ""));
        break;
      case AGGREGATE_DATA_TYPE:
        if (parameters.aggregateValueType != null) {
          switch (parameters.aggregateValueType) {
            case MEAN:
              exportValues.add("E");
              break;
            case MEDIAN:
              exportValues.add("D");
              break;
            case MAX:
              exportValues.add("A");
              break;
            case MIN:
              exportValues.add("I");
              break;
            default:
              break;
          }
        } else {
          exportValues.add("");
        }
        break;
      case ORIENTATION_SUBGROUPS: {
        if (parameters.outputGraphs != null) {
          StringBuilder builder = new StringBuilder();
          builder.append(parameters.outputGraphs.contains(Graph.ANY_ANY) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.ANY_CONV) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.DIV_ANY) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.ANY_TAND) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.TAND_ANY) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.TAND_TAND) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.TAND_CONV) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.DIV_TAND) ? "1" : "0");
          builder.append(";");
          builder.append(parameters.outputGraphs.contains(Graph.DIV_CONV) ? "1" : "0");
          exportValues.add(builder.toString());
        } else {
          exportValues.add("0;0;0;0;0;0;0;0;0");
        }
        break;
      }
      case WRITE_INDIVIDUAL_REFERENCES:
        exportValues.add(parameters.outputData ? "1" : "0");
        break;
      case GENERATE_HEATMAP:
        exportValues.add(parameters.geneChartParameters.generateHeatmap ? "1" : "0");
        break;
      case GENERATE_AGGREGATE_GRAPHS:
        exportValues.add(parameters.geneChartParameters.generateAggregateGraphs ? "1" : "0");
        break;
      case ONE_GRAPH_PER_GROUP:
        exportValues.add(parameters.oneReferenceGroupPerGraph ? "1" : "0");
        break;
      case ONE_GRAPH_PER_DATASET:
        exportValues.add(parameters.oneDataFilePerGraph ? "1" : "0");
        break;
      case ONE_GRAPH_PER_ORIENTATION:
        exportValues.add(parameters.oneOrientationPerGraph ? "1" : "0");
        break;
      case MEAN_DISPERSION_VALUE:
        exportValues.add(parameters.dispersionType == DispersionType.SD ? "D" : "E");
        break;
      case DISPLAY_DISPERSION_VALUES:
        if (parameters.aggregateValueType == AggregateValueType.MEAN) {
          exportValues.add(parameters.geneChartParameters.dispersion ? "1" : "0");
        } else {
          exportValues.add("0");
        }
        break;
      case Y_AXIS_SCALE:
        if (parameters.geneChartParameters.yaxisScale != null) {
          StringBuilder builder = new StringBuilder();
          builder.append(defaultString(parameters.geneChartParameters.yaxisScale.from, ""));
          builder.append(";");
          builder.append(defaultString(parameters.geneChartParameters.yaxisScale.to, ""));
          exportValues.add(builder.toString());
        } else {
          exportValues.add(";");
        }
        break;
      default:
        exportValues.add(value);
        break;
    }
    return exportValues.stream().map(v -> String.format(PARAMETER_EXPORT, name, v))
        .collect(Collectors.toList());
  }

  private String path(File file) {
    if (file != null) {
      return file.getPath();
    } else {
      return "";
    }
  }

  private String defaultString(Object object, String defaultValue) {
    if (object == null) {
      return defaultValue;
    } else {
      return String.valueOf(object);
    }
  }

  private String relativize(File source, File other) {
    if (source.isAbsolute() && other.isAbsolute()) {
      try {
        return source.toPath().relativize(other.toPath()).toString();
      } catch (IllegalArgumentException e) {
        return other.getPath();
      }
    } else {
      return other.getPath();
    }
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.version;

import static org.labjacquespe.vap.core.service.ParameterFileService.PARAMETER_EXPORT;
import static org.labjacquespe.vap.core.service.ParameterFileService.VERSION_EXPORT;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.labjacquespe.vap.core.service.ParameterParser;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;

/**
 * Converts parameter files from 1.0. to 1.1.0.
 */
public class ParameterFileConverter100To110 implements ParameterFileConverter {
  private final ArtifactVersion from = new DefaultArtifactVersion("1.0.0");
  private final ArtifactVersion to = new DefaultArtifactVersion("1.1.0");
  private final ParameterParser parser;

  public ParameterFileConverter100To110(ParameterParser parser) {
    this.parser = parser;
  }

  @Override
  public void updateParameters(Reader input, Writer output, final File relativeDirectory)
      throws IOException {
    List<String> lines = new ArrayList<>();
    ParameterHandlers handlers = new ParameterHandlers();
    handlers.versionHandler = version -> lines.add(String.format(VERSION_EXPORT, "1.1.0"));
    handlers.parameterHandler = (name, value) -> {
      lines.add(String.format(PARAMETER_EXPORT, name, value));
      if (name.equals("write_individual_references")) {
        lines.add(String.format(PARAMETER_EXPORT, "generate_heatmaps", "0"));
      }
      if (name.equals("mean_dispersion_value")) {
        lines.add(String.format(PARAMETER_EXPORT, "process_missing_data", "0"));
      }
      if (name.equals("display_dispersion_values")) {
        lines.add(String.format(PARAMETER_EXPORT, "generate_aggregate_graphs", "1"));
      }
    };
    handlers.otherHandler = (line) -> lines.add(line);
    parser.parse(input, handlers);

    try (Writer writer = new BufferedWriter(output)) {
      for (String line : lines) {
        writer.write(line);
        writer.write(System.lineSeparator());
      }
    }
  }

  @Override
  public ArtifactVersion from() {
    return from;
  }

  @Override
  public ArtifactVersion to() {
    return to;
  }
}

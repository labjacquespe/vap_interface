/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.version;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.labjacquespe.vap.core.service.ParameterParser;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;
import org.springframework.stereotype.Component;

/**
 * Services that handles old versions of APT files.
 */
@Component
public class ParameterFileVersionService {
  @Inject
  private Set<ParameterFileConverter> converters;
  @Inject
  private ParameterParser parser;
  private Comparator<ParameterFileConverter> converterComparator =
      (o1, o2) -> o1.to().compareTo(o2.to());

  protected ParameterFileVersionService(Set<ParameterFileConverter> converters,
      ParameterParser parser) {
    this.converters = converters;
    this.parser = parser;
  }

  /**
   * Updates parameters in APT format to current version.
   *
   * @param input
   *          parameters in APT format
   * @param output
   *          where to write parameters in APT format updated to current version
   * @param relativeDirectory
   *          directory used to resolve relative paths
   * @throws IOException
   *           could not update APT
   */
  public void updateParameters(Reader input, Writer output, File relativeDirectory)
      throws IOException {
    StringWriter memoryOutput = new StringWriter();
    IOUtils.copy(input, memoryOutput);
    ArtifactVersion version = parseVersion(new StringReader(memoryOutput.toString()));

    if (version != null) {
      List<ParameterFileConverter> convertersToApply = convertersToApply(version);
      // Apply converters.
      Reader updated = new StringReader(memoryOutput.toString());
      for (ParameterFileConverter converter : convertersToApply) {
        StringWriter tempOutput = new StringWriter();
        converter.updateParameters(updated, tempOutput, relativeDirectory);
        updated = new StringReader(tempOutput.toString());
      }
      IOUtils.copy(updated, output);
    } else {
      // Cannot update since no version could be parsed.
      IOUtils.copy(new StringReader(memoryOutput.toString()), output);
    }
  }

  private ArtifactVersion parseVersion(Reader input) throws IOException {
    class VersionContainer {
      private String version;
    }

    ParameterHandlers handlers = new ParameterHandlers();
    VersionContainer versionProperty = new VersionContainer();
    handlers.versionHandler = version -> versionProperty.version = version;
    parser.parse(input, handlers);
    if (versionProperty.version != null) {
      return new DefaultArtifactVersion(versionProperty.version);
    } else {
      return null;
    }
  }

  private List<ParameterFileConverter> convertersToApply(ArtifactVersion version)
      throws IOException {
    List<ParameterFileConverter> convertersToApply = new ArrayList<>();
    for (ParameterFileConverter converter : converters) {
      if (converter.to().compareTo(version) > 0) {
        convertersToApply.add(converter);
      }
    }
    Collections.sort(convertersToApply, converterComparator);
    return convertersToApply;
  }
}

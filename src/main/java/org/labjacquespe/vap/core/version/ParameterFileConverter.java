/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.version;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import org.apache.maven.artifact.versioning.ArtifactVersion;

/**
 * Converts text parameters from one version to another.
 */
public interface ParameterFileConverter {
  /**
   * Updates parameters in APT format from one version to another.
   *
   * @param input
   *          parameters matching version {@link #from()}
   * @param output
   *          where parameters version {@link #to()} are written
   * @param relativeDirectory
   *          parent directory of all relative paths
   * @throws IOException
   *           could not update parameters
   */
  public void updateParameters(Reader input, Writer output, File relativeDirectory)
      throws IOException;

  /**
   * Returns input version.
   *
   * @return input version
   */
  public ArtifactVersion from();

  /**
   * Returns output version.
   *
   * @return output version
   */
  public ArtifactVersion to();
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import java.io.File;
import javafx.concurrent.Task;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.progressbar.JavafxProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Task that analyse genes.
 */
public class AnalysisTask extends Task<Void> {
  private final Logger logger = LoggerFactory.getLogger(AnalysisTask.class);
  private final AnalysisService analysisService;
  private final AnalysisChartService analysisChartService;
  private final AnalysisParameters parameters;

  protected AnalysisTask(AnalysisService analysisService, AnalysisChartService analysisChartService,
      AnalysisParameters parameters) {
    this.analysisService = analysisService;
    this.analysisChartService = analysisChartService;
    this.parameters = parameters;
  }

  @Override
  protected Void call() throws Exception {
    try {
      addLogAppender(parameters.outputFolder);
      JavafxProgressBar progressBar = new JavafxProgressBar();
      progressBar.title().addListener((ov, oldValue, newValue) -> updateTitle(newValue));
      progressBar.message().addListener((ov, oldValue, newValue) -> updateMessage(newValue));
      progressBar.progress()
          .addListener((ov, oldValue, newValue) -> updateProgress(newValue.doubleValue(),
              Math.max(1.0, newValue.doubleValue())));
      analysisService.mapGenes(parameters, progressBar.step(0.5));
      updateMessage("");
      File chartMap =
          analysisChartService.chartMap(parameters.outputFolder, parameters.ouputFilePrefix);
      File heatmapList =
          analysisChartService.heatmapList(parameters.outputFolder, parameters.ouputFilePrefix);
      GeneChartParameters geneChartParameters = parameters.geneChartParameters;
      analysisChartService.createCharts(chartMap, heatmapList, geneChartParameters,
          progressBar.step(0.5));
      return null;
    } catch (Exception e) {
      logger.error("Exception while analysing data", e);
      throw e;
    } finally {
      removeLogAppender();
    }
  }

  private void addLogAppender(File outputFolder) {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
    FileAppender<ILoggingEvent> sourceAppender =
        (FileAppender<ILoggingEvent>) rootLogger.getAppender("FILE");
    PatternLayoutEncoder sourceEncoder = (PatternLayoutEncoder) sourceAppender.getEncoder();
    PatternLayoutEncoder encoder = new PatternLayoutEncoder();
    encoder.setPattern(sourceEncoder.getPattern());
    encoder.setContext(context);
    encoder.start();
    FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
    fileAppender.setName(appenderName());
    fileAppender.setFile(outputFolder.getPath() + "/VAP_interface_logfile.log");
    fileAppender.setEncoder(encoder);
    fileAppender.setContext(context);
    fileAppender.start();
    rootLogger.addAppender(fileAppender);
  }

  private void removeLogAppender() {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    ch.qos.logback.classic.Logger rootLogger = context.getLogger(Logger.ROOT_LOGGER_NAME);
    rootLogger.detachAppender(appenderName());
  }

  private String appenderName() {
    return getClass().getName() + "-" + Thread.currentThread().getName();
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.File;
import org.labjacquespe.vap.OperatingSystem;

/**
 * VAP core configuration.
 */
public interface VapCoreConfiguration {
  /**
   * Returns pre-installed VAP core filename.
   *
   * @param operatingSystem
   *          operating system
   * @return pre-installed VAP core filename
   */
  public File nativeVap(OperatingSystem operatingSystem);

  /**
   * Returns VAP core executable resource.
   *
   * @param operatingSystem
   *          operating system
   * @return VAP core executable resource
   */
  public String resource(OperatingSystem operatingSystem);

  /**
   * Returns where to copy VAP core executable resource.
   *
   * @param operatingSystem
   *          operating system
   * @return where to copy VAP core executable resource
   */
  public File output(OperatingSystem operatingSystem);
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import org.labjacquespe.vap.chart.BlockType;

/**
 * Simple implementation of {@link Block}.
 */
public class Block {
  public Long windows;
  public BlockAlignment alignment;
  public SplitType splitType;
  public Long split;
  public SplitAlignment splitAlignment;
  public BlockType type;

  public Block() {
  }

  /**
   * Creates a block.
   * 
   * @param windows
   *          number of windows
   * @param alignment
   *          block alignment
   * @param splitType
   *          split type
   * @param split
   *          split length
   * @param splitAlignment
   *          split alignment
   * @param type
   *          block type
   */
  public Block(Long windows, BlockAlignment alignment, SplitType splitType, Long split,
      SplitAlignment splitAlignment, BlockType type) {
    this.windows = windows;
    this.alignment = alignment;
    this.splitType = splitType;
    this.split = split;
    this.splitAlignment = splitAlignment;
    this.type = type;
  }

  public static Block firstBlock(Long windows) {
    return new Block(windows, BlockAlignment.RIGHT, null, null, null, BlockType.UPSTREAM);
  }

  public static Block lastBlock(Long windows) {
    return new Block(windows, BlockAlignment.LEFT, null, null, null, BlockType.DOWNSTREAM);
  }

  public static Block relativeBlock(Long windows, BlockType type) {
    return new Block(windows, BlockAlignment.LEFT, SplitType.PERCENTAGE, 10L, SplitAlignment.LEFT,
        type);
  }

  @Override
  public String toString() {
    return "Block[windows=" + windows + ", alignment=" + alignment + ", splitType=" + splitType
        + ", split=" + split + ", splitAlignment=" + splitAlignment + ", type=" + type + "]";
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.labjacquespe.vap.OperatingSystem;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = VapCoreConfigurationSpringBoot.PREFIX)
public class VapCoreConfigurationSpringBoot implements VapCoreConfiguration {
  public static final String PREFIX = "vap.core";
  private Map<String, String> nativeVap = new HashMap<>();
  private Map<String, String> resources = new HashMap<>();
  private Map<String, String> resourcesOutput = new HashMap<>();

  @Override
  public File nativeVap(OperatingSystem operatingSystem) {
    return new File(nativeVap.get(operatingSystem.name()));
  }

  @Override
  public String resource(OperatingSystem operatingSystem) {
    if (operatingSystem == OperatingSystem.OTHER) {
      return null;
    }
    return resources.get(operatingSystem.name());
  }

  @Override
  public File output(OperatingSystem operatingSystem) {
    if (operatingSystem == OperatingSystem.OTHER) {
      return null;
    }
    return new File(resourcesOutput.get(operatingSystem.name()));
  }

  public Map<String, String> getNative() {
    return nativeVap;
  }

  public Map<String, String> getResources() {
    return resources;
  }

  public Map<String, String> getResourcesOutput() {
    return resourcesOutput;
  }
}

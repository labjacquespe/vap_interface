/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.labjacquespe.vap.chart.GeneChartParameters;

/**
 * Complete analysis program's parameters.
 */
public class AnalysisParameters {
  public static final boolean DEFAULT_PROCESS_DATA_BY_CHUNK = false;
  public static final int DEFAULT_DATA_CHUNK = 10000000;
  public static final CoordinatesType DEFAULT_COORDINATES_TYPE = CoordinatesType.TX;
  public static final MergeMiddleIntron DEFAULT_MERGE_MIDDLE_INTRON = null;
  public static final int DEFAULT_SMOOTH_DATA_WINDOWS = 6;
  public static final AggregateValueType DEFAULT_AGGREGATE_VALUE_TYPE = AggregateValueType.MEAN;
  public static final MissingDataReplacement DEFAULT_MISSING_DATA_REPLACEMENT =
      MissingDataReplacement.ZERO;
  public static final Collection<Graph> DEFAULT_OUTPUT_GRAPHS =
      Collections.unmodifiableCollection(Arrays.asList(Graph.ANY_ANY));
  public static final DispersionType DEFAULT_DISPERSION_TYPE = DispersionType.SEM;
  public static final boolean DEFAULT_ONE_DATA_FILE_PER_GRAPH = true;
  public static final boolean DEFAULT_ONE_REFERENCE_GROUP_PER_GRAPH = false;
  public static final boolean DEFAULT_ONE_ORIENTATION_PER_GRAPH = true;
  public static final boolean DEFAULT_OUTPUT_DATA = false;
  public File outputFolder;
  public List<File> dataFiles;
  public List<File> referenceFiles;
  public boolean processDataByChunk = DEFAULT_PROCESS_DATA_BY_CHUNK;
  public int dataChunkSize = DEFAULT_DATA_CHUNK;
  public File genomeAnnotations;
  public File selectionAnnotationFilter;
  public File exclusionAnnotationFilter;
  public CoordinatesType coordinatesType = DEFAULT_COORDINATES_TYPE;
  public MergeMiddleIntron mergeMiddleIntron = DEFAULT_MERGE_MIDDLE_INTRON;
  public int smoothDataWindows = DEFAULT_SMOOTH_DATA_WINDOWS;
  public AggregateValueType aggregateValueType = DEFAULT_AGGREGATE_VALUE_TYPE;
  public MissingDataReplacement missingDataReplacement = DEFAULT_MISSING_DATA_REPLACEMENT;
  public String ouputFilePrefix;
  public Collection<Graph> outputGraphs = DEFAULT_OUTPUT_GRAPHS;
  public DispersionType dispersionType = DEFAULT_DISPERSION_TYPE;
  public boolean oneDataFilePerGraph = DEFAULT_ONE_DATA_FILE_PER_GRAPH;
  public boolean oneReferenceGroupPerGraph = DEFAULT_ONE_REFERENCE_GROUP_PER_GRAPH;
  public boolean oneOrientationPerGraph = DEFAULT_ONE_ORIENTATION_PER_GRAPH;
  public boolean outputData = DEFAULT_OUTPUT_DATA;
  public GeneChartParameters geneChartParameters = new GeneChartParameters();
  public boolean testData;
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.labjacquespe.vap.chart.CreateGraphFilesType.LIST_FILES;
import static org.labjacquespe.vap.chart.CreateGraphFilesType.PARAMETER_FILE;
import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_REFERENCE_POINTS;
import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_WINDOW_SIZE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.Writer;
import java.nio.file.Files;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.CommandLineParameterService.ParsedGeneChartArguments;
import org.labjacquespe.vap.chart.ChartGroup;
import org.labjacquespe.vap.chart.CreateGraphFiles;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.service.AnalysisChartService;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class CommandLineParameterServiceTest {
  private CommandLineParameterService commandLineParameterService;
  @Mock
  private Consumer<String> errorHandler;
  @Mock
  private ParameterFileService parameterFileService;
  @Mock
  private AnalysisChartService analysisChartService;
  @Mock
  private List<ChartGroup> chartGroups;
  @Mock
  private YAxisScale yaxisScale;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private AnalysisParameters analysisParameters;
  private Locale locale;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    commandLineParameterService =
        new CommandLineParameterService(parameterFileService, analysisChartService);
    analysisParameters = new AnalysisParameters();
    locale = Locale.getDefault();
  }

  @Test
  public void validateGeneChartArguments_InvalidArgumentFirst() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "abc", "create_graphs", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_InvalidArgumentSecond() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "abc", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingCreateGraphs() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(new String[] { "-la", map.toString() },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_CreateGraphsNotFirst() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "-gh", "1", "create_graphs", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingFileType() throws Throwable {
    commandLineParameterService.validateGeneChartArguments(new String[] { "create_graphs" }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParameterAndMapFile() throws Throwable {
    File parameter = temporaryFolder.newFile("parameter.txt");
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", parameter.toString(), "-la", map.toString() }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParameterAndHeatmapListFile() throws Throwable {
    File parameter = temporaryFolder.newFile("parameter.txt");
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", parameter.toString(), "-lh", heatmapList.toString() },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MapAndHeatmapListFile() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-lh", heatmapList.toString() },
        locale, errorHandler);

    verify(errorHandler, never()).accept(errorCaptor.capture());
  }

  @Test
  public void validateGeneChartArguments_ParameterAndMapAndHeatmapListFile() throws Throwable {
    File parameter = temporaryFolder.newFile("parameter.txt");
    File map = temporaryFolder.newFile("map.txt");
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");

    commandLineParameterService.validateGeneChartArguments(new String[] { "create_graphs", "-p",
        parameter.toString(), "-la", map.toString(), "-lh", heatmapList.toString() }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_Parameters() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    File map = temporaryFolder.newFile("map.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService).chartMap(temporaryFolder.getRoot(), "test");
    verify(analysisChartService).validateChartMap(eq(map), eq(locale), any());
    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MultipleParameterFiles() throws Throwable {
    File parameter1 = temporaryFolder.newFile("parameter1.txt");
    File parameter2 = temporaryFolder.newFile("parameter2.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", parameter1.toString(), "-p", parameter2.toString() },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingParameterFile() throws Throwable {
    commandLineParameterService.validateGeneChartArguments(new String[] { "create_graphs", "-p" },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParameterFileNotExists() throws Throwable {
    File file = new File(temporaryFolder.getRoot(), "test.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_ParametersError() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    final String parameterError = "parameter_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(parameterError);
        return null;
      }
    }).when(parameterFileService).validate(any(), any(), any());

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParametersNoMap() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    File map = new File(temporaryFolder.getRoot(), "map.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService).chartMap(temporaryFolder.getRoot(), "test");
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParametersNoHeatmapList() throws Throwable {
    final File file = temporaryFolder.newFile("test.txt");
    final File map = temporaryFolder.newFile("map.txt");
    final File heatmapList = new File(temporaryFolder.getRoot(), "heatmapList.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    when(analysisChartService.heatmapList(any(), any())).thenReturn(heatmapList);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString(), "-gh", "1" }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService).chartMap(temporaryFolder.getRoot(), "test");
    verify(analysisChartService).heatmapList(temporaryFolder.getRoot(), "test");
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_ParametersMapError() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    File map = temporaryFolder.newFile("map.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";
    final String mapError = "map_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(mapError);
        return null;
      }
    }).when(analysisChartService).validateChartMap(any(), any(), any());

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService).chartMap(temporaryFolder.getRoot(), "test");
    verify(analysisChartService).validateChartMap(eq(map), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParametersMapErrorNoGenerateAggregateGraphs()
      throws Throwable {
    final File file = temporaryFolder.newFile("test.txt");
    final File map = temporaryFolder.newFile("map.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(parameterFileService.parseGenerateAggregateGraphs(any(String.class))).thenReturn(false);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString(), "-gag", "0" }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService, never()).validateChartMap(eq(map), eq(locale), any());
    verify(errorHandler, never()).accept(errorCaptor.capture());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_ParametersHeatmapListError() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    final File map = temporaryFolder.newFile("map.txt");
    final File heatmapList = temporaryFolder.newFile("heatmapList.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    when(analysisChartService.heatmapList(any(), any())).thenReturn(heatmapList);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";
    final String mapError = "map_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(mapError);
        return null;
      }
    }).when(analysisChartService).validateHeatmapList(any(), any(), any());

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString(), "-gh", "1" }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService).chartMap(temporaryFolder.getRoot(), "test");
    verify(analysisChartService).heatmapList(temporaryFolder.getRoot(), "test");
    verify(analysisChartService).validateHeatmapList(eq(heatmapList), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_ParametersHeatmapListErrorNoGenerateHeatmap()
      throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    final File map = temporaryFolder.newFile("map.txt");
    final File heatmapList = temporaryFolder.newFile("heatmapList.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(false);
    when(analysisChartService.chartMap(any(), any())).thenReturn(map);
    when(analysisChartService.heatmapList(any(), any())).thenReturn(heatmapList);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-p", file.toString(), "-gh", "0" }, locale, errorHandler);

    verify(parameterFileService).validate(eq(file), eq(locale), any());
    verify(parameterFileService).parse(eq(file));
    verify(analysisChartService, never()).validateHeatmapList(eq(heatmapList), eq(locale), any());
    verify(errorHandler, never()).accept(errorCaptor.capture());
  }

  @Test
  public void validateGeneChartArguments_Map() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("#Comment line");
      writer.write("\n");
      writer.write("group\tdata");
    }

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", file.toString() }, locale, errorHandler);

    verify(analysisChartService).validateChartMap(eq(file), eq(locale), any());
    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MultipleMapFile() throws Throwable {
    File map1 = temporaryFolder.newFile("map1.txt");
    File map2 = temporaryFolder.newFile("map2.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map1.toString(), "-la", map2.toString() }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingMapFile() throws Throwable {
    commandLineParameterService.validateGeneChartArguments(new String[] { "create_graphs", "-la" },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MapFileNotExists() throws Throwable {
    File file = new File(temporaryFolder.getRoot(), "test.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", file.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_MapError() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("#Comment line");
      writer.write("\n");
      writer.write("group\tdata");
    }
    final String mapError = "map_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(mapError);
        return null;
      }
    }).when(analysisChartService).validateChartMap(any(), any(), any());

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", file.toString() }, locale, errorHandler);

    verify(analysisChartService).validateChartMap(eq(file), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MapErrorNoGenerateAggregateGraphs() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("#Comment line");
      writer.write("\n");
      writer.write("group\tdata");
    }

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", file.toString(), "-gag", "0" }, locale,
        errorHandler);

    verify(analysisChartService, never()).validateChartMap(eq(file), eq(locale), any());
    verify(errorHandler, never()).accept(errorCaptor.capture());
  }

  @Test
  public void validateGeneChartArguments_HeatmapList() throws Throwable {
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-lh", heatmapList.toString(), "-gh", "1" }, locale,
        errorHandler);

    verify(analysisChartService).validateHeatmapList(eq(heatmapList), eq(locale), any());
    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MultipleHeatmapListFile() throws Throwable {
    File heatmapList1 = temporaryFolder.newFile("heatmapList1.txt");
    File heatmapList2 = temporaryFolder.newFile("heatmapList2.txt");
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    commandLineParameterService.validateGeneChartArguments(new String[] { "create_graphs", "-lh",
        heatmapList1.toString(), "-lh", heatmapList2.toString(), "-gh", "1" }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingHeatmapListFile() throws Throwable {
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-lh", "-gh", "1" }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_HeatmapListFileNotExists() throws Throwable {
    File heatmapList = new File(temporaryFolder.getRoot(), "heatmapList.txt");
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-lh", heatmapList.toString(), "-gh", "1" }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_HeatmapListError() throws Throwable {
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");
    final String mapError = "map_error";
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(mapError);
        return null;
      }
    }).when(analysisChartService).validateHeatmapList(any(), any(), any());

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-lh", heatmapList.toString(), "-gh", "1" }, locale,
        errorHandler);

    verify(analysisChartService).validateHeatmapList(eq(heatmapList), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_HeatmapListErrorNoGenerateHeatmap() throws Throwable {
    File heatmapList = temporaryFolder.newFile("heatmapList.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-lh", heatmapList.toString(), "-gh", "0" }, locale,
        errorHandler);

    verify(analysisChartService, never()).validateHeatmapList(eq(heatmapList), eq(locale), any());
    verify(errorHandler, never()).accept(errorCaptor.capture());
  }

  @Test
  public void validateGeneChartArguments_MissingGenerateHeatmap() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MissingGenerateHeatmapValue() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-gh" }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_InvalidGenerateHeatmap() throws Throwable {
    final String parameterError = "parameter_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(parameterError);
        return null;
      }
    }).when(parameterFileService).validateGenerateHeatmap(any(String.class), any(Locale.class),
        any());
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-gh", "a" }, locale, errorHandler);

    verify(parameterFileService).validateGenerateHeatmap(eq("a"), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingDispersion() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MissingDispersionValue() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-ddv" }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_InvalidDispersion() throws Throwable {
    final String parameterError = "parameter_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(parameterError);
        return null;
      }
    }).when(parameterFileService).validateDispersion(any(String.class), any(Locale.class), any());
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-ddv", "a" }, locale, errorHandler);

    verify(parameterFileService).validateDispersion(eq("a"), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateGeneChartArguments_MissingYaxisScale() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGeneChartArguments_MissingYaxisScaleValue() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-yas" }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateGeneChartArguments_InvalidYaxisScale() throws Throwable {
    final String parameterError = "parameter_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(parameterError);
        return null;
      }
    }).when(parameterFileService).validateYaxisScale(any(String.class), any(Locale.class), any());
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateGeneChartArguments(
        new String[] { "create_graphs", "-la", map.toString(), "-yas", "-20;D" }, locale,
        errorHandler);

    verify(parameterFileService).validateYaxisScale(eq("-20;D"), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void parseGeneChartArguments_ParameterFile() throws Throwable {
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.ouputFilePrefix = "test";
    analysisParameters.geneChartParameters = new GeneChartParameters();
    analysisParameters.geneChartParameters.dispersion = true;
    YAxisScale yas = new YAxisScale(-1.0, 2.0);
    analysisParameters.geneChartParameters.yaxisScale = yas;
    File parameter = temporaryFolder.newFile("test.txt");

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-p", parameter.toString() });

    verify(parameterFileService).parse(eq(parameter));
    CreateGraphFiles files = parsedArguments.getFiles();
    assertEquals(PARAMETER_FILE, files.getType());
    assertEquals(parameter, files.getParameterFile());
    assertEquals(null, files.getChartMapFile());
    assertEquals(null, files.getListHeatmapFile());
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(analysisParameters.geneChartParameters.referenceType, parameters.referenceType);
    assertEquals(analysisParameters.geneChartParameters.representationType,
        parameters.representationType);
    assertEquals(analysisParameters.geneChartParameters.referencePoints,
        parameters.referencePoints);
    assertEquals(analysisParameters.geneChartParameters.boundary, parameters.boundary);
    assertEquals(analysisParameters.geneChartParameters.windowSize, parameters.windowSize);
    assertEquals(analysisParameters.geneChartParameters.blocks, parameters.blocks);
    assertEquals(true, parameters.dispersion);
    assertEquals(yas, parameters.yaxisScale);
  }

  @Test
  public void parseGeneChartArguments_MapFile() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("group1\tdata1.txt");
    }

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-la", file.toString() });

    CreateGraphFiles files = parsedArguments.getFiles();
    assertEquals(LIST_FILES, files.getType());
    assertEquals(null, files.getParameterFile());
    assertEquals(file, files.getChartMapFile());
    assertEquals(null, files.getListHeatmapFile());
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(null, parameters.referenceType);
    assertEquals(null, parameters.representationType);
    assertEquals(DEFAULT_REFERENCE_POINTS, parameters.referencePoints);
    assertEquals(null, parameters.boundary);
    assertEquals(DEFAULT_WINDOW_SIZE, parameters.windowSize);
    assertEquals(null, parameters.blocks);
    assertEquals(false, parameters.dispersion);
    if (parameters.yaxisScale != null) {
      assertEquals(null, parameters.yaxisScale.from);
      assertEquals(null, parameters.yaxisScale.to);
    }
  }

  @Test
  public void parseGeneChartArguments_HeatmapFile() throws Throwable {
    File heatmapFile = temporaryFolder.newFile("heatmapFile.txt");

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-lh", heatmapFile.toString() });

    CreateGraphFiles files = parsedArguments.getFiles();
    assertEquals(LIST_FILES, files.getType());
    assertEquals(null, files.getParameterFile());
    assertEquals(null, files.getChartMapFile());
    assertEquals(heatmapFile, files.getListHeatmapFile());
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(null, parameters.referenceType);
    assertEquals(null, parameters.representationType);
    assertEquals(DEFAULT_REFERENCE_POINTS, parameters.referencePoints);
    assertEquals(null, parameters.boundary);
    assertEquals(DEFAULT_WINDOW_SIZE, parameters.windowSize);
    assertEquals(null, parameters.blocks);
    assertEquals(false, parameters.dispersion);
    if (parameters.yaxisScale != null) {
      assertEquals(null, parameters.yaxisScale.from);
      assertEquals(null, parameters.yaxisScale.to);
    }
  }

  @Test
  public void parseGeneChartArguments_MapAndHeatmapFile() throws Throwable {
    File file = temporaryFolder.newFile("test.txt");
    File heatmapFile = temporaryFolder.newFile("heatmapFile.txt");
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write("group1\tdata1.txt");
    }

    ParsedGeneChartArguments parsedArguments = commandLineParameterService.parseGeneChartArguments(
        new String[] { "create_graphs", "-la", file.toString(), "-lh", heatmapFile.toString() });

    CreateGraphFiles files = parsedArguments.getFiles();
    assertEquals(LIST_FILES, files.getType());
    assertEquals(null, files.getParameterFile());
    assertEquals(file, files.getChartMapFile());
    assertEquals(heatmapFile, files.getListHeatmapFile());
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(null, parameters.referenceType);
    assertEquals(null, parameters.representationType);
    assertEquals(DEFAULT_REFERENCE_POINTS, parameters.referencePoints);
    assertEquals(null, parameters.boundary);
    assertEquals(DEFAULT_WINDOW_SIZE, parameters.windowSize);
    assertEquals(null, parameters.blocks);
    assertEquals(false, parameters.dispersion);
    if (parameters.yaxisScale != null) {
      assertEquals(null, parameters.yaxisScale.from);
      assertEquals(null, parameters.yaxisScale.to);
    }
  }

  @Test
  public void parseGeneChartArguments_MissingGenerateAggregateGraphs() throws Throwable {
    ParsedGeneChartArguments parsedArguments =
        commandLineParameterService.parseGeneChartArguments(new String[] { "create_graphs" });

    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.generateAggregateGraphs);
  }

  @Test
  public void parseGeneChartArguments_GenerateAggregateGraphs() throws Throwable {
    when(parameterFileService.parseGenerateAggregateGraphs(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-gag", "1" });

    verify(parameterFileService).parseGenerateAggregateGraphs("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.generateAggregateGraphs);
  }

  @Test
  public void parseGeneChartArguments_GenerateAggregateGraphsLongName() throws Throwable {
    when(parameterFileService.parseGenerateAggregateGraphs(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService.parseGeneChartArguments(
        new String[] { "create_graphs", "--generate_aggregate_graphs", "1" });

    verify(parameterFileService).parseGenerateAggregateGraphs("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.generateAggregateGraphs);
  }

  @Test
  public void parseGeneChartArguments_MissingGenerateHeatmap() throws Throwable {
    ParsedGeneChartArguments parsedArguments =
        commandLineParameterService.parseGeneChartArguments(new String[] { "create_graphs" });

    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(false, parameters.generateHeatmap);
  }

  @Test
  public void parseGeneChartArguments_GenerateHeatmap() throws Throwable {
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-gh", "1" });

    verify(parameterFileService).parseGenerateHeatmap("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.generateHeatmap);
  }

  @Test
  public void parseGeneChartArguments_GenerateHeatmapLongName() throws Throwable {
    when(parameterFileService.parseGenerateHeatmap(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "--generate_heatmaps", "1" });

    verify(parameterFileService).parseGenerateHeatmap("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.generateHeatmap);
  }

  @Test
  public void parseGeneChartArguments_MissingDispersion() throws Throwable {
    ParsedGeneChartArguments parsedArguments =
        commandLineParameterService.parseGeneChartArguments(new String[] { "create_graphs" });

    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(false, parameters.dispersion);
  }

  @Test
  public void parseGeneChartArguments_Dispersion() throws Throwable {
    when(parameterFileService.parseDispersion(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-ddv", "1" });

    verify(parameterFileService).parseDispersion("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.dispersion);
  }

  @Test
  public void parseGeneChartArguments_DispersionLongName() throws Throwable {
    when(parameterFileService.parseDispersion(any(String.class))).thenReturn(true);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService.parseGeneChartArguments(
        new String[] { "create_graphs", "--display_dispersion_values", "1" });

    verify(parameterFileService).parseDispersion("1");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertEquals(true, parameters.dispersion);
  }

  @Test
  public void parseGeneChartArguments_MissingYAxisScale() throws Throwable {
    ParsedGeneChartArguments parsedArguments =
        commandLineParameterService.parseGeneChartArguments(new String[] { "create_graphs" });

    GeneChartParameters parameters = parsedArguments.getParameters();
    if (parameters.yaxisScale != null) {
      assertNull(parameters.yaxisScale.from);
      assertNull(parameters.yaxisScale.to);
    }
  }

  @Test
  public void parseGeneChartArguments_YAxisScale() throws Throwable {
    when(parameterFileService.parseYaxisScale(any(String.class))).thenReturn(yaxisScale);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "-yas", "abc" });

    verify(parameterFileService).parseYaxisScale("abc");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertSame(yaxisScale, parameters.yaxisScale);
  }

  @Test
  public void parseGeneChartArguments_YAxisScaleLongName() throws Throwable {
    when(parameterFileService.parseYaxisScale(any(String.class))).thenReturn(yaxisScale);

    ParsedGeneChartArguments parsedArguments = commandLineParameterService
        .parseGeneChartArguments(new String[] { "create_graphs", "--y_axis_scale", "abc" });

    verify(parameterFileService).parseYaxisScale("abc");
    GeneChartParameters parameters = parsedArguments.getParameters();
    assertSame(yaxisScale, parameters.yaxisScale);
  }

  @Test
  public void validateRunArguments_InvalidArgumentFirst() throws Throwable {
    File map = temporaryFolder.newFile("map.txt");

    commandLineParameterService.validateRunArguments(
        new String[] { "abc", "run", "-la", map.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_InvalidArgumentSecond() throws Throwable {
    File parameter = temporaryFolder.newFile("parameter.txt");

    commandLineParameterService.validateRunArguments(
        new String[] { "run", "abc", "-p", parameter.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_MissingRun() throws Throwable {
    File parameter = temporaryFolder.newFile("parameter.txt");

    commandLineParameterService.validateRunArguments(new String[] { "-p", parameter.toString() },
        locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_MissingFileType() throws Throwable {
    commandLineParameterService.validateRunArguments(new String[] { "run" }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_MultipleParameterFiles() throws Throwable {
    File parameter1 = temporaryFolder.newFile("parameter1.txt");
    File parameter2 = temporaryFolder.newFile("parameter2.txt");

    commandLineParameterService.validateRunArguments(
        new String[] { "run", "-p", parameter1.toString(), "-p", parameter2.toString() }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_MissingParameterFile() throws Throwable {
    commandLineParameterService.validateRunArguments(new String[] { "run", "-p" }, locale,
        errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_ParameterFileNotExists() throws Throwable {
    File parameter = new File(temporaryFolder.getRoot(), "test.txt");

    commandLineParameterService.validateRunArguments(
        new String[] { "run", "-p", parameter.toString() }, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateRunArguments_Parameters() throws Throwable {
    File parameter = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(parameter.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);

    commandLineParameterService.validateRunArguments(
        new String[] { "run", "-p", parameter.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(parameter), eq(locale), any());
    verify(errorHandler, never()).accept(any());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateRunArguments_ParametersError() throws Throwable {
    File parameter = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(parameter.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    final String parameterError = "parameter_error";
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept(parameterError);
        return null;
      }
    }).when(parameterFileService).validate(any(), any(), any());

    commandLineParameterService.validateRunArguments(
        new String[] { "run", "-p", parameter.toString() }, locale, errorHandler);

    verify(parameterFileService).validate(eq(parameter), eq(locale), any());
    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void parseRunArguments_ParameterFile() throws Throwable {
    File parameter = temporaryFolder.newFile("test.txt");
    try (Writer writer = Files.newBufferedWriter(parameter.toPath(), UTF_8)) {
      writer.write("~~@test=test");
    }
    when(parameterFileService.parse(any())).thenReturn(analysisParameters);

    AnalysisParameters analysisParameters = commandLineParameterService
        .parseRunArguments(new String[] { "run", "-p", parameter.toString() });

    verify(parameterFileService).parse(eq(parameter));
    assertEquals(analysisParameters, analysisParameters);
  }
}

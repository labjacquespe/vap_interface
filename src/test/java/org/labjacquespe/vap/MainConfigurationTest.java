/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import static org.junit.Assert.assertEquals;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MainConfigurationTest {
  @Inject
  private MainConfiguration mainConfiguration;

  @Test
  public void versionTest() throws Throwable {
    String versionLineStart = "  version: ";
    Path applicationProperties = Paths.get(getClass().getResource("/application.yml").toURI());
    String versionLine = Files.readAllLines(applicationProperties).stream()
        .filter(line -> line.startsWith(versionLineStart)).findFirst().orElseThrow(
            () -> new IllegalStateException("Version property not found in application.yml"));
    String version = versionLine.substring(versionLineStart.length());
    assertEquals(version, mainConfiguration.version());
  }
}

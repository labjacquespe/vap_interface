/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import java.nio.file.Files;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.ChartGroup;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ChartGroupParserTest {
  private ChartGroupParser chartGroupParser;
  @Mock
  private Consumer<String> handler;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Locale locale;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    chartGroupParser = new ChartGroupParser(UTF_8);
    locale = Locale.getDefault();
  }

  private void writeFile(File file, String content) throws IOException {
    try (Writer writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      IOUtils.copy(new StringReader(content), writer);
    }
  }

  @Test
  public void validate_Empty() throws Throwable {
    File file = temporaryFolder.newFile("map.txt");

    chartGroupParser.validate(file, locale, handler);

    verify(handler, never()).accept(any());
  }

  @Test
  public void validate_Valid() throws Throwable {
    final File file = temporaryFolder.newFile("map.txt");
    StringBuilder content = new StringBuilder();
    content.append("group1\tdata1.txt");
    content.append("\n");
    content.append("group2\tdata2.txt");
    writeFile(file, content.toString());

    chartGroupParser.validate(file, locale, handler);

    verify(handler, never()).accept(any());
  }

  @Test
  public void validate_MissingColumn() throws Throwable {
    final File file = temporaryFolder.newFile("map.txt");
    StringBuilder content = new StringBuilder();
    content.append("group1\tdata1.txt");
    content.append("\n");
    content.append("group2");
    writeFile(file, content.toString());

    chartGroupParser.validate(file, locale, handler);

    verify(handler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_ExtraColumn() throws Throwable {
    final File file = temporaryFolder.newFile("map.txt");
    StringBuilder content = new StringBuilder();
    content.append("group1\tdata1.txt");
    content.append("\n");
    content.append("group2\tdata2.txt\tdata3.txt");
    writeFile(file, content.toString());

    chartGroupParser.validate(file, locale, handler);

    verify(handler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptyFirstColumn() throws Throwable {
    final File file = temporaryFolder.newFile("map.txt");
    StringBuilder content = new StringBuilder();
    content.append("group1\tdata1.txt");
    content.append("\n");
    content.append("\tdata2.txt");
    writeFile(file, content.toString());

    chartGroupParser.validate(file, locale, handler);

    verify(handler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptySecondColumn() throws Throwable {
    final File file = temporaryFolder.newFile("map.txt");
    StringBuilder content = new StringBuilder();
    content.append("group1\tdata1.txt");
    content.append("\n");
    content.append("group2\t");
    writeFile(file, content.toString());

    chartGroupParser.validate(file, locale, handler);

    verify(handler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void parse() throws Throwable {
    File file = new File(this.getClass().getResource("/chart/list_agg_graphs.txt").toURI());
    List<ChartGroup> groups = chartGroupParser.parse(file);
    assertEquals(6, groups.size());
    ChartGroup group = groups.get(0);
    assertEquals("groupe_ORF_8WG16_4rep_--1_NWN", group.getName());
    assertEquals(5, group.getResources().size());
    assertEquals("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt",
        group.getResources().get(0));
    assertEquals("agg_3E8_WT_3rep__ratio_40050_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt",
        group.getResources().get(1));
    assertEquals("agg_8WG16_WT_4rep__ratio_40054_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt",
        group.getResources().get(2));
    assertEquals("agg_Young_H3K36me3vsH3.YPD_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt",
        group.getResources().get(3));
    assertEquals("agg_Z-BRw_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt", group.getResources().get(4));
    group = groups.get(1);
    assertEquals("groupe_ORF_8WG16_4rep_-0.5-0_NWN", group.getName());
    assertEquals(5, group.getResources().size());
    assertEquals("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt",
        group.getResources().get(0));
    assertEquals("agg_3E8_WT_3rep__ratio_40050_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt",
        group.getResources().get(1));
    assertEquals("agg_8WG16_WT_4rep__ratio_40054_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt",
        group.getResources().get(2));
    assertEquals("agg_Young_H3K36me3vsH3.YPD_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt",
        group.getResources().get(3));
    assertEquals("agg_Z-BRw_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt", group.getResources().get(4));
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.BrokenLineChart;
import org.labjacquespe.vap.chart.CategoryValueAxis;
import org.labjacquespe.vap.chart.Data;
import org.labjacquespe.vap.chart.DataGroup;
import org.labjacquespe.vap.chart.GeneAxis;
import org.labjacquespe.vap.chart.GeneAxis.Region;
import org.labjacquespe.vap.chart.GeneAxis.Separator;
import org.labjacquespe.vap.chart.GeneChart;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.HeatmapChart;
import org.labjacquespe.vap.chart.HeatmapColors;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapParameters;
import org.labjacquespe.vap.chart.HeatmapWindow;
import org.labjacquespe.vap.chart.StandardDeviationBrokenLineChart;
import org.labjacquespe.vap.chart.StandardDeviationBrokenLineChart.Extra;
import org.labjacquespe.vap.chart.service.GeneChartService.RepresentationTypeLegend;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class GeneChartServiceTest extends ApplicationTest {
  private GeneChartService geneChartService;
  private GeneChartParameters parameters;
  private HeatmapParameters heatmapParameters;
  private YAxisScale scale;
  private List<Block> regions = new ArrayList<>();
  private DataParser dataParser;
  private HeatmapDataParser heatmapParser;

  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
  }

  /**
   * Before tests.
   */
  @Before
  public void beforeTest() {
    geneChartService = new GeneChartService();
    parameters = new GeneChartParameters();
    scale = new YAxisScale();
    parameters.yaxisScale = scale;
    parameters.blocks = regions;
    Block region = new Block();
    region.windows = 20L;
    region.type = BlockType.UPSTREAM;
    regions.add(region);
    region = new Block();
    region.windows = 40L;
    region.type = BlockType.INTER;
    regions.add(region);
    region = new Block();
    region.windows = 45L;
    region.type = BlockType.REFERENCE_FEATURE;
    regions.add(region);
    region = new Block();
    region.windows = 30L;
    region.type = BlockType.INTER;
    regions.add(region);
    region = new Block();
    region.windows = 10L;
    region.type = BlockType.DOWNSTREAM;
    regions.add(region);
    heatmapParameters = new HeatmapParameters();
    parameters.heatmapParameters = heatmapParameters;
    dataParser = new DataParser(UTF_8);
    heatmapParser = new HeatmapDataParser(UTF_8);
  }

  private void exonRegions() {
    regions.clear();
    Block region = new Block();
    region.windows = 20L;
    region.type = BlockType.UPSTREAM;
    regions.add(region);
    region = new Block();
    region.windows = 15L;
    region.type = BlockType.EXON;
    regions.add(region);
    region = new Block();
    region.windows = 5L;
    region.type = BlockType.INTRON;
    regions.add(region);
    region = new Block();
    region.windows = 10L;
    region.type = BlockType.EXON;
    regions.add(region);
    region = new Block();
    region.windows = 7L;
    region.type = BlockType.INTRON;
    regions.add(region);
    region = new Block();
    region.windows = 8L;
    region.type = BlockType.EXON;
    regions.add(region);
    region = new Block();
    region.windows = 9L;
    region.type = BlockType.DOWNSTREAM;
    regions.add(region);
  }

  @Test
  public void geneChart() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = true;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    geneChart.getNode().layout();

    assertEquals(StandardDeviationBrokenLineChart.class, chart.getClass());
    assertEquals(NumberAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    StandardDeviationBrokenLineChart<Number> stdChart =
        (StandardDeviationBrokenLineChart<Number>) chart;
    final NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    // Validate data.
    List<XYChart.Series<Number, Number>> allSeries = stdChart.getData();
    assertEquals(dataGroups.size(), allSeries.size());
    for (int i = 0; i < dataGroups.size(); i++) {
      XYChart.Series<Number, Number> series = allSeries.get(i);
      DataGroup dataGroup = dataGroups.get(i);
      assertEquals(dataGroup.getName(), series.getName());
      int serieDataIndex = 0;
      boolean broken = false;
      for (int j = 0; j < dataGroup.getDatas().size(); j++) {
        Data expectedData = dataGroup.getDatas().get(j);
        if (expectedData.getType() == Data.Type.NORMAL) {
          XYChart.Data<Number, Number> data = series.getData().get(serieDataIndex++);
          assertEquals(expectedData.getX(), data.getXValue());
          assertEquals(expectedData.getAverage(), data.getYValue());
          if (data.getExtraValue() instanceof Extra) {
            Extra extra = (Extra) data.getExtraValue();
            assertEquals(expectedData.getStandardDeviation(), extra.getStandardDeviation());
            assertEquals(broken, extra.isBroken());
          } else {
            assertEquals(expectedData.getStandardDeviation(), data.getExtraValue());
            assertEquals(false, broken);
          }
          broken = false;
        }
        if (expectedData.getType() == Data.Type.SEPARATOR) {
          broken = true;
        }
      }
    }
    assertEquals(null, stdChart.getTitle());
    assertEquals(true, stdChart.getVerticalGridLinesVisible());
    // Validate Y axis.
    assertEquals("Signal", yaxis.getLabel());
    assertEquals(-0.675, yaxis.getLowerBound(), 0.01);
    assertEquals(3.755, yaxis.getUpperBound(), 0.01);
    assertEquals((0.675 + 3.755) / 5, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("gene-axis"));
    assertEquals(true, xaxis.getStyleClass().contains("ABSOLUTE"));
    assertEquals(-850, xaxis.getLowerBound(), 0.01);
    assertEquals(850, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(4, separators.size());
    for (DataGroup dataGroup : dataGroups) {
      for (Data data : dataGroup.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          Separator expectedSeparator = new Separator(data.getX(), data.getX() + data.getLength());
          assertEquals(true, separators.contains(expectedSeparator));
        }
      }
    }
    assertEquals((Long) 100L, xaxis.getScaleLegendLength());
    // Validate representation type legend.
    RepresentationTypeLegend legend =
        (RepresentationTypeLegend) geneChart.getRepresentationTypeLegend();
    assertEquals("Absolute method", legend.getText());
  }

  @Test
  public void geneChart_Relative() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.RELATIVE;
    parameters.dispersion = true;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data_relative.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    assertEquals(StandardDeviationBrokenLineChart.class, chart.getClass());
    assertEquals(NumberAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    StandardDeviationBrokenLineChart<Number> stdChart =
        (StandardDeviationBrokenLineChart<Number>) chart;
    final NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    // Validate data.
    List<XYChart.Series<Number, Number>> allSeries = stdChart.getData();
    assertEquals(dataGroups.size(), allSeries.size());
    for (int i = 0; i < dataGroups.size(); i++) {
      XYChart.Series<Number, Number> series = allSeries.get(i);
      DataGroup dataGroup = dataGroups.get(i);
      assertEquals(dataGroup.getName(), series.getName());
      int serieDataIndex = 0;
      boolean broken = false;
      for (int j = 0; j < dataGroup.getDatas().size(); j++) {
        Data expectedData = dataGroup.getDatas().get(j);
        if (expectedData.getType() == Data.Type.NORMAL) {
          XYChart.Data<Number, Number> data = series.getData().get(serieDataIndex++);
          assertEquals(expectedData.getX(), data.getXValue());
          assertEquals(expectedData.getAverage(), data.getYValue());
          if (data.getExtraValue() instanceof Extra) {
            Extra extra = (Extra) data.getExtraValue();
            assertEquals(expectedData.getStandardDeviation(), extra.getStandardDeviation());
            assertEquals(broken, extra.isBroken());
          } else {
            assertEquals(expectedData.getStandardDeviation(), data.getExtraValue());
            assertEquals(false, broken);
          }
          broken = false;
        }
        if (expectedData.getType() == Data.Type.SEPARATOR) {
          broken = true;
        }
      }
    }
    assertEquals(null, stdChart.getTitle());
    assertEquals(true, stdChart.getVerticalGridLinesVisible());
    // Validate Y axis.
    assertEquals("Signal", yaxis.getLabel());
    assertEquals(-0.675, yaxis.getLowerBound(), 0.01);
    assertEquals(3.755, yaxis.getUpperBound(), 0.01);
    assertEquals((0.675 + 3.755) / 5, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("gene-axis"));
    assertEquals(-65, xaxis.getLowerBound(), 0.01);
    assertEquals(65, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(2, separators.size());
    for (DataGroup dataGroup : dataGroups) {
      for (Data data : dataGroup.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          Separator expectedSeparator = new Separator(data.getX(), data.getX() + data.getLength());
          assertEquals(true, separators.contains(expectedSeparator));
        }
      }
    }
    assertEquals(true, xaxis.getStyleClass().contains("RELATIVE"));
  }

  @Test
  public void geneChart_GuessAbsolute() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = null;
    parameters.dispersion = false;
    scale.from = null;
    scale.to = null;
    final List<DataGroup> dataGroups = new ArrayList<>();
    DataGroup dataGroup = new DataGroup();
    dataGroup.setName("test_group");
    List<Data> datas = new ArrayList<>();
    dataGroup.setDatas(datas);
    Data data = new Data();
    data.setX(0.0);
    data.setOriginalX(0.0);
    data.setLength(50.0);
    data.setType(Data.Type.NORMAL);
    data.setAverage(20.0);
    datas.add(data);
    data = new Data();
    data.setX(50.0);
    data.setOriginalX(50.0);
    data.setLength(50.0);
    data.setType(Data.Type.NORMAL);
    data.setAverage(25.0);
    datas.add(data);
    dataGroups.add(dataGroup);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);

    assertEquals(true, geneChart.getChart().getXAxis().getStyleClass().contains("ABSOLUTE"));
  }

  @Test
  public void geneChart_GuessRelative() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = null;
    parameters.dispersion = false;
    scale.from = null;
    scale.to = null;
    final List<DataGroup> dataGroups = new ArrayList<>();
    DataGroup dataGroup = new DataGroup();
    dataGroup.setName("test_group");
    List<Data> datas = new ArrayList<>();
    dataGroup.setDatas(datas);
    Data data = new Data();
    data.setX(0.0);
    data.setOriginalX(0.0);
    data.setLength(1.0);
    data.setType(Data.Type.NORMAL);
    data.setAverage(20.0);
    datas.add(data);
    data = new Data();
    data.setX(1.0);
    data.setOriginalX(1.0);
    data.setLength(1.0);
    data.setType(Data.Type.NORMAL);
    data.setAverage(25.0);
    datas.add(data);
    dataGroups.add(dataGroup);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);

    assertEquals(true, geneChart.getChart().getXAxis().getStyleClass().contains("RELATIVE"));
  }

  @Test
  public void geneChart_Scale() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    scale.from = -2.0;
    scale.to = 4.0;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();

    NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    assertEquals(-2, yaxis.getLowerBound(), 0.01);
    assertEquals(4, yaxis.getUpperBound(), 0.01);
    assertEquals((2 + 4) / 5.0, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
  }

  @Test
  public void geneChart_ScaleNull() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = true;
    parameters.yaxisScale = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();

    NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    assertEquals(-0.675, yaxis.getLowerBound(), 0.01);
    assertEquals(3.755, yaxis.getUpperBound(), 0.01);
    assertEquals((0.675 + 3.755) / 5, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
  }

  @Test
  public void geneChart_YTickUnit() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    scale.from = 2.0;
    scale.to = 4.0;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();

    NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    assertEquals(2, yaxis.getLowerBound(), 0.01);
    assertEquals(4, yaxis.getUpperBound(), 0.01);
    assertEquals((4 - 2) / 5.0, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
  }

  @Test
  public void geneChart_5Regions() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(5, regions.size());
    Region region = regions.get(0);
    assertEquals(-650.0, region.getStart(), 0.1);
    assertEquals(-400.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-400.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(4);
    assertEquals(650.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void geneChart_5Regions_Relative() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.RELATIVE;
    parameters.windowSize = 10L;
    final File file = new File(this.getClass().getResource("/chart/data_relative.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(5, regions.size());
    Region region = regions.get(0);
    assertEquals(-65.0, region.getStart(), 0.1);
    assertEquals(-40.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-40.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(60.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(60.0, region.getStart(), 0.1);
    assertEquals(65.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(4);
    assertEquals(65.0, region.getStart(), 0.1);
    assertEquals(65.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void geneChart_4Regions() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 3;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    regions.remove(3);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(4, regions.size());
    Region region = regions.get(0);
    assertEquals(-650.0, region.getStart(), 0.1);
    assertEquals(-400.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-400.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void geneChart_2Regions_FivePrime() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    parameters.boundary = Boundary.FIVE_PRIME;
    regions.remove(3);
    regions.remove(2);
    regions.remove(1);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(2, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(100.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
  }

  @Test
  public void geneChart_2Regions_ThreePrime() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    parameters.boundary = Boundary.THREE_PRIME;
    regions.remove(3);
    regions.remove(2);
    regions.remove(1);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(2, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(100.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void geneChart_Exon() throws Throwable {
    exonRegions();
    parameters.referenceType = ReferenceType.EXONS;
    parameters.referencePoints = 6;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = true;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(7, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(150.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(2);
    assertEquals(150.0, region.getStart(), 0.1);
    assertEquals(200.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTRON, region.getType());
    region = regions.get(3);
    assertEquals(200.0, region.getStart(), 0.1);
    assertEquals(300.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(4);
    assertEquals(300.0, region.getStart(), 0.1);
    assertEquals(500.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTRON, region.getType());
    region = regions.get(5);
    assertEquals(500.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(6);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void geneChart_NoDispersion() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    assertEquals(BrokenLineChart.class, chart.getClass());
    assertEquals(NumberAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    BrokenLineChart<Number, Number> stdChart = (BrokenLineChart<Number, Number>) chart;
    final NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    // Validate data.
    List<XYChart.Series<Number, Number>> allSeries = stdChart.getData();
    assertEquals(dataGroups.size(), allSeries.size());
    for (int i = 0; i < dataGroups.size(); i++) {
      XYChart.Series<Number, Number> series = allSeries.get(i);
      DataGroup dataGroup = dataGroups.get(i);
      assertEquals(dataGroup.getName(), series.getName());
      int serieDataIndex = 0;
      boolean broken = false;
      for (int j = 0; j < dataGroup.getDatas().size(); j++) {
        Data expectedData = dataGroup.getDatas().get(j);
        if (expectedData.getType() == Data.Type.NORMAL) {
          XYChart.Data<Number, Number> data = series.getData().get(serieDataIndex++);
          assertEquals(expectedData.getX(), data.getXValue());
          assertEquals(expectedData.getAverage(), data.getYValue());
          assertEquals(true, (!broken && data.getExtraValue() == null)
              || ((Boolean) data.getExtraValue() == broken));
          broken = false;
        }
        if (expectedData.getType() == Data.Type.SEPARATOR) {
          broken = true;
        }
      }
    }
    assertEquals(null, stdChart.getTitle());
    assertEquals(true, stdChart.getVerticalGridLinesVisible());
    // Validate Y axis.
    assertEquals("Signal", yaxis.getLabel());
    assertEquals(-0.415095, yaxis.getLowerBound(), 0.01);
    assertEquals(3.29289, yaxis.getUpperBound(), 0.01);
    assertEquals((0.415095 + 3.29289) / 5, yaxis.getTickUnit(), 0.05);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
    // Validate X axis.
    assertEquals(-850, xaxis.getLowerBound(), 0.01);
    assertEquals(850, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(4, separators.size());
    for (DataGroup dataGroup : dataGroups) {
      for (Data data : dataGroup.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          Separator expectedSeparator = new Separator(data.getX(), data.getX() + data.getLength());
          assertEquals(true, separators.contains(expectedSeparator));
        }
      }
    }
  }

  @Test
  public void geneChart_NoReferenceType() throws Throwable {
    parameters.referenceType = null;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void geneChart_NoRepresentationType() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = null;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void geneChart_NoBoundary_1ReferencePoint() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = null;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void geneChart_NoBlocks() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    parameters.blocks = null;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.geneChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void proportionChart() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    assertEquals(BrokenLineChart.class, chart.getClass());
    assertEquals(NumberAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    BrokenLineChart<Number, Number> stdChart = (BrokenLineChart<Number, Number>) chart;
    final NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    // Validate data.
    List<XYChart.Series<Number, Number>> allSeries = stdChart.getData();
    assertEquals(dataGroups.size(), allSeries.size());
    for (int i = 0; i < dataGroups.size(); i++) {
      XYChart.Series<Number, Number> series = allSeries.get(i);
      DataGroup dataGroup = dataGroups.get(i);
      assertEquals(dataGroup.getName(), series.getName());
      int serieDataIndex = 0;
      boolean broken = false;
      for (int j = 0; j < dataGroup.getDatas().size(); j++) {
        Data expectedData = dataGroup.getDatas().get(j);
        if (expectedData.getType() == Data.Type.NORMAL) {
          XYChart.Data<Number, Number> data = series.getData().get(serieDataIndex++);
          assertEquals(expectedData.getX(), data.getXValue());
          assertEquals(expectedData.getProp(), data.getYValue());
          assertEquals(true, (!broken && data.getExtraValue() == null)
              || ((Boolean) data.getExtraValue() == broken));
          broken = false;
        }
        if (expectedData.getType() == Data.Type.SEPARATOR) {
          broken = true;
        }
      }
    }
    assertEquals(null, stdChart.getTitle());
    assertEquals(true, stdChart.getVerticalGridLinesVisible());
    // Validate Y axis.
    assertEquals("Proportion", yaxis.getLabel());
    assertEquals(0.0, yaxis.getLowerBound(), 0.001);
    assertEquals(1.0, yaxis.getUpperBound(), 0.001);
    assertEquals((0.0 + 1.0) / 5, yaxis.getTickUnit(), 0.005);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("gene-axis"));
    assertEquals(true, xaxis.getStyleClass().contains("ABSOLUTE"));
    assertEquals(-850, xaxis.getLowerBound(), 0.01);
    assertEquals(850, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(4, separators.size());
    for (DataGroup dataGroup : dataGroups) {
      for (Data data : dataGroup.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          Separator expectedSeparator = new Separator(data.getX(), data.getX() + data.getLength());
          assertEquals(true, separators.contains(expectedSeparator));
        }
      }
    }
    assertEquals((Long) 100L, xaxis.getScaleLegendLength());
    // Validate representation type legend.
    RepresentationTypeLegend legend =
        (RepresentationTypeLegend) geneChart.getRepresentationTypeLegend();
    assertEquals("Absolute method", legend.getText());
  }

  @Test
  public void proportionChart_Relative() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.RELATIVE;
    final File file = new File(this.getClass().getResource("/chart/data_relative.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    assertEquals(BrokenLineChart.class, chart.getClass());
    assertEquals(NumberAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    BrokenLineChart<Number, Number> stdChart = (BrokenLineChart<Number, Number>) chart;
    final NumberAxis yaxis = (NumberAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    // Validate data.
    List<XYChart.Series<Number, Number>> allSeries = stdChart.getData();
    assertEquals(dataGroups.size(), allSeries.size());
    for (int i = 0; i < dataGroups.size(); i++) {
      XYChart.Series<Number, Number> series = allSeries.get(i);
      DataGroup dataGroup = dataGroups.get(i);
      assertEquals(dataGroup.getName(), series.getName());
      int serieDataIndex = 0;
      boolean broken = false;
      for (int j = 0; j < dataGroup.getDatas().size(); j++) {
        Data expectedData = dataGroup.getDatas().get(j);
        if (expectedData.getType() == Data.Type.NORMAL) {
          XYChart.Data<Number, Number> data = series.getData().get(serieDataIndex++);
          assertEquals(expectedData.getX(), data.getXValue());
          assertEquals(expectedData.getProp(), data.getYValue());
          assertEquals(true, (!broken && data.getExtraValue() == null)
              || ((Boolean) data.getExtraValue() == broken));
          broken = false;
        }
        if (expectedData.getType() == Data.Type.SEPARATOR) {
          broken = true;
        }
      }
    }
    assertEquals(null, stdChart.getTitle());
    assertEquals(true, stdChart.getVerticalGridLinesVisible());
    // Validate Y axis.
    assertEquals("Proportion", yaxis.getLabel());
    assertEquals(0.0, yaxis.getLowerBound(), 0.001);
    assertEquals(1.0, yaxis.getUpperBound(), 0.001);
    assertEquals((0.0 + 1.0) / 5, yaxis.getTickUnit(), 0.005);
    assertEquals(5.0, yaxis.getMinorTickCount(), 0.1);
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("gene-axis"));
    assertEquals(true, xaxis.getStyleClass().contains("RELATIVE"));
    assertEquals(-65, xaxis.getLowerBound(), 0.01);
    assertEquals(65, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(2, separators.size());
    for (DataGroup dataGroup : dataGroups) {
      for (Data data : dataGroup.getDatas()) {
        if (data.getType() == Data.Type.SEPARATOR) {
          Separator expectedSeparator = new Separator(data.getX(), data.getX() + data.getLength());
          assertEquals(true, separators.contains(expectedSeparator));
        }
      }
    }
  }

  @Test
  public void proportionChart_5Regions() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(5, regions.size());
    Region region = regions.get(0);
    assertEquals(-650.0, region.getStart(), 0.1);
    assertEquals(-400.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-400.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(4);
    assertEquals(650.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void proportionChart_5Regions_Relative() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.RELATIVE;
    parameters.windowSize = 10L;
    final File file = new File(this.getClass().getResource("/chart/data_relative.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(5, regions.size());
    Region region = regions.get(0);
    assertEquals(-65.0, region.getStart(), 0.1);
    assertEquals(-40.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-40.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(60.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(60.0, region.getStart(), 0.1);
    assertEquals(65.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(4);
    assertEquals(65.0, region.getStart(), 0.1);
    assertEquals(65.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void proportionChart_4Regions() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 3;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    regions.remove(3);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(4, regions.size());
    Region region = regions.get(0);
    assertEquals(-650.0, region.getStart(), 0.1);
    assertEquals(-400.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(-400.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTER, region.getType());
    region = regions.get(2);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(3);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void proportionChart_2Regions_FivePrime() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    parameters.boundary = Boundary.FIVE_PRIME;
    regions.remove(3);
    regions.remove(2);
    regions.remove(1);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(2, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(100.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
  }

  @Test
  public void proportionChart_2Regions_ThreePrime() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.windowSize = 10L;
    parameters.boundary = Boundary.THREE_PRIME;
    regions.remove(3);
    regions.remove(2);
    regions.remove(1);
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(2, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.REFERENCE_FEATURE, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(100.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void proportionChart_Exon() throws Throwable {
    exonRegions();
    parameters.referenceType = ReferenceType.EXONS;
    parameters.referencePoints = 6;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = true;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(7, regions.size());
    Region region = regions.get(0);
    assertEquals(-200.0, region.getStart(), 0.1);
    assertEquals(0.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.UPSTREAM, region.getType());
    region = regions.get(1);
    assertEquals(0.0, region.getStart(), 0.1);
    assertEquals(150.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(true, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(2);
    assertEquals(150.0, region.getStart(), 0.1);
    assertEquals(200.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTRON, region.getType());
    region = regions.get(3);
    assertEquals(200.0, region.getStart(), 0.1);
    assertEquals(300.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(4);
    assertEquals(300.0, region.getStart(), 0.1);
    assertEquals(500.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.INTRON, region.getType());
    region = regions.get(5);
    assertEquals(500.0, region.getStart(), 0.1);
    assertEquals(600.0, region.getEnd(), 0.1);
    assertEquals(false, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.EXON, region.getType());
    region = regions.get(6);
    assertEquals(600.0, region.getStart(), 0.1);
    assertEquals(650.0, region.getEnd(), 0.1);
    assertEquals(true, region.isLine());
    assertEquals(false, region.isReference());
    assertEquals(BlockType.DOWNSTREAM, region.getType());
  }

  @Test
  public void proportionChart_NoReferenceType() throws Throwable {
    parameters.referenceType = null;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void proportionChart_NoRepresentationType() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = null;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void proportionChart_NoBoundary_1ReferencePoint() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 1;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = null;
    parameters.windowSize = 10L;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void proportionChart_NoBlocks() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.referencePoints = 4;
    parameters.representationType = RepresentationType.ABSOLUTE;
    parameters.dispersion = false;
    parameters.boundary = Boundary.FIVE_PRIME;
    parameters.windowSize = 10L;
    parameters.blocks = null;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/data.txt").toURI());
    List<DataGroup> dataGroups = dataParser.parse(file);

    GeneChart<Number, Number> geneChart = geneChartService.proportionChart(dataGroups, parameters);
    XYChart<Number, Number> chart = geneChart.getChart();
    chart.layout();

    GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<Region> regions = xaxis.getRegions();
    assertEquals(true, regions == null || regions.isEmpty());
  }

  @Test
  public void regionType_null() {
    assertEquals(null, geneChartService.regionType(0, null, 6));
    assertEquals(null, geneChartService.regionType(0, null, 5));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, null));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.COORDINATES, null));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.EXONS, null));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 0));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.COORDINATES, 0));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.EXONS, 0));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, -1));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.COORDINATES, -1));
    assertEquals(null, geneChartService.regionType(0, ReferenceType.EXONS, -1));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 6));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 6));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 6));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 5));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 5));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 5));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 4));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 4));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 4));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 3));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 3));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 3));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 2));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 2));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 2));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.ANNOTATIONS, 1));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.COORDINATES, 1));
    assertEquals(null, geneChartService.regionType(-1, ReferenceType.EXONS, 1));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 6));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 5));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 4));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 3));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 2));
    assertEquals(null, geneChartService.regionType(7, ReferenceType.ANNOTATIONS, 1));
    assertEquals(null, geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 5));
    assertEquals(null, geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 4));
    assertEquals(null, geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 3));
    assertEquals(null, geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 2));
    assertEquals(null, geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 1));
  }

  @Test
  public void regionType_Annotations() {
    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 1));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 1));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 2));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 2));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(2, ReferenceType.ANNOTATIONS, 2));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 3));
    assertEquals(BlockType.INTER, geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 3));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(2, ReferenceType.ANNOTATIONS, 3));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(3, ReferenceType.ANNOTATIONS, 3));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 4));
    assertEquals(BlockType.INTER, geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 4));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(2, ReferenceType.ANNOTATIONS, 4));
    assertEquals(BlockType.INTER, geneChartService.regionType(3, ReferenceType.ANNOTATIONS, 4));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(4, ReferenceType.ANNOTATIONS, 4));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 5));
    assertEquals(BlockType.ANNOTATION,
        geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 5));
    assertEquals(BlockType.INTER, geneChartService.regionType(2, ReferenceType.ANNOTATIONS, 5));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(3, ReferenceType.ANNOTATIONS, 5));
    assertEquals(BlockType.INTER, geneChartService.regionType(4, ReferenceType.ANNOTATIONS, 5));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(5, ReferenceType.ANNOTATIONS, 5));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.ANNOTATION,
        geneChartService.regionType(1, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.INTER, geneChartService.regionType(2, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(3, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.INTER, geneChartService.regionType(4, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.ANNOTATION,
        geneChartService.regionType(5, ReferenceType.ANNOTATIONS, 6));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(6, ReferenceType.ANNOTATIONS, 6));
  }

  @Test
  public void regionType_Exons() {
    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 1));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(1, ReferenceType.EXONS, 1));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 2));
    assertEquals(BlockType.EXON, geneChartService.regionType(1, ReferenceType.EXONS, 2));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(2, ReferenceType.EXONS, 2));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 3));
    assertEquals(BlockType.INTRON, geneChartService.regionType(1, ReferenceType.EXONS, 3));
    assertEquals(BlockType.EXON, geneChartService.regionType(2, ReferenceType.EXONS, 3));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(3, ReferenceType.EXONS, 3));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 4));
    assertEquals(BlockType.INTRON, geneChartService.regionType(1, ReferenceType.EXONS, 4));
    assertEquals(BlockType.EXON, geneChartService.regionType(2, ReferenceType.EXONS, 4));
    assertEquals(BlockType.INTRON, geneChartService.regionType(3, ReferenceType.EXONS, 4));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(4, ReferenceType.EXONS, 4));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 5));
    assertEquals(BlockType.EXON, geneChartService.regionType(1, ReferenceType.EXONS, 5));
    assertEquals(BlockType.INTRON, geneChartService.regionType(2, ReferenceType.EXONS, 5));
    assertEquals(BlockType.EXON, geneChartService.regionType(3, ReferenceType.EXONS, 5));
    assertEquals(BlockType.INTRON, geneChartService.regionType(4, ReferenceType.EXONS, 5));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(5, ReferenceType.EXONS, 5));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.EXONS, 6));
    assertEquals(BlockType.EXON, geneChartService.regionType(1, ReferenceType.EXONS, 6));
    assertEquals(BlockType.INTRON, geneChartService.regionType(2, ReferenceType.EXONS, 6));
    assertEquals(BlockType.EXON, geneChartService.regionType(3, ReferenceType.EXONS, 6));
    assertEquals(BlockType.INTRON, geneChartService.regionType(4, ReferenceType.EXONS, 6));
    assertEquals(BlockType.EXON, geneChartService.regionType(5, ReferenceType.EXONS, 6));
    assertEquals(BlockType.DOWNSTREAM, geneChartService.regionType(6, ReferenceType.EXONS, 6));
  }

  @Test
  public void regionType_Coordinates() {
    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 1));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(1, ReferenceType.COORDINATES, 1));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 2));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(1, ReferenceType.COORDINATES, 2));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(2, ReferenceType.COORDINATES, 2));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 3));
    assertEquals(BlockType.INTER, geneChartService.regionType(1, ReferenceType.COORDINATES, 3));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(2, ReferenceType.COORDINATES, 3));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(3, ReferenceType.COORDINATES, 3));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 4));
    assertEquals(BlockType.INTER, geneChartService.regionType(1, ReferenceType.COORDINATES, 4));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(2, ReferenceType.COORDINATES, 4));
    assertEquals(BlockType.INTER, geneChartService.regionType(3, ReferenceType.COORDINATES, 4));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(4, ReferenceType.COORDINATES, 4));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 5));
    assertEquals(BlockType.REGION, geneChartService.regionType(1, ReferenceType.COORDINATES, 5));
    assertEquals(BlockType.INTER, geneChartService.regionType(2, ReferenceType.COORDINATES, 5));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(3, ReferenceType.COORDINATES, 5));
    assertEquals(BlockType.INTER, geneChartService.regionType(4, ReferenceType.COORDINATES, 5));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(5, ReferenceType.COORDINATES, 5));

    assertEquals(BlockType.UPSTREAM, geneChartService.regionType(0, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.REGION, geneChartService.regionType(1, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.INTER, geneChartService.regionType(2, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.REFERENCE_FEATURE,
        geneChartService.regionType(3, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.INTER, geneChartService.regionType(4, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.REGION, geneChartService.regionType(5, ReferenceType.COORDINATES, 6));
    assertEquals(BlockType.DOWNSTREAM,
        geneChartService.regionType(6, ReferenceType.COORDINATES, 6));
  }

  @Test
  public void heatmapChart() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.representationType = RepresentationType.ABSOLUTE;
    heatmapParameters.heatmapColors = HeatmapColors.RAINBOW;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/heatmap.txt").toURI());
    HeatmapMetadata metadata = heatmapParser.parseMetadata(file);
    List<HeatmapFeature> features = new ArrayList<>();
    heatmapParser.parseData(file, featureData -> {
      features.add(featureData);
    });

    GeneChart<Number, Integer> geneChart =
        geneChartService.heatmapChart(metadata, features, parameters);
    XYChart<Number, Integer> chart = geneChart.getChart();
    geneChart.getNode().layout();

    assertTrue(chart instanceof HeatmapChart);
    assertEquals(CategoryValueAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    // Validate data.
    HeatmapChart<Number, Integer> heatmapChart = (HeatmapChart<Number, Integer>) chart;
    final CategoryValueAxis yaxis = (CategoryValueAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<XYChart.Series<Number, Integer>> allSeries = heatmapChart.getData();
    assertEquals(11, allSeries.size());
    List<String> featureNames = new ArrayList<>();
    Map<String, HeatmapFeature> featureByNames = new HashMap<>();
    for (HeatmapFeature feature : features) {
      featureNames.add(feature.name);
      featureByNames.put(feature.name, feature);
    }
    Collections.reverse(featureNames);
    for (int i = 0; i < featureNames.size(); i++) {
      HeatmapFeature feature = featureByNames.get(featureNames.get(i));
      XYChart.Series<Number, Integer> series = allSeries.get(i);
      assertEquals(feature.name, series.getName());
      int serieDataIndex = 0;
      for (int j = 0; j < metadata.windows.size(); j++) {
        HeatmapWindow window = metadata.windows.get(j);
        if (!window.isSeparator()) {
          Double windowValue = feature.heats.get(j);
          if (windowValue != null) {
            XYChart.Data<Number, Integer> serieData = series.getData().get(serieDataIndex++);
            assertEquals(window.getX(), serieData.getXValue());
            assertEquals((Integer) (featureNames.size() - i - 1), serieData.getYValue());
            assertEquals(windowValue, serieData.getExtraValue());
          }
        }
      }
    }
    assertEquals(null, chart.getTitle());
    assertEquals(true, chart.getVerticalGridLinesVisible());
    assertEquals(false, chart.isHorizontalGridLinesVisible());
    assertEquals(false, chart.isAlternativeRowFillVisible());
    // Validate Y axis.
    assertEquals(null, yaxis.getLabel());
    assertEquals(11, yaxis.getTickLabels().size());
    assertEquals("YAL038W", yaxis.getTickLabels().get(0));
    assertEquals("YAL037C-B", yaxis.getTickLabels().get(1));
    assertEquals("YJL177W", yaxis.getTickLabels().get(10));
    assertEquals(false, yaxis.isTickMarkVisible());
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("ABSOLUTE"));
    assertEquals(-1150, xaxis.getLowerBound(), 0.01);
    assertEquals(2300, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(3, separators.size());
    separators.contains(new Separator(-1150, -1000));
    separators.contains(new Separator(500, 650));
    separators.contains(new Separator(2150, 2300));
    assertEquals((Long) 500L, xaxis.getScaleLegendLength());
  }

  @Test
  public void heatmapChart_NotAllFeatures() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.representationType = RepresentationType.ABSOLUTE;
    heatmapParameters.heatmapColors = HeatmapColors.RAINBOW;
    scale.from = null;
    scale.to = null;
    final File file = new File(this.getClass().getResource("/chart/heatmap.txt").toURI());
    HeatmapMetadata metadata = heatmapParser.parseMetadata(file);
    List<HeatmapFeature> features = new ArrayList<>();
    heatmapParser.parseData(file, featureData -> {
      features.add(featureData);
    });

    GeneChart<Number, Integer> geneChart =
        geneChartService.heatmapChart(metadata, features.subList(0, 10), parameters);
    XYChart<Number, Integer> chart = geneChart.getChart();
    geneChart.getNode().layout();

    assertTrue(chart instanceof HeatmapChart);
    assertEquals(CategoryValueAxis.class, chart.getYAxis().getClass());
    assertEquals(GeneAxis.class, chart.getXAxis().getClass());

    // Validate data.
    HeatmapChart<Number, Integer> heatmapChart = (HeatmapChart<Number, Integer>) chart;
    final CategoryValueAxis yaxis = (CategoryValueAxis) chart.getYAxis();
    final GeneAxis xaxis = (GeneAxis) chart.getXAxis();
    List<XYChart.Series<Number, Integer>> allSeries = heatmapChart.getData();
    assertEquals(10, allSeries.size());
    List<String> featureNames = new ArrayList<>();
    Map<String, HeatmapFeature> featureByNames = new HashMap<>();
    for (HeatmapFeature feature : features.subList(0, 10)) {
      featureNames.add(feature.name);
      featureByNames.put(feature.name, feature);
    }
    Collections.reverse(featureNames);
    for (int i = 0; i < featureNames.size(); i++) {
      HeatmapFeature feature = featureByNames.get(featureNames.get(i));
      XYChart.Series<Number, Integer> series = allSeries.get(i);
      assertEquals(feature.name, series.getName());
      int serieDataIndex = 0;
      for (int j = 0; j < metadata.windows.size(); j++) {
        HeatmapWindow window = metadata.windows.get(j);
        if (!window.isSeparator()) {
          Double windowValue = feature.heats.get(j);
          if (windowValue != null) {
            XYChart.Data<Number, Integer> serieData = series.getData().get(serieDataIndex++);
            assertEquals(window.getX(), serieData.getXValue());
            assertEquals((Integer) (featureNames.size() - i - 1), serieData.getYValue());
            assertEquals(windowValue, serieData.getExtraValue());
          }
        }
      }
    }
    assertEquals(null, chart.getTitle());
    assertEquals(true, chart.getVerticalGridLinesVisible());
    assertEquals(false, chart.isHorizontalGridLinesVisible());
    assertEquals(false, chart.isAlternativeRowFillVisible());
    // Validate Y axis.
    assertEquals(null, yaxis.getLabel());
    assertEquals(10, yaxis.getTickLabels().size());
    assertEquals("YAL038W", yaxis.getTickLabels().get(0));
    assertEquals("YAL037C-B", yaxis.getTickLabels().get(1));
    assertEquals(false, yaxis.getTickLabels().containsKey(10));
    assertEquals(false, yaxis.isTickMarkVisible());
    // Validate X axis.
    assertEquals(true, xaxis.getStyleClass().contains("ABSOLUTE"));
    assertEquals(-1150, xaxis.getLowerBound(), 0.01);
    assertEquals(2300, xaxis.getUpperBound(), 0.01);
    Set<Separator> separators = xaxis.getSeparators();
    assertEquals(3, separators.size());
    separators.contains(new Separator(-1150, -1000));
    separators.contains(new Separator(500, 650));
    separators.contains(new Separator(2150, 2300));
    assertEquals((Long) 500L, xaxis.getScaleLegendLength());
  }

  @Test
  public void heatmapChart_Scale() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.representationType = RepresentationType.ABSOLUTE;
    heatmapParameters.heatmapColors = HeatmapColors.RAINBOW;
    scale.from = -2.0;
    scale.to = 4.0;
    final File file = new File(this.getClass().getResource("/chart/heatmap.txt").toURI());
    HeatmapMetadata metadata = heatmapParser.parseMetadata(file);
    List<HeatmapFeature> features = new ArrayList<>();
    heatmapParser.parseData(file, featureData -> {
      features.add(featureData);
    });

    GeneChart<Number, Integer> geneChart =
        geneChartService.heatmapChart(metadata, features, parameters);
    XYChart<Number, Integer> chart = geneChart.getChart();

    HeatmapChart<Number, Integer> heatmapChart = (HeatmapChart<Number, Integer>) chart;
    assertEquals(-2, heatmapChart.getMinHeat(), 0.01);
    assertEquals(4, heatmapChart.getMaxHeat(), 0.01);
  }

  @Test
  public void heatmapChart_ScaleNull() throws Throwable {
    parameters.referenceType = ReferenceType.ANNOTATIONS;
    parameters.representationType = RepresentationType.ABSOLUTE;
    heatmapParameters.heatmapColors = HeatmapColors.RAINBOW;
    parameters.yaxisScale = null;
    final File file = new File(this.getClass().getResource("/chart/heatmap.txt").toURI());
    HeatmapMetadata metadata = heatmapParser.parseMetadata(file);
    List<HeatmapFeature> features = new ArrayList<>();
    heatmapParser.parseData(file, featureData -> {
      features.add(featureData);
    });

    GeneChart<Number, Integer> geneChart =
        geneChartService.heatmapChart(metadata, features, parameters);
    XYChart<Number, Integer> chart = geneChart.getChart();

    HeatmapChart<Number, Integer> heatmapChart = (HeatmapChart<Number, Integer>) chart;
    assertEquals(-1.16211, heatmapChart.getMinHeat(), 0.01);
    assertEquals(5.25, heatmapChart.getMaxHeat(), 0.01);
  }
}

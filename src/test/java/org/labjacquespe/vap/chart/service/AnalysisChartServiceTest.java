/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyListOf;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.CategoryValueAxis;
import org.labjacquespe.vap.chart.ChartGroup;
import org.labjacquespe.vap.chart.DataGroup;
import org.labjacquespe.vap.chart.GeneChart;
import org.labjacquespe.vap.chart.GeneChartParameters;
import org.labjacquespe.vap.chart.HeatmapChart;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapFile;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapParameters;
import org.labjacquespe.vap.progressbar.ProgressBar;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class AnalysisChartServiceTest extends ApplicationTest {
  private AnalysisChartService analysisChartService;
  @Mock
  private ChartGroupParser chartGroupParser;
  @Mock
  private DataParser dataParser;
  @Mock
  private HeatmapListParser heatmapListParser;
  @Mock
  private HeatmapDataParser heatmapDataParser;
  @Mock
  private GeneChartService geneChartService;
  @Mock
  private File chartMap;
  @Mock
  private DataGroup chartData;
  @Mock
  private GeneChartParameters parameters;
  @Mock
  private ProgressBar progressBar;
  @Mock
  private GeneChart<Number, Number> geneGeneChart;
  @Mock
  private GeneChart<Number, Number> proportionGeneChart;
  @Mock
  private GeneChart<Number, Integer> heatmapGeneChart;
  @Mock
  private Consumer<String> errorHandler;
  @Captor
  private ArgumentCaptor<List<DataGroup>> chartDataCaptor;
  @Captor
  private ArgumentCaptor<List<HeatmapFeature>> featuresDataCaptor;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private List<DataGroup> chartDataList;
  private XYChart<Number, Number> geneChart;
  private XYChart<Number, Number> proportionChart;
  private HeatmapChart<Number, Integer> heatmapChart;
  private HeatmapParameters heatmapParameters;
  private Locale locale;

  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
  }

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    analysisChartService = new AnalysisChartService(chartGroupParser, dataParser, heatmapListParser,
        heatmapDataParser, geneChartService);
    chartDataList = Arrays.asList(chartData);
    when(progressBar.step(any(Double.class))).thenReturn(progressBar);
    when(dataParser.parse(any())).thenReturn(chartDataList);
    when(geneChartService.geneChart(anyListOf(DataGroup.class), any(GeneChartParameters.class)))
        .thenReturn(geneGeneChart);
    when(geneChartService.proportionChart(anyListOf(DataGroup.class),
        any(GeneChartParameters.class))).thenReturn(proportionGeneChart);
    when(geneChartService.heatmapChart(any(), any(), any())).thenReturn(heatmapGeneChart);
    geneChart = new ScatterChart<>(new NumberAxis(), new NumberAxis());
    geneChart.getXAxis().setLabel("Value");
    geneChart.getYAxis().setLabel("Country");
    geneChart.setTitle("Country Summary");
    XYChart.Series<Number, Number> geneSeries = new XYChart.Series<>();
    geneSeries.setName("2003");
    geneSeries.getData().add(new XYChart.Data<Number, Number>(25601.34, 242.01));
    geneSeries.getData().add(new XYChart.Data<Number, Number>(20148.82, 473.14));
    geneChart.getData().add(geneSeries);
    when(geneGeneChart.getChart()).thenReturn(geneChart);
    Pane genePane = new VBox();
    genePane.getChildren().add(geneChart);
    when(geneGeneChart.getNode()).thenReturn(genePane);
    proportionChart = new ScatterChart<>(new NumberAxis(), new NumberAxis());
    proportionChart.getXAxis().setLabel("Value");
    proportionChart.getYAxis().setLabel("Country");
    proportionChart.setTitle("Country Summary");
    XYChart.Series<Number, Number> proportionSeries = new XYChart.Series<>();
    proportionSeries.setName("2003");
    proportionSeries.getData().add(new XYChart.Data<Number, Number>(25601.34, 242.01));
    proportionSeries.getData().add(new XYChart.Data<Number, Number>(20148.82, 473.14));
    proportionChart.getData().add(proportionSeries);
    when(proportionGeneChart.getChart()).thenReturn(proportionChart);
    Pane proportionPane = new VBox();
    proportionPane.getChildren().add(proportionChart);
    when(proportionGeneChart.getNode()).thenReturn(proportionPane);
    heatmapChart = new HeatmapChart<>(new NumberAxis(), new CategoryValueAxis());
    heatmapChart.getXAxis().setLabel("Value");
    heatmapChart.getYAxis().setLabel("Country");
    heatmapChart.setTitle("Country Summary");
    XYChart.Series<Number, Integer> heatmapSeries = new XYChart.Series<>();
    heatmapSeries.setName("2003");
    heatmapSeries.getData().add(new XYChart.Data<Number, Integer>(25601.34, 1));
    heatmapSeries.getData().add(new XYChart.Data<Number, Integer>(20148.82, 2));
    heatmapChart.getData().add(heatmapSeries);
    when(heatmapGeneChart.getChart()).thenReturn(heatmapChart);
    Pane heatmapPane = new VBox();
    heatmapPane.getChildren().add(heatmapChart);
    when(heatmapGeneChart.getNode()).thenReturn(heatmapPane);
    heatmapParameters = new HeatmapParameters();
    locale = Locale.getDefault();
  }

  @Test
  public void validateChartMap() throws Throwable {
    File chartMap = new File("list_agg_graphs.txt");

    analysisChartService.validateChartMap(chartMap, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(chartGroupParser).validate(eq(chartMap), eq(locale), any());
  }

  @Test
  public void validateChartMap_Prefix() throws Throwable {
    File chartMap = new File("chris_list_agg_graphs.txt");

    analysisChartService.validateChartMap(chartMap, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(chartGroupParser).validate(eq(chartMap), eq(locale), any());
  }

  @Test
  public void validateChartMap_NoPrefixUnderscore() throws Throwable {
    File chartMap = new File("_list_agg_graphs.txt");

    analysisChartService.validateChartMap(chartMap, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(chartGroupParser).validate(eq(chartMap), eq(locale), any());
  }

  @Test
  public void validateChartMap_Invalid() throws Throwable {
    File chartMap = new File("abc.txt");

    analysisChartService.validateChartMap(chartMap, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(chartGroupParser).validate(eq(chartMap), eq(locale), any());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateChartMap_ParserValidation() throws Throwable {
    File chartMap = new File("list_agg_graphs.txt");
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept("test");
        return null;
      }
    }).when(chartGroupParser).validate(any(), any(), any());

    analysisChartService.validateChartMap(chartMap, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    assertNotNull(errorCaptor.getValue());
    verify(chartGroupParser).validate(eq(chartMap), eq(locale), any());
  }

  @Test
  public void validateHeatmapList() throws Throwable {
    File heatmapList = new File("list_ind_heatmaps.txt");

    analysisChartService.validateHeatmapList(heatmapList, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(heatmapListParser).validate(eq(heatmapList), eq(locale), any());
  }

  @Test
  public void validateHeatmapList_Prefix() throws Throwable {
    File heatmapList = new File("chris_list_ind_heatmaps.txt");

    analysisChartService.validateHeatmapList(heatmapList, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(heatmapListParser).validate(eq(heatmapList), eq(locale), any());
  }

  @Test
  public void validateHeatmapList_NoPrefixUnderscore() throws Throwable {
    File heatmapList = new File("_list_ind_heatmaps.txt");

    analysisChartService.validateHeatmapList(heatmapList, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(heatmapListParser).validate(eq(heatmapList), eq(locale), any());
  }

  @Test
  public void validateHeatmapList_Invalid() throws Throwable {
    File heatmapList = new File("abc.txt");

    analysisChartService.validateHeatmapList(heatmapList, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
    verify(heatmapListParser).validate(eq(heatmapList), eq(locale), any());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void validateHeatmapList_ParserValidation() throws Throwable {
    File heatmapList = new File("list_ind_heatmaps.txt");
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Consumer<String> handler = (Consumer<String>) invocation.getArguments()[2];
        handler.accept("test");
        return null;
      }
    }).when(heatmapListParser).validate(any(), any(), any());

    analysisChartService.validateHeatmapList(heatmapList, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    assertNotNull(errorCaptor.getValue());
    verify(heatmapListParser).validate(eq(heatmapList), eq(locale), any());
  }

  @Test
  public void chartMap() throws Throwable {
    File folder = new File(SystemUtils.JAVA_IO_TMPDIR);

    File chartMap = analysisChartService.chartMap(folder, "chris");

    assertEquals(new File(folder, "chris_list_agg_graphs.txt"), chartMap);
  }

  @Test
  public void chartMap_NullPrefix() throws Throwable {
    File folder = new File(SystemUtils.JAVA_IO_TMPDIR);

    File chartMap = analysisChartService.chartMap(folder, null);

    assertEquals(new File(folder, "list_agg_graphs.txt"), chartMap);
  }

  @Test
  public void heatmapList() throws Throwable {
    File folder = new File(SystemUtils.JAVA_IO_TMPDIR);

    File heatmapList = analysisChartService.heatmapList(folder, "chris");

    assertEquals(new File(folder, "chris_list_ind_heatmaps.txt"), heatmapList);
  }

  @Test
  public void heatmaps_NullPrefix() throws Throwable {
    File folder = new File(SystemUtils.JAVA_IO_TMPDIR);

    File heatmapList = analysisChartService.heatmapList(folder, null);

    assertEquals(new File(folder, "list_ind_heatmaps.txt"), heatmapList);
  }

  @Test
  public void createCharts_AggregateGraphs() throws Throwable {
    File chartMapSource = new File(getClass().getResource("/chart/list_agg_graphs.txt").toURI());
    File chartMap = temporaryFolder.newFile("chartMap.txt");
    Files.copy(chartMapSource.toPath(), chartMap.toPath(), StandardCopyOption.REPLACE_EXISTING);
    File data = new File(getClass().getResource("/chart/data.txt").toURI());
    final List<ChartGroup> chartGroups = new ArrayList<>();
    ChartGroup chartGroup = new ChartGroup();
    chartGroup.setName("group_1");
    File data1 = temporaryFolder.newFile("chart1.txt");
    Files.copy(data.toPath(), data1.toPath(), StandardCopyOption.REPLACE_EXISTING);
    chartGroup.setResources(Arrays.asList(data1.toString()));
    chartGroups.add(chartGroup);
    chartGroup = new ChartGroup();
    chartGroup.setName("group_2");
    File data2 = temporaryFolder.newFile("chart2.txt");
    File data3 = temporaryFolder.newFile("chart3.txt");
    Files.copy(data.toPath(), data2.toPath(), StandardCopyOption.REPLACE_EXISTING);
    Files.copy(data.toPath(), data3.toPath(), StandardCopyOption.REPLACE_EXISTING);
    chartGroup.setResources(Arrays.asList(data2.toString(), data3.toString()));
    chartGroups.add(chartGroup);
    when(chartGroupParser.parse(chartMap)).thenReturn(chartGroups);
    parameters.generateAggregateGraphs = true;
    File heatmap = new File(getClass().getResource("/chart/heatmap.txt").toURI());
    List<HeatmapFile> heatmapFiles = new ArrayList<>();
    heatmapFiles.add(new HeatmapFile("heatmap1", temporaryFolder.newFile("heatmap1.txt")));
    heatmapFiles.add(new HeatmapFile("heatmap2", temporaryFolder.newFile("heatmap2.txt")));
    Files.copy(heatmap.toPath(), heatmapFiles.get(0).getFile().toPath(),
        StandardCopyOption.REPLACE_EXISTING);
    Files.copy(heatmap.toPath(), heatmapFiles.get(1).getFile().toPath(),
        StandardCopyOption.REPLACE_EXISTING);
    when(heatmapListParser.parse(any())).thenReturn(heatmapFiles);
    parameters.generateHeatmap = false;

    analysisChartService.createCharts(chartMap, null, parameters, progressBar);

    verify(chartGroupParser).parse(chartMap);
    verify(dataParser).parse(data1);
    verify(dataParser).parse(data2);
    verify(dataParser).parse(data3);
    verify(geneChartService, times(2)).geneChart(chartDataCaptor.capture(), eq(parameters));
    verify(geneChartService, times(2)).proportionChart(chartDataCaptor.capture(), eq(parameters));
    assertEquals(true, chartDataCaptor.getAllValues().get(0).contains(chartData));
    assertEquals(true, chartDataCaptor.getAllValues().get(1).contains(chartData));
    assertEquals(true, chartDataCaptor.getAllValues().get(2).contains(chartData));
    assertEquals(true, chartDataCaptor.getAllValues().get(3).contains(chartData));
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_1.png").exists());
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_2.png").exists());
    verify(heatmapDataParser, never()).parseMetadata(any());
    verify(heatmapDataParser, never()).parseData(any(), any());
    verify(geneChartService, never()).heatmapChart(any(), any(), any());
    assertEquals(false, new File(temporaryFolder.getRoot(), "heatmap1.png").exists());
    assertEquals(false, new File(temporaryFolder.getRoot(), "heatmap2.png").exists());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void createCharts_Heatmap() throws Throwable {
    File chartMapSource = new File(getClass().getResource("/chart/list_agg_graphs.txt").toURI());
    File chartMap = temporaryFolder.newFile("chartMap.txt");
    Files.copy(chartMapSource.toPath(), chartMap.toPath(), StandardCopyOption.REPLACE_EXISTING);
    File data = new File(getClass().getResource("/chart/data.txt").toURI());
    final List<ChartGroup> chartGroups = new ArrayList<>();
    ChartGroup chartGroup = new ChartGroup();
    chartGroup.setName("group_1");
    File data1 = temporaryFolder.newFile("chart1.txt");
    Files.copy(data.toPath(), data1.toPath(), StandardCopyOption.REPLACE_EXISTING);
    chartGroup.setResources(Arrays.asList(data1.toString()));
    chartGroups.add(chartGroup);
    chartGroup = new ChartGroup();
    chartGroup.setName("group_2");
    File data2 = temporaryFolder.newFile("chart2.txt");
    File data3 = temporaryFolder.newFile("chart3.txt");
    Files.copy(data.toPath(), data2.toPath(), StandardCopyOption.REPLACE_EXISTING);
    Files.copy(data.toPath(), data3.toPath(), StandardCopyOption.REPLACE_EXISTING);
    chartGroup.setResources(Arrays.asList(data2.toString(), data3.toString()));
    chartGroups.add(chartGroup);
    when(chartGroupParser.parse(chartMap)).thenReturn(chartGroups);
    parameters.generateAggregateGraphs = true;
    final File heatmaplist = new File(getClass().getResource("/chart/heatmaplist.txt").toURI());
    File heatmap = new File(getClass().getResource("/chart/heatmap.txt").toURI());
    List<HeatmapFile> heatmapFiles = new ArrayList<>();
    heatmapFiles.add(new HeatmapFile("heatmap1", temporaryFolder.newFile("heatmap1.txt")));
    heatmapFiles.add(new HeatmapFile("heatmap2", temporaryFolder.newFile("heatmap2.txt")));
    Files.copy(heatmap.toPath(), heatmapFiles.get(0).getFile().toPath(),
        StandardCopyOption.REPLACE_EXISTING);
    Files.copy(heatmap.toPath(), heatmapFiles.get(1).getFile().toPath(),
        StandardCopyOption.REPLACE_EXISTING);
    when(heatmapListParser.parse(any())).thenReturn(heatmapFiles);
    parameters.generateHeatmap = true;
    parameters.heatmapParameters = heatmapParameters;
    heatmapParameters.axisVisible = true;
    String featureName1 = "feature-1";
    final String filenameFeatureName1 = featureName1.replaceAll("\\W", "_");
    String featureNameLast = "feature-last";
    final String filenameFeatureNameLast = featureNameLast.replaceAll("\\W", "_");
    List<HeatmapFeature> features = new ArrayList<>();
    IntStream.range(0, 3).forEach(i -> features.add(new HeatmapFeature()));
    features.get(0).name = featureName1;
    features.get(2).name = featureNameLast;
    HeatmapMetadata metadata = new HeatmapMetadata();
    metadata.featureCount = features.size();
    when(heatmapDataParser.parseMetadata(any())).thenReturn(metadata);
    doAnswer(invocation -> {
      Consumer<HeatmapFeature> consumer = invocation.getArgument(1, Consumer.class);
      features.forEach(d -> consumer.accept(d));
      return null;
    }).when(heatmapDataParser).parseData(any(), any());

    analysisChartService.createCharts(null, heatmaplist, parameters, progressBar);

    verify(dataParser, never()).parse(data1);
    verify(dataParser, never()).parse(data2);
    verify(dataParser, never()).parse(data3);
    verify(geneChartService, never()).geneChart(chartDataCaptor.capture(), eq(parameters));
    verify(geneChartService, never()).proportionChart(chartDataCaptor.capture(), eq(parameters));
    assertEquals(false, new File(temporaryFolder.getRoot(), "group_1.png").exists());
    assertEquals(false, new File(temporaryFolder.getRoot(), "group_2.png").exists());
    verify(heatmapListParser).parse(heatmaplist);
    verify(heatmapDataParser).parseMetadata(heatmapFiles.get(0).getFile());
    verify(heatmapDataParser).parseData(eq(heatmapFiles.get(0).getFile()), any());
    verify(heatmapDataParser).parseMetadata(heatmapFiles.get(1).getFile());
    verify(heatmapDataParser).parseData(eq(heatmapFiles.get(1).getFile()), any());
    verify(geneChartService, times(2)).heatmapChart(metadata, features, parameters);
    assertEquals(true,
        new File(temporaryFolder.getRoot(),
            "heatmap1_heatmap_" + filenameFeatureName1 + "-" + filenameFeatureNameLast + ".png")
                .exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(),
            "heatmap2_heatmap_" + filenameFeatureName1 + "-" + filenameFeatureNameLast + ".png")
                .exists());
  }
}

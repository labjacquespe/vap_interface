/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.HeatmapFeature;
import org.labjacquespe.vap.chart.HeatmapMetadata;
import org.labjacquespe.vap.chart.HeatmapWindow;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class HeatmapDataParserTest {
  private HeatmapDataParser heatmapDataParser;
  @Mock
  private Consumer<HeatmapFeature> dataConsumer;
  @Captor
  private ArgumentCaptor<HeatmapFeature> dataCaptor;
  @Mock
  private Consumer<String> errorHandler;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Locale locale;
  private Charset charset;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    charset = UTF_8;
    heatmapDataParser = new HeatmapDataParser(charset);
    locale = Locale.getDefault();
  }

  private void append(File file, String content) throws IOException {
    try (BufferedWriter writer =
        Files.newBufferedWriter(file.toPath(), charset, StandardOpenOption.APPEND)) {
      writer.write(content);
      writer.write("\n");
    }
  }

  @Test
  public void validate_BelowMinimumColumns() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "YAL038W\t#INV\t-0.300354");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_AboveMaximumColumns() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "YAL038W\t#INV\t-0.300354\tNA\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validate_EmptyColumn_WindowName() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\t\tW1_-950");
    append(file, "YAL038W\t#INV\t-0.300354\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptyColumn_FeatureName() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "\t#INV\t-0.300354\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptyColumn_WindowValue() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "YAL038W\t#INV\t\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptyColumn_WindowValueSeparator() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "YAL038W\t\t-0.300354\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_EmptyColumn_SkippedColumn() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\t\tInvalid Window\tW0_-1000\tW1_-950");
    append(file, "YAL038W\t\t#INV\t-0.300354\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validate_InvalidWindowName() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\t\tInvalid Window\tW0a_-1000\tW1_-950");
    append(file, "YAL038W\t\t#INV\t-0.300354\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validate_InvalidValue() throws Throwable {
    File file = temporaryFolder.newFile("heatmap.txt");
    append(file, "###groupe_ORF_8WG16_4rep_+2_3E8_WT_3rep__ratio_40050###");
    append(file, "\tInvalid Window\tW0_-1000\tW1_-950\tW2_-900");
    append(file, "YAL038W\t#INV\t-0.300354\tabc\tNA");

    heatmapDataParser.validate(file, locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void parseMetadata() throws Throwable {
    File file = new File(getClass().getResource("/chart/heatmap.txt").toURI());

    HeatmapMetadata metadata = heatmapDataParser.parseMetadata(file);

    assertEquals("heatmap", metadata.name);
    assertEquals(50.0, metadata.windowLength, 0.0001);
    assertEquals(11, metadata.featureCount);
    assertEquals(-1.16211, metadata.minHeat, 0.0001);
    assertEquals(5.25, metadata.maxHeat, 0.0001);
    List<HeatmapWindow> windows = metadata.windows;
    assertEquals(69, windows.size());
    HeatmapWindow window = windows.get(0);
    assertEquals("Invalid Window", window.getName());
    assertEquals(true, window.isSeparator());
    assertEquals(-1150, window.getX(), 0.1);
    assertEquals(null, window.getOriginalX());
    window = windows.get(3);
    assertEquals("W0_-1000", window.getName());
    assertEquals(false, window.isSeparator());
    assertEquals(-1000, window.getX(), 0.1);
    assertEquals(-1000, window.getOriginalX(), 0.1);
    window = windows.get(25);
    assertEquals("W2_100", window.getName());
    assertEquals(false, window.isSeparator());
    assertEquals(100, window.getX(), 0.1);
    assertEquals(100, window.getOriginalX(), 0.1);
    window = windows.get(33);
    assertEquals("Invalid Window", window.getName());
    assertEquals(true, window.isSeparator());
    assertEquals(500, window.getX(), 0.1);
    assertEquals(null, window.getOriginalX());
    window = windows.get(42);
    assertEquals("W16_800", window.getName());
    assertEquals(false, window.isSeparator());
    assertEquals(950, window.getX(), 0.1);
    assertEquals(800, window.getOriginalX(), 0.1);
  }

  @Test
  public void parseData() throws Throwable {
    File file = new File(getClass().getResource("/chart/heatmap.txt").toURI());

    heatmapDataParser.parseData(file, dataConsumer);

    verify(dataConsumer, times(11)).accept(dataCaptor.capture());
    assertEquals("YAL038W", dataCaptor.getAllValues().get(0).name);
    List<Double> heats = dataCaptor.getAllValues().get(0).heats;
    assertEquals(null, heats.get(0));
    assertEquals(-0.300354, heats.get(3), 0.00001);
    assertEquals(4.54999, heats.get(25), 0.00001);
    assertEquals(null, heats.get(33));
    assertEquals(null, heats.get(42));
    assertEquals("YAL037C-A", dataCaptor.getAllValues().get(2).name);
    heats = dataCaptor.getAllValues().get(2).heats;
    assertEquals(null, heats.get(0));
    assertEquals(null, heats.get(3));
    assertEquals(null, heats.get(25));
    assertEquals(null, heats.get(33));
    assertEquals(null, heats.get(42));
    assertEquals("YAL003W", dataCaptor.getAllValues().get(5).name);
    heats = dataCaptor.getAllValues().get(5).heats;
    assertEquals(null, heats.get(0));
    assertEquals(2.05652, heats.get(3), 0.00001);
    assertEquals(null, heats.get(25));
    assertEquals(null, heats.get(33));
    assertEquals(2.79889, heats.get(42), 0.00001);
  }
}

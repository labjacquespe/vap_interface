/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.HeatmapFile;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class HeatmapListParserTest {
  private HeatmapListParser heatmapListParser;
  @Mock
  public Consumer<String> errorHandler;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Locale locale;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    heatmapListParser = new HeatmapListParser(UTF_8);
    locale = Locale.getDefault();
  }

  @Test
  public void validate() throws Throwable {
    File file = temporaryFolder.newFile("heatmap_list.txt");

    heatmapListParser.validate(file, locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void parse_Empty() throws Throwable {
    File file = temporaryFolder.newFile("heatmap_list.txt");

    List<HeatmapFile> heatmaps = heatmapListParser.parse(file);

    assertEquals(0, heatmaps.size());
  }

  @Test
  @SuppressWarnings("checkstyle:linelength")
  public void parse() throws Throwable {
    File file = new File(this.getClass().getResource("/chart/heatmaplist.txt").toURI());

    List<HeatmapFile> heatmaps = heatmapListParser.parse(file);

    assertEquals(3, heatmaps.size());
    HeatmapFile heatmap = heatmaps.get(0);
    assertEquals("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_--1.txt_NWN",
        heatmap.getName());
    assertEquals(new File("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_--1.txt_NWN.txt"),
        heatmap.getFile());
    heatmap = heatmaps.get(1);
    assertEquals("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN",
        heatmap.getName());
    assertEquals(new File("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_-0.5-0.txt_NWN.txt"),
        heatmap.getFile());
    heatmap = heatmaps.get(2);
    assertEquals("agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_-1--0.5.txt_NWN",
        heatmap.getName());
    assertEquals(new File(
        "/users/poitrac/documents/agg_3E10_WT_3rep__ratio_40052_groupe_ORF_8WG16_4rep_-1--0.5.txt_NWN.txt"),
        heatmap.getFile());
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.Data;
import org.labjacquespe.vap.chart.DataGroup;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class DataParserTest {
  private DataParser dataParser;

  @Before
  public void beforeTest() {
    dataParser = new DataParser(UTF_8);
  }

  @Test
  public void parse() throws Throwable {
    File dataFile = new File(getClass().getResource("/chart/data.txt").toURI());

    List<DataGroup> groups = dataParser.parse(dataFile);

    assertEquals(3, groups.size());
    DataGroup group = groups.get(0);
    assertEquals("groupe_ORF_8WG16_4rep_+2.txt", group.getName());
    group = groups.get(1);
    assertEquals("groupe_ORF_8WG16_4rep_0.7-2.txt", group.getName());
    group = groups.get(2);
    assertEquals("groupe_ORF_8WG16_4rep_0-0.7.txt", group.getName());
    assertEquals(25, group.getDatas().size());
    group = groups.get(0);
    Data data = group.getDatas().get(0);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-850.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(1);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(-650.0, data.getX(), 0.1);
    assertEquals(-500.0, data.getOriginalX(), 0.1);
    assertEquals(-0.244158, data.getAverage(), 0.001);
    assertEquals(0.430716, data.getStandardDeviation(), 0.001);
    assertEquals(0.0288462, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
    data = group.getDatas().get(3);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-600.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(19);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(300.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(21);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(550.0, data.getX(), 0.1);
    assertEquals(400.0, data.getOriginalX(), 0.1);
    assertEquals(3.09168, data.getAverage(), 0.001);
    assertEquals(0.210323, data.getStandardDeviation(), 0.001);
    assertEquals(0.0448718, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
    group = groups.get(1);
    assertEquals(25, group.getDatas().size());
    data = group.getDatas().get(0);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-850.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(1);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(-650.0, data.getX(), 0.1);
    assertEquals(-500.0, data.getOriginalX(), 0.1);
    assertEquals(-0.137526, data.getAverage(), 0.001);
    assertEquals(0.483609, data.getStandardDeviation(), 0.001);
    assertEquals(0.0123305, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
    data = group.getDatas().get(3);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-600.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(19);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(300.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(21);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(550.0, data.getX(), 0.1);
    assertEquals(400.0, data.getOriginalX(), 0.1);
    assertEquals(1.18733, data.getAverage(), 0.001);
    assertEquals(0.10851, data.getStandardDeviation(), 0.001);
    assertEquals(0.0394575, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
    group = groups.get(2);
    assertEquals(25, group.getDatas().size());
    data = group.getDatas().get(0);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-850.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(1);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(-650.0, data.getX(), 0.1);
    assertEquals(-500.0, data.getOriginalX(), 0.1);
    assertEquals(-0.415095, data.getAverage(), 0.001);
    assertEquals(0.21275, data.getStandardDeviation(), 0.001);
    assertEquals(0.00803213, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
    data = group.getDatas().get(3);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(-600.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(19);
    assertEquals(Data.Type.SEPARATOR, data.getType());
    assertEquals(300.0, data.getX(), 0.1);
    assertEquals(null, data.getOriginalX());
    assertEquals(true, Double.isNaN(data.getAverage()));
    assertEquals(true, Double.isNaN(data.getStandardDeviation()));
    assertEquals(true, Double.isNaN(data.getProp()));
    assertEquals(200.0, data.getLength(), 0.1);
    data = group.getDatas().get(21);
    assertEquals(Data.Type.NORMAL, data.getType());
    assertEquals(550.0, data.getX(), 0.1);
    assertEquals(400.0, data.getOriginalX(), 0.1);
    assertEquals(0.323735, data.getAverage(), 0.001);
    assertEquals(0.0, data.getStandardDeviation(), 0.001);
    assertEquals(0.0546185, data.getProp(), 0.001);
    assertEquals(0.0, data.getLength(), 0.1);
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.TestFxTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@TestFxTestAnnotations
public class CategoryValueAxisTest extends ApplicationTest {
  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
  }

  @Test
  public void calculateTickValues_PositiveUnit() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 100, 10);
    List<Integer> ticks =
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange());
    assertEquals(11, ticks.size());
    assertEquals((Integer) 1, ticks.get(0));
    assertEquals((Integer) 11, ticks.get(1));
    assertEquals((Integer) 21, ticks.get(2));
    assertEquals((Integer) 31, ticks.get(3));
    assertEquals((Integer) 41, ticks.get(4));
    assertEquals((Integer) 51, ticks.get(5));
    assertEquals((Integer) 61, ticks.get(6));
    assertEquals((Integer) 71, ticks.get(7));
    assertEquals((Integer) 81, ticks.get(8));
    assertEquals((Integer) 91, ticks.get(9));
    assertEquals((Integer) 100, ticks.get(10));
  }

  @Test
  public void calculateTickValues_PerfectMatch() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 101, 10);
    List<Integer> ticks =
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange());
    assertEquals(11, ticks.size());
    assertEquals((Integer) 1, ticks.get(0));
    assertEquals((Integer) 11, ticks.get(1));
    assertEquals((Integer) 21, ticks.get(2));
    assertEquals((Integer) 31, ticks.get(3));
    assertEquals((Integer) 41, ticks.get(4));
    assertEquals((Integer) 51, ticks.get(5));
    assertEquals((Integer) 61, ticks.get(6));
    assertEquals((Integer) 71, ticks.get(7));
    assertEquals((Integer) 81, ticks.get(8));
    assertEquals((Integer) 91, ticks.get(9));
    assertEquals((Integer) 101, ticks.get(10));
  }

  @Test
  public void calculateTickValues_NegativeUnit() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 100, -10);
    List<Integer> ticks =
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange());
    assertEquals(2, ticks.size());
    assertEquals((Integer) 1, ticks.get(0));
    assertEquals((Integer) 100, ticks.get(1));
  }

  @Test
  public void calculateTickValues_SameBounds() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 1, 10);
    List<Integer> ticks =
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange());
    assertEquals(1, ticks.size());
    assertEquals((Integer) 1, ticks.get(0));
  }

  @Test
  public void calculateMinorTickMarks() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 100, 10);
    Set<Integer> majorTicks = new HashSet<>(
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange()));
    List<Integer> ticks = axis.calculateMinorTickMarks();
    assertEquals(5, axis.getMinorTickCount());
    int unit = axis.getTickUnit() / axis.getMinorTickCount();
    int lower = (int) axis.getLowerBound();
    int upper = (int) axis.getUpperBound();
    for (int tick = lower; tick < axis.getUpperBound(); tick += unit) {
      assertEquals("" + tick, !majorTicks.contains(tick) && tick + unit <= upper,
          ticks.contains(tick));
    }
  }

  @Test
  public void calculateMinorTickMarks_PerfectMatch() {
    CategoryValueAxis axis = new CategoryValueAxis(1, 101, 10);
    Set<Integer> majorTicks = new HashSet<>(
        axis.calculateTickValues(axis.getUpperBound() - axis.getLowerBound(), axis.getRange()));
    List<Integer> ticks = axis.calculateMinorTickMarks();
    assertEquals(5, axis.getMinorTickCount());
    int unit = axis.getTickUnit() / axis.getMinorTickCount();
    int lower = (int) axis.getLowerBound();
    int upper = (int) axis.getUpperBound();
    for (int tick = lower; tick < axis.getUpperBound(); tick += unit) {
      assertEquals("" + tick, !majorTicks.contains(tick) && tick + unit <= upper,
          ticks.contains(tick));
    }
  }

  @Test
  public void getTickMarkLabel() {
    ObservableMap<Integer, String> labels = FXCollections.observableMap(new HashMap<>());
    labels.put(1, "First");
    labels.put(11, "Second");
    labels.put(21, "Third");
    labels.put(30, "Fourth");
    CategoryValueAxis axis = new CategoryValueAxis(1, 30, 10, labels);
    assertEquals("First", axis.getTickMarkLabel(1));
    assertEquals("Second", axis.getTickMarkLabel(11));
    assertEquals("Third", axis.getTickMarkLabel(21));
    assertEquals("Fourth", axis.getTickMarkLabel(30));
  }
}

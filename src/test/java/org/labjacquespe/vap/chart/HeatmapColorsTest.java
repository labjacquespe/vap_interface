/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import static org.junit.Assert.assertEquals;

import java.util.List;
import javafx.scene.paint.Color;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class HeatmapColorsTest {
  @Test
  public void getColors_PurpleGreenRed() {
    List<Color> colors = HeatmapColors.RAINBOW.getColors();
    assertEquals(19, colors.size());
    assertEquals(Color.valueOf("#000066"), colors.get(0));
    assertEquals(Color.valueOf("#0000D1"), colors.get(2));
    assertEquals(Color.valueOf("#1F3DFF"), colors.get(4));
    assertEquals(Color.valueOf("#66CCFF"), colors.get(6));
    assertEquals(Color.valueOf("#66ED7A"), colors.get(8));
    assertEquals(Color.valueOf("#B2FF1A"), colors.get(10));
    assertEquals(Color.valueOf("#FFCC00"), colors.get(12));
    assertEquals(Color.valueOf("#FF4C00"), colors.get(14));
    assertEquals(Color.valueOf("#D90000"), colors.get(16));
    assertEquals(Color.valueOf("#800000"), colors.get(18));
  }
}

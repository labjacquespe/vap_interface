/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.chart;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.chart.GeneAxis.Region;
import org.labjacquespe.vap.chart.GeneAxis.RegionBox;
import org.labjacquespe.vap.chart.GeneAxis.ScaleLegend;
import org.labjacquespe.vap.chart.GeneAxis.Separator;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.test.config.TestFxTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@TestFxTestAnnotations
public class GeneAxisTest extends ApplicationTest {
  private Set<Separator> separators;

  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
  }

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    separators = new HashSet<>();
    separators.add(new Separator(-600, -400));
    separators.add(new Separator(300, 500));
  }

  @Test
  public void geneAxis() throws Throwable {
    GeneAxis geneAxis = new GeneAxis(-650, 650, separators);

    assertEquals(true, geneAxis.getStyleClass().contains("gene-axis"));
    assertEquals(null, geneAxis.getLabel());
    assertEquals(-650, geneAxis.getLowerBound(), 0.01);
    assertEquals(650, geneAxis.getUpperBound(), 0.01);
    Set<Separator> actualSeparators = geneAxis.getSeparators();
    assertEquals(separators.size(), actualSeparators.size());
    for (Separator separator : separators) {
      assertEquals(true, actualSeparators.contains(separator));
    }
    ScaleLegend scaleLegend = geneAxis.getScaleLegend();
    assertEquals(true, scaleLegend.getStyleClass().contains("scale-legend"));
    assertEquals(geneAxis.getDisplayPosition(650 - 500), scaleLegend.getTranslateX(), 1.0);
    assertEquals(0.0, scaleLegend.getTranslateY(), 1.0);
    assertEquals("500 bp", scaleLegend.getText());
    assertEquals((Long) 500L, scaleLegend.getLength());
  }

  @Test
  public void geneAxis_ScaleLegendLength() throws Throwable {
    GeneAxis geneAxis = new GeneAxis(-650, 650, separators);

    geneAxis.setScaleLegendLength(100L);

    ScaleLegend scaleLegend = geneAxis.getScaleLegend();
    assertEquals(true, scaleLegend.getStyleClass().contains("scale-legend"));
    assertEquals(geneAxis.getDisplayPosition(650 - 100L), scaleLegend.getTranslateX(), 1.0);
    assertEquals(0.0, scaleLegend.getTranslateY(), 1.0);
    assertEquals("100 bp", scaleLegend.getText());
    assertEquals((Long) 100L, scaleLegend.getLength());
  }

  @Test
  public void regions() throws Throwable {
    List<Region> regions = new ArrayList<>();
    regions.add(new Region(-650, -400, null));
    regions.add(new Region(-400, 0, null));
    regions.add(new Region(0, 600, null));
    regions.add(new Region(600, 650, null));
    regions.add(new Region(650, 650, null));

    GeneAxis geneAxis = new GeneAxis(-650, 650, separators, regions);

    List<RegionBox> regionBoxes = geneAxis.createRegionBoxes();
    assertEquals(5, regionBoxes.size());
    RegionBox regionBox = regionBoxes.get(0);
    assertEquals(-650.0, regionBox.getStart(), 0.1);
    assertEquals(-400.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(1);
    assertEquals(-400.0, regionBox.getStart(), 0.1);
    assertEquals(0.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(2);
    assertEquals(0.0, regionBox.getStart(), 0.1);
    assertEquals(600.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(3);
    assertEquals(600.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(4);
    assertEquals(650.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
  }

  @Test
  public void regions_no0() throws Throwable {
    List<Region> regions = new ArrayList<>();
    regions.add(new Region(-650, -400, null));
    regions.add(new Region(-400, 50, null));
    regions.add(new Region(50, 600, null));
    regions.add(new Region(600, 650, null));
    regions.add(new Region(650, 650, null));

    GeneAxis geneAxis = new GeneAxis(-650, 650, separators, regions);

    List<RegionBox> regionBoxes = geneAxis.createRegionBoxes();
    assertEquals(5, regionBoxes.size());
    RegionBox regionBox = regionBoxes.get(0);
    assertEquals(-650.0, regionBox.getStart(), 0.1);
    assertEquals(-400.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(1);
    assertEquals(-400.0, regionBox.getStart(), 0.1);
    assertEquals(50.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(2);
    assertEquals(50.0, regionBox.getStart(), 0.1);
    assertEquals(600.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(3);
    assertEquals(600.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(4);
    assertEquals(650.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
  }

  @Test
  public void regions_reference() throws Throwable {
    List<Region> regions = new ArrayList<>();
    regions.add(new Region(-650, -400, null));
    regions.add(new Region(-400, 50, null, true));
    regions.add(new Region(50, 600, null));
    regions.add(new Region(600, 650, null));
    regions.add(new Region(650, 650, null));

    GeneAxis geneAxis = new GeneAxis(-650, 650, separators, regions);

    List<RegionBox> regionBoxes = geneAxis.createRegionBoxes();
    assertEquals(5, regionBoxes.size());
    RegionBox regionBox = regionBoxes.get(0);
    assertEquals(-650.0, regionBox.getStart(), 0.1);
    assertEquals(-400.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(1);
    assertEquals(-400.0, regionBox.getStart(), 0.1);
    assertEquals(50.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(2);
    assertEquals(50.0, regionBox.getStart(), 0.1);
    assertEquals(600.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
    regionBox = regionBoxes.get(3);
    assertEquals(600.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(false, regionBox.isLine());
    regionBox = regionBoxes.get(4);
    assertEquals(650.0, regionBox.getStart(), 0.1);
    assertEquals(650.0, regionBox.getEnd(), 0.1);
    assertEquals(true, regionBox.isLine());
  }

  @Test
  public void regions_text() throws Throwable {
    final MessageResources resources = new MessageResources(GeneAxis.class, Locale.getDefault());

    List<Region> regions = new ArrayList<>();
    regions.add(new Region(-400, -300, BlockType.REFERENCE_FEATURE));
    regions.add(new Region(-300, -200, BlockType.ANNOTATION));
    regions.add(new Region(-200, -100, BlockType.INTER, true));
    regions.add(new Region(-100, 0, BlockType.REGION));
    regions.add(new Region(0, 100, BlockType.EXON));
    regions.add(new Region(100, 200, BlockType.INTRON));
    regions.add(new Region(200, 300, BlockType.UPSTREAM));
    regions.add(new Region(300, 400, BlockType.DOWNSTREAM));
    regions.add(new Region(400, 500, null));
    regions.add(new Region(500, 600, BlockType.EXON));
    regions.add(new Region(600, 700, BlockType.EXON));

    GeneAxis geneAxis = new GeneAxis(-650, 650, separators, regions);

    List<RegionBox> regionBoxes = geneAxis.createRegionBoxes();
    assertEquals(11, regionBoxes.size());
    for (RegionBox regionBox : regionBoxes) {
      regionBox.layoutChildren();
    }
    RegionBox regionBox = regionBoxes.get(0);
    assertEquals(resources.message("region.REFERENCE_FEATURE"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(1);
    assertEquals(resources.message("region.ANNOTATION"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(2);
    assertEquals(resources.message("region.INTER"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(3);
    assertEquals(resources.message("region.REGION"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(4);
    assertEquals(resources.message("region.EXON.0"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(5);
    assertEquals(resources.message("region.INTRON"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(6);
    assertEquals(resources.message("region.UPSTREAM"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(7);
    assertEquals(resources.message("region.DOWNSTREAM"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(8);
    assertEquals(true, regionBox.getText() == null || regionBox.getText().isEmpty());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(9);
    assertEquals(resources.message("region.EXON.1"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
    regionBox = regionBoxes.get(10);
    assertEquals(resources.message("region.EXON.2"), regionBox.getText());
    assertEquals(true, regionBox.isTextVisible());
  }

  @Test
  public void geneChart_RegionWidth() throws Throwable {
    List<Region> regions = new ArrayList<>();
    regions.add(new Region(-650, -400, null));
    regions.add(new Region(-400, 50, null));
    regions.add(new Region(50, 600, null));
    regions.add(new Region(600, 650, null));
    regions.add(new Region(650, 650, null));

    GeneAxis geneAxis = new GeneAxis(-650, 650, separators, regions);

    List<RegionBox> regionBoxes = geneAxis.createRegionBoxes();
    assertEquals(5, regionBoxes.size());
    RegionBox regionBox = regionBoxes.get(0);
    assertEquals(geneAxis.getDisplayPosition(-650), regionBox.getTranslateX(), 1.0);
    assertEquals(geneAxis.getDisplayPosition(150) - geneAxis.getDisplayPosition(0),
        regionBox.getRectangleWidth(), 1.0);
    regionBox = regionBoxes.get(1);
    assertEquals(geneAxis.getDisplayPosition(-400), regionBox.getTranslateX(), 1.0);
    assertEquals(geneAxis.getDisplayPosition(400) - geneAxis.getDisplayPosition(0),
        regionBox.getRectangleWidth(), 1.0);
    regionBox = regionBoxes.get(2);
    assertEquals(geneAxis.getDisplayPosition(0), regionBox.getTranslateX(), 1.0);
    assertEquals(geneAxis.getDisplayPosition(600) - geneAxis.getDisplayPosition(0),
        regionBox.getRectangleWidth(), 1.0);
    regionBox = regionBoxes.get(3);
    assertEquals(geneAxis.getDisplayPosition(600), regionBox.getTranslateX(), 1.0);
    assertEquals(geneAxis.getDisplayPosition(50) - geneAxis.getDisplayPosition(0),
        regionBox.getRectangleWidth(), 1.0);
    regionBox = regionBoxes.get(4);
    assertEquals(geneAxis.getDisplayPosition(650), regionBox.getTranslateX(), 1.0);
    assertEquals(geneAxis.getDisplayPosition(0) - geneAxis.getDisplayPosition(0),
        regionBox.getRectangleWidth(), 1.0);
  }
}

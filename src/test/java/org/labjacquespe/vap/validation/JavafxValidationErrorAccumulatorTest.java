/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.validation;

import static org.junit.Assert.assertEquals;

import java.util.List;
import org.junit.Test;

public class JavafxValidationErrorAccumulatorTest {
  @Test
  public void errors() {
    JavafxValidationErrorAccumulator accumulator = new JavafxValidationErrorAccumulator();
    accumulator.accept("test1");
    accumulator.accept("test2", "tooltip2");
    accumulator.accept("test3");
    accumulator.accept("test4", "tooltip4");

    List<JavafxValidationError> errors = accumulator.errors();

    assertEquals(4, errors.size());
    assertEquals("test1", errors.get(0).error);
    assertEquals(null, errors.get(0).tooltip);
    assertEquals("test2", errors.get(1).error);
    assertEquals("tooltip2", errors.get(1).tooltip);
    assertEquals("test3", errors.get(2).error);
    assertEquals(null, errors.get(2).tooltip);
    assertEquals("test4", errors.get(3).error);
    assertEquals("tooltip4", errors.get(3).tooltip);
  }
}

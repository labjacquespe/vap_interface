/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.message;

import static org.junit.Assert.assertEquals;

import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import org.junit.Test;

public class MessageResourcesXmlTest {
  @Test
  public void message() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.CANADA);

    String message = messageResource.message("message");

    assertEquals("This is a test", message);
  }

  @Test
  public void message_NoReplacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.CANADA);

    String message = messageResource.message("replacements");

    assertEquals("This is a test {0} {1} {2} {3}", message);
  }

  @Test
  public void message_Replacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.CANADA);

    String message = messageResource.message("replacements", "test", 1, 0.32,
        new GregorianCalendar(2015, 7, 24, 15, 23, 45).getTime());

    assertEquals("This is a test test second 32% 2015-08-24T15:23:45", message);
  }

  @Test
  public void message_Class() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class, Locale.CANADA);

    String message = messageResource.message("message");

    assertEquals("This is a test", message);
  }

  @Test
  public void message_Class_NoReplacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class, Locale.CANADA);

    String message = messageResource.message("replacements");

    assertEquals("This is a test {0} {1} {2} {3}", message);
  }

  @Test
  public void message_Class_Replacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class, Locale.CANADA);

    String message = messageResource.message("replacements", "test", 1, 0.32,
        new GregorianCalendar(2015, 7, 24, 15, 23, 45).getTime());

    assertEquals("This is a test test second 32% 2015-08-24T15:23:45", message);
  }

  @Test
  public void message_ResourceBundle() {
    ResourceBundle bundle =
        ResourceBundle.getBundle(MessageResourcesXmlTest.class.getName(), Locale.CANADA);
    MessageResources messageResource = new MessageResources(bundle);

    String message = messageResource.message("message");

    assertEquals("This is a test", message);
  }

  @Test
  public void message_ResourceBundle_NoReplacements() {
    ResourceBundle bundle =
        ResourceBundle.getBundle(MessageResourcesXmlTest.class.getName(), Locale.CANADA);
    MessageResources messageResource = new MessageResources(bundle);

    String message = messageResource.message("replacements");

    assertEquals("This is a test {0} {1} {2} {3}", message);
  }

  @Test
  public void message_ResourceBundle_Replacements() {
    ResourceBundle bundle =
        ResourceBundle.getBundle(MessageResourcesXmlTest.class.getName(), Locale.CANADA);
    MessageResources messageResource = new MessageResources(bundle);

    String message = messageResource.message("replacements", "test", 1, 0.32,
        new GregorianCalendar(2015, 7, 24, 15, 23, 45).getTime());

    assertEquals("This is a test test second 32% 2015-08-24T15:23:45", message);
  }

  @Test
  public void message_French() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.FRENCH);

    String message = messageResource.message("message");

    assertEquals("Ceci est un test", message);
  }

  @Test
  public void message_French_NoReplacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.FRENCH);

    String message = messageResource.message("replacements");

    assertEquals("Ceci est un test {0} {1} {2} {3}", message);
  }

  @Test
  public void message_French_Replacements() {
    MessageResources messageResource =
        new MessageResources(MessageResourcesXmlTest.class.getName(), Locale.FRENCH);

    String message = messageResource.message("replacements", "test", 1, 0.32,
        new GregorianCalendar(2015, 7, 24, 15, 23, 45).getTime());

    assertEquals("Ceci est un test test second 32% 2015-08-24T15:23:45", message);
  }
}

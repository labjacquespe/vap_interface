/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap;

import static org.junit.Assert.assertArrayEquals;
import static org.labjacquespe.vap.OperatingSystem.MAC;
import static org.labjacquespe.vap.OperatingSystem.OTHER;
import static org.labjacquespe.vap.OperatingSystem.WINDOWS;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class OperatingSystemServiceTest {
  private OperatingSystemService operatingSystemService;

  @Before
  public void beforeTest() {
    operatingSystemService = new OperatingSystemService();
  }

  @Test
  @Ignore("Cannot test since it would depend on the operating system")
  public void currentOs() throws Throwable {
  }

  @Test
  public void dataFileExtensions_Windows() {
    assertArrayEquals(new String[] { "*" }, operatingSystemService.dataFileExtensions(WINDOWS));
    assertArrayEquals(
        new String[] { "*", "*.txt", "*.bed", "*.tab", "*.wig", "*.bigWig", "*.bw", "*.bedGraph",
            "*.bg", "*.wiggle", "*.genePred", "*.gp", "*.gtf", "*.coord", "*.coordX", "*.coord1",
            "*.coord2", "*.coord3", "*.coord4", "*.coord5", "*.coord6" },
        operatingSystemService.dataFileExtensions(MAC));
    assertArrayEquals(new String[] { "*" }, operatingSystemService.dataFileExtensions(OTHER));
  }

  @Test
  public void is64bit() {
    // Cannot test since it would depend on the operating system.
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.function;

import static org.mockito.Mockito.verify;

import java.util.function.BiConsumer;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class OnceBiConsumerTest {
  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();
  @Mock
  private BiConsumer<String, String> consumer;

  @Test
  public void test() {
    OnceBiConsumer<String, String> once = new OnceBiConsumer<>(consumer);
    once.accept("test1", "1");
    once.accept("test2", "2");
    once.accept("test3", "3");

    verify(consumer).accept("test1", "1");
  }

  @Test
  public void test_Factory() {
    OnceBiConsumer<String, String> once = OnceBiConsumer.once(consumer);
    once.accept("test1", "1");
    once.accept("test2", "2");
    once.accept("test3", "3");

    verify(consumer).accept("test1", "1");
  }
}

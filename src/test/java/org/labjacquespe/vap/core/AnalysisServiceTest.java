/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.core.ExecutableService.VapEventListener;
import org.labjacquespe.vap.core.service.ParameterFileService;
import org.labjacquespe.vap.progressbar.ProgressBar;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class AnalysisServiceTest {
  private AnalysisService analysisServiceDefault;
  @Mock
  private ParameterFileService parameterFileService;
  @Mock
  private ExecutableService executableService;
  @Mock
  private ProgressBar progressBar;
  @Captor
  private ArgumentCaptor<AnalysisParameters> analysisParametersCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private AnalysisParameters parameters;
  private File uncompressed;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    analysisServiceDefault = new AnalysisService(parameterFileService, executableService);
    parameters = new AnalysisParameters();
    parameters.dataFiles = new ArrayList<>();
    parameters.referenceFiles = new ArrayList<>();
    parameters.outputFolder = temporaryFolder.getRoot();
    parameters.ouputFilePrefix = "unit_test";
    uncompressTestFiles();
  }

  private void uncompressTestFiles() throws IOException {
    uncompressed = temporaryFolder.newFolder("uncompressed");
    try (ZipInputStream zipInput =
        new ZipInputStream(getClass().getResourceAsStream("/test/test_files.zip"))) {
      ZipEntry entry;
      while ((entry = zipInput.getNextEntry()) != null) {
        try {
          File outputFile = new File(uncompressed, entry.getName());
          try (OutputStream output = new FileOutputStream(outputFile)) {
            IOUtils.copy(zipInput, output);
          }
        } finally {
          zipInput.closeEntry();
        }
      }
    }
  }

  private File getTestFile(String resource) {
    return new File(uncompressed, resource);
  }

  @Test
  public void mapGenes() throws Throwable {
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        VapEventListener listener = (VapEventListener) invocation.getArguments()[2];
        listener.groupHandler.accept("group1");
        listener.groupHandler.accept("group2");
        listener.datasetHandler.accept("dataset1");
        listener.datasetHandler.accept("dataset2");
        return null;
      }
    }).when(executableService).runVap(any(), any(), any());

    analysisServiceDefault.mapGenes(parameters, progressBar);

    File expectedParametersFile =
        new File(temporaryFolder.getRoot(), "unit_test_VAP_parameters.txt");
    verify(parameterFileService).export(eq(expectedParametersFile),
        analysisParametersCaptor.capture(), eq(false));
    AnalysisParameters analysisParameters = analysisParametersCaptor.getValue();
    assertSame(parameters, analysisParameters);
    verify(executableService).runVap(eq(temporaryFolder.getRoot()), eq(expectedParametersFile),
        any());
    verify(progressBar, atLeastOnce()).setProgress(anyDouble());
    verify(progressBar, atLeastOnce()).setTitle(any());
    verify(progressBar, atLeastOnce()).setMessage(any());
  }

  @Test
  public void mapGenes_NoPrefix() throws Throwable {
    parameters.ouputFilePrefix = "";

    analysisServiceDefault.mapGenes(parameters, progressBar);

    File expectedParametersFile = new File(temporaryFolder.getRoot(), "VAP_parameters.txt");
    verify(parameterFileService).export(eq(expectedParametersFile),
        analysisParametersCaptor.capture(), eq(false));
    verify(executableService).runVap(eq(temporaryFolder.getRoot()), eq(expectedParametersFile),
        any());
  }

  @Test
  public void mapGenes_TestData() throws Throwable {
    parameters.dataFiles.add(new File("H2AZ-vs-H2B_Robert_sacCer1.bedgraph"));
    parameters.dataFiles.add(new File("H3K36me3-vs-H3_Young_sacCer1.bedgraph"));
    parameters.dataFiles.add(new File("additional_data.txt"));
    parameters.genomeAnnotations = new File("SGDannot_20080202_on_sacCer1.genepred");
    parameters.referenceFiles.add(new File("group_genes_trxFreq_-1.txt"));
    parameters.referenceFiles.add(new File("group_genes_trxFreq_16-50.txt"));
    parameters.referenceFiles.add(new File("group_genes_trxFreq_2-4.txt"));
    parameters.dataFiles.add(new File("additional_group.txt"));
    parameters.testData = true;

    analysisServiceDefault.mapGenes(parameters, progressBar);

    assertEquals(true,
        new File(temporaryFolder.getRoot(), "H2AZ-vs-H2B_Robert_sacCer1.bedgraph").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "H3K36me3-vs-H3_Young_sacCer1.bedgraph").exists());
    assertEquals(false, new File(temporaryFolder.getRoot(), "additional_data.txt").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "SGDannot_20080202_on_sacCer1.genepred").exists());
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_-1.txt").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "group_genes_trxFreq_16-50.txt").exists());
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_2-4.txt").exists());
    assertEquals(false, new File(temporaryFolder.getRoot(), "additional_group.txt").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_-1.coord4").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_2-4.coord4").exists());
    assertEquals(getTestFile("H2AZ-vs-H2B_Robert_sacCer1.bedgraph").length(),
        new File(temporaryFolder.getRoot(), "H2AZ-vs-H2B_Robert_sacCer1.bedgraph").length());
    assertEquals(getTestFile("H3K36me3-vs-H3_Young_sacCer1.bedgraph").length(),
        new File(temporaryFolder.getRoot(), "H3K36me3-vs-H3_Young_sacCer1.bedgraph").length());
    assertEquals(getTestFile("SGDannot_20080202_on_sacCer1.genepred").length(),
        new File(temporaryFolder.getRoot(), "SGDannot_20080202_on_sacCer1.genepred").length());
    assertEquals(getTestFile("group_genes_trxFreq_-1.txt").length(),
        new File(temporaryFolder.getRoot(), "group_genes_trxFreq_-1.txt").length());
    assertEquals(getTestFile("group_genes_trxFreq_16-50.txt").length(),
        new File(temporaryFolder.getRoot(), "group_genes_trxFreq_16-50.txt").length());
    assertEquals(getTestFile("group_genes_trxFreq_2-4.txt").length(),
        new File(temporaryFolder.getRoot(), "group_genes_trxFreq_2-4.txt").length());
    assertEquals(getTestFile("coord_group_genes_trxFreq_-1.coord4").length(),
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_-1.coord4").length());
    assertEquals(getTestFile("coord_group_genes_trxFreq_2-4.coord4").length(),
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_2-4.coord4").length());
  }

  @Test
  public void mapGenes_TestData_Exists() throws Throwable {
    temporaryFolder.newFile("H2AZ-vs-H2B_Robert_sacCer1.bedgraph");
    temporaryFolder.newFile("H3K36me3-vs-H3_Young_sacCer1.bedgraph");
    temporaryFolder.newFile("SGDannot_20080202_on_sacCer1.genepred");
    temporaryFolder.newFile("group_genes_trxFreq_-1.txt");
    temporaryFolder.newFile("group_genes_trxFreq_16-50.txt");
    temporaryFolder.newFile("group_genes_trxFreq_2-4.txt");
    temporaryFolder.newFile("coord_group_genes_trxFreq_-1.coord4");
    temporaryFolder.newFile("coord_group_genes_trxFreq_2-4.coord4");
    parameters.dataFiles.add(new File("H2AZ-vs-H2B_Robert_sacCer1.bedgraph"));
    parameters.dataFiles.add(new File("H3K36me3-vs-H3_Young_sacCer1.bedgraph"));
    parameters.genomeAnnotations = new File("SGDannot_20080202_on_sacCer1.genepred");
    parameters.referenceFiles.add(new File("group_genes_trxFreq_-1.txt"));
    parameters.referenceFiles.add(new File("group_genes_trxFreq_16-50.txt"));
    parameters.referenceFiles.add(new File("group_genes_trxFreq_2-4.txt"));
    parameters.testData = true;

    analysisServiceDefault.mapGenes(parameters, progressBar);

    assertEquals(true,
        new File(temporaryFolder.getRoot(), "H2AZ-vs-H2B_Robert_sacCer1.bedgraph").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "H3K36me3-vs-H3_Young_sacCer1.bedgraph").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "SGDannot_20080202_on_sacCer1.genepred").exists());
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_-1.txt").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "group_genes_trxFreq_16-50.txt").exists());
    assertEquals(true, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_2-4.txt").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_-1.coord4").exists());
    assertEquals(true,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_2-4.coord4").exists());
    assertEquals(0,
        new File(temporaryFolder.getRoot(), "H2AZ-vs-H2B_Robert_sacCer1.bedgraph").length());
    assertEquals(0,
        new File(temporaryFolder.getRoot(), "H3K36me3-vs-H3_Young_sacCer1.bedgraph").length());
    assertEquals(0,
        new File(temporaryFolder.getRoot(), "SGDannot_20080202_on_sacCer1.genepred").length());
    assertEquals(0, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_-1.txt").length());
    assertEquals(0, new File(temporaryFolder.getRoot(), "group_genes_trxFreq_16-50.txt").length());
    assertEquals(0,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_-1.coord4").length());
    assertEquals(0,
        new File(temporaryFolder.getRoot(), "coord_group_genes_trxFreq_2-4.coord4").length());
  }

  @Test
  public void mapGenes_TestData_NotEmptyPath() throws Throwable {
    parameters.dataFiles.add(new File("data/H2AZ-vs-H2B_Robert_sacCer1.bedgraph"));
    parameters.testData = true;

    analysisServiceDefault.mapGenes(parameters, progressBar);

    assertEquals(false,
        new File(temporaryFolder.getRoot(), "data/H2AZ-vs-H2B_Robert_sacCer1.bedgraph").exists());
  }
}

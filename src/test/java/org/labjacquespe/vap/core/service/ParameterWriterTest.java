/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import static org.labjacquespe.vap.core.service.ParameterFileService.OUTPUT_DIRECTORY;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Writer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ParameterWriterTest {
  private ParameterWriter parameterWriter;
  @Mock
  private Writer writer;

  @Before
  public void beforeTest() {
    parameterWriter = new ParameterWriter(writer);
  }

  @Test
  public void write() throws Throwable {
    parameterWriter.writeOther("~~ TGAP (The General Aggregate Profiler) configuration file");
    parameterWriter.writeVersion("1.0.0");
    parameterWriter.writeOther("");
    parameterWriter.writeParameter("interface_layout", "B");
    parameterWriter.writeOther("abc");
    parameterWriter.writeOther("output_directory { path }");
    parameterWriter
        .writeOther(" Directory where all the files (including param and log) will be generated");
    parameterWriter.writeParameter(OUTPUT_DIRECTORY, "output");

    verify(writer, times(16)).write(anyString());
    InOrder inOrder = inOrder(writer);
    inOrder.verify(writer).write("~~ TGAP (The General Aggregate Profiler) configuration file");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("~~version 1.0.0");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("~~@interface_layout=B");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("abc");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("output_directory { path }");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer)
        .write(" Directory where all the files (including param and log) will be generated");
    inOrder.verify(writer).write(System.lineSeparator());
    inOrder.verify(writer).write("~~@output_directory=output");
    inOrder.verify(writer).write(System.lineSeparator());
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import static org.junit.Assert.assertEquals;
import static org.labjacquespe.vap.core.service.ParameterFileService.AGGREGATE_DATA_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_METHOD;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_MODE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANNOTATIONS_PATH;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANNOTATION_COORDINATES_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_VALUE;
import static org.labjacquespe.vap.core.service.ParameterFileService.DATASET_CHUNK_SIZE;
import static org.labjacquespe.vap.core.service.ParameterFileService.DATASET_PATH;
import static org.labjacquespe.vap.core.service.ParameterFileService.DISPLAY_DISPERSION_VALUES;
import static org.labjacquespe.vap.core.service.ParameterFileService.EXCLUSION_PATH;
import static org.labjacquespe.vap.core.service.ParameterFileService.FIRST_PT_BOUNDARY;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_AGGREGATE_GRAPHS;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_HEATMAP;
import static org.labjacquespe.vap.core.service.ParameterFileService.MEAN_DISPERSION_VALUE;
import static org.labjacquespe.vap.core.service.ParameterFileService.MERGE_MID_INTRONS;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_DATASET;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_GROUP;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_ORIENTATION;
import static org.labjacquespe.vap.core.service.ParameterFileService.ORIENTATION_SUBGROUPS;
import static org.labjacquespe.vap.core.service.ParameterFileService.OUTPUT_DIRECTORY;
import static org.labjacquespe.vap.core.service.ParameterFileService.PREFIX_FILENAME;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_DATA_BY_CHUNK;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_MISSING_DATA;
import static org.labjacquespe.vap.core.service.ParameterFileService.REFERENCE_POINTS;
import static org.labjacquespe.vap.core.service.ParameterFileService.REFGROUP_PATH;
import static org.labjacquespe.vap.core.service.ParameterFileService.SELECTION_PATH;
import static org.labjacquespe.vap.core.service.ParameterFileService.SMOOTHING_WINDOWS;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOWS_PER_BLOCK;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOW_SIZE;
import static org.labjacquespe.vap.core.service.ParameterFileService.WRITE_INDIVIDUAL_REFERENCES;
import static org.labjacquespe.vap.core.service.ParameterFileService.Y_AXIS_SCALE;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ParameterParserTest {
  private ParameterParser parameterParser;
  private ParameterHandlers handlers;
  @Mock
  private Consumer<String> versionHandler;
  @Mock
  private BiConsumer<String, String> parameterHandler;
  @Mock
  private Consumer<String> otherHandler;
  @Captor
  private ArgumentCaptor<String> lineCaptor;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    parameterParser = new ParameterParser();
    handlers = new ParameterHandlers();
    handlers.versionHandler = versionHandler;
    handlers.parameterHandler = parameterHandler;
    handlers.otherHandler = otherHandler;
  }

  @Test
  public void parse_Reader() throws Throwable {
    try (Reader reader =
        new InputStreamReader(getClass().getResourceAsStream("/file/parameters.txt"))) {
      parameterParser.parse(reader, handlers);
    }

    verify();
  }

  @SuppressWarnings("checkstyle:linelength")
  private void verify() throws Throwable {
    Mockito.verify(versionHandler).accept("1.0.0-beta5");
    Mockito.verify(otherHandler, times(476)).accept(lineCaptor.capture());
    Mockito.verify(parameterHandler, times(34)).accept(anyString(), anyString());
    assertEquals("\t\t\t---------------------------------------------------",
        lineCaptor.getAllValues().get(0));
    assertEquals(
        "~~IMPORTANT: Format must be \"parameter_name=value\" where accepted values are inside []",
        lineCaptor.getAllValues().get(7));
    assertEquals(
        "      * (A)nnotation: the reference features contained in the {{{refgroup_path}reference groups}} are annotation name (unique identifier) from a provided {{{annotations_path}annotations file}}, and the {{{reference_points}reference points}} are derived from the annotation boundaries (e.g. a reference feature is a gene where it's start and end coordinates correspond to two reference points). The orientation of the flanking annotations is used to create {{{orientation_subgroups}orientation subsets}} if requested.",
        lineCaptor.getAllValues().get(30));

    InOrder inOrder = inOrder(versionHandler, parameterHandler, otherHandler);
    inOrder.verify(otherHandler, times(6)).accept(anyString());
    inOrder.verify(versionHandler).accept("1.0.0-beta5");
    inOrder.verify(otherHandler, times(17)).accept(anyString());
    inOrder.verify(parameterHandler).accept(OUTPUT_DIRECTORY, "output");
    inOrder.verify(otherHandler, times(15)).accept(anyString());
    inOrder.verify(parameterHandler).accept(ANALYSIS_MODE, "A");
    inOrder = inOrder(parameterHandler);
    inOrder.verify(parameterHandler).accept(OUTPUT_DIRECTORY, "output");
    inOrder.verify(parameterHandler).accept(ANALYSIS_MODE, "A");
    inOrder.verify(parameterHandler).accept(DATASET_PATH, "dataset.txt");
    inOrder.verify(parameterHandler).accept(REFGROUP_PATH, "refGroup.txt");
    inOrder.verify(parameterHandler).accept(PROCESS_MISSING_DATA, "0");
    inOrder.verify(parameterHandler).accept(PROCESS_DATA_BY_CHUNK, "0");
    inOrder.verify(parameterHandler).accept(DATASET_CHUNK_SIZE, "100");
    inOrder.verify(parameterHandler).accept(ANNOTATIONS_PATH, "test_dataset.txt");
    inOrder.verify(parameterHandler).accept(SELECTION_PATH, "selection.txt");
    inOrder.verify(parameterHandler).accept(EXCLUSION_PATH, "exclusion.txt");
    inOrder.verify(parameterHandler).accept(ANNOTATION_COORDINATES_TYPE, "T");
    inOrder.verify(parameterHandler).accept(ANALYSIS_METHOD, "A");
    inOrder.verify(parameterHandler).accept(REFERENCE_POINTS, "3");
    inOrder.verify(parameterHandler).accept(FIRST_PT_BOUNDARY, "5");
    inOrder.verify(parameterHandler).accept(MERGE_MID_INTRONS, "F");
    inOrder.verify(parameterHandler).accept(WINDOW_SIZE, "50");
    inOrder.verify(parameterHandler).accept(WINDOWS_PER_BLOCK, "10;5;10;5");
    inOrder.verify(parameterHandler).accept(BLOCK_ALIGNMENT, "R;S;S;L");
    inOrder.verify(parameterHandler).accept(BLOCK_SPLIT_TYPE, "N;P;P;A");
    inOrder.verify(parameterHandler).accept(BLOCK_SPLIT_VALUE, "0;50;50;100");
    inOrder.verify(parameterHandler).accept(BLOCK_SPLIT_ALIGNMENT, "N;L;L;R;");
    inOrder.verify(parameterHandler).accept(SMOOTHING_WINDOWS, "6");
    inOrder.verify(parameterHandler).accept(AGGREGATE_DATA_TYPE, "E");
    inOrder.verify(parameterHandler).accept(DISPLAY_DISPERSION_VALUES, "1");
    inOrder.verify(parameterHandler).accept(MEAN_DISPERSION_VALUE, "E");
    inOrder.verify(parameterHandler).accept(PREFIX_FILENAME, "christian");
    inOrder.verify(parameterHandler).accept(WRITE_INDIVIDUAL_REFERENCES, "1");
    inOrder.verify(parameterHandler).accept(GENERATE_HEATMAP, "1");
    inOrder.verify(parameterHandler).accept(ONE_GRAPH_PER_GROUP, "1");
    inOrder.verify(parameterHandler).accept(GENERATE_AGGREGATE_GRAPHS, "1");
    inOrder.verify(parameterHandler).accept(ONE_GRAPH_PER_DATASET, "1");
    inOrder.verify(parameterHandler).accept(ONE_GRAPH_PER_ORIENTATION, "1");
    inOrder.verify(parameterHandler).accept(ORIENTATION_SUBGROUPS, "1;0;1;1;0;0;1;0;1");
    inOrder.verify(parameterHandler).accept(Y_AXIS_SCALE, "-20;-10");
  }
}

/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.service;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.labjacquespe.vap.chart.GeneChartParameters.DEFAULT_BOUNDARY;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_AGGREGATE_VALUE_TYPE;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_COORDINATES_TYPE;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_DISPERSION_TYPE;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_MISSING_DATA_REPLACEMENT;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_ONE_DATA_FILE_PER_GRAPH;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_ONE_ORIENTATION_PER_GRAPH;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_OUTPUT_GRAPHS;
import static org.labjacquespe.vap.core.AnalysisParameters.DEFAULT_SMOOTH_DATA_WINDOWS;
import static org.labjacquespe.vap.core.service.ParameterFileService.AGGREGATE_DATA_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_METHOD;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANALYSIS_MODE;
import static org.labjacquespe.vap.core.service.ParameterFileService.ANNOTATION_COORDINATES_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_ALIGNMENT;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_TYPE;
import static org.labjacquespe.vap.core.service.ParameterFileService.BLOCK_SPLIT_VALUE;
import static org.labjacquespe.vap.core.service.ParameterFileService.DATASET_CHUNK_SIZE;
import static org.labjacquespe.vap.core.service.ParameterFileService.DISPLAY_DISPERSION_VALUES;
import static org.labjacquespe.vap.core.service.ParameterFileService.FIRST_PT_BOUNDARY;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_AGGREGATE_GRAPHS;
import static org.labjacquespe.vap.core.service.ParameterFileService.GENERATE_HEATMAP;
import static org.labjacquespe.vap.core.service.ParameterFileService.MEAN_DISPERSION_VALUE;
import static org.labjacquespe.vap.core.service.ParameterFileService.MERGE_MID_INTRONS;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_DATASET;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_GROUP;
import static org.labjacquespe.vap.core.service.ParameterFileService.ONE_GRAPH_PER_ORIENTATION;
import static org.labjacquespe.vap.core.service.ParameterFileService.ORIENTATION_SUBGROUPS;
import static org.labjacquespe.vap.core.service.ParameterFileService.OUTPUT_DIRECTORY;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_DATA_BY_CHUNK;
import static org.labjacquespe.vap.core.service.ParameterFileService.PROCESS_MISSING_DATA;
import static org.labjacquespe.vap.core.service.ParameterFileService.REFERENCE_POINTS;
import static org.labjacquespe.vap.core.service.ParameterFileService.SMOOTHING_WINDOWS;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOWS_PER_BLOCK;
import static org.labjacquespe.vap.core.service.ParameterFileService.WINDOW_SIZE;
import static org.labjacquespe.vap.core.service.ParameterFileService.WRITE_INDIVIDUAL_REFERENCES;
import static org.labjacquespe.vap.core.service.ParameterFileService.Y_AXIS_SCALE;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.MainConfiguration;
import org.labjacquespe.vap.chart.BlockType;
import org.labjacquespe.vap.chart.service.GeneChartService;
import org.labjacquespe.vap.core.AggregateValueType;
import org.labjacquespe.vap.core.AnalysisParameters;
import org.labjacquespe.vap.core.Block;
import org.labjacquespe.vap.core.BlockAlignment;
import org.labjacquespe.vap.core.Boundary;
import org.labjacquespe.vap.core.CoordinatesType;
import org.labjacquespe.vap.core.DispersionType;
import org.labjacquespe.vap.core.Graph;
import org.labjacquespe.vap.core.MergeMiddleIntron;
import org.labjacquespe.vap.core.MissingDataReplacement;
import org.labjacquespe.vap.core.ReferenceType;
import org.labjacquespe.vap.core.RepresentationType;
import org.labjacquespe.vap.core.SplitAlignment;
import org.labjacquespe.vap.core.SplitType;
import org.labjacquespe.vap.core.YAxisScale;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;
import org.labjacquespe.vap.core.version.ParameterFileVersionService;
import org.labjacquespe.vap.message.MessageResources;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ParameterFileServiceTest {
  private ParameterFileService parameterFileService;
  @Mock
  private ParameterFileVersionService parameterFileVersionService;
  @Mock
  private GeneChartService geneChartService;
  @Mock
  private ParameterParser parameterParser;
  @Mock
  private Consumer<String> errorHandler;
  @Captor
  private ArgumentCaptor<String> lineCaptor;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  @Inject
  private MainConfiguration mainConfiguration;
  private AnalysisParameters analysisParameters;
  private File file;
  private File outputFolder;
  private Locale locale;
  private MessageResources resources;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Reader reader = (Reader) invocation.getArguments()[0];
        Writer writer = (Writer) invocation.getArguments()[1];
        IOUtils.copy(reader, writer);
        return null;
      }
    }).when(parameterFileVersionService).updateParameters(any(), any(), any());
    ParameterParser parameterParser = new ParameterParser();
    parameterFileService = new ParameterFileService(parameterFileVersionService, parameterParser,
        geneChartService, UTF_8);
    locale = Locale.getDefault();
    resources = new MessageResources(ParameterFileService.class, locale);
    analysisParameters = new AnalysisParameters();
    analysisParameters.outputFolder = temporaryFolder.getRoot();
    analysisParameters.geneChartParameters.referenceType = ReferenceType.ANNOTATIONS;
    analysisParameters.processDataByChunk = true;
    analysisParameters.dataChunkSize = 100;
    analysisParameters.missingDataReplacement = MissingDataReplacement.ZERO;
    analysisParameters.genomeAnnotations = new File("dataset.txt");
    analysisParameters.selectionAnnotationFilter = new File("selection_w.txt");
    analysisParameters.exclusionAnnotationFilter = new File("exclusion_w.txt");
    analysisParameters.coordinatesType = CoordinatesType.CDS;
    analysisParameters.mergeMiddleIntron = MergeMiddleIntron.LAST;
    analysisParameters.dataFiles = Arrays.asList(new File("data1.txt"), new File("data2.txt"));
    analysisParameters.referenceFiles =
        Arrays.asList(new File("reference1.txt"), new File("reference2.txt"));
    analysisParameters.geneChartParameters.representationType = RepresentationType.ABSOLUTE;
    analysisParameters.geneChartParameters.referencePoints = 5;
    analysisParameters.geneChartParameters.boundary = Boundary.THREE_PRIME;
    analysisParameters.geneChartParameters.windowSize = 55;
    analysisParameters.geneChartParameters.blocks = new ArrayList<>();
    Block block =
        new Block(20L, BlockAlignment.LEFT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block = new Block(10L, BlockAlignment.RIGHT, null, null, null, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(5L, BlockAlignment.SPLIT, SplitType.PERCENTAGE, 50L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(10L, BlockAlignment.LEFT, SplitType.ABSOLUTE, 100L, SplitAlignment.RIGHT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(5L, BlockAlignment.LEFT, SplitType.ABSOLUTE, 100L, SplitAlignment.RIGHT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(20L, BlockAlignment.RIGHT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    analysisParameters.smoothDataWindows = 7;
    analysisParameters.aggregateValueType = AggregateValueType.MEDIAN;
    analysisParameters.ouputFilePrefix = "christian";
    analysisParameters.outputGraphs = new HashSet<>();
    analysisParameters.outputGraphs.add(Graph.ANY_ANY);
    analysisParameters.outputGraphs.add(Graph.ANY_CONV);
    analysisParameters.outputGraphs.add(Graph.DIV_ANY);
    analysisParameters.outputGraphs.add(Graph.ANY_TAND);
    analysisParameters.outputGraphs.add(Graph.TAND_CONV);
    analysisParameters.outputGraphs.add(Graph.DIV_CONV);
    analysisParameters.dispersionType = DispersionType.SD;
    analysisParameters.geneChartParameters.dispersion = true;
    analysisParameters.geneChartParameters.generateAggregateGraphs = true;
    analysisParameters.oneDataFilePerGraph = false;
    analysisParameters.oneReferenceGroupPerGraph = false;
    analysisParameters.oneOrientationPerGraph = false;
    analysisParameters.outputData = false;
    analysisParameters.geneChartParameters.generateHeatmap = false;
    analysisParameters.geneChartParameters.yaxisScale = new YAxisScale(10.0, 20.0);
    when(geneChartService.regionType(any(Integer.class), any(ReferenceType.class),
        any(Integer.class))).thenReturn(BlockType.REFERENCE_FEATURE);
    if (SystemUtils.IS_OS_WINDOWS) {
      outputFolder = new File("c:/output");
    } else {
      outputFolder = new File("/output");
    }
  }

  public void mockParameterParser() {
    parameterFileService = new ParameterFileService(parameterFileVersionService, parameterParser,
        geneChartService, UTF_8);
  }

  /**
   * After test.
   */
  @After
  public void afterTest() throws Throwable {
    if (file != null) {
      file.delete();
    }
  }

  private void writeFile(String content) throws IOException {
    file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      writer.write(content);
    }
  }

  private String toSystem(String path) {
    return FilenameUtils.separatorsToSystem(path);
  }

  @Test
  public void validate_Version() throws Throwable {
    writeFile("");

    parameterFileService.validate(file, locale, errorHandler);

    verify(parameterFileVersionService).updateParameters(any(), any(), any());
  }

  @Test
  public void validate_MissingParameters() throws Throwable {
    writeFile("");

    parameterFileService.validate(file, Locale.ENGLISH, errorHandler);

    verify(errorHandler, times(34)).accept(errorCaptor.capture());
    for (String error : errorCaptor.getAllValues()) {
      assertNotNull(error);
      assertTrue(error.contains("missing"));
    }
  }

  @Test
  public void validate_MissingFile() throws Throwable {
    File file = new File(temporaryFolder.getRoot(), "to_delete.txt");

    try {
      parameterFileService.validate(file, locale, errorHandler);
      fail("Expected IOException");
    } catch (IOException e) {
      // Success.
    }
  }

  @Test
  public void validate_InvalidReferenceType() throws Throwable {
    writeFile("~~@analysis_mode=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_REFERENCE_TYPE", ANALYSIS_MODE)));
  }

  @Test
  public void validate_RelativeOutput() throws Throwable {
    writeFile("~~@output_directory=myPath/VAP_results/");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("RELATIVE_OUTPUT_FOLDER", OUTPUT_DIRECTORY)));
  }

  @Test
  public void validate_InvalidMissingDataReplacement() throws Throwable {
    writeFile("~~@process_missing_data=a");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_PROCESS_MISSING_DATA", PROCESS_MISSING_DATA)));
  }

  @Test
  public void validate_InvalidProcessDataByChunk() throws Throwable {
    writeFile("~~@process_data_by_chunk=a");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_PROCESS_DATA_BY_CHUNK", PROCESS_DATA_BY_CHUNK)));
  }

  @Test
  public void validate_InvalidDataChunkSize() throws Throwable {
    writeFile("~~@dataset_chunk_size=a");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_DATA_CHUNK_SIZE", DATASET_CHUNK_SIZE)));
  }

  @Test
  public void validate_BelowMinimumDataChunkSize() throws Throwable {
    writeFile("~~@dataset_chunk_size=0");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_DATA_CHUNK_SIZE", DATASET_CHUNK_SIZE, 1)));
  }

  @Test
  public void validate_InvalidCoordinatesType() throws Throwable {
    writeFile("~~@annotation_coordinates_type=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_COORDINATES_TYPE", ANNOTATION_COORDINATES_TYPE)));
  }

  @Test
  public void validate_CoordinatesType_N() throws Throwable {
    writeFile("~~@annotation_coordinates_type=N");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_COORDINATES_TYPE", ANNOTATION_COORDINATES_TYPE)));
  }

  @Test
  public void validate_InvalidMergeMiddleIntron() throws Throwable {
    writeFile("~~@merge_mid_introns=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_MERGE_MIDDLE_INTRON", MERGE_MID_INTRONS)));
  }

  @Test
  public void validate_InvalidRepresentationType() throws Throwable {
    writeFile("~~@analysis_method=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_REPRESENTATION_TYPE", ANALYSIS_METHOD)));
  }

  @Test
  public void validate_InvalidReferencePoints() throws Throwable {
    writeFile("~~@reference_points=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_REFERENCE_POINTS", REFERENCE_POINTS)));
  }

  @Test
  public void validate_BelowMinimumReferencePoints_Annotations() throws Throwable {
    writeFile("~~@analysis_mode=A\n~~@reference_points=0");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 1)));
  }

  @Test
  public void validate_BelowMinimumReferencePoints_Annotations_Reverse() throws Throwable {
    writeFile("~~@reference_points=0\n~~@analysis_mode=A");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 1)));
  }

  @Test
  public void validate_BelowMinimumReferencePoints_Coordinates() throws Throwable {
    writeFile("~~@analysis_mode=C\n~~@reference_points=0");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 1)));
  }

  @Test
  public void validate_BelowMinimumReferencePoints_Exons() throws Throwable {
    writeFile("~~@analysis_mode=E\n~~@reference_points=5");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 6)));
  }

  @Test
  public void validate_AboveMaximumReferencePoints_Annotations() throws Throwable {
    writeFile("~~@analysis_mode=A\n~~@reference_points=7");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("ABOVE_MAXIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 6)));
  }

  @Test
  public void validate_AboveMaximumReferencePoints_Annotations_Reverse() throws Throwable {
    writeFile("~~@reference_points=7\n~~@analysis_mode=A");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("ABOVE_MAXIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 6)));
  }

  @Test
  public void validate_AboveMaximumReferencePoints_Coordinates() throws Throwable {
    writeFile("~~@analysis_mode=C\n~~@reference_points=7");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("ABOVE_MAXIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 6)));
  }

  @Test
  public void validate_AboveMaximumReferencePoints_Exons() throws Throwable {
    writeFile("~~@analysis_mode=E\n~~@reference_points=7");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("ABOVE_MAXIMUM_REFERENCE_POINTS", REFERENCE_POINTS, 6)));
  }

  @Test
  public void validate_InvalidBoundary() throws Throwable {
    writeFile("~~@1pt_boundary=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BOUNDARY", FIRST_PT_BOUNDARY)));
  }

  @Test
  public void validate_InvalidWindowSize() throws Throwable {
    writeFile("~~@window_size=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(
        errorCaptor.getAllValues().contains(resources.message("INVALID_WINDOW_SIZE", WINDOW_SIZE)));
  }

  @Test
  public void validate_BelowMinimumWindowSize() throws Throwable {
    writeFile("~~@window_size=-1");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_WINDOW_SIZE", WINDOW_SIZE, 0)));
  }

  @Test
  public void validate_ValidBlockWindow() throws Throwable {
    writeFile("~~@windows_per_block=10;5;10;10");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_WINDOW", WINDOWS_PER_BLOCK)));
  }

  @Test
  public void validate_InvalidBlockWindow() throws Throwable {
    writeFile("~~@windows_per_block=10;5;10;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_WINDOW", WINDOWS_PER_BLOCK)));
  }

  @Test
  public void validate_BlockWindowCountMissmatch() throws Throwable {
    writeFile("~~@reference_points=2\n~~@windows_per_block=10");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_WINDOW_COUNT_MISSMATCH", WINDOWS_PER_BLOCK, 2)));
  }

  @Test
  public void validate_BlockWindowCountMissmatch_Reverse() throws Throwable {
    writeFile("~~@windows_per_block=10\n~~@reference_points=2");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_WINDOW_COUNT_MISSMATCH", WINDOWS_PER_BLOCK, 2)));
  }

  @Test
  public void validate_BelowMinimumBlockWindow() throws Throwable {
    writeFile("~~@windows_per_block=10;5;10;-1");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_BLOCK_WINDOW", WINDOWS_PER_BLOCK, 0)));
  }

  @Test
  public void validate_ValidBlockAlignment() throws Throwable {
    writeFile("~~@block_alignment=R;S;S;L");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_ALIGNMENT", BLOCK_ALIGNMENT)));
  }

  @Test
  public void validate_InvalidBlockAlignment() throws Throwable {
    writeFile("~~@block_alignment=R;S;S;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_ALIGNMENT", BLOCK_ALIGNMENT)));
  }

  @Test
  public void validate_BlockAlignmentCountMissmatch() throws Throwable {
    writeFile("~~@reference_points=2\n~~@block_alignment=R");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_ALIGNMENT_COUNT_MISSMATCH", BLOCK_ALIGNMENT, 2)));
  }

  @Test
  public void validate_BlockAlignmentCountMissmatch_Reverse() throws Throwable {
    writeFile("~~@block_alignment=R\n~~@reference_points=2");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_ALIGNMENT_COUNT_MISSMATCH", BLOCK_ALIGNMENT, 2)));
  }

  @Test
  public void validate_ValidBlockSplitType() throws Throwable {
    writeFile("~~@block_split_type=N;P;P;A");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT_TYPE", BLOCK_SPLIT_TYPE)));
  }

  @Test
  public void validate_InvalidBlockSplitType() throws Throwable {
    writeFile("~~@block_split_type=N;P;P;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT_TYPE", BLOCK_SPLIT_TYPE)));
  }

  @Test
  public void validate_BlockSplitTypeCountMissmatch() throws Throwable {
    writeFile("~~@reference_points=2\n~~@block_split_type=N");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_SPLIT_TYPE_COUNT_MISSMATCH", BLOCK_SPLIT_TYPE, 2)));
  }

  @Test
  public void validate_BlockSplitTypeCountMissmatch_Reverse() throws Throwable {
    writeFile("~~@block_split_type=N\n~~@reference_points=2");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_SPLIT_TYPE_COUNT_MISSMATCH", BLOCK_SPLIT_TYPE, 2)));
  }

  @Test
  public void validate_ValidBlockSplit() throws Throwable {
    writeFile("~~@block_split_value=0;50;50;0");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT", BLOCK_SPLIT_VALUE)));
  }

  @Test
  public void validate_InvalidBlockSplit() throws Throwable {
    writeFile("~~@block_split_value=0;50;50;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT", BLOCK_SPLIT_VALUE)));
  }

  @Test
  public void validate_BlockSplitCountMissmatch() throws Throwable {
    writeFile("~~@reference_points=2\n~~@block_split_value=0");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_SPLIT_COUNT_MISSMATCH", BLOCK_SPLIT_VALUE, 2)));
  }

  @Test
  public void validate_BlockSplitCountMissmatch_Reverse() throws Throwable {
    writeFile("~~@block_split_value=0\n~~@reference_points=2");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BLOCK_SPLIT_COUNT_MISSMATCH", BLOCK_SPLIT_VALUE, 2)));
  }

  @Test
  public void validate_BelowMinimumBlockSplit() throws Throwable {
    writeFile("~~@block_split_value=0;50;50;-1");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_BLOCK_SPLIT", BLOCK_SPLIT_VALUE, 0)));
  }

  @Test
  public void validate_ValidBlockSplitAlignment() throws Throwable {
    writeFile("~~@block_split_alignment=N;L;L;R");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT_ALIGNMENT", BLOCK_SPLIT_ALIGNMENT)));
  }

  @Test
  public void validate_InvalidBlockSplitAlignment() throws Throwable {
    writeFile("~~@block_split_alignment=N;L;L;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_BLOCK_SPLIT_ALIGNMENT", BLOCK_SPLIT_ALIGNMENT)));
  }

  @Test
  public void validate_BlockSplitAlignmentCountMissmatch() throws Throwable {
    writeFile("~~@reference_points=2\n~~@block_split_alignment=N");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("BLOCK_SPLIT_ALIGNMENT_COUNT_MISSMATCH", BLOCK_SPLIT_ALIGNMENT, 2)));
  }

  @Test
  public void validate_BlockSplitAlignmentCountMissmatch_Reverse() throws Throwable {
    writeFile("~~@block_split_alignment=N\n~~@reference_points=2");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("BLOCK_SPLIT_ALIGNMENT_COUNT_MISSMATCH", BLOCK_SPLIT_ALIGNMENT, 2)));
  }

  @Test
  public void validate_InvalidSmoothDataWindow() throws Throwable {
    writeFile("~~@smoothing_windows=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_SMOOTH_CURVE_WINDOW", SMOOTHING_WINDOWS)));
  }

  @Test
  public void validate_BelowMinimumSmoothDataWindow() throws Throwable {
    writeFile("~~@smoothing_windows=-1");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("BELOW_MINIMUM_SMOOTH_CURVE_WINDOW", SMOOTHING_WINDOWS, 0)));
  }

  @Test
  public void validate_SmoothDataWindowNotEven() throws Throwable {
    writeFile("~~@smoothing_windows=3");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("SMOOTH_CURVE_WINDOW_NOT_EVEN", SMOOTHING_WINDOWS)));
  }

  @Test
  public void validate_InvalidCalculatedType() throws Throwable {
    writeFile("~~@aggregate_data_type=F");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_CALCULATED_TYPE", AGGREGATE_DATA_TYPE)));
  }

  @Test
  public void validate_InvalidDispersionType() throws Throwable {
    writeFile("~~@mean_dispersion_value=F");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_DISPERSION_TYPE", MEAN_DISPERSION_VALUE)));
  }

  @Test
  public void validate_InvalidDispersion() throws Throwable {
    writeFile("~~@display_dispersion_values=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_DISPERSION", DISPLAY_DISPERSION_VALUES)));
  }

  @Test
  public void validate_InvalidGenerateAggregateGraphs() throws Throwable {
    writeFile("~~@generate_aggregate_graphs=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("INVALID_GENERATE_AGGREGATE_GRAPHS", GENERATE_AGGREGATE_GRAPHS)));
  }

  @Test
  public void validate_InvalidOneDataFilePerGraph() throws Throwable {
    writeFile("~~@one_graph_per_dataset=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_ONE_DATA_FILE_PER_GRAPH", ONE_GRAPH_PER_DATASET)));
  }

  @Test
  public void validate_InvalidOneReferenceGroupPerGraph() throws Throwable {
    writeFile("~~@one_graph_per_group=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_ONE_REFERENCE_GROUP_PER_GRAPH", ONE_GRAPH_PER_GROUP)));
  }

  @Test
  public void validate_InvalidOneOrientationPerGraph() throws Throwable {
    writeFile("~~@one_graph_per_orientation=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("INVALID_ONE_ORIENTATION_PER_GRAPH", ONE_GRAPH_PER_ORIENTATION)));
  }

  @Test
  public void validate_InvalidOutputData() throws Throwable {
    writeFile("~~@write_individual_references=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_OUTPUT_DATA", WRITE_INDIVIDUAL_REFERENCES)));
  }

  @Test
  public void validate_InvalidGenerateHeatmap() throws Throwable {
    writeFile("~~@generate_heatmaps=D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_GENERATE_HEATMAP", GENERATE_HEATMAP)));
  }

  @Test
  public void validate_InvalidOutputGraph() throws Throwable {
    writeFile("~~@orientation_subgroups=1;0;1;0;0;0;1;0;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_OUTPUT_GRAPH", ORIENTATION_SUBGROUPS)));
  }

  @Test
  public void validate_OutputGraphCountMissmatch() throws Throwable {
    writeFile("~~@orientation_subgroups=1;0;1;0;1");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("OUTPUT_GRAPH_COUNT_MISSMATCH", ORIENTATION_SUBGROUPS, 9)));
  }

  @Test
  public void validate_EmptyYaxisScale() throws Throwable {
    writeFile("~~@Y_axis_scale=;");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_YAXIS_SCALE", Y_AXIS_SCALE)));
  }

  @Test
  public void validate_ValidYaxisScale() throws Throwable {
    writeFile("~~@Y_axis_scale=-20;10");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeast(0)).accept(errorCaptor.capture());
    assertFalse(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_YAXIS_SCALE", Y_AXIS_SCALE)));
  }

  @Test
  public void validate_InvalidYaxisScale() throws Throwable {
    writeFile("~~@Y_axis_scale=-20;D");

    parameterFileService.validate(file, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_YAXIS_SCALE", Y_AXIS_SCALE)));
  }

  @Test
  public void validateGenerateAggregateGraphs_Null() throws Throwable {
    parameterFileService.validateGenerateAggregateGraphs(null, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("INVALID_GENERATE_AGGREGATE_GRAPHS", GENERATE_AGGREGATE_GRAPHS)));
  }

  @Test
  public void validateGenerateAggregateGraphs_Valid() throws Throwable {
    parameterFileService.validateGenerateAggregateGraphs("1", locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGenerateAggregateGraphs_Invalid() throws Throwable {
    parameterFileService.validateGenerateAggregateGraphs("a", locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues().contains(
        resources.message("INVALID_GENERATE_AGGREGATE_GRAPHS", GENERATE_AGGREGATE_GRAPHS)));
  }

  @Test
  public void validateGenerateHeatmap_Null() throws Throwable {
    parameterFileService.validateGenerateHeatmap(null, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_GENERATE_HEATMAP", GENERATE_HEATMAP)));
  }

  @Test
  public void validateGenerateHeatmap_Valid() throws Throwable {
    parameterFileService.validateGenerateHeatmap("1", locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateGenerateHeatmap_Invalid() throws Throwable {
    parameterFileService.validateGenerateHeatmap("a", locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_GENERATE_HEATMAP", GENERATE_HEATMAP)));
  }

  @Test
  public void validateDispersion_Null() throws Throwable {
    parameterFileService.validateDispersion(null, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_DISPERSION", DISPLAY_DISPERSION_VALUES)));
  }

  @Test
  public void validateDispersion_Valid() throws Throwable {
    parameterFileService.validateDispersion("1", locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateDispersion_Invalid() throws Throwable {
    parameterFileService.validateDispersion("a", locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_DISPERSION", DISPLAY_DISPERSION_VALUES)));
  }

  @Test
  public void validateYAxisScale_Null() throws Throwable {
    parameterFileService.validateYaxisScale(null, locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_YAXIS_SCALE", Y_AXIS_SCALE)));
  }

  @Test
  public void validateYAxisScale_Empty() throws Throwable {
    parameterFileService.validateYaxisScale(";", locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateYAxisScale_Valid() throws Throwable {
    parameterFileService.validateYaxisScale("-1;1.3", locale, errorHandler);

    verify(errorHandler, never()).accept(any());
  }

  @Test
  public void validateYAxisScale_Invalid() throws Throwable {
    parameterFileService.validateYaxisScale("a", locale, errorHandler);

    verify(errorHandler, atLeastOnce()).accept(errorCaptor.capture());
    assertTrue(errorCaptor.getAllValues()
        .contains(resources.message("INVALID_YAXIS_SCALE", Y_AXIS_SCALE)));
  }

  @Test
  public void parse() throws Throwable {
    File file = temporaryFolder.newFile("parameters.txt");
    try (
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(getClass().getResourceAsStream("/file/parameters.txt"), "UTF-8"));
        BufferedWriter writer = Files.newBufferedWriter(file.toPath(), UTF_8)) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith("~~@output_directory=")) {
          writer.write("~~@output_directory=" + outputFolder.toString());
        } else {
          writer.write(line);
        }
        writer.write("\n");
      }
    }

    final AnalysisParameters parameters = parameterFileService.parse(file);

    verify(parameterFileVersionService).updateParameters(any(), any(), any());
    verify(geneChartService).regionType(0, ReferenceType.ANNOTATIONS, 3);
    verify(geneChartService).regionType(1, ReferenceType.ANNOTATIONS, 3);
    verify(geneChartService).regionType(2, ReferenceType.ANNOTATIONS, 3);
    verify(geneChartService).regionType(3, ReferenceType.ANNOTATIONS, 3);
    assertEquals(outputFolder, parameters.outputFolder);
    assertEquals(ReferenceType.ANNOTATIONS, parameters.geneChartParameters.referenceType);
    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File(outputFolder, "refGroup.txt"), parameters.referenceFiles.get(0));
    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File(outputFolder, "dataset.txt"), parameters.dataFiles.get(0));
    assertEquals(MissingDataReplacement.IGNORE, parameters.missingDataReplacement);
    assertEquals(false, parameters.processDataByChunk);
    assertEquals(100, parameters.dataChunkSize);
    assertEquals(new File(outputFolder, "test_dataset.txt"), parameters.genomeAnnotations);
    assertEquals(new File(outputFolder, "selection.txt"), parameters.selectionAnnotationFilter);
    assertEquals(new File(outputFolder, "exclusion.txt"), parameters.exclusionAnnotationFilter);
    assertEquals(CoordinatesType.TX, parameters.coordinatesType);
    assertEquals(MergeMiddleIntron.FIRST, parameters.mergeMiddleIntron);
    assertEquals(RepresentationType.ABSOLUTE, parameters.geneChartParameters.representationType);
    assertEquals(3, parameters.geneChartParameters.referencePoints);
    assertEquals(Boundary.FIVE_PRIME, parameters.geneChartParameters.boundary);
    assertEquals(50, parameters.geneChartParameters.windowSize);
    assertEquals(4, parameters.geneChartParameters.blocks.size());
    List<Block> blocks = parameters.geneChartParameters.blocks;
    Block block = blocks.get(0);
    assertEquals((Long) 10L, block.windows);
    assertEquals(BlockAlignment.RIGHT, block.alignment);
    assertEquals(null, block.splitType);
    assertEquals((Long) 0L, block.split);
    assertEquals(null, block.splitAlignment);
    assertEquals(BlockType.REFERENCE_FEATURE, block.type);
    block = blocks.get(1);
    assertEquals((Long) 5L, block.windows);
    assertEquals(BlockAlignment.SPLIT, block.alignment);
    assertEquals(SplitType.PERCENTAGE, block.splitType);
    assertEquals((Long) 50L, block.split);
    assertEquals(SplitAlignment.LEFT, block.splitAlignment);
    assertEquals(BlockType.REFERENCE_FEATURE, block.type);
    block = blocks.get(2);
    assertEquals((Long) 10L, block.windows);
    assertEquals(BlockAlignment.SPLIT, block.alignment);
    assertEquals(SplitType.PERCENTAGE, block.splitType);
    assertEquals((Long) 50L, block.split);
    assertEquals(SplitAlignment.LEFT, block.splitAlignment);
    assertEquals(BlockType.REFERENCE_FEATURE, block.type);
    block = blocks.get(3);
    assertEquals((Long) 5L, block.windows);
    assertEquals(BlockAlignment.LEFT, block.alignment);
    assertEquals(SplitType.ABSOLUTE, block.splitType);
    assertEquals((Long) 100L, block.split);
    assertEquals(SplitAlignment.RIGHT, block.splitAlignment);
    assertEquals(BlockType.REFERENCE_FEATURE, block.type);
    assertEquals(6, parameters.smoothDataWindows);
    assertEquals(AggregateValueType.MEAN, parameters.aggregateValueType);
    assertEquals("christian", parameters.ouputFilePrefix);
    assertEquals(true, parameters.outputGraphs.contains(Graph.ANY_ANY));
    assertEquals(false, parameters.outputGraphs.contains(Graph.ANY_CONV));
    assertEquals(true, parameters.outputGraphs.contains(Graph.DIV_ANY));
    assertEquals(true, parameters.outputGraphs.contains(Graph.ANY_TAND));
    assertEquals(false, parameters.outputGraphs.contains(Graph.TAND_ANY));
    assertEquals(false, parameters.outputGraphs.contains(Graph.TAND_TAND));
    assertEquals(true, parameters.outputGraphs.contains(Graph.TAND_CONV));
    assertEquals(false, parameters.outputGraphs.contains(Graph.DIV_TAND));
    assertEquals(true, parameters.outputGraphs.contains(Graph.DIV_CONV));
    assertEquals(DispersionType.SEM, parameters.dispersionType);
    assertEquals(true, parameters.geneChartParameters.dispersion);
    assertEquals(true, parameters.geneChartParameters.generateAggregateGraphs);
    assertEquals(true, parameters.oneDataFilePerGraph);
    assertEquals(true, parameters.oneReferenceGroupPerGraph);
    assertEquals(true, parameters.oneOrientationPerGraph);
    assertEquals(true, parameters.outputData);
    assertEquals(true, parameters.geneChartParameters.generateHeatmap);
    assertEquals(-20.0, parameters.geneChartParameters.yaxisScale.from, 0.1);
    assertEquals(-10.0, parameters.geneChartParameters.yaxisScale.to, 0.1);
  }

  @Test
  public void parseEmptyFile() throws Throwable {
    writeFile("");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(null, parameters.outputFolder);
    assertEquals(null, parameters.geneChartParameters.referenceType);
    assertEquals(DEFAULT_MISSING_DATA_REPLACEMENT, parameters.missingDataReplacement);
    assertEquals(false, parameters.processDataByChunk);
    assertEquals(-1, parameters.dataChunkSize);
    assertEquals(0, parameters.dataFiles.size());
    assertEquals(0, parameters.referenceFiles.size());
    assertEquals(null, parameters.genomeAnnotations);
    assertEquals(null, parameters.selectionAnnotationFilter);
    assertEquals(null, parameters.exclusionAnnotationFilter);
    assertEquals(DEFAULT_COORDINATES_TYPE, parameters.coordinatesType);
    assertEquals(null, parameters.mergeMiddleIntron);

    assertEquals(null, parameters.geneChartParameters.representationType);
    assertEquals(-1, parameters.geneChartParameters.referencePoints);
    assertEquals(DEFAULT_BOUNDARY, parameters.geneChartParameters.boundary);
    assertEquals(-1, parameters.geneChartParameters.windowSize);
    assertEquals(0, parameters.geneChartParameters.blocks.size());
    assertEquals(DEFAULT_SMOOTH_DATA_WINDOWS, parameters.smoothDataWindows);
    assertEquals(DEFAULT_AGGREGATE_VALUE_TYPE, parameters.aggregateValueType);
    assertEquals(null, parameters.ouputFilePrefix);
    assertEquals(DEFAULT_OUTPUT_GRAPHS, parameters.outputGraphs);
    assertEquals(DEFAULT_DISPERSION_TYPE, parameters.dispersionType);
    assertEquals(false, parameters.geneChartParameters.dispersion);
    assertEquals(true, parameters.geneChartParameters.generateAggregateGraphs);
    assertEquals(DEFAULT_ONE_DATA_FILE_PER_GRAPH, parameters.oneDataFilePerGraph);
    assertEquals(false, parameters.oneReferenceGroupPerGraph);
    assertEquals(DEFAULT_ONE_ORIENTATION_PER_GRAPH, parameters.oneOrientationPerGraph);
    assertEquals(false, parameters.outputData);
    assertEquals(false, parameters.geneChartParameters.generateHeatmap);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.from);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.to);
  }

  @Test
  public void parseEmptyParameters() throws Throwable {
    File file = new File(getClass().getResource("/file/parameters_empty.txt").toURI());

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(null, parameters.outputFolder);
    assertEquals(null, parameters.geneChartParameters.referenceType);
    assertEquals(DEFAULT_MISSING_DATA_REPLACEMENT, parameters.missingDataReplacement);
    assertEquals(false, parameters.processDataByChunk);
    assertEquals(-1, parameters.dataChunkSize);
    assertEquals(0, parameters.dataFiles.size());
    assertEquals(0, parameters.referenceFiles.size());
    assertEquals(null, parameters.genomeAnnotations);
    assertEquals(null, parameters.selectionAnnotationFilter);
    assertEquals(null, parameters.exclusionAnnotationFilter);
    assertEquals(DEFAULT_COORDINATES_TYPE, parameters.coordinatesType);
    assertEquals(null, parameters.mergeMiddleIntron);

    assertEquals(null, parameters.geneChartParameters.representationType);
    assertEquals(-1, parameters.geneChartParameters.referencePoints);
    assertEquals(DEFAULT_BOUNDARY, parameters.geneChartParameters.boundary);
    assertEquals(-1, parameters.geneChartParameters.windowSize);
    assertEquals(0, parameters.geneChartParameters.blocks.size());
    assertEquals(DEFAULT_SMOOTH_DATA_WINDOWS, parameters.smoothDataWindows);
    assertEquals(DEFAULT_AGGREGATE_VALUE_TYPE, parameters.aggregateValueType);
    assertEquals("", parameters.ouputFilePrefix);
    assertEquals(DEFAULT_OUTPUT_GRAPHS, parameters.outputGraphs);
    assertEquals(DEFAULT_DISPERSION_TYPE, parameters.dispersionType);
    assertEquals(false, parameters.geneChartParameters.dispersion);
    assertEquals(true, parameters.geneChartParameters.generateAggregateGraphs);
    assertEquals(DEFAULT_ONE_DATA_FILE_PER_GRAPH, parameters.oneDataFilePerGraph);
    assertEquals(false, parameters.oneReferenceGroupPerGraph);
    assertEquals(DEFAULT_ONE_ORIENTATION_PER_GRAPH, parameters.oneOrientationPerGraph);
    assertEquals(false, parameters.outputData);
    assertEquals(false, parameters.geneChartParameters.generateHeatmap);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.from);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.to);
  }

  @Test
  public void parse_DataWithAlias() throws Throwable {
    writeFile("~~@refgroup_path=R1:=:reference.txt\n~~@dataset_path=R1:=:data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("data.txt"), parameters.dataFiles.get(0));
    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("reference.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  @SuppressWarnings("checkstyle:linelength")
  public void parse_DataWithAlias_Multiple() throws Throwable {
    writeFile(
        "~~@refgroup_path=R1:=:reference1.txt\n~~@refgroup_path=R2:=:reference2.txt\n~~@dataset_path=R1:=:data1.txt\n~~@dataset_path=R2:=:data2.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("data1.txt"), parameters.dataFiles.get(0));
    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("reference1.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  @SuppressWarnings("checkstyle:linelength")
  public void parse_DataWithAlias_MultipleEmpty() throws Throwable {
    writeFile(
        "~~@refgroup_path=R1:=:reference1.txt\n~~@refgroup_path=R2:=:reference2.txt\n~~@refgroup_path=reference3.txt\n~~@dataset_path=R1:=:data1.txt\n~~@dataset_path=R2:=:data2.txt\n~~@dataset_path=data3.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("data1.txt"), parameters.dataFiles.get(0));
    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("reference1.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  @SuppressWarnings("checkstyle:linelength")
  public void parse_DataWithAlias_EmptyMultiple() throws Throwable {
    writeFile(
        "~~@refgroup_path=reference3.txt\n~~@refgroup_path=R1:=:reference1.txt\n~~@refgroup_path=R2:=:reference2.txt\n~~@dataset_path=data3.txt\n~~@dataset_path=R1:=:data1.txt\n~~@dataset_path=R2:=:data2.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("data3.txt"), parameters.dataFiles.get(0));
    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("reference3.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  public void parse_IvalidMissingDataReplacement() throws Throwable {
    mockParameterParser();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ParameterHandlers handlers = (ParameterHandlers) invocation.getArguments()[1];
        handlers.parameterHandler.accept(PROCESS_MISSING_DATA, "a");
        return null;
      }
    }).when(parameterParser).parse(any(Reader.class), any(ParameterHandlers.class));
    writeFile("");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(DEFAULT_MISSING_DATA_REPLACEMENT, parameters.missingDataReplacement);
  }

  @Test
  public void parse_DataChunkSizeNotNumber() throws Throwable {
    mockParameterParser();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ParameterHandlers handlers = (ParameterHandlers) invocation.getArguments()[1];
        handlers.parameterHandler.accept(DATASET_CHUNK_SIZE, "a");
        return null;
      }
    }).when(parameterParser).parse(any(Reader.class), any(ParameterHandlers.class));
    writeFile("");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(-1, parameters.dataChunkSize);
  }

  @Test
  public void parse_DataChunkSizeBelowMinimum() throws Throwable {
    mockParameterParser();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ParameterHandlers handlers = (ParameterHandlers) invocation.getArguments()[1];
        handlers.parameterHandler.accept(DATASET_CHUNK_SIZE, "-1");
        return null;
      }
    }).when(parameterParser).parse(any(Reader.class), any(ParameterHandlers.class));
    writeFile("");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(-1, parameters.dataChunkSize);
  }

  @Test
  public void parse_CoordinatesType_N() throws Throwable {
    writeFile("~~@annotation_coordinates_type=N");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(null, parameters.coordinatesType);
  }

  @Test
  public void parseReferencePointsNotNumber() throws Throwable {
    File file =
        new File(getClass().getResource("/file/parameters_referencepoints_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(-1, parameters.geneChartParameters.referencePoints);
  }

  @Test
  public void parseWindowSizeNotNumber() throws Throwable {
    File file =
        new File(getClass().getResource("/file/parameters_windowsize_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(-1, parameters.geneChartParameters.windowSize);
  }

  @Test
  public void parseBlockWindowsNotNumber() throws Throwable {
    File file =
        new File(getClass().getResource("/file/parameters_windowsperblock_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(0, parameters.geneChartParameters.blocks.size());
  }

  @Test
  public void parseBlockSplitNotNumber() throws Throwable {
    File file =
        new File(getClass().getResource("/file/parameters_blocksplitvalue_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(0, parameters.geneChartParameters.blocks.size());
  }

  @Test
  public void parseSmoothDataWindowsNotNumber() throws Throwable {
    File file =
        new File(getClass().getResource("/file/parameters_smoothwindows_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(DEFAULT_SMOOTH_DATA_WINDOWS, parameters.smoothDataWindows);
  }

  @Test
  public void parseEmptyYAxis() throws Throwable {
    writeFile("~~@Y_axis_scale=;");
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.from);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.to);
  }

  @Test
  public void parseYAxisNotNumber() throws Throwable {
    File file = new File(getClass().getResource("/file/parameters_yaxis_notnumber.txt").toURI());
    AnalysisParameters parameters = parameterFileService.parse(file);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.from);
    assertEquals(null, parameters.geneChartParameters.yaxisScale.to);
  }

  @Test
  public void parse_DataFiles_NormalizeInvalid() throws Throwable {
    writeFile("~~@dataset_path=../annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("../annotations/data.txt"), parameters.dataFiles.get(0));
  }

  @Test
  public void parse_DataFiles_RelativePath() throws Throwable {
    writeFile("~~@output_directory=" + outputFolder.toString()
        + "\n~~@dataset_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File(outputFolder, "annotations/data.txt"), parameters.dataFiles.get(0));
  }

  @Test
  public void parse_DataFiles_RelativePath_RelativeOutputFolder() throws Throwable {
    writeFile("~~@output_directory=./temp\n~~@dataset_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("annotations/data.txt"), parameters.dataFiles.get(0));
  }

  @Test
  public void parse_DataFiles_RelativePath_OutputFolderMissing() throws Throwable {
    writeFile("~~@dataset_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.dataFiles.size());
    assertEquals(new File("annotations/data.txt"), parameters.dataFiles.get(0));
  }

  @Test
  public void parse_ReferenceFiles_RelativePath() throws Throwable {
    writeFile("~~@output_directory=" + outputFolder.toString()
        + "\n~~@refgroup_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File(outputFolder, "annotations/data.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  public void parse_ReferenceFiles_RelativePath_RelativeOutputFolder() throws Throwable {
    writeFile("~~@output_directory=./temp\n~~@refgroup_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("annotations/data.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  public void parse_ReferenceFiles_RelativePath_OutputFolderMissing() throws Throwable {
    writeFile("~~@refgroup_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(1, parameters.referenceFiles.size());
    assertEquals(new File("annotations/data.txt"), parameters.referenceFiles.get(0));
  }

  @Test
  public void parse_SelectionAnnotationFilter_RelativePath() throws Throwable {
    writeFile("~~@output_directory=" + outputFolder.toString()
        + "\n~~@selection_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File(outputFolder, "annotations/data.txt"),
        parameters.selectionAnnotationFilter);
  }

  @Test
  public void parse_SelectionAnnotationFilter_RelativePath_RelativeOutputFolder() throws Throwable {
    writeFile("~~@output_directory=./temp\n~~@selection_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.selectionAnnotationFilter);
  }

  @Test
  public void parse_SelectionAnnotationFilter_RelativePath_OutputFolderMissing() throws Throwable {
    writeFile("~~@selection_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.selectionAnnotationFilter);
  }

  @Test
  public void parse_ExclusionAnnotationFilter_RelativePath() throws Throwable {
    writeFile("~~@output_directory=" + outputFolder.toString()
        + "\n~~@exclusion_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File(outputFolder, "annotations/data.txt"),
        parameters.exclusionAnnotationFilter);
  }

  @Test
  public void parse_ExclusionAnnotationFilter_RelativePath_RelativeOutputFolder() throws Throwable {
    writeFile("~~@output_directory=./temp\n~~@exclusion_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.exclusionAnnotationFilter);
  }

  @Test
  public void parse_ExclusionAnnotationFilter_RelativePath_OutputFolderMissing() throws Throwable {
    writeFile("~~@exclusion_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.exclusionAnnotationFilter);
  }

  @Test
  public void parse_GenomeAnnotations_RelativePath() throws Throwable {
    writeFile("~~@output_directory=" + outputFolder.toString()
        + "\n~~@annotations_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File(outputFolder, "annotations/data.txt"), parameters.genomeAnnotations);
  }

  @Test
  public void parse_GenomeAnnotations_RelativePath_RelativeOutputFolder() throws Throwable {
    writeFile("~~@output_directory=./temp\n~~@annotations_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.genomeAnnotations);
  }

  @Test
  public void parse_GenomeAnnotations_RelativePath_OutputFolderMissing() throws Throwable {
    writeFile("~~@annotations_path=./annotations/data.txt");

    AnalysisParameters parameters = parameterFileService.parse(file);

    assertEquals(new File("annotations/data.txt"), parameters.genomeAnnotations);
  }

  @Test
  public void parseGenerateAggregateGraphs_Null() throws Throwable {
    assertEquals(false, parameterFileService.parseGenerateAggregateGraphs(null));
  }

  @Test
  public void parseGenerateAggregateGraphs_Valid() throws Throwable {
    assertEquals(true, parameterFileService.parseGenerateAggregateGraphs("1"));
  }

  @Test
  public void parseGenerateAggregateGraphs_Invalid() throws Throwable {
    assertEquals(false, parameterFileService.parseGenerateAggregateGraphs("a"));
  }

  @Test
  public void parseGenerateHeatmap_Null() throws Throwable {
    assertEquals(false, parameterFileService.parseGenerateHeatmap(null));
  }

  @Test
  public void parseGenerateHeatmap_Valid() throws Throwable {
    assertEquals(true, parameterFileService.parseGenerateHeatmap("1"));
  }

  @Test
  public void parseGenerateHeatmap_Invalid() throws Throwable {
    assertEquals(false, parameterFileService.parseGenerateHeatmap("a"));
  }

  @Test
  public void parseDispersion_Null() throws Throwable {
    assertEquals(false, parameterFileService.parseDispersion(null));
  }

  @Test
  public void parseDispersion_Valid() throws Throwable {
    assertEquals(true, parameterFileService.parseDispersion("1"));
  }

  @Test
  public void parseDispersion_Invalid() throws Throwable {
    assertEquals(false, parameterFileService.parseDispersion("a"));
  }

  @Test
  public void parseYAxisScale_Null() throws Throwable {
    YAxisScale yaxisScale = parameterFileService.parseYaxisScale(null);

    assertEquals(null, yaxisScale.from);
    assertEquals(null, yaxisScale.to);
  }

  @Test
  public void parseYAxisScale_Empty() throws Throwable {
    YAxisScale yaxisScale = parameterFileService.parseYaxisScale(";");

    assertEquals(null, yaxisScale.from);
    assertEquals(null, yaxisScale.to);
  }

  @Test
  public void parseYAxisScale_Valid() throws Throwable {
    YAxisScale yaxisScale = parameterFileService.parseYaxisScale("-1;1.3");

    assertEquals(-1.0, yaxisScale.from, 0.001);
    assertEquals(1.3, yaxisScale.to, 0.001);
  }

  @Test
  public void parseYAxisScale_Invalid() throws Throwable {
    YAxisScale yaxisScale = parameterFileService.parseYaxisScale("a");

    assertEquals(null, yaxisScale.from);
    assertEquals(null, yaxisScale.to);
  }

  @Test
  public void export() throws Throwable {
    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");

    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals(1, lines.stream().filter(line -> line.startsWith("~~version")).count());
    assertEquals(36, lines.stream().filter(line -> line.startsWith("~~@")).count());
    assertEquals(522, lines.stream()
        .filter(line -> !line.startsWith("~~@") && !line.startsWith("~~version")).count());
    assertEquals("\t\t\t---------------------------------------------------", lines.get(0));
    assertEquals(
        "~~IMPORTANT: Format must be \"parameter_name=value\" where accepted values are inside []",
        lines.get(8));
    assertEquals("* Files selection", lines.get(30));
    assertFalse(lines.get(0).startsWith("~~@"));
    assertFalse(lines.get(1).startsWith("~~@"));
    assertFalse(lines.get(2).startsWith("~~@"));
    assertFalse(lines.get(3).startsWith("~~@"));
    assertFalse(lines.get(4).startsWith("~~@"));
    assertFalse(lines.get(5).startsWith("~~@"));
    assertEquals("~~version " + mainConfiguration.version(), lines.get(6));
    assertFalse(lines.get(7).startsWith("~~@"));
    assertFalse(lines.get(8).startsWith("~~@"));
    assertFalse(lines.get(9).startsWith("~~@"));
    assertFalse(lines.get(10).startsWith("~~@"));
    assertFalse(lines.get(11).startsWith("~~@"));
    assertFalse(lines.get(12).startsWith("~~@"));
    assertFalse(lines.get(13).startsWith("~~@"));
    assertFalse(lines.get(14).startsWith("~~@"));
    assertFalse(lines.get(15).startsWith("~~@"));
    assertFalse(lines.get(16).startsWith("~~@"));
    assertFalse(lines.get(17).startsWith("~~@"));
    assertFalse(lines.get(18).startsWith("~~@"));
    assertFalse(lines.get(19).startsWith("~~@"));
    assertFalse(lines.get(20).startsWith("~~@"));
    assertFalse(lines.get(21).startsWith("~~@"));
    assertFalse(lines.get(22).startsWith("~~@"));
    assertFalse(lines.get(23).startsWith("~~@"));
    assertFalse(lines.get(24).startsWith("~~@"));
    assertFalse(lines.get(25).startsWith("~~@"));
    assertEquals("~~@analysis_mode=A", lines.get(26));
    assertFalse(lines.get(27).startsWith("~~@"));
    assertFalse(lines.get(28).startsWith("~~@"));
    assertFalse(lines.get(29).startsWith("~~@"));
    assertFalse(lines.get(30).startsWith("~~@"));
    assertFalse(lines.get(31).startsWith("~~@"));
    assertFalse(lines.get(32).startsWith("~~@"));
    assertFalse(lines.get(33).startsWith("~~@"));
    assertFalse(lines.get(34).startsWith("~~@"));
    assertFalse(lines.get(35).startsWith("~~@"));
    assertFalse(lines.get(36).startsWith("~~@"));
    assertFalse(lines.get(37).startsWith("~~@"));
    assertFalse(lines.get(38).startsWith("~~@"));
    assertFalse(lines.get(39).startsWith("~~@"));
    assertFalse(lines.get(40).startsWith("~~@"));
    assertFalse(lines.get(41).startsWith("~~@"));
    assertEquals("~~@dataset_path=R1:=:data1.txt", lines.get(42));
    assertEquals("~~@dataset_path=R1:=:data2.txt", lines.get(43));
    assertEquals("~~@refgroup_path=R1:=:reference1.txt", lines.get(62));
    assertEquals("~~@refgroup_path=R1:=:reference2.txt", lines.get(63));
    assertEquals("~~@annotations_path=dataset.txt", lines.get(75));
    assertEquals("~~@selection_path=selection_w.txt", lines.get(89));
    assertEquals("~~@exclusion_path=exclusion_w.txt", lines.get(103));
    assertEquals("~~@analysis_method=A", lines.get(121));
    assertEquals("~~@annotation_coordinates_type=C", lines.get(137));
    assertEquals("~~@reference_points=5", lines.get(149));
    assertEquals("~~@1pt_boundary=N", lines.get(165));
    assertEquals("~~@window_size=55", lines.get(179));
    assertEquals("~~@windows_per_block=20;10;5;10;5;20;", lines.get(193));
    assertEquals("~~@block_alignment=R;R;S;L;L;L", lines.get(213));
    assertEquals("~~@block_split_type=N;N;P;A;A;N", lines.get(233));
    assertEquals("~~@block_split_value=0;0;50;100;100;0", lines.get(249));
    assertEquals("~~@block_split_alignment=N;N;L;R;R;N", lines.get(269));
    assertEquals("~~@merge_mid_introns=L", lines.get(287));
    assertEquals("~~@aggregate_data_type=D", lines.get(305));
    assertEquals("~~@smoothing_windows=7", lines.get(319));
    assertEquals("~~@mean_dispersion_value=D", lines.get(333));
    assertEquals("~~@process_missing_data=1", lines.get(347));
    assertEquals("~~@process_data_by_chunk=1", lines.get(360));
    assertEquals("~~@dataset_chunk_size=100", lines.get(372));
    assertEquals("~~@output_directory=" + temporaryFolder.getRoot().getPath(), lines.get(388));
    assertEquals("~~@prefix_filename=christian", lines.get(400));
    assertEquals("~~@write_individual_references=0", lines.get(414));
    assertEquals("~~@generate_heatmaps=0", lines.get(428));
    assertEquals("~~@display_dispersion_values=0", lines.get(442));
    assertEquals("~~@generate_aggregate_graphs=1", lines.get(456));
    assertEquals("~~@one_graph_per_dataset=0", lines.get(470));
    assertEquals("~~@one_graph_per_group=0", lines.get(484));
    assertEquals("~~@orientation_subgroups=1;1;1;1;0;0;1;0;1", lines.get(522));
    assertEquals("~~@one_graph_per_orientation=0", lines.get(536));
    assertEquals("~~@Y_axis_scale=10.0;20.0", lines.get(550));
  }

  @Test
  public void export_RelativePath() throws Throwable {
    analysisParameters.dataFiles =
        Arrays.asList(new File("../data1.txt"), new File("../data2.txt"));
    analysisParameters.referenceFiles =
        Arrays.asList(new File("../reference1.txt"), new File("../reference2.txt"));
    analysisParameters.genomeAnnotations = new File("../dataset.txt");
    analysisParameters.selectionAnnotationFilter = new File("../selection_w.txt");
    analysisParameters.exclusionAnnotationFilter = new File("../exclusion_w.txt");

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@dataset_path=R1:=:" + toSystem("../data1.txt"), lines.get(42));
    assertEquals("~~@dataset_path=R1:=:" + toSystem("../data2.txt"), lines.get(43));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem("../reference1.txt"), lines.get(62));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem("../reference2.txt"), lines.get(63));
    assertEquals("~~@annotations_path=" + toSystem("../dataset.txt"), lines.get(75));
    assertEquals("~~@selection_path=" + toSystem("../selection_w.txt"), lines.get(89));
    assertEquals("~~@exclusion_path=" + toSystem("../exclusion_w.txt"), lines.get(103));
  }

  @Test
  public void export_RelativePath_OutputFolderRelative() throws Throwable {
    analysisParameters.dataFiles =
        Arrays.asList(new File("../data1.txt"), new File("../data2.txt"));
    analysisParameters.referenceFiles =
        Arrays.asList(new File("../reference1.txt"), new File("../reference2.txt"));
    analysisParameters.genomeAnnotations = new File("../dataset.txt");
    analysisParameters.selectionAnnotationFilter = new File("../selection_w.txt");
    analysisParameters.exclusionAnnotationFilter = new File("../exclusion_w.txt");
    analysisParameters.outputFolder = new File(temporaryFolder.getRoot().getName());

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@dataset_path=R1:=:" + toSystem("../data1.txt"), lines.get(42));
    assertEquals("~~@dataset_path=R1:=:" + toSystem("../data2.txt"), lines.get(43));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem("../reference1.txt"), lines.get(62));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem("../reference2.txt"), lines.get(63));
    assertEquals("~~@annotations_path=" + toSystem("../dataset.txt"), lines.get(75));
    assertEquals("~~@selection_path=" + toSystem("../selection_w.txt"), lines.get(89));
    assertEquals("~~@exclusion_path=" + toSystem("../exclusion_w.txt"), lines.get(103));
  }

  @Test
  public void export_AbsolutePath() throws Throwable {
    analysisParameters.dataFiles = Arrays.asList(new File(temporaryFolder.getRoot(), "data1.txt"),
        new File(temporaryFolder.getRoot(), "data2.txt"));
    analysisParameters.referenceFiles =
        Arrays.asList(new File(temporaryFolder.getRoot(), "reference1.txt"),
            new File(temporaryFolder.getRoot(), "reference2.txt"));
    analysisParameters.genomeAnnotations = new File(temporaryFolder.getRoot(), "dataset.txt");
    analysisParameters.selectionAnnotationFilter =
        new File(temporaryFolder.getRoot(), "selection_w.txt");
    analysisParameters.exclusionAnnotationFilter =
        new File(temporaryFolder.getRoot(), "exclusion_w.txt");

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@dataset_path=R1:=:data1.txt", lines.get(42));
    assertEquals("~~@dataset_path=R1:=:data2.txt", lines.get(43));
    assertEquals("~~@refgroup_path=R1:=:reference1.txt", lines.get(62));
    assertEquals("~~@refgroup_path=R1:=:reference2.txt", lines.get(63));
    assertEquals("~~@annotations_path=dataset.txt", lines.get(75));
    assertEquals("~~@selection_path=selection_w.txt", lines.get(89));
    assertEquals("~~@exclusion_path=exclusion_w.txt", lines.get(103));
  }

  @Test
  public void export_AbsolutePath_OutputFolderRelative() throws Throwable {
    analysisParameters.dataFiles = Arrays.asList(new File(temporaryFolder.getRoot(), "data1.txt"),
        new File(temporaryFolder.getRoot(), "data2.txt"));
    analysisParameters.referenceFiles =
        Arrays.asList(new File(temporaryFolder.getRoot(), "reference1.txt"),
            new File(temporaryFolder.getRoot(), "reference2.txt"));
    analysisParameters.genomeAnnotations = new File(temporaryFolder.getRoot(), "dataset.txt");
    analysisParameters.selectionAnnotationFilter =
        new File(temporaryFolder.getRoot(), "selection_w.txt");
    analysisParameters.exclusionAnnotationFilter =
        new File(temporaryFolder.getRoot(), "exclusion_w.txt");
    analysisParameters.outputFolder = new File(temporaryFolder.getRoot().getName());

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    String outputFolderPath = temporaryFolder.getRoot().getPath() + "/";
    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@dataset_path=R1:=:" + toSystem(outputFolderPath + "data1.txt"), lines.get(42));
    assertEquals("~~@dataset_path=R1:=:" + toSystem(outputFolderPath + "data2.txt"), lines.get(43));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem(outputFolderPath + "reference1.txt"),
        lines.get(62));
    assertEquals("~~@refgroup_path=R1:=:" + toSystem(outputFolderPath + "reference2.txt"),
        lines.get(63));
    assertEquals("~~@annotations_path=" + toSystem(outputFolderPath + "dataset.txt"),
        lines.get(75));
    assertEquals("~~@selection_path=" + toSystem(outputFolderPath + "selection_w.txt"),
        lines.get(89));
    assertEquals("~~@exclusion_path=" + toSystem(outputFolderPath + "exclusion_w.txt"),
        lines.get(103));
  }

  @Test
  public void export_Exon() throws Throwable {
    analysisParameters.geneChartParameters.referenceType = ReferenceType.EXONS;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@analysis_mode=E", lines.get(26));
    assertEquals("~~@annotation_coordinates_type=N", lines.get(137));
    assertEquals("~~@1pt_boundary=N", lines.get(165));
    assertEquals("~~@annotations_path=dataset.txt", lines.get(75));
  }

  @Test
  public void export_Coordinates() throws Throwable {
    analysisParameters.geneChartParameters.referenceType = ReferenceType.COORDINATES;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@analysis_mode=C", lines.get(26));
    assertEquals("~~@annotation_coordinates_type=N", lines.get(137));
    assertEquals("~~@1pt_boundary=N", lines.get(165));
    assertEquals("~~@annotations_path=", lines.get(75));
  }

  @Test
  public void export_ReferencePointsOne() throws Throwable {
    analysisParameters.geneChartParameters.referencePoints = 1;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@reference_points=1", lines.get(149));
    assertEquals("~~@1pt_boundary=3", lines.get(165));
  }

  @Test
  public void export_AggregateValueType_Mean() throws Throwable {
    analysisParameters.aggregateValueType = AggregateValueType.MEAN;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@aggregate_data_type=E", lines.get(305));
    assertEquals("~~@display_dispersion_values=1", lines.get(442));
    assertEquals("~~@mean_dispersion_value=D", lines.get(333));
  }

  @Test
  public void export_AggregateValueType_Max() throws Throwable {
    analysisParameters.aggregateValueType = AggregateValueType.MAX;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@aggregate_data_type=A", lines.get(305));
    assertEquals("~~@display_dispersion_values=0", lines.get(442));
    assertEquals("~~@mean_dispersion_value=D", lines.get(333));
  }

  @Test
  public void export_AggregateValueType_Min() throws Throwable {
    analysisParameters.aggregateValueType = AggregateValueType.MIN;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@aggregate_data_type=I", lines.get(305));
    assertEquals("~~@display_dispersion_values=0", lines.get(442));
    assertEquals("~~@mean_dispersion_value=D", lines.get(333));
  }

  @Test
  public void exportNullCoordinatesType() throws Throwable {
    analysisParameters.coordinatesType = null;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@annotation_coordinates_type=T", lines.get(137));
  }

  @Test
  public void exportNullMergeMiddleIntron() throws Throwable {
    analysisParameters.mergeMiddleIntron = null;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@merge_mid_introns=N", lines.get(287));
  }

  @Test
  public void exportNullDispersion() throws Throwable {
    analysisParameters.dispersionType = null;
    analysisParameters.geneChartParameters.dispersion = false;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@mean_dispersion_value=E", lines.get(333));
    assertEquals("~~@display_dispersion_values=0", lines.get(442));
  }

  @Test
  public void exportNullBoundary() throws Throwable {
    analysisParameters.geneChartParameters.boundary = null;

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@1pt_boundary=N", lines.get(165));
  }

  @Test
  public void exportAutomaticYAxisScale() throws Throwable {
    analysisParameters.geneChartParameters.yaxisScale = new YAxisScale(null, null);

    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");
    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@Y_axis_scale=;", lines.get(550));
  }

  @Test
  public void export_ReplaceAboluteBlocks() throws Throwable {
    analysisParameters.geneChartParameters.representationType = RepresentationType.ABSOLUTE;
    analysisParameters.geneChartParameters.referencePoints = 2;
    analysisParameters.geneChartParameters.blocks = new ArrayList<>();
    Block block =
        new Block(20L, BlockAlignment.LEFT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(5L, BlockAlignment.SPLIT, SplitType.PERCENTAGE, 50L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(20L, BlockAlignment.RIGHT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");

    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@windows_per_block=20;5;20;", lines.get(193));
    assertEquals("~~@block_alignment=R;S;L", lines.get(213));
    assertEquals("~~@block_split_type=N;P;N", lines.get(233));
    assertEquals("~~@block_split_value=0;50;0", lines.get(249));
    assertEquals("~~@block_split_alignment=N;L;N", lines.get(269));
  }

  @Test
  public void export_ReplaceRelativeBlocks() throws Throwable {
    analysisParameters.geneChartParameters.representationType = RepresentationType.RELATIVE;
    analysisParameters.geneChartParameters.referencePoints = 2;
    analysisParameters.geneChartParameters.blocks = new ArrayList<>();
    Block block =
        new Block(20L, BlockAlignment.LEFT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(5L, BlockAlignment.SPLIT, SplitType.PERCENTAGE, 50L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    block =
        new Block(20L, BlockAlignment.RIGHT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");

    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@windows_per_block=20;5;20;", lines.get(193));
    assertEquals("~~@block_alignment=L;L;L", lines.get(213));
    assertEquals("~~@block_split_type=P;P;P", lines.get(233));
    assertEquals("~~@block_split_value=10;10;10", lines.get(249));
    assertEquals("~~@block_split_alignment=L;L;L", lines.get(269));
  }

  @Test
  public void export_TooManyBlocks() throws Throwable {
    Block block =
        new Block(20L, BlockAlignment.LEFT, SplitType.PERCENTAGE, 30L, SplitAlignment.LEFT, null);
    analysisParameters.geneChartParameters.blocks.add(block);
    analysisParameters.geneChartParameters.blocks.add(block);
    analysisParameters.geneChartParameters.blocks.add(block);
    File file = temporaryFolder.newFile("ParameterServiceDefaultTest.txt");

    parameterFileService.export(file, analysisParameters, false);

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("~~@windows_per_block=20;10;5;10;5;20;", lines.get(193));
    assertEquals("~~@block_alignment=R;R;S;L;L;L", lines.get(213));
    assertEquals("~~@block_split_type=N;N;P;A;A;N", lines.get(233));
    assertEquals("~~@block_split_value=0;0;50;100;100;0", lines.get(249));
    assertEquals("~~@block_split_alignment=N;N;L;R;R;N", lines.get(269));
  }
}

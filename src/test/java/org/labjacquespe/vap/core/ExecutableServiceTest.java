/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.function.Consumer;
import javax.inject.Inject;
import javax.inject.Provider;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.Executor;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.OperatingSystem;
import org.labjacquespe.vap.OperatingSystemService;
import org.labjacquespe.vap.core.ExecutableService.VapEventListener;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.labjacquespe.vap.test.config.RetryOnFail;
import org.labjacquespe.vap.test.config.RetryOnFailRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for {@link ExecutableService}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExecutableServiceTest {
  public RetryOnFailRule retryOnFailRule = new RetryOnFailRule();
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  @Rule
  public RuleChain ruleChain = RuleChain.outerRule(retryOnFailRule).around(temporaryFolder);
  private ExecutableService executableServiceDefault;
  @Mock
  private OperatingSystemService operatingSystemService;
  @Mock
  private Provider<Executor> executorProvider;
  @Mock
  private Executor executor;
  @Mock
  private Consumer<String> groupHandler;
  @Mock
  private Consumer<String> datasetHandler;
  @Mock
  private Consumer<String> errorHandler;
  @Captor
  private ArgumentCaptor<CommandLine> commandLineCaptor;
  @Captor
  private ArgumentCaptor<ExecuteStreamHandler> streamHandlerCaptor;
  @Captor
  private ArgumentCaptor<String> errorCaptor;
  @Inject
  private VapCoreConfiguration vapCoreConfiguration;
  private Locale locale;
  private VapEventListener listener;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    executableServiceDefault =
        new ExecutableService(operatingSystemService, executorProvider, vapCoreConfiguration);
    locale = Locale.getDefault();
    listener = new VapEventListener();
    listener.groupHandler = groupHandler;
    listener.datasetHandler = datasetHandler;
    listener.errorHandler = errorHandler;
    when(executorProvider.get()).thenReturn(executor);
    when(executor.execute(any(CommandLine.class))).thenReturn(0);
  }

  @Test
  public void validateVap_Windows() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);

    executableServiceDefault.validateVap(temporaryFolder.getRoot(), locale, errorHandler);

    verifyZeroInteractions(errorHandler);
  }

  @Test
  public void validateVap_Mac() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.MAC);

    executableServiceDefault.validateVap(temporaryFolder.getRoot(), locale, errorHandler);

    verifyZeroInteractions(errorHandler);
  }

  @Test
  public void validateVap_Other() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.OTHER);

    executableServiceDefault.validateVap(temporaryFolder.getRoot(), locale, errorHandler);

    verify(errorHandler).accept(errorCaptor.capture());
    String error = errorCaptor.getValue();
    assertNotNull(error);
  }

  @Test
  public void validateVap_OtherWithNative() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.OTHER);
    temporaryFolder.newFile(vapCoreConfiguration.nativeVap(OperatingSystem.OTHER).getPath());

    executableServiceDefault.validateVap(temporaryFolder.getRoot(), locale, errorHandler);

    verifyZeroInteractions(errorHandler);
  }

  @Test
  public void runVap() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);
    File parameters = temporaryFolder.newFile("param_mac.txt");

    executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters);

    verify(operatingSystemService).currentOs();
    verify(executorProvider).get();
    verify(executor).setWorkingDirectory(temporaryFolder.getRoot());
    verify(executor).execute(commandLineCaptor.capture());
    CommandLine commandLine = commandLineCaptor.getValue();
    File executable = new File(commandLine.getExecutable());
    assertEquals(true, executable.exists());
    assertEquals(true, executable.canExecute());
    String executableResource = vapCoreConfiguration.resource(OperatingSystem.WINDOWS);
    File expectedExecutable = new File(this.getClass().getResource(executableResource).toURI());
    assertEquals(FileUtils.checksumCRC32(expectedExecutable), FileUtils.checksumCRC32(executable));
    assertEquals("-p", commandLine.getArguments()[0]);
    assertEquals(parameters.getAbsolutePath(), commandLine.getArguments()[1]);
  }

  @Test
  public void runVap_Mac() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.MAC);
    File parameters = temporaryFolder.newFile("param_mac.txt");

    executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters);

    verify(operatingSystemService).currentOs();
    verify(executorProvider).get();
    verify(executor).setWorkingDirectory(temporaryFolder.getRoot());
    verify(executor).execute(commandLineCaptor.capture());
    CommandLine commandLine = commandLineCaptor.getValue();
    File executable = new File(commandLine.getExecutable());
    assertEquals(true, executable.exists());
    assertEquals(true, executable.canExecute());
    String executableResource = vapCoreConfiguration.resource(OperatingSystem.MAC);
    File expectedExecutable = new File(this.getClass().getResource(executableResource).toURI());
    assertEquals(FileUtils.checksumCRC32(expectedExecutable), FileUtils.checksumCRC32(executable));
  }

  @Test
  @RetryOnFail(5)
  public void runVap_Event() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);
    final File parameters = temporaryFolder.newFile("param_mac.txt");
    StringBuilder processOutput = new StringBuilder();
    processOutput.append("Other\n");
    processOutput.append("Reading group: group1\n");
    processOutput.append("Reading group: group2\n");
    processOutput.append("Other\n");
    processOutput.append("Processing dataset: dataset1\n");
    processOutput.append("Processing dataset: dataset2\n");
    processOutput.append("Other\n");
    final ByteArrayInputStream input =
        new ByteArrayInputStream(processOutput.toString().getBytes(Charset.defaultCharset()));
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ExecuteStreamHandler streamHandler = (ExecuteStreamHandler) invocation.getArguments()[0];
        streamHandler.setProcessOutputStream(input);
        streamHandler.start();
        return null;
      }
    }).when(executor).setStreamHandler(any(ExecuteStreamHandler.class));

    executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters, listener);

    verify(executor).setStreamHandler(streamHandlerCaptor.capture());
    verify(executorProvider).get();
    verify(executor).setWorkingDirectory(temporaryFolder.getRoot());
    verify(executor).execute(commandLineCaptor.capture());
    CommandLine commandLine = commandLineCaptor.getValue();
    File executable = new File(commandLine.getExecutable());
    assertEquals(true, executable.exists());
    assertEquals(true, executable.canExecute());
    String executableResource = vapCoreConfiguration.resource(OperatingSystem.WINDOWS);
    File expectedExecutable = new File(this.getClass().getResource(executableResource).toURI());
    assertEquals(FileUtils.checksumCRC32(expectedExecutable), FileUtils.checksumCRC32(executable));
    // Validate events.
    Thread.sleep(200);
    streamHandlerCaptor.getValue().stop();
    verify(groupHandler).accept("group1");
    verify(groupHandler).accept("group2");
    verify(datasetHandler).accept("dataset1");
    verify(datasetHandler).accept("dataset2");
  }

  @Test
  public void runVap_Error() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);
    when(executor.isFailure(0)).thenReturn(true);
    File parameters = temporaryFolder.newFile("param_mac.txt");

    try {
      executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters);
      fail("Expected IOException");
    } catch (IOException e) {
      // Success.
    }

    verify(executor).isFailure(0);
  }

  @Test
  public void runVap_OverwriteExecutable() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);
    File parameters = temporaryFolder.newFile("param_mac.txt");
    String outputFilename = vapCoreConfiguration.output(OperatingSystem.WINDOWS).toString();
    File executable = temporaryFolder.newFile(outputFilename);

    executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters);

    String executableResource = vapCoreConfiguration.resource(OperatingSystem.WINDOWS);
    File expectedExecutable = new File(getClass().getResource(executableResource).toURI());
    assertEquals(true, executable.length() > 0);
    assertEquals(FileUtils.checksumCRC32(expectedExecutable), FileUtils.checksumCRC32(executable));
  }

  @Test
  public void runVap_UseNative() throws Throwable {
    when(operatingSystemService.currentOs()).thenReturn(OperatingSystem.WINDOWS);
    File expectedExecutable =
        temporaryFolder.newFile(vapCoreConfiguration.nativeVap(OperatingSystem.WINDOWS).getPath());
    File parameters = temporaryFolder.newFile("param_mac.txt");

    executableServiceDefault.runVap(temporaryFolder.getRoot(), parameters);
    verify(executor).execute(commandLineCaptor.capture());
    CommandLine commandLine = commandLineCaptor.getValue();
    File executable = new File(commandLine.getExecutable());
    assertEquals(expectedExecutable, executable);
  }
}

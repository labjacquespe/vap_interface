/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.labjacquespe.vap.OperatingSystem.MAC;
import static org.labjacquespe.vap.OperatingSystem.OTHER;
import static org.labjacquespe.vap.OperatingSystem.UNIX;
import static org.labjacquespe.vap.OperatingSystem.WINDOWS;

import java.io.File;
import java.nio.file.Files;
import java.util.List;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.OperatingSystem;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class VapCoreConfigurationTest {
  @Inject
  private VapCoreConfiguration vapCoreConfiguration;

  @Test
  public void nativeVap_Windows() throws Throwable {
    assertEquals(new File("vap_native.exe"), vapCoreConfiguration.nativeVap(WINDOWS));
  }

  @Test
  public void nativeVap_Mac() throws Throwable {
    assertEquals(new File("vap_native"), vapCoreConfiguration.nativeVap(MAC));
  }

  @Test
  public void nativeVap_Unix() throws Throwable {
    assertEquals(new File("vap_native"), vapCoreConfiguration.nativeVap(UNIX));
  }

  @Test
  public void nativeVap_Other() throws Throwable {
    assertEquals(new File("vap_native"), vapCoreConfiguration.nativeVap(OTHER));
  }

  private String resource(OperatingSystem os) throws Throwable {
    String truncate = "    " + os.name() + ": ";
    String lineStart = truncate + "/vap-core/";
    File applicationProperties = new File(getClass().getResource("/application.yml").toURI());
    String resourceLine = Files.readAllLines(applicationProperties.toPath()).stream()
        .filter(line -> line.startsWith(lineStart)).findFirst()
        .orElseThrow(() -> new IllegalStateException(
            "Resource property not found in application.yml for " + os));
    return resourceLine.substring(truncate.length());
  }

  @Test
  public void resource_Windows() throws Throwable {
    assertEquals(resource(WINDOWS), vapCoreConfiguration.resource(WINDOWS));
  }

  @Test
  public void resource_Mac() throws Throwable {
    assertEquals(resource(MAC), vapCoreConfiguration.resource(MAC));
  }

  @Test
  public void resource_Unix() throws Throwable {
    assertEquals(resource(UNIX), vapCoreConfiguration.resource(UNIX));
  }

  @Test
  public void resource_Other() throws Throwable {
    assertNull(vapCoreConfiguration.resource(OTHER));
  }

  private File output(OperatingSystem os) throws Throwable {
    File applicationProperties = new File(getClass().getResource("/application.yml").toURI());
    List<String> lines = Files.readAllLines(applicationProperties.toPath());
    String output = "  resources-output:";
    int outputIndex = lines.indexOf(output);
    String truncate = "    " + os.name() + ": ";
    String lineStart = truncate;
    String resourceLine =
        lines.stream().skip(outputIndex).filter(line -> line.startsWith(lineStart)).findFirst()
            .orElseThrow(() -> new IllegalStateException(
                "Resource output property not found in application.yml for " + os));
    return new File(resourceLine.substring(truncate.length()));
  }

  @Test
  public void output_Windows() throws Throwable {
    assertEquals(output(WINDOWS), vapCoreConfiguration.output(WINDOWS));
  }

  @Test
  public void output_Mac() throws Throwable {
    assertEquals(output(MAC), vapCoreConfiguration.output(MAC));
  }

  @Test
  public void output_Unix() throws Throwable {
    assertEquals(output(UNIX), vapCoreConfiguration.output(UNIX));
  }

  @Test
  public void output_Other() throws Throwable {
    assertNull(vapCoreConfiguration.output(OTHER));
  }
}

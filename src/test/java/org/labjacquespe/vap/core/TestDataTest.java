/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class TestDataTest {
  private TestData testData;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private File uncompressed;

  @Before
  public void beforeTest() throws Throwable {
    testData = new TestData();
    uncompressTestFiles();
  }

  private void uncompressTestFiles() throws IOException {
    uncompressed = temporaryFolder.newFolder("uncompressed");
    try (ZipInputStream zipInput =
        new ZipInputStream(getClass().getResourceAsStream("/test/test_files.zip"))) {
      ZipEntry entry;
      while ((entry = zipInput.getNextEntry()) != null) {
        try {
          File outputFile = new File(uncompressed, entry.getName());
          try (OutputStream output = new FileOutputStream(outputFile)) {
            IOUtils.copy(zipInput, output);
          }
        } finally {
          zipInput.closeEntry();
        }
      }
    }
  }

  private File getTestFile(String resource) {
    return new File(uncompressed, resource);
  }

  @Test
  public void isTestFile() {
    assertEquals(false, testData.isTestFile(new File("test.txt")));
    assertEquals(true, testData.isTestFile(new File("coord_group_genes_trxFreq_-1.coord4")));
    assertEquals(true, testData.isTestFile(new File("coord_group_genes_trxFreq_2-4.coord4")));
    assertEquals(true, testData.isTestFile(new File("group_genes_trxFreq_-1.txt")));
    assertEquals(true, testData.isTestFile(new File("group_genes_trxFreq_1-2.txt")));
    assertEquals(true, testData.isTestFile(new File("H2AZ-vs-H2B_Robert_sacCer1.bedgraph")));
    assertEquals(true, testData.isTestFile(new File("H3K36me3-vs-H3_Young_sacCer1.bedgraph")));
    assertEquals(true, testData.isTestFile(new File("SGDannot_20080202_on_sacCer1.genepred")));
    assertEquals(false, testData.isTestFile(new File("test_files.zip")));
    assertEquals(false, testData.isTestFile(new File("testInstructions.html")));
  }

  @Test
  public void getDataFiles() {
    for (File file : testData.dataFiles) {
      assertNull(file.getParentFile());
      assertNotNull(getTestFile(file.getName()));
    }
  }

  @Test
  public void getGenomeAnnotations() {
    File file = testData.genomeAnnotations;

    assertNull(file.getParentFile());
    assertNotNull(getTestFile(file.getName()));
  }

  @Test
  public void getReferenceFiles() {
    for (File file : testData.referenceFiles) {
      assertNull(file.getParentFile());
      assertNotNull(getTestFile(file.getName()));
    }
  }

  @Test
  public void getCoordinatesReferenceFiles() {
    for (File file : testData.getCoordinatesReferenceFiles()) {
      assertNull(file.getParentFile());
      assertNotNull(getTestFile(file.getName()));
    }
  }

  @Test
  public void getSelectionAnnotationFilter() {
    File file = testData.selectionAnnotationFilter;

    if (file != null) {
      assertNull(file.getParentFile());
      assertNotNull(getTestFile(file.getName()));
    }
  }

  @Test
  public void getExclusionAnnotationFilter() {
    File file = testData.exclusionAnnotationFilter;

    if (file != null) {
      assertNull(file.getParentFile());
      assertNotNull(getTestFile(file.getName()));
    }
  }
}

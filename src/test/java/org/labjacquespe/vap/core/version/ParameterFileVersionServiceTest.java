/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.version;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.core.service.ParameterParser;
import org.labjacquespe.vap.core.service.ParameterParser.ParameterHandlers;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ParameterFileVersionServiceTest {
  private ParameterFileVersionService parameterFileVersionService;
  @Mock
  private ParameterFileConverter converter;
  @Mock
  private ParameterParser parser;
  private String charset;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    charset = "UTF-8";
    Set<ParameterFileConverter> converters = new HashSet<>();
    converters.add(converter);
    parameterFileVersionService = new ParameterFileVersionService(converters, parser);
  }

  @Test
  public void updateParameters() throws Throwable {
    doAnswer(invocation -> {
      ParameterHandlers handlers = (ParameterHandlers) invocation.getArguments()[1];
      handlers.versionHandler.accept("1.0.0");
      return null;
    }).when(parser).parse(any(Reader.class), any());
    when(converter.to()).thenReturn(new DefaultArtifactVersion("1.0.1"));
    doAnswer(invocation -> {
      Reader reader = (Reader) invocation.getArguments()[0];
      Writer writer = (Writer) invocation.getArguments()[1];
      writer.write("unit_test\n");
      IOUtils.copy(reader, writer);
      return null;
    }).when(converter).updateParameters(any(), any(), any());
    try (Reader reader = new InputStreamReader(
        getClass().getResourceAsStream("/file/version/parameters_1.0.0.txt"), charset)) {
      File relativeDirectory = new File(getClass().getResource("/file/version").toURI());
      StringWriter writer = new StringWriter();
      parameterFileVersionService.updateParameters(reader, writer, relativeDirectory);
      verify(parser).parse(any(Reader.class), any());
      verify(converter).to();
      verify(converter).updateParameters(any(), any(), eq(relativeDirectory));
      assertEquals(true, writer.toString().startsWith("unit_test\n"));
    }
  }

  @Test
  public void updateParameters_NoVersion() throws Throwable {
    when(converter.to()).thenReturn(new DefaultArtifactVersion("1.0.1"));
    Reader reader = new StringReader("abc\ndef\nghi");
    File relativeDirectory = new File(getClass().getResource("/file/version").toURI());
    StringWriter writer = new StringWriter();
    parameterFileVersionService.updateParameters(reader, writer, relativeDirectory);
    verify(parser).parse(any(Reader.class), any());
    verify(converter, never()).updateParameters(any(), any(), eq(relativeDirectory));
    assertEquals("abc\ndef\nghi", writer.toString());
  }
}

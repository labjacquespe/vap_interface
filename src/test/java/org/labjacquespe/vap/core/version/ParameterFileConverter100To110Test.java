/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.core.version;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.core.service.ParameterParser;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ParameterFileConverter100To110Test {
  private ParameterFileConverter100To110 converter;
  private ParameterParser parser;
  private StringWriter output;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    parser = new ParameterParser();
    converter = new ParameterFileConverter100To110(parser);
    output = new StringWriter();
  }

  @Test
  public void updateParameters_From100To110() throws Throwable {
    try (Reader input = new InputStreamReader(
        getClass().getResourceAsStream("/file/version/parameters_1.0.0.txt"))) {
      File relativeDirectory = new File(getClass().getResource("/file/version").toURI());

      converter.updateParameters(input, output, relativeDirectory);

      List<String> lines = Arrays.asList(output.toString().split("\r?\n", -1));
      assertTrue(lines.contains("~~version 1.1.0"));
      assertTrue(lines.contains("~~@output_directory=output"));
      assertTrue(lines.contains("~~@analysis_mode=A"));
      assertTrue(lines.contains("~~@refgroup_path=refGroup.txt"));
      assertTrue(lines.contains("~~@dataset_path=dataset.txt"));
      assertTrue(lines.contains("~~@annotations_path=test_dataset.txt"));
      assertTrue(lines.contains("~~@selection_path=selection.txt"));
      assertTrue(lines.contains("~~@exclusion_path=exclusion.txt"));
      assertTrue(lines.contains("~~@annotation_coordinates_type=T"));
      assertTrue(lines.contains("~~@process_data_by_chunk=0"));
      assertTrue(lines.contains("~~@dataset_chunk_size=100"));
      assertTrue(lines.contains("~~@analysis_method=A"));
      assertTrue(lines.contains("~~@reference_points=3"));
      assertTrue(lines.contains("~~@1pt_boundary=5"));
      assertTrue(lines.contains("~~@merge_mid_introns=F"));
      assertTrue(lines.contains("~~@window_size=50"));
      assertTrue(lines.contains("~~@windows_per_block=10;5;10;5"));
      assertTrue(lines.contains("~~@block_alignment=R;S;S;L"));
      assertTrue(lines.contains("~~@block_split_type=N;P;P;A"));
      assertTrue(lines.contains("~~@block_split_value=0;50;50;100"));
      assertTrue(lines.contains("~~@block_split_alignment=N;L;L;R;"));
      assertTrue(lines.contains("~~@smoothing_windows=6"));
      assertTrue(lines.contains("~~@aggregate_data_type=E"));
      assertTrue(lines.contains("~~@mean_dispersion_value=E"));
      assertTrue(lines.contains("~~@process_missing_data=0"));
      assertTrue(lines.contains("~~@prefix_filename=christian"));
      assertTrue(lines.contains("~~@write_individual_references=1"));
      assertTrue(lines.contains("~~@generate_heatmaps=0"));
      assertTrue(lines.contains("~~@generate_aggregate_graphs=1"));
      assertTrue(lines.contains("~~@one_graph_per_group=1"));
      assertTrue(lines.contains("~~@one_graph_per_dataset=1"));
      assertTrue(lines.contains("~~@one_graph_per_orientation=1"));
      assertTrue(lines.contains("~~@orientation_subgroups=1;0;1;0;0;0;1;0;1"));
      assertTrue(lines.contains("~~@display_dispersion_values=1"));
      assertTrue(lines.contains("~~@Y_axis_scale=-20;-10"));
      assertTrue(lines.contains("~~@create_graph_from_file=map.txt"));
    }
  }

  @Test
  public void from() {
    assertEquals(new DefaultArtifactVersion("1.0.0"), converter.from());
  }

  @Test
  public void to() {
    assertEquals(new DefaultArtifactVersion("1.1.0"), converter.to());
  }
}

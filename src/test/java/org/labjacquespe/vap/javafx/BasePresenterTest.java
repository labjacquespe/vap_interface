/*
 * Copyright (c) 2010 Pierre-Étienne Jacques Laboratory
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.labjacquespe.vap.javafx;

import static org.junit.Assert.assertEquals;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjacquespe.vap.test.config.NonTransactionalTestAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class BasePresenterTest extends ApplicationTest {
  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
  }

  @Test
  public void test() throws Throwable {
    ResourceBundle bundle =
        ResourceBundle.getBundle(BasePresenterTest.class.getName(), Locale.getDefault());
    FXMLLoader loader = new FXMLLoader(
        getClass().getResource("/" + BasePresenterTest.class.getName().replace(".", "/") + ".fxml"),
        bundle);
    loader.load();
    TestBasePresenter presenter = (TestBasePresenter) loader.getController();

    assertEquals("Windows size is 12", presenter.message("label", 12));
  }

  public static class TestBasePresenter extends BasePresenter {
    @FXML
    private Pane view;
    @FXML
    private Label label;
  }
}
